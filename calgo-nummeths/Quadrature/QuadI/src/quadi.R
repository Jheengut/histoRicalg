#R. J. Herbold. 1960. 
#Algorithms: QuadI. Commun. ACM 3, 2 (February 1960), 74-. 
#DOI=http://dx.doi.org/10.1145/36695

arrayOfFunctions <- function (...) {
  
   array <- list()
   
   for (i in list(...)) {
      array[i] <- i
   }
   
   array
   
}

evaluationFunction <- function(startInterval, endInterval, subIntervals) {

  segments <- 1/length(subIntervals)  
  int <-seq(from=startInterval, by=segments, to=endInterval)
  sapply(1:(length(int)-1), function(x) (int[x+1]+int[x])/2)
  
}

quadI <- function (startInterval, endInterval, noSubintervals, npointQuad,
                  noFunctions, normWeight = 1/length(noSubintervals), 
                  evaluationFunction = NULL,
                  arrayOfFuns) {
  
  # interval limits
  a <- startInterval; b <- endInterval; 
  # number of subintervals
  m <- noSubintervals 
  # for an n-point quadrant integration
  n <- npointQuad 
  # the number of functions to be integrated
  p <- noFunctions 
  # normalized weights, an array of n values
  w <- normWeight 
  # function that evaluates c
  u <- evaluationFunction(a, b, m)
  # arrayOfFunctions(x)
  P <- arrayOfFuns 
  
     I <- vector()
     h <-  (b - a) / n
     for (j in 1:length(p)) {
        I[j] = 0.0
     }
     A <-  (a - h) / 2
     for (i in 1:length(m)) {
        A <-  A + h
        for (k in 1:length(n)) {
           B <- A + (h / 2) * u[k]
           for (j in 1:length(p)) {
              I[j] <- I[j] + w[k] * P[j]
           }
        }
     }
     I[j] <- (h / 2) * I[j]
     I
}
