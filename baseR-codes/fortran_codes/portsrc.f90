    Subroutine drn2g(d,dr,iv,liv,lv,n,nd,n1,n2,p,r,rd,v,x)
!
!     *** REVISED ITERATION DRIVER FOR NL2SOL (VERSION 2.3) ***
!
!
!     --------------------------  PARAMETER USAGE  --------------------------
!
!     D........ SCALE VECTOR.
!     DR....... DERIVATIVES OF R AT X.
!     IV....... INTEGER VALUES ARRAY.
!     LIV...... LENGTH OF IV... LIV MUST BE AT LEAST P + 82.
!     LV....... LENGTH OF V...  LV  MUST BE AT LEAST 105 + P*(2*P+16).
!     N........ TOTAL NUMBER OF RESIDUALS.
!     ND....... MAX. NO. OF RESIDUALS PASSED ON ONE CALL.
!     N1....... LOWEST  ROW INDEX FOR RESIDUALS SUPPLIED THIS TIME.
!     N2....... HIGHEST ROW INDEX FOR RESIDUALS SUPPLIED THIS TIME.
!     P........ NUMBER OF PARAMETERS (COMPONENTS OF X) BEING ESTIMATED.
!     R........ RESIDUALS.
!     RD....... RD(I) = SQRT(G(I)**T * H(I)**-1 * G(I)) ON OUTPUT WHEN
!        IV(RDREQ) IS NONZERO.   DRN2G SETS IV(REGD) = 1 IF RD
!        IS SUCCESSFULLY COMPUTED, TO 0 IF NO ATTEMPT WAS MADE
!        TO COMPUTE IT, AND TO -1 IF H (THE FINITE-DIFFERENCE HESSIAN)
!        WAS INDEFINITE.  IF ND .GE. N, THEN RD IS ALSO USED AS
!        TEMPORARY STORAGE.
!     V........ FLOATING-POINT VALUES ARRAY.
!     X........ PARAMETER VECTOR BEING ESTIMATED (INPUT = INITIAL GUESS,
!             OUTPUT = BEST VALUE FOUND).
!
!     ***  DISCUSSION  ***
!
!     NOTE... NL2SOL AND NL2ITR (MENTIONED BELOW) ARE DESCRIBED IN
!     ACM TRANS. MATH. SOFTWARE, VOL. 7, PP. 369-383 (AN ADAPTIVE
!     NONLINEAR LEAST-SQUARES ALGORITHM, BY J.E. DENNIS, D.M. GAY,
!     AND R.E. WELSCH).
!
!     THIS ROUTINE CARRIES OUT ITERATIONS FOR SOLVING NONLINEAR
!     LEAST SQUARES PROBLEMS.  WHEN ND = N, IT IS SIMILAR TO NL2ITR
!     (WITH J = DR), EXCEPT THAT R(X) AND DR(X) NEED NOT BE INITIALIZED
!     WHEN  DRN2G IS CALLED WITH IV(1) = 0 OR 12.   DRN2G ALSO ALLOWS
!     R AND DR TO BE SUPPLIED ROW-WISE -- JUST SET ND = 1 AND CALL
!     DRN2G ONCE FOR EACH ROW WHEN PROVIDING RESIDUALS AND JACOBIANS.
!     ANOTHER NEW FEATURE IS THAT CALLING  DRN2G WITH IV(1) = 13
!     CAUSES STORAGE ALLOCATION ONLY TO BE PERFORMED -- ON RETURN, SUCH
!     COMPONENTS AS IV(G) (THE FIRST SUBSCRIPT IN G OF THE GRADIENT)
!     AND IV(S) (THE FIRST SUBSCRIPT IN V OF THE S LOWER TRIANGLE OF
!     THE S MATRIX) WILL HAVE BEEN SET (UNLESS LIV OR LV IS TOO SMALL),
!     AND IV(1) WILL HAVE BEEN SET TO 14. CALLING  DRN2G WITH IV(1) = 14
!     CAUSES EXECUTION OF THE ALGORITHM TO BEGIN UNDER THE ASSUMPTION
!     THAT STORAGE HAS BEEN ALLOCATED.
!
!     ***  SUPPLYING R AND DR  ***
!
!      DRN2G USES IV AND V IN THE SAME WAY AS NL2SOL, WITH A SMALL
!     NUMBER OF OBVIOUS CHANGES.  ONE DIFFERENCE BETWEEN  DRN2G AND
!     NL2ITR IS THAT INITIAL FUNCTION AND GRADIENT INFORMATION NEED NOT
!     BE SUPPLIED IN THE VERY FIRST CALL ON  DRN2G, THE ONE WITH
!     IV(1) = 0 OR 12.  ANOTHER DIFFERENCE IS THAT  DRN2G RETURNS WITH
!     IV(1) = -2 WHEN IT WANTS ANOTHER LOOK AT THE OLD JACOBIAN MATRIX
!     AND THE CURRENT RESIDUAL -- THE ONE CORRESPONDING TO X AND
!     IV(NFGCAL).  IT THEN RETURNS WITH IV(1) = -3 WHEN IT WANTS TO SEE
!     BOTH THE NEW RESIDUAL AND THE NEW JACOBIAN MATRIX AT ONCE.  NOTE
!     THAT IV(NFGCAL) = IV(7) CONTAINS THE VALUE THAT IV(NFCALL) = IV(6)
!     HAD WHEN THE CURRENT RESIDUAL WAS EVALUATED.  ALSO NOTE THAT THE
!     VALUE OF X CORRESPONDING TO THE OLD JACOBIAN MATRIX IS STORED IN
!     V, STARTING AT V(IV(X0)) = V(IV(43)).
!     ANOTHER NEW RETURN...  DRN2G IV(1) = -1 WHEN IT WANTS BOTH THE
!     RESIDUAL AND THE JACOBIAN TO BE EVALUATED AT X.
!     A NEW RESIDUAL VECTOR MUST BE SUPPLIED WHEN  DRN2G RETURNS WITH
!     IV(1) = 1 OR -1.  THIS TAKES THE FORM OF VALUES OF R(I,X) PASSED
!     IN R(I-N1+1), I = N1(1)N2.  YOU MAY PASS ALL THESE VALUES AT ONCE
!     (I.E., N1 = 1 AND N2 = N) OR IN PIECES BY MAKING SEVERAL CALLS ON
!     DRN2G.  EACH TIME  DRN2G RETURNS WITH IV(1) = 1, N1 WILL HAVE
!     BEEN SET TO THE INDEX OF THE NEXT RESIDUAL THAT  DRN2G EXPECTS TO
!     SEE, AND N2 WILL BE SET TO THE INDEX OF THE HIGHEST RESIDUAL THAT
!     COULD BE GIVEN ON THE NEXT CALL, I.E., N2 = N1 + ND - 1.  (THUS
!     WHEN  DRN2G FIRST RETURNS WITH IV(1) = 1 FOR A NEW X, IT WILL
!     HAVE SET N1 TO 1 AND N2 TO MIN(ND,N).)  THE CALLER MAY PROVIDE
!     FEWER THAN N2-N1+1 RESIDUALS ON THE NEXT CALL BY SETTING N2 TO
!     A SMALLER VALUE.   DRN2G ASSUMES IT HAS SEEN ALL THE RESIDUALS
!     FOR THE CURRENT X WHEN IT IS CALLED WITH N2 .GE. N.
!     EXAMPLE... SUPPOSE N = 80 AND THAT R IS TO BE PASSED IN 8
!     BLOCKS OF SIZE 10.  THE FOLLOWING CODE WOULD DO THE JOB.
!
!      N = 80
!      ND = 10
!      ...
!      DO 10 K = 1, 8
!           ***  COMPUTE R(I,X) FOR I = 10*K-9 TO 10*K  ***
!           ***  AND STORE THEM IN R(1),...,R(10)  ***
!           CALL  DRN2G(..., R, ...)
!     10      CONTINUE
!
!     THE SITUATION IS SIMILAR WHEN GRADIENT INFORMATION IS
!     REQUIRED, I.E., WHEN  DRN2G RETURNS WITH IV(1) = 2, -1, OR -2.
!     NOTE THAT  DRN2G OVERWRITES R, BUT THAT IN THE SPECIAL CASE OF
!     N1 = 1 AND N2 = N ON PREVIOUS CALLS,  DRN2G NEVER RETURNS WITH
!     IV(1) = -2.  IT SHOULD BE CLEAR THAT THE PARTIAL DERIVATIVE OF
!     R(I,X) WITH RESPECT TO X(L) IS TO BE STORED IN DR(I-N1+1,L),
!     L = 1(1)P, I = N1(1)N2.  IT IS ESSENTIAL THAT R(I) AND DR(I,L)
!     ALL CORRESPOND TO THE SAME RESIDUALS WHEN IV(1) = -1 OR -2.
!
!     ***  COVARIANCE MATRIX  ***
!
!     IV(RDREQ) = IV(57) TELLS WHETHER TO COMPUTE A COVARIANCE
!     MATRIX AND/OR REGRESSION DIAGNOSTICS... 0 MEANS NEITHER,
!     1 MEANS COVARIANCE MATRIX ONLY, 2 MEANS REG. DIAGNOSTICS ONLY,
!     3 MEANS BOTH.  AS WITH NL2SOL, IV(COVREQ) = IV(15) TELLS WHAT
!     HESSIAN APPROXIMATION TO USE IN THIS COMPUTING.
!
!     ***  REGRESSION DIAGNOSTICS  ***
!
!     SEE THE COMMENTS IN SUBROUTINE   DN2G.
!
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY.
!
!     +++++++++++++++++++++++++++++  DECLARATIONS  ++++++++++++++++++++++++++
!
!     ***  INTRINSIC FUNCTIONS  ***
!     /+
!/
!     ***  EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!
!     DC7VFN... FINISHES COVARIANCE COMPUTATION.
!     DIVSET.... PROVIDES DEFAULT IV AND V INPUT COMPONENTS.
!     DD7TPR... COMPUTES INNER PRODUCT OF TWO VECTORS.
!     DD7UPD...  UPDATES SCALE VECTOR D.
!     DG7LIT.... PERFORMS BASIC MINIMIZATION ALGORITHM.
!     DITSUM.... PRINTS ITERATION SUMMARY, INFO ABOUT INITIAL AND FINAL X.
!     DL7VML.... COMPUTES L * V, V = VECTOR, L = LOWER TRIANGULAR MATRIX.
!     DN2CVP... PRINTS COVARIANCE MATRIX.
!     DN2LRD... COMPUTES REGRESSION DIAGNOSTICS.
!     DQ7APL... APPLIES QR TRANSFORMATIONS STORED BY DQ7RAD.
!     DQ7RAD.... ADDS A NEW BLOCK OF ROWS TO QR DECOMPOSITION.
!     DV7CPY.... COPIES ONE VECTOR TO ANOTHER.
!     DV7SCP... SETS ALL ELEMENTS OF A VECTOR TO A SCALAR.
!
!     ***  LOCAL VARIABLES  ***
!
!
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!
!
!     ***  IV SUBSCRIPT VALUES  ***
!
!
!     ***  V SUBSCRIPT VALUES  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: cnvcod = 55, covmat = 26,            &
                                          covreq = 15, d0init = 40,            &
                                          dinit = 38, dtinit = 39, dtype = 16, &
                                          f = 10, fdh = 74, g = 28, h = 56,    &
                                          ipivot = 76, ivneed = 3, jcn = 66,   &
                                          jtol = 59, lmat = 42, mode = 35,     &
                                          nextiv = 46, nextv = 47, nf0 = 68,   &
                                          nf00 = 81, nf1 = 69, nfcall = 6,     &
                                          nfcov = 52, nfgcal = 7, ngcall = 30, &
                                          ngcov = 53, qtr = 77, rdreq = 57,    &
                                          regd = 67, restor = 9, rlimit = 46,  &
                                          rmat = 78, toobig = 2, vneed = 4,    &
                                          y = 48
!     .. Scalar Arguments ..
      Integer                          :: liv, lv, n, n1, n2, nd, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(p), dr(nd,p), r(nd), rd(nd),       &
                                          v(lv), x(p)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t
      Integer                          :: g1, gi, i, iv1, ivmode, jtol1, k, l, &
                                          lh, nn, qtr1, rmat1, y1, yi
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, dv2nrm
      External                         :: dc7vfn, dd7upd, dg7lit, ditsum,      &
                                          divset, dl7vml, dn2cvp, dn2lrd,      &
                                          dq7apl, dq7rad, dv7cpy, dv7scp
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, mod
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      lh = p*(p+1)/2
      If (iv(1)==0) Call divset(1,iv,liv,lv,v)
      iv1 = iv(1)
      If (iv1>2) Go To 100
      nn = n2 - n1 + 1
      iv(restor) = 0
      i = iv1 + 4
      If (iv(toobig)==0) Go To (220,210,220,200,200,220) i
      If (i/=5) iv(1) = 2
      Go To 130
!
!     ***  FRESH START OR RESTART -- CHECK INPUT INTEGERS  ***
!
100   If (nd<=0) Go To 270
      If (p<=0) Go To 270
      If (n<=0) Go To 270
      If (iv1==14) Go To 120
      If (iv1>16) Go To 360
      If (iv1<12) Go To 130
      If (iv1==12) iv(1) = 13
      If (iv(1)/=13) Go To 110
      iv(ivneed) = iv(ivneed) + p
      iv(vneed) = iv(vneed) + p*(p+13)/2
110   Call dg7lit(d,x,iv,liv,lv,p,p,v,x,x)
      If (iv(1)/=14) Go To 370
!
!     ***  STORAGE ALLOCATION  ***
!
      iv(ipivot) = iv(nextiv)
      iv(nextiv) = iv(ipivot) + p
      iv(y) = iv(nextv)
      iv(g) = iv(y) + p
      iv(jcn) = iv(g) + p
      iv(rmat) = iv(jcn) + p
      iv(qtr) = iv(rmat) + lh
      iv(jtol) = iv(qtr) + p
      iv(nextv) = iv(jtol) + 2*p
      If (iv1==13) Go To 370
!
120   jtol1 = iv(jtol)
      If (v(dinit)>=zero) Call dv7scp(p,d,v(dinit))
      If (v(dtinit)>zero) Call dv7scp(p,v(jtol1),v(dtinit))
      i = jtol1 + p
      If (v(d0init)>zero) Call dv7scp(p,v(i),v(d0init))
      iv(nf0) = 0
      iv(nf1) = 0
      If (nd>=n) Go To 130
!
!     ***  SPECIAL CASE HANDLING OF FIRST FUNCTION AND GRADIENT EVALUATION
!     ***  -- ASK FOR BOTH RESIDUAL AND JACOBIAN AT ONCE
!
      g1 = iv(g)
      y1 = iv(y)
      Call dg7lit(d,v(g1),iv,liv,lv,p,p,v,x,v(y1))
      If (iv(1)/=1) Go To 280
      v(f) = zero
      Call dv7scp(p,v(g1),zero)
      iv(1) = -1
      qtr1 = iv(qtr)
      Call dv7scp(p,v(qtr1),zero)
      iv(regd) = 0
      rmat1 = iv(rmat)
      Go To 180
!
130   g1 = iv(g)
      y1 = iv(y)
      Call dg7lit(d,v(g1),iv,liv,lv,p,p,v,x,v(y1))
      If (iv(1)==2) Go To 140
      If (iv(1)>2) Go To 280
!
      v(f) = zero
      If (iv(nf1)==0) Go To 320
      If (iv(restor)/=2) Go To 320
      iv(nf0) = iv(nf1)
      Call dv7cpy(n,rd,r)
      iv(regd) = 0
      Go To 320
!
140   Call dv7scp(p,v(g1),zero)
      If (iv(mode)>0) Go To 290
      rmat1 = iv(rmat)
      qtr1 = iv(qtr)
      Call dv7scp(p,v(qtr1),zero)
      iv(regd) = 0
      If (nd<n) Go To 170
      If (n1/=1) Go To 170
      If (iv(mode)<0) Go To 180
      If (iv(nf1)==iv(nfgcal)) Go To 150
      If (iv(nf0)/=iv(nfgcal)) Go To 170
      Call dv7cpy(n,r,rd)
      Go To 160
150   Call dv7cpy(n,rd,r)
160   Call dq7apl(nd,n,p,dr,rd,0)
      Call dl7vml(p,v(y1),v(rmat1),rd)
      Go To 190
!
170   iv(1) = -2
      If (iv(mode)<0) iv(1) = -1
180   Call dv7scp(p,v(y1),zero)
190   Call dv7scp(lh,v(rmat1),zero)
      Go To 320
!
!     ***  COMPUTE F(X)  ***
!
200   t = dv2nrm(nn,r)
      If (t>v(rlimit)) Go To 260
      v(f) = v(f) + half*t**2
      If (n2<n) Go To 330
      If (n1==1) iv(nf1) = iv(nfcall)
      Go To 130
!
!     ***  COMPUTE Y  ***
!
210   y1 = iv(y)
      yi = y1
      Do l = 1, p
        v(yi) = v(yi) + dd7tpr(nn,dr(1,l),r)
        yi = yi + 1
      End Do
      If (n2<n) Go To 330
      iv(1) = 2
      If (n1>1) iv(1) = -3
      Go To 320
!
!     ***  COMPUTE GRADIENT INFORMATION  ***
!
220   If (iv(mode)>p) Go To 300
      g1 = iv(g)
      ivmode = iv(mode)
      If (ivmode<0) Go To 230
      If (ivmode==0) Go To 240
      iv(1) = 2
!
!     ***  COMPUTE GRADIENT ONLY (FOR USE IN COVARIANCE COMPUTATION)  ***
!
      gi = g1
      Do l = 1, p
        v(gi) = v(gi) + dd7tpr(nn,r,dr(1,l))
        gi = gi + 1
      End Do
      Go To 250
!
!     *** COMPUTE INITIAL FUNCTION VALUE WHEN ND .LT. N ***
!
230   If (n<=nd) Go To 240
      t = dv2nrm(nn,r)
      If (t>v(rlimit)) Go To 260
      v(f) = v(f) + half*t**2
!
!     ***  UPDATE D IF DESIRED  ***
!
240   If (iv(dtype)>0) Call dd7upd(d,dr,iv,liv,lv,n,nd,nn,n2,p,v)
!
!     ***  COMPUTE RMAT AND QTR  ***
!
      qtr1 = iv(qtr)
      rmat1 = iv(rmat)
      Call dq7rad(nn,nd,p,v(qtr1),.True.,v(rmat1),dr,r)
      iv(nf1) = 0
!
250   If (n2<n) Go To 330
      If (ivmode>0) Go To 130
      iv(nf00) = iv(nfgcal)
!
!     ***  COMPUTE G FROM RMAT AND QTR  ***
!
      Call dl7vml(p,v(g1),v(rmat1),v(qtr1))
      iv(1) = 2
      If (ivmode==0) Go To 130
      If (n<=nd) Go To 130
!
!     ***  FINISH SPECIAL CASE HANDLING OF FIRST FUNCTION AND GRADIENT
!
      y1 = iv(y)
      iv(1) = 1
      Call dg7lit(d,v(g1),iv,liv,lv,p,p,v,x,v(y1))
      If (iv(1)/=2) Go To 280
      Go To 130
!
!     ***  MISC. DETAILS  ***
!
!     ***  X IS OUT OF RANGE (OVERSIZE STEP)  ***
!
260   iv(toobig) = 1
      Go To 130
!
!     ***  BAD N, ND, OR P  ***
!
270   iv(1) = 66
      Go To 360
!
!     ***  CONVERGENCE OBTAINED -- SEE WHETHER TO COMPUTE COVARIANCE  ***
!
280   If (iv(covmat)/=0) Go To 350
      If (iv(regd)/=0) Go To 350
!
!     ***  SEE IF CHOLESKY FACTOR OF HESSIAN IS AVAILABLE  ***
!
      k = iv(fdh)
      If (k<=0) Go To 340
      If (iv(rdreq)<=0) Go To 350
!
!     ***  COMPUTE REGRESSION DIAGNOSTICS AND DEFAULT COVARIANCE IF
!          DESIRED  ***
!
      i = 0
      If (mod(iv(rdreq),4)>=2) i = 1
      If (mod(iv(rdreq),2)==1 .And. abs(iv(covreq))<=1) i = i + 2
      If (i==0) Go To 310
      iv(mode) = p + i
      iv(ngcall) = iv(ngcall) + 1
      iv(ngcov) = iv(ngcov) + 1
      iv(cnvcod) = iv(1)
      If (i<2) Go To 290
      l = abs(iv(h))
      Call dv7scp(lh,v(l),zero)
290   iv(nfcov) = iv(nfcov) + 1
      iv(nfcall) = iv(nfcall) + 1
      iv(nfgcal) = iv(nfcall)
      iv(1) = -1
      Go To 320
!
300   l = iv(lmat)
      Call dn2lrd(dr,iv,v(l),lh,liv,lv,nd,nn,p,r,rd,v)
      If (n2<n) Go To 330
      If (n1>1) Go To 310
!
!     ***  ENSURE WE CAN RESTART -- AND MAKE RETURN STATE OF DR
!     ***  INDEPENDENT OF WHETHER REGRESSION DIAGNOSTICS ARE COMPUTED.
!     ***  USE STEP VECTOR (ALLOCATED BY DG7LIT) FOR SCRATCH.
!
      rmat1 = iv(rmat)
      Call dv7scp(lh,v(rmat1),zero)
      Call dq7rad(nn,nd,p,r,.False.,v(rmat1),dr,r)
      iv(nf1) = 0
!
!     ***  FINISH COMPUTING COVARIANCE  ***
!
310   l = iv(lmat)
      Call dc7vfn(iv,v(l),lh,liv,lv,n,p,v)
      Go To 350
!
!     ***  RETURN FOR MORE FUNCTION OR GRADIENT INFORMATION  ***
!
320   n2 = 0
330   n1 = n2 + 1
      n2 = n2 + nd
      If (n2>n) n2 = n
      Go To 370
!
!     ***  COME HERE FOR INDEFINITE FINITE-DIFFERENCE HESSIAN  ***
!
340   iv(covmat) = k
      iv(regd) = k
!
!     ***  PRINT SUMMARY OF FINAL ITERATION AND OTHER REQUESTED ITEMS  ***
!
350   g1 = iv(g)
360   Call ditsum(d,v(g1),iv,liv,lv,p,v,x)
      If (iv(1)<=6 .And. iv(rdreq)>0) Call dn2cvp(iv,liv,lv,p,v)
!
370   Return
!     ***  LAST LINE OF  DRN2G FOLLOWS  ***
    End Subroutine drn2g
    Subroutine dl7sqr(n,a,l)
!
!     ***  COMPUTE  A = LOWER TRIANGLE OF  L*(L**T),  WITH BOTH
!     ***  L  AND  A  STORED COMPACTLY BY ROWS.  (BOTH MAY OCCUPY THE
!     ***  SAME STORAGE.
!
!     ***  PARAMETERS  ***
!
!     DIMENSION A(N*(N+1)/2), L(N*(N+1)/2)
!
!     ***  LOCAL VARIABLES  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(*), l(*)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t
      Integer                          :: i, i0, ii, ij, ik, ip1, j, j0, jj,   &
                                          jk, k, np1
!     .. Executable Statements ..
!
      np1 = n + 1
      i0 = n*(n+1)/2
      Do ii = 1, n
        i = np1 - ii
        ip1 = i + 1
        i0 = i0 - i
        j0 = i*(i+1)/2
        Do jj = 1, i
          j = ip1 - jj
          j0 = j0 - j
          t = 0.0E0_wp
          Do k = 1, j
            ik = i0 + k
            jk = j0 + k
            t = t + l(ik)*l(jk)
          End Do
          ij = i0 + j
          a(ij) = t
        End Do
      End Do
      Return
    End Subroutine dl7sqr
    Subroutine drmnhb(b,d,fx,g,h,iv,lh,liv,lv,n,v,x)
!
!     ***  CARRY OUT  DMNHB (SIMPLY BOUNDED MINIMIZATION) ITERATIONS,
!     ***  USING HESSIAN MATRIX PROVIDED BY THE CALLER.
!
!     ***  PARAMETER DECLARATIONS  ***
!
!
!     --------------------------  PARAMETER USAGE  --------------------------
!
!     D.... SCALE VECTOR.
!     FX... FUNCTION VALUE.
!     G.... GRADIENT VECTOR.
!     H.... LOWER TRIANGLE OF THE HESSIAN, STORED ROWWISE.
!     IV... INTEGER VALUE ARRAY.
!     LH... LENGTH OF H = P*(P+1)/2.
!     LIV.. LENGTH OF IV (AT LEAST 59 + 3*N).
!     LV... LENGTH OF V (AT LEAST 78 + N*(N+27)/2).
!     N.... NUMBER OF VARIABLES (COMPONENTS IN X AND G).
!     V.... FLOATING-POINT VALUE ARRAY.
!     X.... PARAMETER VECTOR.
!
!     ***  DISCUSSION  ***
!
!        PARAMETERS IV, N, V, AND X ARE THE SAME AS THE CORRESPONDING
!     ONES TO  DMNHB (WHICH SEE), EXCEPT THAT V CAN BE SHORTER (SINCE
!     THE PART OF V THAT  DMNHB USES FOR STORING G AND H IS NOT NEEDED).
!     MOREOVER, COMPARED WITH  DMNHB, IV(1) MAY HAVE THE TWO ADDITIONAL
!     OUTPUT VALUES 1 AND 2, WHICH ARE EXPLAINED BELOW, AS IS THE USE
!     OF IV(TOOBIG) AND IV(NFGCAL).  THE VALUE IV(G), WHICH IS AN
!     OUTPUT VALUE FROM  DMNHB, IS NOT REFERENCED BY DRMNHB OR THE
!     SUBROUTINES IT CALLS.
!
!     IV(1) = 1 MEANS THE CALLER SHOULD SET FX TO F(X), THE FUNCTION VALUE
!             AT X, AND CALL DRMNHB AGAIN, HAVING CHANGED NONE OF THE
!             OTHER PARAMETERS.  AN EXCEPTION OCCURS IF F(X) CANNOT BE
!             COMPUTED (E.G. IF OVERFLOW WOULD OCCUR), WHICH MAY HAPPEN
!             BECAUSE OF AN OVERSIZED STEP.  IN THIS CASE THE CALLER
!             SHOULD SET IV(TOOBIG) = IV(2) TO 1, WHICH WILL CAUSE
!             DRMNHB TO IGNORE FX AND TRY A SMALLER STEP.  THE PARA-
!             METER NF THAT  DMNH PASSES TO CALCF (FOR POSSIBLE USE BY
!             CALCGH) IS A COPY OF IV(NFCALL) = IV(6).
!     IV(1) = 2 MEANS THE CALLER SHOULD SET G TO G(X), THE GRADIENT OF F AT
!             X, AND H TO THE LOWER TRIANGLE OF H(X), THE HESSIAN OF F
!             AT X, AND CALL DRMNHB AGAIN, HAVING CHANGED NONE OF THE
!             OTHER PARAMETERS EXCEPT PERHAPS THE SCALE VECTOR D.
!                  THE PARAMETER NF THAT  DMNHB PASSES TO CALCG IS
!             IV(NFGCAL) = IV(7).  IF G(X) AND H(X) CANNOT BE EVALUATED,
!             THEN THE CALLER MAY SET IV(NFGCAL) TO 0, IN WHICH CASE
!             DRMNHB WILL RETURN WITH IV(1) = 65.
!                  NOTE -- DRMNHB OVERWRITES H WITH THE LOWER TRIANGLE
!             OF  DIAG(D)**-1 * H(X) * DIAG(D)**-1.
!.
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY (WINTER, SPRING 1983).
!
!        (SEE  DMNG AND  DMNH FOR REFERENCES.)
!
!     +++++++++++++++++++++++++++  DECLARATIONS  ++++++++++++++++++++++++++++
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  CONSTANTS  ***
!
!
!     ***  NO INTRINSIC FUNCTIONS  ***
!
!     ***  EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!
!     DA7SST.... ASSESSES CANDIDATE STEP.
!     DIVSET.... PROVIDES DEFAULT IV AND V INPUT VALUES.
!     DD7TPR... RETURNS INNER PRODUCT OF TWO VECTORS.
!     DD7DUP.... UPDATES SCALE VECTOR D.
!     DG7QSB... COMPUTES APPROXIMATE OPTIMAL BOUNDED STEP.
!     I7PNVR... INVERTS PERMUTATION ARRAY.
!     DITSUM.... PRINTS ITERATION SUMMARY AND INFO ON INITIAL AND FINAL X.
!     DPARCK.... CHECKS VALIDITY OF INPUT IV AND V VALUES.
!     DRLDST... COMPUTES V(RELDX) = RELATIVE STEP SIZE.
!     DS7IPR... APPLIES PERMUTATION TO LOWER TRIANG. OF SYM. MATRIX.
!     DS7LVM... MULTIPLIES SYMMETRIC MATRIX TIMES VECTOR, GIVEN THE LOWER
!             TRIANGLE OF THE MATRIX.
!     STOPX.... RETURNS .TRUE. IF THE BREAK KEY HAS BEEN PRESSED.
!     DV2NRM... RETURNS THE 2-NORM OF A VECTOR.
!     DV2AXY.... COMPUTES SCALAR TIMES ONE VECTOR PLUS ANOTHER.
!     DV7CPY.... COPIES ONE VECTOR TO ANOTHER.
!     DV7IPR... APPLIES PERMUTATION TO VECTOR.
!     DV7SCP... SETS ALL ELEMENTS OF A VECTOR TO A SCALAR.
!     DV7VMP... MULTIPLIES (OR DIVIDES) TWO VECTORS COMPONENTWISE.
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!
!     ***  IV SUBSCRIPT VALUES  ***
!
!     ***  (NOTE THAT NC AND N0 ARE STORED IN IV(G0) AND IV(STLSTG) RESP.)
!
!
!     ***  V SUBSCRIPT VALUES  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: negone = -1.E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: onep2 = 1.2E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: cnvcod = 55, d0init = 40, dg = 37,   &
                                          dgnorm = 1, dinit = 38, dstnrm = 2,  &
                                          dtinit = 39, dtol = 59, dtype = 16,  &
                                          f = 10, f0 = 13, fdif = 11,          &
                                          gtstep = 4, incfac = 23, irc = 29,   &
                                          ivneed = 3, kagqt = 33, lmat = 42,   &
                                          lmax0 = 35, lmaxs = 36, mode = 35,   &
                                          model = 5, mxfcal = 17, mxiter = 18, &
                                          n0 = 41, nc = 48, nextiv = 46,       &
                                          nextv = 47, nfcall = 6, nfgcal = 7,  &
                                          ngcall = 30, niter = 31, perm = 58,  &
                                          phmxfc = 21, preduc = 7, rad0 = 9,   &
                                          radfac = 16, radinc = 8, radius = 8, &
                                          reldx = 17, restor = 9, step = 40,   &
                                          stglim = 11, stppar = 5, toobig = 2, &
                                          tuner4 = 29, tuner5 = 30, vneed = 4, &
                                          w = 34, x0 = 43, xirc = 13
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: fx
      Integer                          :: lh, liv, lv, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(2,n), d(n), g(n), h(lh), v(lv),    &
                                          x(n)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: gi, t, xi
      Integer                          :: dg1, dummy, i, ipi, ipiv2, ipn, j,   &
                                          k, l, lstgst, nn1o2, rstrst, step0,  &
                                          step1, td1, temp0, temp1, tg1, w1,   &
                                          x01, x11
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, drldst, dv2nrm
      Logical, External                :: stopx
      External                         :: da7sst, dd7dup, dg7qsb, ditsum,      &
                                          divset, dparck, ds7ipr, ds7lvm,      &
                                          dv2axy, dv7cpy, dv7ipr, dv7scp,      &
                                          dv7vmp, i7pnvr
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      i = iv(1)
      If (i==1) Go To 130
      If (i==2) Go To 140
!
!     ***  CHECK VALIDITY OF IV AND V INPUT VALUES  ***
!
      If (iv(1)==0) Call divset(2,iv,liv,lv,v)
      If (iv(1)<12) Go To 100
      If (iv(1)>13) Go To 100
      iv(vneed) = iv(vneed) + n*(n+27)/2 + 7
      iv(ivneed) = iv(ivneed) + 3*n
100   Call dparck(2,d,iv,liv,lv,n,v)
      i = iv(1) - 2
      If (i>12) Go To 470
      nn1o2 = n*(n+1)/2
      If (lh>=nn1o2) Go To (290,290,290,290,290,290,230,200,230,110,110,120) i
      iv(1) = 81
      Go To 450
!
!     ***  STORAGE ALLOCATION  ***
!
110   iv(dtol) = iv(lmat) + nn1o2
      iv(x0) = iv(dtol) + 2*n
      iv(step) = iv(x0) + 2*n
      iv(dg) = iv(step) + 3*n
      iv(w) = iv(dg) + 2*n
      iv(nextv) = iv(w) + 4*n + 7
      iv(nextiv) = iv(perm) + 3*n
      If (iv(1)/=13) Go To 120
      iv(1) = 14
      Go To 470
!
!     ***  INITIALIZATION  ***
!
120   iv(niter) = 0
      iv(nfcall) = 1
      iv(ngcall) = 1
      iv(nfgcal) = 1
      iv(mode) = -1
      iv(model) = 1
      iv(stglim) = 1
      iv(toobig) = 0
      iv(cnvcod) = 0
      iv(radinc) = 0
      iv(nc) = n
      v(rad0) = zero
      v(stppar) = zero
      If (v(dinit)>=zero) Call dv7scp(n,d,v(dinit))
      k = iv(dtol)
      If (v(dtinit)>zero) Call dv7scp(n,v(k),v(dtinit))
      k = k + n
      If (v(d0init)>zero) Call dv7scp(n,v(k),v(d0init))
!
!     ***  CHECK CONSISTENCY OF B AND INITIALIZE IP ARRAY  ***
!
      ipi = iv(perm)
      Do i = 1, n
        iv(ipi) = i
        ipi = ipi + 1
        If (b(1,i)>b(2,i)) Go To 430
      End Do
!
!     ***  GET INITIAL FUNCTION VALUE  ***
!
      iv(1) = 1
      Go To 460
!
130   v(f) = fx
      If (iv(mode)>=0) Go To 290
      v(f0) = fx
      iv(1) = 2
      If (iv(toobig)==0) Go To 470
      iv(1) = 63
      Go To 450
!
!     ***  MAKE SURE GRADIENT COULD BE COMPUTED  ***
!
140   If (iv(toobig)==0) Go To 150
      iv(1) = 65
      Go To 450
!
!     ***  UPDATE THE SCALE VECTOR D  ***
!
150   dg1 = iv(dg)
      If (iv(dtype)<=0) Go To 160
      k = dg1
      j = 0
      Do i = 1, n
        j = j + i
        v(k) = h(j)
        k = k + 1
      End Do
      Call dd7dup(d,v(dg1),iv,liv,lv,n,v)
!
!     ***  COMPUTE SCALED GRADIENT AND ITS NORM  ***
!
160   dg1 = iv(dg)
      Call dv7vmp(n,v(dg1),g,d,-1)
!
!     ***  COMPUTE SCALED HESSIAN  ***
!
      k = 1
      Do i = 1, n
        t = one/d(i)
        Do j = 1, i
          h(k) = t*h(k)/d(j)
          k = k + 1
        End Do
      End Do
!
!     ***  CHOOSE INITIAL PERMUTATION  ***
!
      ipi = iv(perm)
      ipn = ipi + n
      ipiv2 = ipn - 1
!     *** INVERT OLD PERMUTATION ARRAY ***
      Call i7pnvr(n,iv(ipn),iv(ipi))
      k = iv(nc)
      Do i = 1, n
        If (b(1,i)>=b(2,i)) Go To 170
        xi = x(i)
        gi = g(i)
        If (xi<=b(1,i) .And. gi>zero) Go To 170
        If (xi>=b(2,i) .And. gi<zero) Go To 170
        iv(ipi) = i
        ipi = ipi + 1
        j = ipiv2 + i
!           *** DISALLOW CONVERGENCE IF X(I) HAS JUST BEEN FREED ***
        If (iv(j)>k) iv(cnvcod) = 0
        Go To 180
170     ipn = ipn - 1
        iv(ipn) = i
180   End Do
      iv(nc) = ipn - iv(perm)
!
!     ***  PERMUTE SCALED GRADIENT AND HESSIAN ACCORDINGLY  ***
!
      ipi = iv(perm)
      Call ds7ipr(n,iv(ipi),h)
      Call dv7ipr(n,iv(ipi),v(dg1))
      v(dgnorm) = zero
      If (iv(nc)>0) v(dgnorm) = dv2nrm(iv(nc),v(dg1))
!
      If (iv(cnvcod)/=0) Go To 440
      If (iv(mode)==0) Go To 400
!
!     ***  ALLOW FIRST STEP TO HAVE SCALED 2-NORM AT MOST V(LMAX0)  ***
!
      v(radius) = v(lmax0)/(one+v(phmxfc))
!
      iv(mode) = 0
!
!
!     -----------------------------  MAIN LOOP  -----------------------------
!
!
!     ***  PRINT ITERATION SUMMARY, CHECK ITERATION LIMIT  ***
!
190   Call ditsum(d,g,iv,liv,lv,n,v,x)
200   k = iv(niter)
      If (k<iv(mxiter)) Go To 210
      iv(1) = 10
      Go To 450
!
210   iv(niter) = k + 1
!
!     ***  INITIALIZE FOR START OF NEXT ITERATION  ***
!
      x01 = iv(x0)
      v(f0) = v(f)
      iv(irc) = 4
      iv(kagqt) = -1
!
!     ***  COPY X TO X0  ***
!
      Call dv7cpy(n,v(x01),x)
!
!     ***  UPDATE RADIUS  ***
!
      If (k==0) Go To 220
      step1 = iv(step)
      k = step1
      Do i = 1, n
        v(k) = d(i)*v(k)
        k = k + 1
      End Do
      t = v(radfac)*dv2nrm(n,v(step1))
      If (v(radfac)<one .Or. t>v(radius)) v(radius) = t
!
!     ***  CHECK STOPX AND FUNCTION EVALUATION LIMIT  ***
!
220   If (.Not. stopx(dummy)) Go To 240
      iv(1) = 11
      Go To 250
!
!     ***  COME HERE WHEN RESTARTING AFTER FUNC. EVAL. LIMIT OR STOPX.
!
230   If (v(f)>=v(f0)) Go To 240
      v(radfac) = one
      k = iv(niter)
      Go To 210
!
240   If (iv(nfcall)<iv(mxfcal)) Go To 260
      iv(1) = 9
250   If (v(f)>=v(f0)) Go To 450
!
!        ***  IN CASE OF STOPX OR FUNCTION EVALUATION LIMIT WITH
!        ***  IMPROVED V(F), EVALUATE THE GRADIENT AT X.
!
      iv(cnvcod) = iv(1)
      Go To 390
!
!     . . . . . . . . . . . . .  COMPUTE CANDIDATE STEP  . . . . . . . . . .
!
260   step1 = iv(step)
      l = iv(lmat)
      w1 = iv(w)
      ipi = iv(perm)
      ipn = ipi + n
      ipiv2 = ipn + n
      tg1 = iv(dg)
      td1 = tg1 + n
      x01 = iv(x0)
      x11 = x01 + n
      Call dg7qsb(b,d,h,g,iv(ipi),iv(ipn),iv(ipiv2),iv(kagqt),v(l),lv,n,       &
        iv(n0),iv(nc),v(step1),v(td1),v(tg1),v,v(w1),v(x11),v(x01))
      If (iv(irc)/=6) Go To 270
      If (iv(restor)/=2) Go To 290
      rstrst = 2
      Go To 300
!
!     ***  CHECK WHETHER EVALUATING F(X0 + STEP) LOOKS WORTHWHILE  ***
!
270   iv(toobig) = 0
      If (v(dstnrm)<=zero) Go To 290
      If (iv(irc)/=5) Go To 280
      If (v(radfac)<=one) Go To 280
      If (v(preduc)>onep2*v(fdif)) Go To 280
      If (iv(restor)/=2) Go To 290
      rstrst = 0
      Go To 300
!
!     ***  COMPUTE F(X0 + STEP)  ***
!
280   Call dv2axy(n,x,one,v(step1),v(x01))
      iv(nfcall) = iv(nfcall) + 1
      iv(1) = 1
      Go To 460
!
!     . . . . . . . . . . . . .  ASSESS CANDIDATE STEP  . . . . . . . . . . .
!
290   rstrst = 3
300   x01 = iv(x0)
      v(reldx) = drldst(n,d,x,v(x01))
      Call da7sst(iv,liv,lv,v)
      step1 = iv(step)
      lstgst = step1 + 2*n
      i = iv(restor) + 1
      Go To (340,310,320,330) i
310   Call dv7cpy(n,x,v(x01))
      Go To 340
320   Call dv7cpy(n,v(lstgst),x)
      Go To 340
330   Call dv7cpy(n,x,v(lstgst))
      Call dv2axy(n,v(step1),negone,v(x01),x)
      v(reldx) = drldst(n,d,x,v(x01))
      iv(restor) = rstrst
!
340   k = iv(irc)
      Go To (350,380,380,380,350,360,370,370,370,370,370,370,420,400) k
!
!     ***  RECOMPUTE STEP WITH NEW RADIUS  ***
!
350   v(radius) = v(radfac)*v(dstnrm)
      Go To 220
!
!     ***  COMPUTE STEP OF LENGTH V(LMAXS) FOR SINGULAR CONVERGENCE TEST.
!
360   v(radius) = v(lmaxs)
      Go To 260
!
!     ***  CONVERGENCE OR FALSE CONVERGENCE  ***
!
370   iv(cnvcod) = k - 4
      If (v(f)>=v(f0)) Go To 440
      If (iv(xirc)==14) Go To 440
      iv(xirc) = 14
!
!     . . . . . . . . . . . .  PROCESS ACCEPTABLE STEP  . . . . . . . . . . .
!
380   If (iv(irc)/=3) Go To 390
      temp1 = lstgst
!
!     ***  PREPARE FOR GRADIENT TESTS  ***
!     ***  SET  TEMP1 = HESSIAN * STEP + G(X0)
!     ***             = DIAG(D) * (H * STEP + G(X0))
!
      k = temp1
      step0 = step1 - 1
      ipi = iv(perm)
      Do i = 1, n
        j = iv(ipi)
        ipi = ipi + 1
        step1 = step0 + j
        v(k) = d(j)*v(step1)
        k = k + 1
      End Do
!        USE X0 VECTOR AS TEMPORARY.
      Call ds7lvm(n,v(x01),h,v(temp1))
      temp0 = temp1 - 1
      ipi = iv(perm)
      Do i = 1, n
        j = iv(ipi)
        ipi = ipi + 1
        temp1 = temp0 + j
        v(temp1) = d(j)*v(x01) + g(j)
        x01 = x01 + 1
      End Do
!
!     ***  COMPUTE GRADIENT AND HESSIAN  ***
!
390   iv(ngcall) = iv(ngcall) + 1
      iv(toobig) = 0
      iv(1) = 2
      Go To 460
!
400   iv(1) = 2
      If (iv(irc)/=3) Go To 190
!
!     ***  SET V(RADFAC) BY GRADIENT TESTS  ***
!
      step1 = iv(step)
!     *** TEMP1 = STLSTG ***
      temp1 = step1 + 2*n
!
!     ***  SET  TEMP1 = DIAG(D)**-1 * (HESSIAN*STEP + (G(X0)-G(X)))  ***
!
      k = temp1
      Do i = 1, n
        v(k) = (v(k)-g(i))/d(i)
        k = k + 1
      End Do
!
!     ***  DO GRADIENT TESTS  ***
!
      If (dv2nrm(n,v(temp1))<=v(dgnorm)*v(tuner4)) Go To 410
      If (dd7tpr(n,g,v(step1))>=v(gtstep)*v(tuner5)) Go To 190
410   v(radfac) = v(incfac)
      Go To 190
!
!     . . . . . . . . . . . . . .  MISC. DETAILS  . . . . . . . . . . . . . .
!
!     ***  BAD PARAMETERS TO ASSESS  ***
!
420   iv(1) = 64
      Go To 450
!
!     ***  INCONSISTENT B  ***
!
430   iv(1) = 82
      Go To 450
!
!     ***  PRINT SUMMARY OF FINAL ITERATION AND OTHER REQUESTED ITEMS  ***
!
440   iv(1) = iv(cnvcod)
      iv(cnvcod) = 0
450   Call ditsum(d,g,iv,liv,lv,n,v,x)
      Go To 470
!
!     ***  PROJECT X INTO FEASIBLE REGION (PRIOR TO COMPUTING F OR G)  ***
!
460   Do i = 1, n
        If (x(i)<b(1,i)) x(i) = b(1,i)
        If (x(i)>b(2,i)) x(i) = b(2,i)
      End Do
!
470   Return
!
!     ***  LAST CARD OF DRMNHB FOLLOWS  ***
    End Subroutine drmnhb
    Subroutine drmnh(d,fx,g,h,iv,lh,liv,lv,n,v,x)
!
!     ***  CARRY OUT  DMNH (UNCONSTRAINED MINIMIZATION) ITERATIONS, USING
!     ***  HESSIAN MATRIX PROVIDED BY THE CALLER.
!
!     ***  PARAMETER DECLARATIONS  ***
!
!
!     --------------------------  PARAMETER USAGE  --------------------------
!
!     D.... SCALE VECTOR.
!     FX... FUNCTION VALUE.
!     G.... GRADIENT VECTOR.
!     H.... LOWER TRIANGLE OF THE HESSIAN, STORED ROWWISE.
!     IV... INTEGER VALUE ARRAY.
!     LH... LENGTH OF H = P*(P+1)/2.
!     LIV.. LENGTH OF IV (AT LEAST 60).
!     LV... LENGTH OF V (AT LEAST 78 + N*(N+21)/2).
!     N.... NUMBER OF VARIABLES (COMPONENTS IN X AND G).
!     V.... FLOATING-POINT VALUE ARRAY.
!     X.... PARAMETER VECTOR.
!
!     ***  DISCUSSION  ***
!
!        PARAMETERS IV, N, V, AND X ARE THE SAME AS THE CORRESPONDING
!     ONES TO  DMNH (WHICH SEE), EXCEPT THAT V CAN BE SHORTER (SINCE
!     THE PART OF V THAT  DMNH USES FOR STORING G AND H IS NOT NEEDED).
!     MOREOVER, COMPARED WITH  DMNH, IV(1) MAY HAVE THE TWO ADDITIONAL
!     OUTPUT VALUES 1 AND 2, WHICH ARE EXPLAINED BELOW, AS IS THE USE
!     OF IV(TOOBIG) AND IV(NFGCAL).  THE VALUE IV(G), WHICH IS AN
!     OUTPUT VALUE FROM  DMNH, IS NOT REFERENCED BY DRMNH OR THE
!     SUBROUTINES IT CALLS.
!
!     IV(1) = 1 MEANS THE CALLER SHOULD SET FX TO F(X), THE FUNCTION VALUE
!             AT X, AND CALL DRMNH AGAIN, HAVING CHANGED NONE OF THE
!             OTHER PARAMETERS.  AN EXCEPTION OCCURS IF F(X) CANNOT BE
!             COMPUTED (E.G. IF OVERFLOW WOULD OCCUR), WHICH MAY HAPPEN
!             BECAUSE OF AN OVERSIZED STEP.  IN THIS CASE THE CALLER
!             SHOULD SET IV(TOOBIG) = IV(2) TO 1, WHICH WILL CAUSE
!             DRMNH TO IGNORE FX AND TRY A SMALLER STEP.  THE PARA-
!             METER NF THAT  DMNH PASSES TO CALCF (FOR POSSIBLE USE BY
!             CALCGH) IS A COPY OF IV(NFCALL) = IV(6).
!     IV(1) = 2 MEANS THE CALLER SHOULD SET G TO G(X), THE GRADIENT OF F AT
!             X, AND H TO THE LOWER TRIANGLE OF H(X), THE HESSIAN OF F
!             AT X, AND CALL DRMNH AGAIN, HAVING CHANGED NONE OF THE
!             OTHER PARAMETERS EXCEPT PERHAPS THE SCALE VECTOR D.
!                  THE PARAMETER NF THAT  DMNH PASSES TO CALCG IS
!             IV(NFGCAL) = IV(7).  IF G(X) AND H(X) CANNOT BE EVALUATED,
!             THEN THE CALLER MAY SET IV(TOOBIG) TO 0, IN WHICH CASE
!             DRMNH WILL RETURN WITH IV(1) = 65.
!                  NOTE -- DRMNH OVERWRITES H WITH THE LOWER TRIANGLE
!             OF  DIAG(D)**-1 * H(X) * DIAG(D)**-1.
!.
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY (WINTER 1980).  REVISED SEPT. 1982.
!     THIS SUBROUTINE WAS WRITTEN IN CONNECTION WITH RESEARCH SUPPORTED
!     IN PART BY THE NATIONAL SCIENCE FOUNDATION UNDER GRANTS
!     MCS-7600324 AND MCS-7906671.
!
!        (SEE  DMNG AND  DMNH FOR REFERENCES.)
!
!     +++++++++++++++++++++++++++  DECLARATIONS  ++++++++++++++++++++++++++++
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  CONSTANTS  ***
!
!
!     ***  NO INTRINSIC FUNCTIONS  ***
!
!     ***  EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!
!     DA7SST.... ASSESSES CANDIDATE STEP.
!     DIVSET.... PROVIDES DEFAULT IV AND V INPUT VALUES.
!     DD7TPR... RETURNS INNER PRODUCT OF TWO VECTORS.
!     DD7DUP.... UPDATES SCALE VECTOR D.
!     DG7QTS.... COMPUTES OPTIMALLY LOCALLY CONSTRAINED STEP.
!     DITSUM.... PRINTS ITERATION SUMMARY AND INFO ON INITIAL AND FINAL X.
!     DPARCK.... CHECKS VALIDITY OF INPUT IV AND V VALUES.
!     DRLDST... COMPUTES V(RELDX) = RELATIVE STEP SIZE.
!     DS7LVM... MULTIPLIES SYMMETRIC MATRIX TIMES VECTOR, GIVEN THE LOWER
!             TRIANGLE OF THE MATRIX.
!     STOPX.... RETURNS .TRUE. IF THE BREAK KEY HAS BEEN PRESSED.
!     DV2AXY.... COMPUTES SCALAR TIMES ONE VECTOR PLUS ANOTHER.
!     DV7CPY.... COPIES ONE VECTOR TO ANOTHER.
!     DV7SCP... SETS ALL ELEMENTS OF A VECTOR TO A SCALAR.
!     DV2NRM... RETURNS THE 2-NORM OF A VECTOR.
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!
!     ***  IV SUBSCRIPT VALUES  ***
!
!
!     ***  V SUBSCRIPT VALUES  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: onep2 = 1.2E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: cnvcod = 55, d0init = 40, dg = 37,   &
                                          dgnorm = 1, dinit = 38, dstnrm = 2,  &
                                          dtinit = 39, dtol = 59, dtype = 16,  &
                                          f = 10, f0 = 13, fdif = 11,          &
                                          gtstep = 4, incfac = 23, irc = 29,   &
                                          kagqt = 33, lmat = 42, lmax0 = 35,   &
                                          lmaxs = 36, mode = 35, model = 5,    &
                                          mxfcal = 17, mxiter = 18,            &
                                          nextv = 47, nfcall = 6, nfgcal = 7,  &
                                          ngcall = 30, niter = 31,             &
                                          phmxfc = 21, preduc = 7, rad0 = 9,   &
                                          radfac = 16, radinc = 8, radius = 8, &
                                          reldx = 17, restor = 9, step = 40,   &
                                          stglim = 11, stlstg = 41,            &
                                          stppar = 5, toobig = 2, tuner4 = 29, &
                                          tuner5 = 30, vneed = 4, w = 34,      &
                                          x0 = 43, xirc = 13
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: fx
      Integer                          :: lh, liv, lv, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(n), g(n), h(lh), v(lv), x(n)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t
      Integer                          :: dg1, dummy, i, j, k, l, lstgst,      &
                                          nn1o2, rstrst, step1, temp1, w1, x01
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, drldst, dv2nrm
      Logical, External                :: stopx
      External                         :: da7sst, dd7dup, dg7qts, ditsum,      &
                                          divset, dparck, ds7lvm, dv2axy,      &
                                          dv7cpy, dv7scp
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      i = iv(1)
      If (i==1) Go To 120
      If (i==2) Go To 130
!
!     ***  CHECK VALIDITY OF IV AND V INPUT VALUES  ***
!
      If (iv(1)==0) Call divset(2,iv,liv,lv,v)
      If (iv(1)==12 .Or. iv(1)==13) iv(vneed) = iv(vneed) + n*(n+21)/2 + 7
      Call dparck(2,d,iv,liv,lv,n,v)
      i = iv(1) - 2
      If (i>12) Go To 420
      nn1o2 = n*(n+1)/2
      If (lh>=nn1o2) Go To (260,260,260,260,260,260,200,170,200,100,100,110) i
      iv(1) = 66
      Go To 410
!
!     ***  STORAGE ALLOCATION  ***
!
100   iv(dtol) = iv(lmat) + nn1o2
      iv(x0) = iv(dtol) + 2*n
      iv(step) = iv(x0) + n
      iv(stlstg) = iv(step) + n
      iv(dg) = iv(stlstg) + n
      iv(w) = iv(dg) + n
      iv(nextv) = iv(w) + 4*n + 7
      If (iv(1)/=13) Go To 110
      iv(1) = 14
      Go To 420
!
!     ***  INITIALIZATION  ***
!
110   iv(niter) = 0
      iv(nfcall) = 1
      iv(ngcall) = 1
      iv(nfgcal) = 1
      iv(mode) = -1
      iv(model) = 1
      iv(stglim) = 1
      iv(toobig) = 0
      iv(cnvcod) = 0
      iv(radinc) = 0
      v(rad0) = zero
      v(stppar) = zero
      If (v(dinit)>=zero) Call dv7scp(n,d,v(dinit))
      k = iv(dtol)
      If (v(dtinit)>zero) Call dv7scp(n,v(k),v(dtinit))
      k = k + n
      If (v(d0init)>zero) Call dv7scp(n,v(k),v(d0init))
      iv(1) = 1
      Go To 420
!
120   v(f) = fx
      If (iv(mode)>=0) Go To 260
      v(f0) = fx
      iv(1) = 2
      If (iv(toobig)==0) Go To 420
      iv(1) = 63
      Go To 410
!
!     ***  MAKE SURE GRADIENT COULD BE COMPUTED  ***
!
130   If (iv(toobig)==0) Go To 140
      iv(1) = 65
      Go To 410
!
!     ***  UPDATE THE SCALE VECTOR D  ***
!
140   dg1 = iv(dg)
      If (iv(dtype)<=0) Go To 150
      k = dg1
      j = 0
      Do i = 1, n
        j = j + i
        v(k) = h(j)
        k = k + 1
      End Do
      Call dd7dup(d,v(dg1),iv,liv,lv,n,v)
!
!     ***  COMPUTE SCALED GRADIENT AND ITS NORM  ***
!
150   dg1 = iv(dg)
      k = dg1
      Do i = 1, n
        v(k) = g(i)/d(i)
        k = k + 1
      End Do
      v(dgnorm) = dv2nrm(n,v(dg1))
!
!     ***  COMPUTE SCALED HESSIAN  ***
!
      k = 1
      Do i = 1, n
        t = one/d(i)
        Do j = 1, i
          h(k) = t*h(k)/d(j)
          k = k + 1
        End Do
      End Do
!
      If (iv(cnvcod)/=0) Go To 400
      If (iv(mode)==0) Go To 370
!
!     ***  ALLOW FIRST STEP TO HAVE SCALED 2-NORM AT MOST V(LMAX0)  ***
!
      v(radius) = v(lmax0)/(one+v(phmxfc))
!
      iv(mode) = 0
!
!
!     -----------------------------  MAIN LOOP  -----------------------------
!
!
!     ***  PRINT ITERATION SUMMARY, CHECK ITERATION LIMIT  ***
!
160   Call ditsum(d,g,iv,liv,lv,n,v,x)
170   k = iv(niter)
      If (k<iv(mxiter)) Go To 180
      iv(1) = 10
      Go To 410
!
180   iv(niter) = k + 1
!
!     ***  INITIALIZE FOR START OF NEXT ITERATION  ***
!
      dg1 = iv(dg)
      x01 = iv(x0)
      v(f0) = v(f)
      iv(irc) = 4
      iv(kagqt) = -1
!
!     ***  COPY X TO X0  ***
!
      Call dv7cpy(n,v(x01),x)
!
!     ***  UPDATE RADIUS  ***
!
      If (k==0) Go To 190
      step1 = iv(step)
      k = step1
      Do i = 1, n
        v(k) = d(i)*v(k)
        k = k + 1
      End Do
      v(radius) = v(radfac)*dv2nrm(n,v(step1))
!
!     ***  CHECK STOPX AND FUNCTION EVALUATION LIMIT  ***
!
190   If (.Not. stopx(dummy)) Go To 210
      iv(1) = 11
      Go To 220
!
!     ***  COME HERE WHEN RESTARTING AFTER FUNC. EVAL. LIMIT OR STOPX.
!
200   If (v(f)>=v(f0)) Go To 210
      v(radfac) = one
      k = iv(niter)
      Go To 180
!
210   If (iv(nfcall)<iv(mxfcal)) Go To 230
      iv(1) = 9
220   If (v(f)>=v(f0)) Go To 410
!
!        ***  IN CASE OF STOPX OR FUNCTION EVALUATION LIMIT WITH
!        ***  IMPROVED V(F), EVALUATE THE GRADIENT AT X.
!
      iv(cnvcod) = iv(1)
      Go To 360
!
!     . . . . . . . . . . . . .  COMPUTE CANDIDATE STEP  . . . . . . . . . .
!
230   step1 = iv(step)
      dg1 = iv(dg)
      l = iv(lmat)
      w1 = iv(w)
      Call dg7qts(d,v(dg1),h,iv(kagqt),v(l),n,v(step1),v,v(w1))
      If (iv(irc)/=6) Go To 240
      If (iv(restor)/=2) Go To 260
      rstrst = 2
      Go To 270
!
!     ***  CHECK WHETHER EVALUATING F(X0 + STEP) LOOKS WORTHWHILE  ***
!
240   iv(toobig) = 0
      If (v(dstnrm)<=zero) Go To 260
      If (iv(irc)/=5) Go To 250
      If (v(radfac)<=one) Go To 250
      If (v(preduc)>onep2*v(fdif)) Go To 250
      If (iv(restor)/=2) Go To 260
      rstrst = 0
      Go To 270
!
!     ***  COMPUTE F(X0 + STEP)  ***
!
250   x01 = iv(x0)
      step1 = iv(step)
      Call dv2axy(n,x,one,v(step1),v(x01))
      iv(nfcall) = iv(nfcall) + 1
      iv(1) = 1
      Go To 420
!
!     . . . . . . . . . . . . .  ASSESS CANDIDATE STEP  . . . . . . . . . . .
!
260   rstrst = 3
270   x01 = iv(x0)
      v(reldx) = drldst(n,d,x,v(x01))
      Call da7sst(iv,liv,lv,v)
      step1 = iv(step)
      lstgst = iv(stlstg)
      i = iv(restor) + 1
      Go To (310,280,290,300) i
280   Call dv7cpy(n,x,v(x01))
      Go To 310
290   Call dv7cpy(n,v(lstgst),v(step1))
      Go To 310
300   Call dv7cpy(n,v(step1),v(lstgst))
      Call dv2axy(n,x,one,v(step1),v(x01))
      v(reldx) = drldst(n,d,x,v(x01))
      iv(restor) = rstrst
!
310   k = iv(irc)
      Go To (320,350,350,350,320,330,340,340,340,340,340,340,390,370) k
!
!     ***  RECOMPUTE STEP WITH NEW RADIUS  ***
!
320   v(radius) = v(radfac)*v(dstnrm)
      Go To 190
!
!     ***  COMPUTE STEP OF LENGTH V(LMAXS) FOR SINGULAR CONVERGENCE TEST.
!
330   v(radius) = v(lmaxs)
      Go To 230
!
!     ***  CONVERGENCE OR FALSE CONVERGENCE  ***
!
340   iv(cnvcod) = k - 4
      If (v(f)>=v(f0)) Go To 400
      If (iv(xirc)==14) Go To 400
      iv(xirc) = 14
!
!     . . . . . . . . . . . .  PROCESS ACCEPTABLE STEP  . . . . . . . . . . .
!
350   If (iv(irc)/=3) Go To 360
      temp1 = lstgst
!
!     ***  PREPARE FOR GRADIENT TESTS  ***
!     ***  SET  TEMP1 = HESSIAN * STEP + G(X0)
!     ***             = DIAG(D) * (H * STEP + G(X0))
!
!        USE X0 VECTOR AS TEMPORARY.
      k = x01
      Do i = 1, n
        v(k) = d(i)*v(step1)
        k = k + 1
        step1 = step1 + 1
      End Do
      Call ds7lvm(n,v(temp1),h,v(x01))
      Do i = 1, n
        v(temp1) = d(i)*v(temp1) + g(i)
        temp1 = temp1 + 1
      End Do
!
!     ***  COMPUTE GRADIENT AND HESSIAN  ***
!
360   iv(ngcall) = iv(ngcall) + 1
      iv(toobig) = 0
      iv(1) = 2
      Go To 420
!
370   iv(1) = 2
      If (iv(irc)/=3) Go To 160
!
!     ***  SET V(RADFAC) BY GRADIENT TESTS  ***
!
      temp1 = iv(stlstg)
      step1 = iv(step)
!
!     ***  SET  TEMP1 = DIAG(D)**-1 * (HESSIAN*STEP + (G(X0)-G(X)))  ***
!
      k = temp1
      Do i = 1, n
        v(k) = (v(k)-g(i))/d(i)
        k = k + 1
      End Do
!
!     ***  DO GRADIENT TESTS  ***
!
      If (dv2nrm(n,v(temp1))<=v(dgnorm)*v(tuner4)) Go To 380
      If (dd7tpr(n,g,v(step1))>=v(gtstep)*v(tuner5)) Go To 160
380   v(radfac) = v(incfac)
      Go To 160
!
!     . . . . . . . . . . . . . .  MISC. DETAILS  . . . . . . . . . . . . . .
!
!     ***  BAD PARAMETERS TO ASSESS  ***
!
390   iv(1) = 64
      Go To 410
!
!     ***  PRINT SUMMARY OF FINAL ITERATION AND OTHER REQUESTED ITEMS  ***
!
400   iv(1) = iv(cnvcod)
      iv(cnvcod) = 0
410   Call ditsum(d,g,iv,liv,lv,n,v,x)
!
420   Return
!
!     ***  LAST CARD OF DRMNH FOLLOWS  ***
    End Subroutine drmnh
    Subroutine dq7rsh(k,p,havqtr,qtr,r,w)
!
!     ***  PERMUTE COLUMN K OF R TO COLUMN P, MODIFY QTR ACCORDINGLY  ***
!
!     DIMENSION R(P*(P+1)/2)
!
!
!     ***  LOCAL VARIABLES  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: k, p
      Logical                          :: havqtr
!     .. Array Arguments ..
      Real (Kind=wp)                   :: qtr(p), r(*), w(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: a, b, t, wj, x, y, z
      Real (Kind=wp), Save             :: zero
      Integer                          :: i, i1, j, j1, jm1, jp1, k1, km1, pm1
!     .. External Procedures ..
      Real (Kind=wp), External         :: dh2rfg
      External                         :: dh2rfa, dv7cpy
!     .. Data Statements ..
      Data zero/0.0E+0_wp/
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      If (k>=p) Go To 130
      km1 = k - 1
      k1 = k*km1/2
      Call dv7cpy(k,w,r(k1+1))
      wj = w(k)
      pm1 = p - 1
      j1 = k1 + km1
      Do j = k, pm1
        jm1 = j - 1
        jp1 = j + 1
        If (jm1>0) Call dv7cpy(jm1,r(k1+1),r(j1+2))
        j1 = j1 + jp1
        k1 = k1 + j
        a = r(j1)
        b = r(j1+1)
        If (b/=zero) Go To 100
        r(k1) = a
        x = zero
        z = zero
        Go To 120
100     r(k1) = dh2rfg(a,b,x,y,z)
        If (j==pm1) Go To 110
        i1 = j1
        Do i = jp1, pm1
          i1 = i1 + i
          Call dh2rfa(1,r(i1),r(i1+1),x,y,z)
        End Do
110     If (havqtr) Call dh2rfa(1,qtr(j),qtr(jp1),x,y,z)
120     t = x*wj
        w(j) = wj + t
        wj = t*z
      End Do
      w(p) = wj
      Call dv7cpy(p,r(k1+1),w)
130   Return
    End Subroutine dq7rsh
    Subroutine drmnf(d,fx,iv,liv,lv,n,v,x)
!
!     ***  ITERATION DRIVER FOR  DMNF...
!     ***  MINIMIZE GENERAL UNCONSTRAINED OBJECTIVE FUNCTION USING
!     ***  FINITE-DIFFERENCE GRADIENTS AND SECANT HESSIAN APPROXIMATIONS.
!
!     DIMENSION V(77 + N*(N+17)/2)
!
!     ***  PURPOSE  ***
!
!        THIS ROUTINE INTERACTS WITH SUBROUTINE  DRMNG  IN AN ATTEMPT
!     TO FIND AN N-VECTOR  X*  THAT MINIMIZES THE (UNCONSTRAINED)
!     OBJECTIVE FUNCTION  FX = F(X)  COMPUTED BY THE CALLER.  (OFTEN
!     THE  X*  FOUND IS A LOCAL MINIMIZER RATHER THAN A GLOBAL ONE.)
!
!     ***  PARAMETERS  ***
!
!        THE PARAMETERS FOR DRMNF ARE THE SAME AS THOSE FOR  DMNG
!     (WHICH SEE), EXCEPT THAT CALCF, CALCG, UIPARM, URPARM, AND UFPARM
!     ARE OMITTED, AND A PARAMETER  FX  FOR THE OBJECTIVE FUNCTION
!     VALUE AT X IS ADDED.  INSTEAD OF CALLING CALCG TO OBTAIN THE
!     GRADIENT OF THE OBJECTIVE FUNCTION AT X, DRMNF CALLS DS7GRD,
!     WHICH COMPUTES AN APPROXIMATION TO THE GRADIENT BY FINITE
!     (FORWARD AND CENTRAL) DIFFERENCES USING THE METHOD OF REF. 1.
!     THE FOLLOWING INPUT COMPONENT IS OF INTEREST IN THIS REGARD
!     (AND IS NOT DESCRIBED IN  DMNG).
!
!     V(ETA0)..... V(42) IS AN ESTIMATED BOUND ON THE RELATIVE ERROR IN THE
!             OBJECTIVE FUNCTION VALUE COMPUTED BY CALCF...
!                  (TRUE VALUE) = (COMPUTED VALUE) * (1 + E),
!             WHERE ABS(E) .LE. V(ETA0).  DEFAULT = MACHEP * 10**3,
!             WHERE MACHEP IS THE UNIT ROUNDOFF.
!
!        THE OUTPUT VALUES IV(NFCALL) AND IV(NGCALL) HAVE DIFFERENT
!     MEANINGS FOR  DMNF THAN FOR  DMNG...
!
!     IV(NFCALL)... IV(6) IS THE NUMBER OF CALLS SO FAR MADE ON CALCF (I.E.,
!             FUNCTION EVALUATIONS) EXCLUDING THOSE MADE ONLY FOR
!             COMPUTING GRADIENTS.  THE INPUT VALUE IV(MXFCAL) IS A
!             LIMIT ON IV(NFCALL).
!     IV(NGCALL)... IV(30) IS THE NUMBER OF FUNCTION EVALUATIONS MADE ONLY
!             FOR COMPUTING GRADIENTS.  THE TOTAL NUMBER OF FUNCTION
!             EVALUATIONS IS THUS  IV(NFCALL) + IV(NGCALL).
!
!     ***  REFERENCES  ***
!
!     1. STEWART, G.W. (1967), A MODIFICATION OF DAVIDON*S MINIMIZATION
!        METHOD TO ACCEPT DIFFERENCE APPROXIMATIONS OF DERIVATIVES,
!        J. ASSOC. COMPUT. MACH. 14, PP. 72-83.
!.
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY (AUGUST 1982).
!
!     ----------------------------  DECLARATIONS  ---------------------------
!
!
!     DIVSET.... SUPPLIES DEFAULT PARAMETER VALUES.
!     DD7TPR... RETURNS INNER PRODUCT OF TWO VECTORS.
!     DS7GRD... COMPUTES FINITE-DIFFERENCE GRADIENT APPROXIMATION.
!     DRMNG.... REVERSE-COMMUNICATION ROUTINE THAT DOES  DMNG ALGORITHM.
!     DV7SCP... SETS ALL ELEMENTS OF A VECTOR TO A SCALAR.
!
!
!     ***  SUBSCRIPTS FOR IV   ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: eta0 = 42, f = 10, g = 28,           &
                                          lmat = 42, nextv = 47, ngcall = 30,  &
                                          niter = 31, sgirc = 57, toobig = 2,  &
                                          vneed = 4
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: fx
      Integer                          :: liv, lv, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(n), v(lv), x(n)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Integer                          :: alpha, g1, i, iv1, j, k, w
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr
      External                         :: divset, drmng, ds7grd, dv7scp
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      iv1 = iv(1)
      If (iv1==1) Go To 100
      If (iv1==2) Go To 120
      If (iv(1)==0) Call divset(2,iv,liv,lv,v)
      iv1 = iv(1)
      If (iv1==12 .Or. iv1==13) iv(vneed) = iv(vneed) + 2*n + 6
      If (iv1==14) Go To 100
      If (iv1>2 .And. iv1<12) Go To 100
      g1 = 1
      If (iv1==12) iv(1) = 13
      Go To 110
!
100   g1 = iv(g)
!
110   Call drmng(d,fx,v(g1),iv,liv,lv,n,v,x)
      If (iv(1)<2) Go To 150
      If (iv(1)>2) Go To 140
!
!     ***  COMPUTE GRADIENT  ***
!
      If (iv(niter)==0) Call dv7scp(n,v(g1),zero)
      j = iv(lmat)
      k = g1 - n
      Do i = 1, n
        v(k) = dd7tpr(i,v(j),v(j))
        k = k + 1
        j = j + i
      End Do
!     ***  UNDO INCREMENT OF IV(NGCALL) DONE BY DRMNG  ***
      iv(ngcall) = iv(ngcall) - 1
!     ***  STORE RETURN CODE FROM DS7GRD IN IV(SGIRC)  ***
      iv(sgirc) = 0
!     ***  X MAY HAVE BEEN RESTORED, SO COPY BACK FX... ***
      fx = v(f)
      Go To 130
!
!     ***  GRADIENT LOOP  ***
!
120   If (iv(toobig)/=0) Go To 100
!
130   g1 = iv(g)
      alpha = g1 - n
      w = alpha - 6
      Call ds7grd(v(alpha),d,v(eta0),fx,v(g1),iv(sgirc),n,v(w),x)
      If (iv(sgirc)==0) Go To 100
      iv(ngcall) = iv(ngcall) + 1
      Go To 150
!
140   If (iv(1)/=14) Go To 150
!
!     ***  STORAGE ALLOCATION  ***
!
      iv(g) = iv(nextv) + n + 6
      iv(nextv) = iv(g) + n
      If (iv1/=13) Go To 100
!
150   Return
!     ***  LAST CARD OF DRMNF FOLLOWS  ***
    End Subroutine drmnf
    Subroutine dl7vml(n,x,l,y)
!
!     ***  COMPUTE  X = L*Y, WHERE  L  IS AN  N X N  LOWER TRIANGULAR
!     ***  MATRIX STORED COMPACTLY BY ROWS.  X AND Y MAY OCCUPY THE SAME
!     ***  STORAGE.  ***
!
!     DIMENSION L(N*(N+1)/2)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
!     .. Scalar Arguments ..
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: l(*), x(n), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t
      Integer                          :: i, i0, ii, ij, j, np1
!     .. Executable Statements ..
!
      np1 = n + 1
      i0 = n*(n+1)/2
      Do ii = 1, n
        i = np1 - ii
        i0 = i0 - i
        t = zero
        Do j = 1, i
          ij = i0 + j
          t = t + l(ij)*y(j)
        End Do
        x(i) = t
      End Do
      Return
!     ***  LAST CARD OF DL7VML FOLLOWS  ***
    End Subroutine dl7vml
    Subroutine da7sst(iv,liv,lv,v)
!
!     ***  ASSESS CANDIDATE STEP (***SOL VERSION 2.3)  ***
!
!
!     ***  PURPOSE  ***
!
!        THIS SUBROUTINE IS CALLED BY AN UNCONSTRAINED MINIMIZATION
!     ROUTINE TO ASSESS THE NEXT CANDIDATE STEP.  IT MAY RECOMMEND ONE
!     OF SEVERAL COURSES OF ACTION, SUCH AS ACCEPTING THE STEP, RECOM-
!     PUTING IT USING THE SAME OR A NEW QUADRATIC MODEL, OR HALTING DUE
!     TO CONVERGENCE OR FALSE CONVERGENCE.  SEE THE RETURN CODE LISTING
!     BELOW.
!
!     --------------------------  PARAMETER USAGE  --------------------------
!
!     IV (I/O) INTEGER PARAMETER AND SCRATCH VECTOR -- SEE DESCRIPTION
!             BELOW OF IV VALUES REFERENCED.
!     LIV (IN)  LENGTH OF IV ARRAY.
!     LV (IN)  LENGTH OF V ARRAY.
!     V (I/O) REAL PARAMETER AND SCRATCH VECTOR -- SEE DESCRIPTION
!             BELOW OF V VALUES REFERENCED.
!
!     ***  IV VALUES REFERENCED  ***
!
!     IV(IRC) (I/O) ON INPUT FOR THE FIRST STEP TRIED IN A NEW ITERATION,
!             IV(IRC) SHOULD BE SET TO 3 OR 4 (THE VALUE TO WHICH IT IS
!             SET WHEN STEP IS DEFINITELY TO BE ACCEPTED).  ON INPUT
!             AFTER STEP HAS BEEN RECOMPUTED, IV(IRC) SHOULD BE
!             UNCHANGED SINCE THE PREVIOUS RETURN OF DA7SST.
!                ON OUTPUT, IV(IRC) IS A RETURN CODE HAVING ONE OF THE
!             FOLLOWING VALUES...
!                  1 = SWITCH MODELS OR TRY SMALLER STEP.
!                  2 = SWITCH MODELS OR ACCEPT STEP.
!                  3 = ACCEPT STEP AND DETERMINE V(RADFAC) BY GRADIENT
!                       TESTS.
!                  4 = ACCEPT STEP, V(RADFAC) HAS BEEN DETERMINED.
!                  5 = RECOMPUTE STEP (USING THE SAME MODEL).
!                  6 = RECOMPUTE STEP WITH RADIUS = V(LMAXS) BUT DO NOT
!                       EVALUATE THE OBJECTIVE FUNCTION.
!                  7 = X-CONVERGENCE (SEE V(XCTOL)).
!                  8 = RELATIVE FUNCTION CONVERGENCE (SEE V(RFCTOL)).
!                  9 = BOTH X- AND RELATIVE FUNCTION CONVERGENCE.
!                 10 = ABSOLUTE FUNCTION CONVERGENCE (SEE V(AFCTOL)).
!                 11 = SINGULAR CONVERGENCE (SEE V(LMAXS)).
!                 12 = FALSE CONVERGENCE (SEE V(XFTOL)).
!                 13 = IV(IRC) WAS OUT OF RANGE ON INPUT.
!             RETURN CODE I HAS PRECEDENCE OVER I+1 FOR I = 9, 10, 11.
!     IV(MLSTGD) (I/O) SAVED VALUE OF IV(MODEL).
!     IV(MODEL) (I/O) ON INPUT, IV(MODEL) SHOULD BE AN INTEGER IDENTIFYING
!             THE CURRENT QUADRATIC MODEL OF THE OBJECTIVE FUNCTION.
!             IF A PREVIOUS STEP YIELDED A BETTER FUNCTION REDUCTION,
!             THEN IV(MODEL) WILL BE SET TO IV(MLSTGD) ON OUTPUT.
!     IV(NFCALL) (IN)  INVOCATION COUNT FOR THE OBJECTIVE FUNCTION.
!     IV(NFGCAL) (I/O) VALUE OF IV(NFCALL) AT STEP THAT GAVE THE BIGGEST
!             FUNCTION REDUCTION THIS ITERATION.  IV(NFGCAL) REMAINS
!             UNCHANGED UNTIL A FUNCTION REDUCTION IS OBTAINED.
!     IV(RADINC) (I/O) THE NUMBER OF RADIUS INCREASES (OR MINUS THE NUMBER
!             OF DECREASES) SO FAR THIS ITERATION.
!     IV(RESTOR) (OUT) SET TO 1 IF V(F) HAS BEEN RESTORED AND X SHOULD BE
!             RESTORED TO ITS INITIAL VALUE, TO 2 IF X SHOULD BE SAVED,
!             TO 3 IF X SHOULD BE RESTORED FROM THE SAVED VALUE, AND TO
!             0 OTHERWISE.
!     IV(STAGE) (I/O) COUNT OF THE NUMBER OF MODELS TRIED SO FAR IN THE
!             CURRENT ITERATION.
!     IV(STGLIM) (IN)  MAXIMUM NUMBER OF MODELS TO CONSIDER.
!     IV(SWITCH) (OUT) SET TO 0 UNLESS A NEW MODEL IS BEING TRIED AND IT
!             GIVES A SMALLER FUNCTION VALUE THAN THE PREVIOUS MODEL,
!             IN WHICH CASE DA7SST SETS IV(SWITCH) = 1.
!     IV(TOOBIG) (I/O)  IS NONZERO ON INPUT IF STEP WAS TOO BIG (E.G., IF
!             IT WOULD CAUSE OVERFLOW).  IT IS SET TO 0 ON RETURN.
!     IV(XIRC) (I/O) VALUE THAT IV(IRC) WOULD HAVE IN THE ABSENCE OF
!             CONVERGENCE, FALSE CONVERGENCE, AND OVERSIZED STEPS.
!
!     ***  V VALUES REFERENCED  ***
!
!     V(AFCTOL) (IN)  ABSOLUTE FUNCTION CONVERGENCE TOLERANCE.  IF THE
!             ABSOLUTE VALUE OF THE CURRENT FUNCTION VALUE V(F) IS LESS
!             THAN V(AFCTOL) AND DA7SST DOES NOT RETURN WITH
!             IV(IRC) = 11, THEN DA7SST RETURNS WITH IV(IRC) = 10.
!     V(DECFAC) (IN)  FACTOR BY WHICH TO DECREASE RADIUS WHEN IV(TOOBIG) IS
!             NONZERO.
!     V(DSTNRM) (IN)  THE 2-NORM OF D*STEP.
!     V(DSTSAV) (I/O) VALUE OF V(DSTNRM) ON SAVED STEP.
!     V(DST0) (IN)  THE 2-NORM OF D TIMES THE NEWTON STEP (WHEN DEFINED,
!             I.E., FOR V(NREDUC) .GE. 0).
!      V(F) (I/O) ON BOTH INPUT AND OUTPUT, V(F) IS THE OBJECTIVE FUNC-
!             TION VALUE AT X.  IF X IS RESTORED TO A PREVIOUS VALUE,
!             THEN V(F) IS RESTORED TO THE CORRESPONDING VALUE.
!     V(FDIF) (OUT) THE FUNCTION REDUCTION V(F0) - V(F) (FOR THE OUTPUT
!             VALUE OF V(F) IF AN EARLIER STEP GAVE A BIGGER FUNCTION
!             DECREASE, AND FOR THE INPUT VALUE OF V(F) OTHERWISE).
!     V(FLSTGD) (I/O) SAVED VALUE OF V(F).
!     V(F0) (IN)  OBJECTIVE FUNCTION VALUE AT START OF ITERATION.
!     V(GTSLST) (I/O) VALUE OF V(GTSTEP) ON SAVED STEP.
!     V(GTSTEP) (IN)  INNER PRODUCT BETWEEN STEP AND GRADIENT.
!     V(INCFAC) (IN)  MINIMUM FACTOR BY WHICH TO INCREASE RADIUS.
!     V(LMAXS) (IN)  MAXIMUM REASONABLE STEP SIZE (AND INITIAL STEP BOUND).
!             IF THE ACTUAL FUNCTION DECREASE IS NO MORE THAN TWICE
!             WHAT WAS PREDICTED, IF A RETURN WITH IV(IRC) = 7, 8, OR 9
!             DOES NOT OCCUR, IF V(DSTNRM) .GT. V(LMAXS) OR THE CURRENT
!             STEP IS A NEWTON STEP, AND IF
!             V(PREDUC) .LE. V(SCTOL) * ABS(V(F0)), THEN DA7SST RETURNS
!             WITH IV(IRC) = 11.  IF SO DOING APPEARS WORTHWHILE, THEN
!            DA7SST REPEATS THIS TEST (DISALLOWING A FULL NEWTON STEP)
!             WITH V(PREDUC) COMPUTED FOR A STEP OF LENGTH V(LMAXS)
!             (BY A RETURN WITH IV(IRC) = 6).
!     V(NREDUC) (I/O)  FUNCTION REDUCTION PREDICTED BY QUADRATIC MODEL FOR
!             NEWTON STEP.  IF DA7SST IS CALLED WITH IV(IRC) = 6, I.E.,
!             IF V(PREDUC) HAS BEEN COMPUTED WITH RADIUS = V(LMAXS) FOR
!             USE IN THE SINGULAR CONVERGENCE TEST, THEN V(NREDUC) IS
!             SET TO -V(PREDUC) BEFORE THE LATTER IS RESTORED.
!     V(PLSTGD) (I/O) VALUE OF V(PREDUC) ON SAVED STEP.
!     V(PREDUC) (I/O) FUNCTION REDUCTION PREDICTED BY QUADRATIC MODEL FOR
!             CURRENT STEP.
!     V(RADFAC) (OUT) FACTOR TO BE USED IN DETERMINING THE NEW RADIUS,
!             WHICH SHOULD BE V(RADFAC)*DST, WHERE  DST  IS EITHER THE
!             OUTPUT VALUE OF V(DSTNRM) OR THE 2-NORM OF
!             DIAG(NEWD)*STEP  FOR THE OUTPUT VALUE OF STEP AND THE
!             UPDATED VERSION, NEWD, OF THE SCALE VECTOR D.  FOR
!             IV(IRC) = 3, V(RADFAC) = 1.0 IS RETURNED.
!     V(RDFCMN) (IN)  MINIMUM VALUE FOR V(RADFAC) IN TERMS OF THE INPUT
!             VALUE OF V(DSTNRM) -- SUGGESTED VALUE = 0.1.
!     V(RDFCMX) (IN)  MAXIMUM VALUE FOR V(RADFAC) -- SUGGESTED VALUE = 4.0.
!     V(RELDX) (IN) SCALED RELATIVE CHANGE IN X CAUSED BY STEP, COMPUTED
!             (E.G.) BY FUNCTION  DRLDST  AS
!                 MAX (D(I)*ABS(X(I)-X0(I)), 1 .LE. I .LE. P) /
!                    MAX (D(I)*(ABS(X(I))+ABS(X0(I))), 1 .LE. I .LE. P).
!     V(RFCTOL) (IN)  RELATIVE FUNCTION CONVERGENCE TOLERANCE.  IF THE
!             ACTUAL FUNCTION REDUCTION IS AT MOST TWICE WHAT WAS PRE-
!             DICTED AND  V(NREDUC) .LE. V(RFCTOL)*ABS(V(F0)),  THEN
!            DA7SST RETURNS WITH IV(IRC) = 8 OR 9.
!     V(SCTOL) (IN)  SINGULAR CONVERGENCE TOLERANCE -- SEE V(LMAXS).
!     V(STPPAR) (IN)  MARQUARDT PARAMETER -- 0 MEANS FULL NEWTON STEP.
!     V(TUNER1) (IN)  TUNING CONSTANT USED TO DECIDE IF THE FUNCTION
!             REDUCTION WAS MUCH LESS THAN EXPECTED.  SUGGESTED
!             VALUE = 0.1.
!     V(TUNER2) (IN)  TUNING CONSTANT USED TO DECIDE IF THE FUNCTION
!             REDUCTION WAS LARGE ENOUGH TO ACCEPT STEP.  SUGGESTED
!             VALUE = 10**-4.
!     V(TUNER3) (IN)  TUNING CONSTANT USED TO DECIDE IF THE RADIUS
!             SHOULD BE INCREASED.  SUGGESTED VALUE = 0.75.
!     V(XCTOL) (IN)  X-CONVERGENCE CRITERION.  IF STEP IS A NEWTON STEP
!             (V(STPPAR) = 0) HAVING V(RELDX) .LE. V(XCTOL) AND GIVING
!             AT MOST TWICE THE PREDICTED FUNCTION DECREASE, THEN
!            DA7SST RETURNS IV(IRC) = 7 OR 9.
!     V(XFTOL) (IN)  FALSE CONVERGENCE TOLERANCE.  IF STEP GAVE NO OR ONLY
!             A SMALL FUNCTION DECREASE AND V(RELDX) .LE. V(XFTOL),
!             THEN DA7SST RETURNS WITH IV(IRC) = 12.
!
!     -------------------------------  NOTES  -------------------------------
!
!     ***  APPLICATION AND USAGE RESTRICTIONS  ***
!
!        THIS ROUTINE IS CALLED AS PART OF THE NL2SOL (NONLINEAR
!     LEAST-SQUARES) PACKAGE.  IT MAY BE USED IN ANY UNCONSTRAINED
!     MINIMIZATION SOLVER THAT USES DOGLEG, GOLDFELD-QUANDT-TROTTER,
!     OR LEVENBERG-MARQUARDT STEPS.
!
!     ***  ALGORITHM NOTES  ***
!
!        SEE (1) FOR FURTHER DISCUSSION OF THE ASSESSING AND MODEL
!     SWITCHING STRATEGIES.  WHILE NL2SOL CONSIDERS ONLY TWO MODELS,
!     DA7SST IS DESIGNED TO HANDLE ANY NUMBER OF MODELS.
!
!     ***  USAGE NOTES  ***
!
!        ON THE FIRST CALL OF AN ITERATION, ONLY THE I/O VARIABLES
!     STEP, X, IV(IRC), IV(MODEL), V(F), V(DSTNRM), V(GTSTEP), AND
!     V(PREDUC) NEED HAVE BEEN INITIALIZED.  BETWEEN CALLS, NO I/O
!     VALUES EXCEPT STEP, X, IV(MODEL), V(F) AND THE STOPPING TOLER-
!     ANCES SHOULD BE CHANGED.
!        AFTER A RETURN FOR CONVERGENCE OR FALSE CONVERGENCE, ONE CAN
!     CHANGE THE STOPPING TOLERANCES AND CALL DA7SST AGAIN, IN WHICH
!     CASE THE STOPPING TESTS WILL BE REPEATED.
!
!     ***  REFERENCES  ***
!
!     (1) DENNIS, J.E., JR., GAY, D.M., AND WELSCH, R.E. (1981),
!        AN ADAPTIVE NONLINEAR LEAST-SQUARES ALGORITHM,
!        ACM TRANS. MATH. SOFTWARE, VOL. 7, NO. 3.
!
!     (2) POWELL, M.J.D. (1970)  A FORTRAN SUBROUTINE FOR SOLVING
!        SYSTEMS OF NONLINEAR ALGEBRAIC EQUATIONS, IN NUMERICAL
!        METHODS FOR NONLINEAR ALGEBRAIC EQUATIONS, EDITED BY
!        P. RABINOWITZ, GORDON AND BREACH, LONDON.
!
!     ***  HISTORY  ***
!
!        JOHN DENNIS DESIGNED MUCH OF THIS ROUTINE, STARTING WITH
!     IDEAS IN (2). ROY WELSCH SUGGESTED THE MODEL SWITCHING STRATEGY.
!        DAVID GAY AND STEPHEN PETERS CAST THIS SUBROUTINE INTO A MORE
!     PORTABLE FORM (WINTER 1977), AND DAVID GAY CAST IT INTO ITS
!     PRESENT FORM (FALL 1978), WITH MINOR CHANGES TO THE SINGULAR
!     CONVERGENCE TEST IN MAY, 1984 (TO DEAL WITH FULL NEWTON STEPS).
!
!     ***  GENERAL  ***
!
!     THIS SUBROUTINE WAS WRITTEN IN CONNECTION WITH RESEARCH
!     SUPPORTED BY THE NATIONAL SCIENCE FOUNDATION UNDER GRANTS
!     MCS-7600324, DCR75-10143, 76-14311DSS, MCS76-11989, AND
!     MCS-7906671.
!
!     ------------------------  EXTERNAL QUANTITIES  ------------------------
!
!     ***  NO EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!     --------------------------  LOCAL VARIABLES  --------------------------
!
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!
!     ***  DATA INITIALIZATIONS  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: onep2 = 1.2E+0_wp
      Real (Kind=wp), Parameter        :: two = 2.E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: afctol = 31, decfac = 22, dst0 = 3,  &
                                          dstnrm = 2, dstsav = 18, f = 10,     &
                                          f0 = 13, fdif = 11, flstgd = 12,     &
                                          gtslst = 14, gtstep = 4,             &
                                          incfac = 23, irc = 29, lmaxs = 36,   &
                                          mlstgd = 32, model = 5, nfcall = 6,  &
                                          nfgcal = 7, nreduc = 6, plstgd = 15, &
                                          preduc = 7, radfac = 16, radinc = 8, &
                                          rdfcmn = 24, rdfcmx = 25,            &
                                          reldx = 17, restor = 9, rfctol = 32, &
                                          sctol = 37, stage = 10, stglim = 11, &
                                          stppar = 5, switch = 12, toobig = 2, &
                                          tuner1 = 26, tuner2 = 27,            &
                                          tuner3 = 28, xctol = 33, xftol = 34, &
                                          xirc = 13
!     .. Scalar Arguments ..
      Integer                          :: liv, lv
!     .. Array Arguments ..
      Real (Kind=wp)                   :: v(lv)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: emax, emaxs, gts, rfac1, xmax
      Integer                          :: i, nfc
      Logical                          :: goodx
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      nfc = iv(nfcall)
      iv(switch) = 0
      iv(restor) = 0
      rfac1 = one
      goodx = .True.
      i = iv(irc)
      If (i>=1 .And. i<=12) Go To (110,120,100,100,130,350,290,290,290,290,    &
        290,240) i
      iv(irc) = 13
      Go To 370
!
!     ***  INITIALIZE FOR NEW ITERATION  ***
!
100   iv(stage) = 1
      iv(radinc) = 0
      v(flstgd) = v(f0)
      If (iv(toobig)==0) Go To 180
      iv(stage) = -1
      iv(xirc) = i
      Go To 150
!
!     ***  STEP WAS RECOMPUTED WITH NEW MODEL OR SMALLER RADIUS  ***
!     ***  FIRST DECIDE WHICH  ***
!
110   If (iv(model)/=iv(mlstgd)) Go To 120
!        ***  OLD MODEL RETAINED, SMALLER RADIUS TRIED  ***
!        ***  DO NOT CONSIDER ANY MORE NEW MODELS THIS ITERATION  ***
      iv(stage) = iv(stglim)
      iv(radinc) = -1
      Go To 180
!
!     ***  A NEW MODEL IS BEING TRIED.  DECIDE WHETHER TO KEEP IT.  ***
!
120   iv(stage) = iv(stage) + 1
!
!     ***  NOW WE ADD THE POSSIBILITY THAT STEP WAS RECOMPUTED WITH  ***
!     ***  THE SAME MODEL, PERHAPS BECAUSE OF AN OVERSIZED STEP.     ***
!
130   If (iv(stage)>0) Go To 140
!
!        ***  STEP WAS RECOMPUTED BECAUSE IT WAS TOO BIG.  ***
!
      If (iv(toobig)/=0) Go To 150
!
!        ***  RESTORE IV(STAGE) AND PICK UP WHERE WE LEFT OFF.  ***
!
      iv(stage) = -iv(stage)
      i = iv(xirc)
      Go To (110,120,180,180,160) i
!
140   If (iv(toobig)==0) Go To 160
!
!     ***  HANDLE OVERSIZE STEP  ***
!
      iv(toobig) = 0
      If (iv(radinc)>0) Go To 170
      iv(stage) = -iv(stage)
      iv(xirc) = iv(irc)
!
150   iv(toobig) = 0
      v(radfac) = v(decfac)
      iv(radinc) = iv(radinc) - 1
      iv(irc) = 5
      iv(restor) = 1
      v(f) = v(flstgd)
      Go To 370
!
160   If (v(f)<v(flstgd)) Go To 180
!
!     *** THE NEW STEP IS A LOSER.  RESTORE OLD MODEL.  ***
!
      If (iv(model)==iv(mlstgd)) Go To 170
      iv(model) = iv(mlstgd)
      iv(switch) = 1
!
!     ***  RESTORE STEP, ETC. ONLY IF A PREVIOUS STEP DECREASED V(F).
!
170   If (v(flstgd)>=v(f0)) Go To 180
      If (iv(stage)<iv(stglim)) Then
        goodx = .False.
      Else If (nfc<iv(nfgcal)+iv(stglim)+2) Then
        goodx = .False.
      Else If (iv(switch)/=0) Then
        goodx = .False.
      End If
      iv(restor) = 3
      v(f) = v(flstgd)
      v(preduc) = v(plstgd)
      v(gtstep) = v(gtslst)
      If (iv(switch)==0) rfac1 = v(dstnrm)/v(dstsav)
      v(dstnrm) = v(dstsav)
      If (goodx) Then
!
!       ***  ACCEPT PREVIOUS SLIGHTLY REDUCING STEP ***
!
        v(fdif) = v(f0) - v(f)
        iv(irc) = 4
        v(radfac) = rfac1
        Go To 370
      End If
      nfc = iv(nfgcal)
!
180   v(fdif) = v(f0) - v(f)
      If (v(fdif)>v(tuner2)*v(preduc)) Go To 210
      If (iv(radinc)>0) Go To 210
!
!        ***  NO (OR ONLY A TRIVIAL) FUNCTION DECREASE
!        ***  -- SO TRY NEW MODEL OR SMALLER RADIUS
!
      If (v(f)<v(f0)) Go To 190
      iv(mlstgd) = iv(model)
      v(flstgd) = v(f)
      v(f) = v(f0)
      iv(restor) = 1
      Go To 200
190   iv(nfgcal) = nfc
200   iv(irc) = 1
      If (iv(stage)<iv(stglim)) Go To 230
      iv(irc) = 5
      iv(radinc) = iv(radinc) - 1
      Go To 230
!
!     ***  NONTRIVIAL FUNCTION DECREASE ACHIEVED  ***
!
210   iv(nfgcal) = nfc
      rfac1 = one
      v(dstsav) = v(dstnrm)
      If (v(fdif)>v(preduc)*v(tuner1)) Go To 260
!
!     ***  DECREASE WAS MUCH LESS THAN PREDICTED -- EITHER CHANGE MODELS
!     ***  OR ACCEPT STEP WITH DECREASED RADIUS.
!
      If (iv(stage)>=iv(stglim)) Go To 220
!        ***  CONSIDER SWITCHING MODELS  ***
      iv(irc) = 2
      Go To 230
!
!     ***  ACCEPT STEP WITH DECREASED RADIUS  ***
!
220   iv(irc) = 4
!
!     ***  SET V(RADFAC) TO FLETCHER*S DECREASE FACTOR  ***
!
230   iv(xirc) = iv(irc)
      emax = v(gtstep) + v(fdif)
      v(radfac) = half*rfac1
      If (emax<v(gtstep)) v(radfac) = rfac1*max(v(rdfcmn),half*v(gtstep)/emax)
!
!     ***  DO FALSE CONVERGENCE TEST  ***
!
240   If (v(reldx)<=v(xftol)) Go To 250
      iv(irc) = iv(xirc)
      If (v(f)<v(f0)) Go To 270
      Go To 300
!
250   iv(irc) = 12
      Go To 310
!
!     ***  HANDLE GOOD FUNCTION DECREASE  ***
!
260   If (v(fdif)<(-v(tuner3)*v(gtstep))) Go To 280
!
!     ***  INCREASING RADIUS LOOKS WORTHWHILE.  SEE IF WE JUST
!     ***  RECOMPUTED STEP WITH A DECREASED RADIUS OR RESTORED STEP
!     ***  AFTER RECOMPUTING IT WITH A LARGER RADIUS.
!
      If (iv(radinc)<0) Go To 280
      If (iv(restor)==1) Go To 280
      If (iv(restor)==3) Go To 280
!
!        ***  WE DID NOT.  TRY A LONGER STEP UNLESS THIS WAS A NEWTON
!        ***  STEP.
!
      v(radfac) = v(rdfcmx)
      gts = v(gtstep)
      If (v(fdif)<(half/v(radfac)-one)*gts) v(radfac) = max(v(incfac),         &
        half*gts/(gts+v(fdif)))
      iv(irc) = 4
      If (v(stppar)==zero) Go To 300
      If (v(dst0)>=zero .And. (v(dst0)<two*v(dstnrm) .Or. v(nreduc)<onep2*v(   &
        fdif))) Go To 300
!             ***  STEP WAS NOT A NEWTON STEP.  RECOMPUTE IT WITH
!             ***  A LARGER RADIUS.
      iv(irc) = 5
      iv(radinc) = iv(radinc) + 1
!
!     ***  SAVE VALUES CORRESPONDING TO GOOD STEP  ***
!
270   v(flstgd) = v(f)
      iv(mlstgd) = iv(model)
      If (iv(restor)==0) iv(restor) = 2
      v(dstsav) = v(dstnrm)
      iv(nfgcal) = nfc
      v(plstgd) = v(preduc)
      v(gtslst) = v(gtstep)
      Go To 300
!
!     ***  ACCEPT STEP WITH RADIUS UNCHANGED  ***
!
280   v(radfac) = one
      iv(irc) = 3
      Go To 300
!
!     ***  COME HERE FOR A RESTART AFTER CONVERGENCE  ***
!
290   iv(irc) = iv(xirc)
      If (v(dstsav)>=zero) Go To 310
      iv(irc) = 12
      Go To 310
!
!     ***  PERFORM CONVERGENCE TESTS  ***
!
300   iv(xirc) = iv(irc)
310   If (iv(restor)==1 .And. v(flstgd)<v(f0)) iv(restor) = 3
      If (abs(v(f))<v(afctol)) iv(irc) = 10
      If (half*v(fdif)>v(preduc)) Go To 370
      emax = v(rfctol)*abs(v(f0))
      emaxs = v(sctol)*abs(v(f0))
      If (v(preduc)<=emaxs .And. (v(dstnrm)>v(lmaxs) .Or. v(stppar)==zero))    &
        iv(irc) = 11
      If (v(dst0)<zero) Go To 320
      i = 0
      If ((v(nreduc)>zero .And. v(nreduc)<=emax) .Or. (v(                      &
        nreduc)==zero .And. v(preduc)==zero)) i = 2
      If (v(stppar)==zero .And. v(reldx)<=v(xctol) .And. goodx) i = i + 1
      If (i>0) iv(irc) = i + 6
!
!     ***  CONSIDER RECOMPUTING STEP OF LENGTH V(LMAXS) FOR SINGULAR
!     ***  CONVERGENCE TEST.
!
320   If (iv(irc)>5 .And. iv(irc)/=12) Go To 370
      If (v(stppar)==zero) Go To 370
      If (v(dstnrm)>v(lmaxs)) Go To 330
      If (v(preduc)>=emaxs) Go To 370
      If (v(dst0)<=zero) Go To 340
      If (half*v(dst0)<=v(lmaxs)) Go To 370
      Go To 340
330   If (half*v(dstnrm)<=v(lmaxs)) Go To 370
      xmax = v(lmaxs)/v(dstnrm)
      If (xmax*(two-xmax)*v(preduc)>=emaxs) Go To 370
340   If (v(nreduc)<zero) Go To 360
!
!     ***  RECOMPUTE V(PREDUC) FOR USE IN SINGULAR CONVERGENCE TEST  ***
!
      v(gtslst) = v(gtstep)
      v(dstsav) = v(dstnrm)
      If (iv(irc)==12) v(dstsav) = -v(dstsav)
      v(plstgd) = v(preduc)
      i = iv(restor)
      iv(restor) = 2
      If (i==3) iv(restor) = 0
      iv(irc) = 6
      Go To 370
!
!     ***  PERFORM SINGULAR CONVERGENCE TEST WITH RECOMPUTED V(PREDUC)  ***
!
350   v(gtstep) = v(gtslst)
      v(dstnrm) = abs(v(dstsav))
      iv(irc) = iv(xirc)
      If (v(dstsav)<=zero) iv(irc) = 12
      v(nreduc) = -v(preduc)
      v(preduc) = v(plstgd)
      iv(restor) = 3
360   If (-v(nreduc)<=v(sctol)*abs(v(f0))) iv(irc) = 11
!
370   Return
!
!     ***  LAST LINE OF DA7SST FOLLOWS  ***
    End Subroutine da7sst
    Subroutine i7shft(n,k,x)
!
!     ***  SHIFT X(K),...,X(N) LEFT CIRCULARLY ONE POSITION IF K .GT. 0.
!     ***  SHIFT X(-K),...,X(N) RIGHT CIRCULARLY ONE POSITION IF K .LT. 0.
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Scalar Arguments ..
      Integer                          :: k, n
!     .. Array Arguments ..
      Integer                          :: x(n)
!     .. Local Scalars ..
      Integer                          :: i, ii, k1, nm1, t
!     .. Executable Statements ..
!
      If (k<0) Go To 100
      If (k>=n) Go To 110
      nm1 = n - 1
      t = x(k)
      Do i = k, nm1
        x(i) = x(i+1)
      End Do
      x(n) = t
      Go To 110
!
100   k1 = -k
      If (k1>=n) Go To 110
      t = x(n)
      nm1 = n - k1
      Do ii = 1, nm1
        i = n - ii
        x(i+1) = x(i)
      End Do
      x(k1) = t
110   Return
!     ***  LAST LINE OF I7SHFT FOLLOWS  ***
    End Subroutine i7shft
    Subroutine s7etr(m,n,indrow,jpntr,indcol,ipntr,iwa)
!     **********
!
!     SUBROUTINE S7ETR
!
!     GIVEN A COLUMN-ORIENTED DEFINITION OF THE SPARSITY PATTERN
!     OF AN M BY N MATRIX A, THIS SUBROUTINE DETERMINES A
!     ROW-ORIENTED DEFINITION OF THE SPARSITY PATTERN OF A.
!
!     ON INPUT THE COLUMN-ORIENTED DEFINITION IS SPECIFIED BY
!     THE ARRAYS INDROW AND JPNTR. ON OUTPUT THE ROW-ORIENTED
!     DEFINITION IS SPECIFIED BY THE ARRAYS INDCOL AND IPNTR.
!
!     THE SUBROUTINE STATEMENT IS
!
!       SUBROUTINE S7ETR(M,N,INDROW,JPNTR,INDCOL,IPNTR,IWA)
!
!     WHERE
!
!       M IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE NUMBER
!         OF ROWS OF A.
!
!       N IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE NUMBER
!         OF COLUMNS OF A.
!
!       INDROW IS AN INTEGER INPUT ARRAY WHICH CONTAINS THE ROW
!         INDICES FOR THE NON-ZEROES IN THE MATRIX A.
!
!       JPNTR IS AN INTEGER INPUT ARRAY OF LENGTH N + 1 WHICH
!         SPECIFIES THE LOCATIONS OF THE ROW INDICES IN INDROW.
!         THE ROW INDICES FOR COLUMN J ARE
!
!               INDROW(K), K = JPNTR(J),...,JPNTR(J+1)-1.
!
!         NOTE THAT JPNTR(N+1)-1 IS THEN THE NUMBER OF NON-ZERO
!         ELEMENTS OF THE MATRIX A.
!
!       INDCOL IS AN INTEGER OUTPUT ARRAY WHICH CONTAINS THE
!         COLUMN INDICES FOR THE NON-ZEROES IN THE MATRIX A.
!
!       IPNTR IS AN INTEGER OUTPUT ARRAY OF LENGTH M + 1 WHICH
!         SPECIFIES THE LOCATIONS OF THE COLUMN INDICES IN INDCOL.
!         THE COLUMN INDICES FOR ROW I ARE
!
!               INDCOL(K), K = IPNTR(I),...,IPNTR(I+1)-1.
!
!         NOTE THAT IPNTR(1) IS SET TO 1 AND THAT IPNTR(M+1)-1 IS
!         THEN THE NUMBER OF NON-ZERO ELEMENTS OF THE MATRIX A.
!
!       IWA IS AN INTEGER WORK ARRAY OF LENGTH M.
!
!     ARGONNE NATIONAL LABORATORY. MINPACK PROJECT. JUNE 1982.
!     THOMAS F. COLEMAN, BURTON S. GARBOW, JORGE J. MORE
!
!     **********

!     .. Implicit None Statement ..
      Implicit None
!     .. Scalar Arguments ..
      Integer                          :: m, n
!     .. Array Arguments ..
      Integer                          :: indcol(1), indrow(1), ipntr(1),      &
                                          iwa(m), jpntr(1)
!     .. Local Scalars ..
      Integer                          :: ir, jcol, jp, jpl, jpu, l, nnz
!     .. Executable Statements ..
!
!     DETERMINE THE NUMBER OF NON-ZEROES IN THE ROWS.
!
      Do ir = 1, m
        iwa(ir) = 0
      End Do
      nnz = jpntr(n+1) - 1
      Do jp = 1, nnz
        ir = indrow(jp)
        iwa(ir) = iwa(ir) + 1
      End Do
!
!     SET POINTERS TO THE START OF THE ROWS IN INDCOL.
!
      ipntr(1) = 1
      Do ir = 1, m
        ipntr(ir+1) = ipntr(ir) + iwa(ir)
        iwa(ir) = ipntr(ir)
      End Do
!
!     FILL INDCOL.
!
      Do jcol = 1, n
        jpl = jpntr(jcol)
        jpu = jpntr(jcol+1) - 1
        If (jpu<jpl) Go To 100
        Do jp = jpl, jpu
          ir = indrow(jp)
          l = iwa(ir)
          indcol(l) = jcol
          iwa(ir) = iwa(ir) + 1
        End Do
100     Continue
      End Do
      Return
!
!     LAST CARD OF SUBROUTINE S7ETR.
!
    End Subroutine s7etr
    Subroutine dg7qsb(b,d,dihdi,g,ipiv,ipiv1,ipiv2,ka,l,lv,p,p0,pc,step,td,tg, &
      v,w,x,x0)
!
!     ***  COMPUTE HEURISTIC BOUNDED NEWTON STEP  ***
!
!     DIMENSION DIHDI(P*(P+1)/2), L(P*(P+1)/2)
!
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  V SUBSCRIPTS  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Integer, Parameter               :: dst0 = 3, dstnrm = 2, gtstep = 4,    &
                                          nreduc = 6, preduc = 7, radius = 8
!     .. Scalar Arguments ..
      Integer                          :: ka, lv, p, p0, pc
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(2,p), d(p), dihdi(*), g(p), l(*),  &
                                          step(p,2), td(p), tg(p), v(lv),      &
                                          w(p), x(p), x0(p)
      Integer                          :: ipiv(p), ipiv1(p), ipiv2(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: ds0, nred, pred, rad
      Real (Kind=wp), Save             :: zero
      Integer                          :: k, kb, kinit, ns, p1, p10
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr
      External                         :: dg7qts, ds7bqn, ds7ipr, dv7cpy,      &
                                          dv7ipr, dv7scp, dv7vmp
!     .. Data Statements ..
      Data zero/0.E+0_wp/
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      p1 = pc
      If (ka<0) Go To 100
      nred = v(nreduc)
      ds0 = v(dst0)
      Go To 110
100   p0 = 0
      ka = -1
!
110   kinit = -1
      If (p0==p1) kinit = ka
      Call dv7cpy(p,x,x0)
      pred = zero
      rad = v(radius)
      kb = -1
      v(dstnrm) = zero
      If (p1>0) Go To 120
      nred = zero
      ds0 = zero
      Call dv7scp(p,step,zero)
      Go To 150
!
120   Call dv7cpy(p,td,d)
      Call dv7ipr(p,ipiv,td)
      Call dv7vmp(p,tg,g,d,-1)
      Call dv7ipr(p,ipiv,tg)
130   k = kinit
      kinit = -1
      v(radius) = rad - v(dstnrm)
      Call dg7qts(td,tg,dihdi,k,l,p1,step,v,w)
      p0 = p1
      If (ka>=0) Go To 140
      nred = v(nreduc)
      ds0 = v(dst0)
!
140   ka = k
      v(radius) = rad
      p10 = p1
      Call ds7bqn(b,d,step(1,2),ipiv,ipiv1,ipiv2,kb,l,lv,ns,p,p1,step,td,tg,v, &
        w,x,x0)
      If (ns>0) Call ds7ipr(p10,ipiv1,dihdi)
      pred = pred + v(preduc)
      If (ns/=0) p0 = 0
      If (kb<=0) Go To 130
!
150   v(dst0) = ds0
      v(nreduc) = nred
      v(preduc) = pred
      v(gtstep) = dd7tpr(p,g,step)
!
      Return
!     ***  LAST LINE OF DG7QSB FOLLOWS  ***
    End Subroutine dg7qsb
    Function dl7svx(p,l,x,y)
!
!     ***  ESTIMATE LARGEST SING. VALUE OF PACKED LOWER TRIANG. MATRIX L
!
!     ***  PARAMETER DECLARATIONS  ***
!
!     DIMENSION L(P*(P+1)/2)
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!     ***  PURPOSE  ***
!
!     THIS FUNCTION RETURNS A GOOD UNDER-ESTIMATE OF THE LARGEST
!     SINGULAR VALUE OF THE PACKED LOWER TRIANGULAR MATRIX L.
!
!     ***  PARAMETER DESCRIPTION  ***
!
!     P (IN)  = THE ORDER OF L.  L IS A  P X P  LOWER TRIANGULAR MATRIX.
!     L (IN)  = ARRAY HOLDING THE ELEMENTS OF  L  IN ROW ORDER, I.E.
!             L(1,1), L(2,1), L(2,2), L(3,1), L(3,2), L(3,3), ETC.
!     X (OUT) IF DL7SVX RETURNS A POSITIVE VALUE, THEN X = (L**T)*Y IS AN
!             (UNNORMALIZED) APPROXIMATE RIGHT SINGULAR VECTOR
!             CORRESPONDING TO THE LARGEST SINGULAR VALUE.  THIS
!             APPROXIMATION MAY BE CRUDE.
!     Y (OUT) IF DL7SVX RETURNS A POSITIVE VALUE, THEN Y = L*X IS A
!             NORMALIZED APPROXIMATE LEFT SINGULAR VECTOR CORRESPOND-
!             ING TO THE LARGEST SINGULAR VALUE.  THIS APPROXIMATION
!             MAY BE VERY CRUDE.  THE CALLER MAY PASS THE SAME VECTOR
!             FOR X AND Y (NONSTANDARD FORTRAN USAGE), IN WHICH CASE X
!             OVER-WRITES Y.
!
!     ***  ALGORITHM NOTES  ***
!
!     THE ALGORITHM IS BASED ON ANALOGY WITH (1).  IT USES A
!     RANDOM NUMBER GENERATOR PROPOSED IN (4), WHICH PASSES THE
!     SPECTRAL TEST WITH FLYING COLORS -- SEE (2) AND (3).
!
!     ***  SUBROUTINES AND FUNCTIONS CALLED  ***
!
!        DV2NRM - FUNCTION, RETURNS THE 2-NORM OF A VECTOR.
!
!     ***  REFERENCES  ***
!
!     (1) CLINE, A., MOLER, C., STEWART, G., AND WILKINSON, J.H.(1977),
!         AN ESTIMATE FOR THE CONDITION NUMBER OF A MATRIX, REPORT
!         TM-310, APPLIED MATH. DIV., ARGONNE NATIONAL LABORATORY.
!
!     (2) HOAGLIN, D.C. (1976), THEORETICAL PROPERTIES OF CONGRUENTIAL
!         RANDOM-NUMBER GENERATORS --  AN EMPIRICAL VIEW,
!         MEMORANDUM NS-340, DEPT. OF STATISTICS, HARVARD UNIV.
!
!     (3) KNUTH, D.E. (1969), THE ART OF COMPUTER PROGRAMMING, VOL. 2
!         (SEMINUMERICAL ALGORITHMS), ADDISON-WESLEY, READING, MASS.
!
!     (4) SMITH, C.S. (1971), MULTIPLICATIVE PSEUDO-RANDOM NUMBER
!         GENERATORS WITH PRIME MODULUS, J. ASSOC. COMPUT. MACH. 18,
!         PP. 586-593.
!
!     ***  HISTORY  ***
!
!     DESIGNED AND CODED BY DAVID M. GAY (WINTER 1977/SUMMER 1978).
!
!     ***  GENERAL  ***
!
!     THIS SUBROUTINE WAS WRITTEN IN CONNECTION WITH RESEARCH
!     SUPPORTED BY THE NATIONAL SCIENCE FOUNDATION UNDER GRANTS
!     MCS-7600324, DCR75-10143, 76-14311DSS, AND MCS76-11989.
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  CONSTANTS  ***
!
!
!     ***  EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Function Return Value ..
      Real (Kind=wp)                   :: dl7svx
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: r9973 = 9973.E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
!     .. Scalar Arguments ..
      Integer                          :: p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: l(*), x(p), y(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: b, blji, sminus, splus, t, yi
      Integer                          :: i, ix, j, j0, ji, jj, jjj, jm1, pm1, &
                                          pplus1
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, dv2nrm
      External                         :: dv2axy
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, mod, real
!     .. Executable Statements ..
!
!     ***  BODY  ***
!
      ix = 2
      pplus1 = p + 1
      pm1 = p - 1
!
!     ***  FIRST INITIALIZE X TO PARTIAL SUMS  ***
!
      j0 = p*pm1/2
      jj = j0 + p
      ix = mod(3432*ix,9973)
      b = half*(one+real(ix,kind=wp)/r9973)
      x(p) = b*l(jj)
      If (p<=1) Go To 100
      Do i = 1, pm1
        ji = j0 + i
        x(i) = b*l(ji)
      End Do
!
!     ***  COMPUTE X = (L**T)*B, WHERE THE COMPONENTS OF B HAVE RANDOMLY
!     ***  CHOSEN MAGNITUDES IN (.5,1) WITH SIGNS CHOSEN TO MAKE X LARGE.
!
!     DO J = P-1 TO 1 BY -1...
      Do jjj = 1, pm1
        j = p - jjj
!       ***  DETERMINE X(J) IN THIS ITERATION. NOTE FOR I = 1,2,...,J
!       ***  THAT X(I) HOLDS THE CURRENT PARTIAL SUM FOR ROW I.
        ix = mod(3432*ix,9973)
        b = half*(one+real(ix,kind=wp)/r9973)
        jm1 = j - 1
        j0 = j*jm1/2
        splus = zero
        sminus = zero
        Do i = 1, j
          ji = j0 + i
          blji = b*l(ji)
          splus = splus + abs(blji+x(i))
          sminus = sminus + abs(blji-x(i))
        End Do
        If (sminus>splus) b = -b
        x(j) = zero
!        ***  UPDATE PARTIAL SUMS  ***
        Call dv2axy(j,x,b,l(j0+1),x)
      End Do
!
!     ***  NORMALIZE X  ***
!
100   t = dv2nrm(p,x)
      If (t<=zero) Go To 110
      t = one/t
      Do i = 1, p
        x(i) = t*x(i)
      End Do
!
!     ***  COMPUTE L*X = Y AND RETURN SVMAX = TWONORM(Y)  ***
!
      Do jjj = 1, p
        j = pplus1 - jjj
        ji = j*(j-1)/2 + 1
        y(j) = dd7tpr(j,l(ji),x)
      End Do
!
!     ***  NORMALIZE Y AND SET X = (L**T)*Y  ***
!
      t = one/dv2nrm(p,y)
      ji = 1
      Do i = 1, p
        yi = t*y(i)
        x(i) = zero
        Call dv2axy(i,x,yi,l(ji),x)
        ji = ji + i
      End Do
      dl7svx = dv2nrm(p,x)
      Go To 120
!
110   dl7svx = zero
!
120   Return
!     ***  LAST CARD OF DL7SVX FOLLOWS  ***
    End Function dl7svx
    Subroutine dd7dup(d,hdiag,iv,liv,lv,n,v)
!
!     ***  UPDATE SCALE VECTOR D FOR  DMNH  ***
!
!     ***  PARAMETER DECLARATIONS  ***
!
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  INTRINSIC FUNCTIONS  ***
!     /+
!/
!     ***  SUBSCRIPTS FOR IV AND V  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Integer, Parameter               :: dfac = 41, dtol = 59, dtype = 16,    &
                                          niter = 31
!     .. Scalar Arguments ..
      Integer                          :: liv, lv, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(n), hdiag(n), v(lv)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t, vdfac
      Integer                          :: d0i, dtoli, i
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max, sqrt
!     .. Executable Statements ..
!
!     -------------------------------  BODY  --------------------------------
!
      i = iv(dtype)
      If (i==1) Go To 100
      If (iv(niter)>0) Go To 110
!
100   dtoli = iv(dtol)
      d0i = dtoli + n
      vdfac = v(dfac)
      Do i = 1, n
        t = max(sqrt(abs(hdiag(i))),vdfac*d(i))
        If (t<v(dtoli)) t = max(v(dtoli),v(d0i))
        d(i) = t
        dtoli = dtoli + 1
        d0i = d0i + 1
      End Do
!
110   Return
!     ***  LAST CARD OF DD7DUP FOLLOWS  ***
    End Subroutine dd7dup
    Subroutine s7rtdt(n,nnz,indrow,indcol,jpntr,iwa)
!     **********
!
!     SUBROUTINE S7RTDT
!
!     GIVEN THE NON-ZERO ELEMENTS OF AN M BY N MATRIX A IN
!     ARBITRARY ORDER AS SPECIFIED BY THEIR ROW AND COLUMN
!     INDICES, THIS SUBROUTINE PERMUTES THESE ELEMENTS SO
!     THAT THEIR COLUMN INDICES ARE IN NON-DECREASING ORDER.
!
!     ON INPUT IT IS ASSUMED THAT THE ELEMENTS ARE SPECIFIED IN
!
!           INDROW(K),INDCOL(K), K = 1,...,NNZ.
!
!     ON OUTPUT THE ELEMENTS ARE PERMUTED SO THAT INDCOL IS
!     IN NON-DECREASING ORDER. IN ADDITION, THE ARRAY JPNTR
!     IS SET SO THAT THE ROW INDICES FOR COLUMN J ARE
!
!           INDROW(K), K = JPNTR(J),...,JPNTR(J+1)-1.
!
!     NOTE THAT THE VALUE OF M IS NOT NEEDED BY S7RTDT AND IS
!     THEREFORE NOT PRESENT IN THE SUBROUTINE STATEMENT.
!
!     THE SUBROUTINE STATEMENT IS
!
!       SUBROUTINE S7RTDT(N,NNZ,INDROW,INDCOL,JPNTR,IWA)
!
!     WHERE
!
!       N IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE NUMBER
!         OF COLUMNS OF A.
!
!       NNZ IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE NUMBER
!         OF NON-ZERO ELEMENTS OF A.
!
!       INDROW IS AN INTEGER ARRAY OF LENGTH NNZ. ON INPUT INDROW
!         MUST CONTAIN THE ROW INDICES OF THE NON-ZERO ELEMENTS OF A.
!         ON OUTPUT INDROW IS PERMUTED SO THAT THE CORRESPONDING
!         COLUMN INDICES OF INDCOL ARE IN NON-DECREASING ORDER.
!
!       INDCOL IS AN INTEGER ARRAY OF LENGTH NNZ. ON INPUT INDCOL
!         MUST CONTAIN THE COLUMN INDICES OF THE NON-ZERO ELEMENTS
!         OF A. ON OUTPUT INDCOL IS PERMUTED SO THAT THESE INDICES
!         ARE IN NON-DECREASING ORDER.
!
!       JPNTR IS AN INTEGER OUTPUT ARRAY OF LENGTH N + 1 WHICH
!         SPECIFIES THE LOCATIONS OF THE ROW INDICES IN THE OUTPUT
!         INDROW. THE ROW INDICES FOR COLUMN J ARE
!
!               INDROW(K), K = JPNTR(J),...,JPNTR(J+1)-1.
!
!         NOTE THAT JPNTR(1) IS SET TO 1 AND THAT JPNTR(N+1)-1
!         IS THEN NNZ.
!
!       IWA IS AN INTEGER WORK ARRAY OF LENGTH N.
!
!     SUBPROGRAMS CALLED
!
!       FORTRAN-SUPPLIED ... MAX0
!
!     ARGONNE NATIONAL LABORATORY. MINPACK PROJECT. JUNE 1982.
!     THOMAS F. COLEMAN, BURTON S. GARBOW, JORGE J. MORE
!
!     **********

!     .. Implicit None Statement ..
      Implicit None
!     .. Scalar Arguments ..
      Integer                          :: n, nnz
!     .. Array Arguments ..
      Integer                          :: indcol(nnz), indrow(nnz), iwa(n),    &
                                          jpntr(1)
!     .. Local Scalars ..
      Integer                          :: i, j, k, l
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max
!     .. Executable Statements ..
!
!     DETERMINE THE NUMBER OF NON-ZEROES IN THE COLUMNS.
!
      Do j = 1, n
        iwa(j) = 0
      End Do
      Do k = 1, nnz
        j = indcol(k)
        iwa(j) = iwa(j) + 1
      End Do
!
!     SET POINTERS TO THE START OF THE COLUMNS IN INDROW.
!
      jpntr(1) = 1
      Do j = 1, n
        jpntr(j+1) = jpntr(j) + iwa(j)
        iwa(j) = jpntr(j)
      End Do
      k = 1
!
!     BEGIN IN-PLACE SORT.
!
100   Continue
      j = indcol(k)
      If (k<jpntr(j) .Or. k>=jpntr(j+1)) Go To 110
!
!           CURRENT ELEMENT IS IN POSITION. NOW EXAMINE THE
!           NEXT ELEMENT OR THE FIRST UN-SORTED ELEMENT IN
!           THE J-TH GROUP.
!
      k = max(k+1,iwa(j))
      Go To 120
110   Continue
!
!           CURRENT ELEMENT IS NOT IN POSITION. PLACE ELEMENT
!           IN POSITION AND MAKE THE DISPLACED ELEMENT THE
!           CURRENT ELEMENT.
!
      l = iwa(j)
      iwa(j) = iwa(j) + 1
      i = indrow(k)
      indrow(k) = indrow(l)
      indcol(k) = indcol(l)
      indrow(l) = i
      indcol(l) = j
120   Continue
      If (k<=nnz) Go To 100
      Return
!
!     LAST CARD OF SUBROUTINE S7RTDT.
!
    End Subroutine s7rtdt
    Subroutine dl7srt(n1,n,l,a,irc)
!
!     ***  COMPUTE ROWS N1 THROUGH N OF THE CHOLESKY FACTOR  L  OF
!     ***  A = L*(L**T),  WHERE  L  AND THE LOWER TRIANGLE OF  A  ARE BOTH
!     ***  STORED COMPACTLY BY ROWS (AND MAY OCCUPY THE SAME STORAGE).
!     ***  IRC = 0 MEANS ALL WENT WELL.  IRC = J MEANS THE LEADING
!     ***  PRINCIPAL  J X J  SUBMATRIX OF  A  IS NOT POSITIVE DEFINITE --
!     ***  AND  L(J*(J+1)/2)  CONTAINS THE (NONPOS.) REDUCED J-TH DIAGONAL.
!
!     ***  PARAMETERS  ***
!
!     DIMENSION L(N*(N+1)/2), A(N*(N+1)/2)
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  INTRINSIC FUNCTIONS  ***
!     /+
!/

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
!     .. Scalar Arguments ..
      Integer                          :: irc, n, n1
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(*), l(*)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t, td
      Integer                          :: i, i0, ij, ik, im1, j, j0, jk, jm1,  &
                                          k
!     .. Intrinsic Procedures ..
      Intrinsic                        :: sqrt
!     .. Executable Statements ..
!
!     ***  BODY  ***
!
      i0 = n1*(n1-1)/2
      Do i = n1, n
        td = zero
        If (i==1) Go To 110
        j0 = 0
        im1 = i - 1
        Do j = 1, im1
          t = zero
          If (j==1) Go To 100
          jm1 = j - 1
          Do k = 1, jm1
            ik = i0 + k
            jk = j0 + k
            t = t + l(ik)*l(jk)
          End Do
100       ij = i0 + j
          j0 = j0 + j
          t = (a(ij)-t)/l(j0)
          l(ij) = t
          td = td + t*t
        End Do
110     i0 = i0 + i
        t = a(i0) - td
        If (t<=zero) Go To 120
        l(i0) = sqrt(t)
      End Do
!
      irc = 0
      Go To 130
!
120   l(i0) = t
      irc = i
!
130   Return
!
!     ***  LAST CARD OF DL7SRT  ***
    End Subroutine dl7srt
    Function dl7svn(p,l,x,y)
!
!     ***  ESTIMATE SMALLEST SING. VALUE OF PACKED LOWER TRIANG. MATRIX L
!
!     ***  PARAMETER DECLARATIONS  ***
!
!     DIMENSION L(P*(P+1)/2)
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!     ***  PURPOSE  ***
!
!     THIS FUNCTION RETURNS A GOOD OVER-ESTIMATE OF THE SMALLEST
!     SINGULAR VALUE OF THE PACKED LOWER TRIANGULAR MATRIX L.
!
!     ***  PARAMETER DESCRIPTION  ***
!
!     P (IN)  = THE ORDER OF L.  L IS A  P X P  LOWER TRIANGULAR MATRIX.
!     L (IN)  = ARRAY HOLDING THE ELEMENTS OF  L  IN ROW ORDER, I.E.
!             L(1,1), L(2,1), L(2,2), L(3,1), L(3,2), L(3,3), ETC.
!     X (OUT) IF DL7SVN RETURNS A POSITIVE VALUE, THEN X IS A NORMALIZED
!             APPROXIMATE LEFT SINGULAR VECTOR CORRESPONDING TO THE
!             SMALLEST SINGULAR VALUE.  THIS APPROXIMATION MAY BE VERY
!             CRUDE.  IF DL7SVN RETURNS ZERO, THEN SOME COMPONENTS OF X
!             ARE ZERO AND THE REST RETAIN THEIR INPUT VALUES.
!     Y (OUT) IF DL7SVN RETURNS A POSITIVE VALUE, THEN Y = (L**-1)*X IS AN
!             UNNORMALIZED APPROXIMATE RIGHT SINGULAR VECTOR CORRESPOND-
!             ING TO THE SMALLEST SINGULAR VALUE.  THIS APPROXIMATION
!             MAY BE CRUDE.  IF DL7SVN RETURNS ZERO, THEN Y RETAINS ITS
!             INPUT VALUE.  THE CALLER MAY PASS THE SAME VECTOR FOR X
!             AND Y (NONSTANDARD FORTRAN USAGE), IN WHICH CASE Y OVER-
!             WRITES X (FOR NONZERO DL7SVN RETURNS).
!
!     ***  ALGORITHM NOTES  ***
!
!     THE ALGORITHM IS BASED ON (1), WITH THE ADDITIONAL PROVISION THAT
!     DL7SVN = 0 IS RETURNED IF THE SMALLEST DIAGONAL ELEMENT OF L
!     (IN MAGNITUDE) IS NOT MORE THAN THE UNIT ROUNDOFF TIMES THE
!     LARGEST.  THE ALGORITHM USES A RANDOM NUMBER GENERATOR PROPOSED
!     IN (4), WHICH PASSES THE SPECTRAL TEST WITH FLYING COLORS -- SEE
!     (2) AND (3).
!
!     ***  SUBROUTINES AND FUNCTIONS CALLED  ***
!
!        DV2NRM - FUNCTION, RETURNS THE 2-NORM OF A VECTOR.
!
!     ***  REFERENCES  ***
!
!     (1) CLINE, A., MOLER, C., STEWART, G., AND WILKINSON, J.H.(1977),
!         AN ESTIMATE FOR THE CONDITION NUMBER OF A MATRIX, REPORT
!         TM-310, APPLIED MATH. DIV., ARGONNE NATIONAL LABORATORY.
!
!     (2) HOAGLIN, D.C. (1976), THEORETICAL PROPERTIES OF CONGRUENTIAL
!         RANDOM-NUMBER GENERATORS --  AN EMPIRICAL VIEW,
!         MEMORANDUM NS-340, DEPT. OF STATISTICS, HARVARD UNIV.
!
!     (3) KNUTH, D.E. (1969), THE ART OF COMPUTER PROGRAMMING, VOL. 2
!         (SEMINUMERICAL ALGORITHMS), ADDISON-WESLEY, READING, MASS.
!
!     (4) SMITH, C.S. (1971), MULTIPLICATIVE PSEUDO-RANDOM NUMBER
!         GENERATORS WITH PRIME MODULUS, J. ASSOC. COMPUT. MACH. 18,
!         PP. 586-593.
!
!     ***  HISTORY  ***
!
!     DESIGNED AND CODED BY DAVID M. GAY (WINTER 1977/SUMMER 1978).
!
!     ***  GENERAL  ***
!
!     THIS SUBROUTINE WAS WRITTEN IN CONNECTION WITH RESEARCH
!     SUPPORTED BY THE NATIONAL SCIENCE FOUNDATION UNDER GRANTS
!     MCS-7600324, DCR75-10143, 76-14311DSS, AND MCS76-11989.
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  CONSTANTS  ***
!
!
!     ***  EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Function Return Value ..
      Real (Kind=wp)                   :: dl7svn
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: r9973 = 9973.E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
!     .. Scalar Arguments ..
      Integer                          :: p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: l(*), x(p), y(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: b, sminus, splus, t, xminus, xplus
      Integer                          :: i, ii, ix, j, j0, ji, jj, jjj, jm1,  &
                                          pm1
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, dv2nrm
      External                         :: dv2axy
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, mod, real
!     .. Executable Statements ..
!
!     ***  BODY  ***
!
      ix = 2
      pm1 = p - 1
!
!     ***  FIRST CHECK WHETHER TO RETURN DL7SVN = 0 AND INITIALIZE X  ***
!
      ii = 0
      j0 = p*pm1/2
      jj = j0 + p
      If (l(jj)==zero) Go To 120
      ix = mod(3432*ix,9973)
      b = half*(one+real(ix,kind=wp)/r9973)
      xplus = b/l(jj)
      x(p) = xplus
      If (p<=1) Go To 110
      Do i = 1, pm1
        ii = ii + i
        If (l(ii)==zero) Go To 120
        ji = j0 + i
        x(i) = xplus*l(ji)
      End Do
!
!     ***  SOLVE (L**T)*X = B, WHERE THE COMPONENTS OF B HAVE RANDOMLY
!     ***  CHOSEN MAGNITUDES IN (.5,1) WITH SIGNS CHOSEN TO MAKE X LARGE.
!
!     DO J = P-1 TO 1 BY -1...
      Do jjj = 1, pm1
        j = p - jjj
!       ***  DETERMINE X(J) IN THIS ITERATION. NOTE FOR I = 1,2,...,J
!       ***  THAT X(I) HOLDS THE CURRENT PARTIAL SUM FOR ROW I.
        ix = mod(3432*ix,9973)
        b = half*(one+real(ix,kind=wp)/r9973)
        xplus = (b-x(j))
        xminus = (-b-x(j))
        splus = abs(xplus)
        sminus = abs(xminus)
        jm1 = j - 1
        j0 = j*jm1/2
        jj = j0 + j
        xplus = xplus/l(jj)
        xminus = xminus/l(jj)
        If (jm1==0) Go To 100
        Do i = 1, jm1
          ji = j0 + i
          splus = splus + abs(x(i)+l(ji)*xplus)
          sminus = sminus + abs(x(i)+l(ji)*xminus)
        End Do
100     If (sminus>splus) xplus = xminus
        x(j) = xplus
!       ***  UPDATE PARTIAL SUMS  ***
        If (jm1>0) Call dv2axy(jm1,x,xplus,l(j0+1),x)
      End Do
!
!     ***  NORMALIZE X  ***
!
110   t = one/dv2nrm(p,x)
      Do i = 1, p
        x(i) = t*x(i)
      End Do
!
!     ***  SOLVE L*Y = X AND RETURN DL7SVN = 1/TWONORM(Y)  ***
!
      Do j = 1, p
        jm1 = j - 1
        j0 = j*jm1/2
        jj = j0 + j
        t = zero
        If (jm1>0) t = dd7tpr(jm1,l(j0+1),y)
        y(j) = (x(j)-t)/l(jj)
      End Do
!
      dl7svn = one/dv2nrm(p,y)
      Go To 130
!
120   dl7svn = zero
130   Return
!     ***  LAST CARD OF DL7SVN FOLLOWS  ***
    End Function dl7svn
    Subroutine ds7lvm(p,y,s,x)
!
!     ***  SET  Y = S * X,  S = P X P SYMMETRIC MATRIX.  ***
!     ***  LOWER TRIANGLE OF  S  STORED ROWWISE.         ***
!
!     ***  PARAMETER DECLARATIONS  ***
!
!     DIMENSION S(P*(P+1)/2)
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  NO INTRINSIC FUNCTIONS  ***
!
!     ***  EXTERNAL FUNCTION  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: s(*), x(p), y(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: xi
      Integer                          :: i, im1, j, k
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr
!     .. Executable Statements ..
!
!-----------------------------------------------------------------------
!
      j = 1
      Do i = 1, p
        y(i) = dd7tpr(i,s(j),x)
        j = j + i
      End Do
!
      If (p<=1) Go To 100
      j = 1
      Do i = 2, p
        xi = x(i)
        im1 = i - 1
        j = j + 1
        Do k = 1, im1
          y(k) = y(k) + s(j)*xi
          j = j + 1
        End Do
      End Do
!
100   Return
!     ***  LAST CARD OF DS7LVM FOLLOWS  ***
    End Subroutine ds7lvm
    Function dh2rfg(a,b,x,y,z)
!
!     ***  DETERMINE X, Y, Z SO  I + (1,Z)**T * (X,Y)  IS A 2X2
!     ***  HOUSEHOLDER REFLECTION SENDING (A,B)**T INTO (C,0)**T,
!     ***  WHERE  C = -SIGN(A)*SQRT(A**2 + B**2)  IS THE VALUE DH2RFG
!     ***  RETURNS.
!
!
!     /+
!/

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Function Return Value ..
      Real (Kind=wp)                   :: dh2rfg
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: a, b, x, y, z
!     .. Local Scalars ..
      Real (Kind=wp)                   :: a1, b1, c, t
      Real (Kind=wp), Save             :: zero
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, sqrt
!     .. Data Statements ..
      Data zero/0.E+0_wp/
!     .. Executable Statements ..
!
!     ***  BODY  ***
!
      If (b/=zero) Go To 100
      x = zero
      y = zero
      z = zero
      dh2rfg = a
      Go To 110
100   t = abs(a) + abs(b)
      a1 = a/t
      b1 = b/t
      c = sqrt(a1**2+b1**2)
      If (a1>zero) c = -c
      a1 = a1 - c
      z = b1/a1
      x = a1/c
      y = b1/c
      dh2rfg = t*c
110   Return
!     ***  LAST LINE OF DH2RFG FOLLOWS  ***
    End Function dh2rfg
    Subroutine dl7nvr(n,lin,l)
!
!     ***  COMPUTE  LIN = L**-1,  BOTH  N X N  LOWER TRIANG. STORED   ***
!     ***  COMPACTLY BY ROWS.  LIN AND L MAY SHARE THE SAME STORAGE.  ***
!
!     ***  PARAMETERS  ***
!
!     DIMENSION L(N*(N+1)/2), LIN(N*(N+1)/2)
!
!     ***  LOCAL VARIABLES  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
!     .. Scalar Arguments ..
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: l(*), lin(*)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t
      Integer                          :: i, ii, im1, j0, j1, jj, k, k0, np1
!     .. Executable Statements ..
!
!     ***  BODY  ***
!
      np1 = n + 1
      j0 = n*np1/2
      Do ii = 1, n
        i = np1 - ii
        lin(j0) = one/l(j0)
        If (i<=1) Go To 100
        j1 = j0
        im1 = i - 1
        Do jj = 1, im1
          t = zero
          j0 = j1
          k0 = j1 - jj
          Do k = 1, jj
            t = t - l(k0)*lin(j0)
            j0 = j0 - 1
            k0 = k0 + k - i
          End Do
          lin(j0) = t/l(k0)
        End Do
        j0 = j0 - 1
      End Do
100   Return
!     ***  LAST CARD OF DL7NVR FOLLOWS  ***
    End Subroutine dl7nvr
    Subroutine dd7dog(dig,lv,n,nwtstp,step,v)
!
!     ***  COMPUTE DOUBLE DOGLEG STEP  ***
!
!     ***  PARAMETER DECLARATIONS  ***
!
!
!     ***  PURPOSE  ***
!
!        THIS SUBROUTINE COMPUTES A CANDIDATE STEP (FOR USE IN AN UNCON-
!     STRAINED MINIMIZATION CODE) BY THE DOUBLE DOGLEG ALGORITHM OF
!     DENNIS AND MEI (REF. 1), WHICH IS A VARIATION ON POWELL*S DOGLEG
!     SCHEME (REF. 2, P. 95).
!
!     --------------------------  PARAMETER USAGE  --------------------------
!
!     DIG (INPUT) DIAG(D)**-2 * G -- SEE ALGORITHM NOTES.
!      G (INPUT) THE CURRENT GRADIENT VECTOR.
!     LV (INPUT) LENGTH OF V.
!      N (INPUT) NUMBER OF COMPONENTS IN  DIG, G, NWTSTP,  AND  STEP.
!     NWTSTP (INPUT) NEGATIVE NEWTON STEP -- SEE ALGORITHM NOTES.
!     STEP (OUTPUT) THE COMPUTED STEP.
!      V (I/O) VALUES ARRAY, THE FOLLOWING COMPONENTS OF WHICH ARE
!             USED HERE...
!     V(BIAS)   (INPUT) BIAS FOR RELAXED NEWTON STEP, WHICH IS V(BIAS) OF
!             THE WAY FROM THE FULL NEWTON TO THE FULLY RELAXED NEWTON
!             STEP.  RECOMMENDED VALUE = 0.8 .
!     V(DGNORM) (INPUT) 2-NORM OF DIAG(D)**-1 * G -- SEE ALGORITHM NOTES.
!     V(DSTNRM) (OUTPUT) 2-NORM OF DIAG(D) * STEP, WHICH IS V(RADIUS)
!             UNLESS V(STPPAR) = 0 -- SEE ALGORITHM NOTES.
!     V(DST0) (INPUT) 2-NORM OF DIAG(D) * NWTSTP -- SEE ALGORITHM NOTES.
!     V(GRDFAC) (OUTPUT) THE COEFFICIENT OF  DIG  IN THE STEP RETURNED --
!             STEP(I) = V(GRDFAC)*DIG(I) + V(NWTFAC)*NWTSTP(I).
!     V(GTHG)   (INPUT) SQUARE-ROOT OF (DIG**T) * (HESSIAN) * DIG -- SEE
!             ALGORITHM NOTES.
!     V(GTSTEP) (OUTPUT) INNER PRODUCT BETWEEN G AND STEP.
!     V(NREDUC) (OUTPUT) FUNCTION REDUCTION PREDICTED FOR THE FULL NEWTON
!             STEP.
!     V(NWTFAC) (OUTPUT) THE COEFFICIENT OF  NWTSTP  IN THE STEP RETURNED --
!             SEE V(GRDFAC) ABOVE.
!     V(PREDUC) (OUTPUT) FUNCTION REDUCTION PREDICTED FOR THE STEP RETURNED.
!     V(RADIUS) (INPUT) THE TRUST REGION RADIUS.  D TIMES THE STEP RETURNED
!             HAS 2-NORM V(RADIUS) UNLESS V(STPPAR) = 0.
!     V(STPPAR) (OUTPUT) CODE TELLING HOW STEP WAS COMPUTED... 0 MEANS A
!             FULL NEWTON STEP.  BETWEEN 0 AND 1 MEANS V(STPPAR) OF THE
!             WAY FROM THE NEWTON TO THE RELAXED NEWTON STEP.  BETWEEN
!             1 AND 2 MEANS A TRUE DOUBLE DOGLEG STEP, V(STPPAR) - 1 OF
!             THE WAY FROM THE RELAXED NEWTON TO THE CAUCHY STEP.
!             GREATER THAN 2 MEANS 1 / (V(STPPAR) - 1) TIMES THE CAUCHY
!             STEP.
!
!     -------------------------------  NOTES  -------------------------------
!
!     ***  ALGORITHM NOTES  ***
!
!        LET  G  AND  H  BE THE CURRENT GRADIENT AND HESSIAN APPROXIMA-
!     TION RESPECTIVELY AND LET D BE THE CURRENT SCALE VECTOR.  THIS
!     ROUTINE ASSUMES DIG = DIAG(D)**-2 * G  AND  NWTSTP = H**-1 * G.
!     THE STEP COMPUTED IS THE SAME ONE WOULD GET BY REPLACING G AND H
!     BY  DIAG(D)**-1 * G  AND  DIAG(D)**-1 * H * DIAG(D)**-1,
!     COMPUTING STEP, AND TRANSLATING STEP BACK TO THE ORIGINAL
!     VARIABLES, I.E., PREMULTIPLYING IT BY DIAG(D)**-1.
!
!     ***  REFERENCES  ***
!
!     1.  DENNIS, J.E., AND MEI, H.H.W. (1979), TWO NEW UNCONSTRAINED OPTI-
!             MIZATION ALGORITHMS WHICH USE FUNCTION AND GRADIENT
!             VALUES, J. OPTIM. THEORY APPLIC. 28, PP. 453-482.
!     2. POWELL, M.J.D. (1970), A HYBRID METHOD FOR NON-LINEAR EQUATIONS,
!             IN NUMERICAL METHODS FOR NON-LINEAR EQUATIONS, EDITED BY
!             P. RABINOWITZ, GORDON AND BREACH, LONDON.
!
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY.
!     THIS SUBROUTINE WAS WRITTEN IN CONNECTION WITH RESEARCH SUPPORTED
!     BY THE NATIONAL SCIENCE FOUNDATION UNDER GRANTS MCS-7600324 AND
!     MCS-7906671.
!
!     ------------------------  EXTERNAL QUANTITIES  ------------------------
!
!     ***  INTRINSIC FUNCTIONS  ***
!     /+
!/
!     --------------------------  LOCAL VARIABLES  --------------------------
!
!
!     ***  V SUBSCRIPTS  ***
!
!
!     ***  DATA INITIALIZATIONS  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: two = 2.E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: bias = 43, dgnorm = 1, dst0 = 3,     &
                                          dstnrm = 2, grdfac = 45, gthg = 44,  &
                                          gtstep = 4, nreduc = 6, nwtfac = 46, &
                                          preduc = 7, radius = 8, stppar = 5
!     .. Scalar Arguments ..
      Integer                          :: lv, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: dig(n), nwtstp(n), step(n), v(lv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: cfact, cnorm, ctrnwt, femnsq,        &
                                          ghinvg, gnorm, nwtnrm, relax,        &
                                          rlambd, t, t1, t2
      Integer                          :: i
!     .. Intrinsic Procedures ..
      Intrinsic                        :: sqrt
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      nwtnrm = v(dst0)
      rlambd = one
      If (nwtnrm>zero) rlambd = v(radius)/nwtnrm
      gnorm = v(dgnorm)
      ghinvg = two*v(nreduc)
      v(grdfac) = zero
      v(nwtfac) = zero
      If (rlambd<one) Go To 100
!
!        ***  THE NEWTON STEP IS INSIDE THE TRUST REGION  ***
!
      v(stppar) = zero
      v(dstnrm) = nwtnrm
      v(gtstep) = -ghinvg
      v(preduc) = v(nreduc)
      v(nwtfac) = -one
      Do i = 1, n
        step(i) = -nwtstp(i)
      End Do
      Go To 130
!
100   v(dstnrm) = v(radius)
      cfact = (gnorm/v(gthg))**2
!     ***  CAUCHY STEP = -CFACT * G.
      cnorm = gnorm*cfact
      relax = one - v(bias)*(one-gnorm*cnorm/ghinvg)
      If (rlambd<relax) Go To 110
!
!        ***  STEP IS BETWEEN RELAXED NEWTON AND FULL NEWTON STEPS  ***
!
      v(stppar) = one - (rlambd-relax)/(one-relax)
      t = -rlambd
      v(gtstep) = t*ghinvg
      v(preduc) = rlambd*(one-half*rlambd)*ghinvg
      v(nwtfac) = t
      Do i = 1, n
        step(i) = t*nwtstp(i)
      End Do
      Go To 130
!
110   If (cnorm<v(radius)) Go To 120
!
!        ***  THE CAUCHY STEP LIES OUTSIDE THE TRUST REGION --
!        ***  STEP = SCALED CAUCHY STEP  ***
!
      t = -v(radius)/gnorm
      v(grdfac) = t
      v(stppar) = one + cnorm/v(radius)
      v(gtstep) = -v(radius)*gnorm
      v(preduc) = v(radius)*(gnorm-half*v(radius)*(v(gthg)/gnorm)**2)
      Do i = 1, n
        step(i) = t*dig(i)
      End Do
      Go To 130
!
!     ***  COMPUTE DOGLEG STEP BETWEEN CAUCHY AND RELAXED NEWTON  ***
!     ***  FEMUR = RELAXED NEWTON STEP MINUS CAUCHY STEP  ***
!
120   ctrnwt = cfact*relax*ghinvg/gnorm
!     *** CTRNWT = INNER PROD. OF CAUCHY AND RELAXED NEWTON STEPS,
!     *** SCALED BY GNORM**-1.
      t1 = ctrnwt - gnorm*cfact**2
!     ***  T1 = INNER PROD. OF FEMUR AND CAUCHY STEP, SCALED BY
!     ***  GNORM**-1.
      t2 = v(radius)*(v(radius)/gnorm) - gnorm*cfact**2
      t = relax*nwtnrm
      femnsq = (t/gnorm)*t - ctrnwt - t1
!     ***  FEMNSQ = SQUARE OF 2-NORM OF FEMUR, SCALED BY GNORM**-1.
      t = t2/(t1+sqrt(t1**2+femnsq*t2))
!     ***  DOGLEG STEP  =  CAUCHY STEP  +  T * FEMUR.
      t1 = (t-one)*cfact
      v(grdfac) = t1
      t2 = -t*relax
      v(nwtfac) = t2
      v(stppar) = two - t
      v(gtstep) = t1*gnorm**2 + t2*ghinvg
      v(preduc) = -t1*gnorm*((t2+one)*gnorm) - t2*(one+half*t2)*ghinvg -       &
        half*(v(gthg)*t1)**2
      Do i = 1, n
        step(i) = t1*dig(i) + t2*nwtstp(i)
      End Do
!
130   Return
!     ***  LAST LINE OF DD7DOG FOLLOWS  ***
    End Subroutine dd7dog
    Subroutine ds7ipr(p,ip,h)
!
!     APPLY THE PERMUTATION DEFINED BY IP TO THE ROWS AND COLUMNS OF THE
!     P X P SYMMETRIC MATRIX WHOSE LOWER TRIANGLE IS STORED COMPACTLY IN H.
!     THUS H.OUTPUT(I,J) = H.INPUT(IP(I), IP(J)).
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: h(*)
      Integer                          :: ip(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t
      Integer                          :: i, j, j1, jm, k, k1, kk, km, kmj, l, &
                                          m
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs
!     .. Executable Statements ..
!
!     ***  BODY  ***
!
      Do i = 1, p
        j = ip(i)
        If (j==i) Go To 150
        ip(i) = abs(j)
        If (j<0) Go To 150
        k = i
100     j1 = j
        k1 = k
        If (j<=k) Go To 110
        j1 = k
        k1 = j
110     kmj = k1 - j1
        l = j1 - 1
        jm = j1*l/2
        km = k1*(k1-1)/2
        If (l<=0) Go To 120
        Do m = 1, l
          jm = jm + 1
          t = h(jm)
          km = km + 1
          h(jm) = h(km)
          h(km) = t
        End Do
120     km = km + 1
        kk = km + kmj
        jm = jm + 1
        t = h(jm)
        h(jm) = h(kk)
        h(kk) = t
        j1 = l
        l = kmj - 1
        If (l<=0) Go To 130
        Do m = 1, l
          jm = jm + j1 + m
          t = h(jm)
          km = km + 1
          h(jm) = h(km)
          h(km) = t
        End Do
130     If (k1>=p) Go To 140
        l = p - k1
        k1 = k1 - 1
        km = kk
        Do m = 1, l
          km = km + k1 + m
          jm = km - kmj
          t = h(jm)
          h(jm) = h(km)
          h(km) = t
        End Do
140     k = j
        j = ip(k)
        ip(k) = -j
        If (j>i) Go To 100
150   End Do
      Return
!     ***  LAST LINE OF DS7IPR FOLLOWS  ***
    End Subroutine ds7ipr
    Subroutine dh2rfa(n,a,b,x,y,z)
!
!     ***  APPLY 2X2 HOUSEHOLDER REFLECTION DETERMINED BY X, Y, Z TO
!     ***  N-VECTORS A, B  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: x, y, z
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(n), b(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t
      Integer                          :: i
!     .. Executable Statements ..
      Do i = 1, n
        t = a(i)*x + b(i)*y
        a(i) = a(i) + t
        b(i) = b(i) + t*z
      End Do
      Return
!     ***  LAST LINE OF DH2RFA FOLLOWS  ***
    End Subroutine dh2rfa
    Subroutine dparck(alg,d,iv,liv,lv,n,v)
!
!     ***  CHECK ***SOL (VERSION 2.3) PARAMETERS, PRINT CHANGED VALUES  ***
!
!     ***  ALG = 1 FOR REGRESSION, ALG = 2 FOR GENERAL UNCONSTRAINED OPT.
!
!
!     DIVSET  -- SUPPLIES DEFAULT VALUES TO BOTH IV AND V.
!     DR7MDC -- RETURNS MACHINE-DEPENDENT CONSTANTS.
!     DV7CPY  -- COPIES ONE VECTOR TO ANOTHER.
!     DV7DFL  -- SUPPLIES DEFAULT PARAMETER VALUES TO V ALONE.
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  IV AND V SUBSCRIPTS  ***
!
!
!
!
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Integer, Parameter               :: algsav = 51, dinit = 38, dtype = 16, &
                                          dtype0 = 54, epslon = 19,            &
                                          inits = 25, ivneed = 3, lastiv = 44, &
                                          lastv = 45, lmat = 42, nextiv = 46,  &
                                          nextv = 47, nvdflt = 50, oldn = 38,  &
                                          parprt = 20, parsav = 49, perm = 58, &
                                          prunit = 21, vneed = 4
!     .. Scalar Arguments ..
      Integer                          :: alg, liv, lv, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(n), v(lv)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp), Save             :: big, machep, tiny, zero
      Real (Kind=wp)                   :: vk
      Integer                          :: alg1, i, ii, iv1, j, k, l, m, miv1,  &
                                          miv2, ndfalt, parsv1, pu
      Integer, Save                    :: ijmp
!     .. Local Arrays ..
      Real (Kind=wp), Save             :: vm(34), vx(34)
      Integer, Save                    :: jlim(4), miniv(4), ndflt(4)
      Character (4), Save              :: cngd(3), dflt(3)
      Character (4)                    :: which(3)
!     .. External Procedures ..
      Real (Kind=wp), External         :: dr7mdc
      External                         :: divset, dv7cpy, dv7dfl
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max, mod
!     .. Data Statements ..
      Data big/0.E+0_wp/, machep/ -1.E+0_wp/, tiny/1.E+0_wp/, zero/0.E+0_wp/
      Data vm(1)/1.0E-3_wp/, vm(2)/ -0.99E+0_wp/, vm(3)/1.0E-3_wp/,            &
        vm(4)/1.0E-2_wp/, vm(5)/1.2E+0_wp/, vm(6)/1.E-2_wp/, vm(7)/1.2E+0_wp/, &
        vm(8)/0.E+0_wp/, vm(9)/0.E+0_wp/, vm(10)/1.E-3_wp/,                    &
        vm(11)/ -1.E+0_wp/, vm(13)/0.E+0_wp/, vm(15)/0.E+0_wp/,                &
        vm(16)/0.E+0_wp/, vm(19)/0.E+0_wp/, vm(20)/ -10.E+0_wp/,               &
        vm(21)/0.E+0_wp/, vm(22)/0.E+0_wp/, vm(23)/0.E+0_wp/,                  &
        vm(27)/1.01E+0_wp/, vm(28)/1.E+10_wp/, vm(30)/0.E+0_wp/,               &
        vm(31)/0.E+0_wp/, vm(32)/0.E+0_wp/, vm(34)/0.E+0_wp/
      Data vx(1)/0.9E+0_wp/, vx(2)/ -1.E-3_wp/, vx(3)/1.E+1_wp/,               &
        vx(4)/0.8E+0_wp/, vx(5)/1.E+2_wp/, vx(6)/0.8E+0_wp/, vx(7)/1.E+2_wp/,  &
        vx(8)/0.5E+0_wp/, vx(9)/0.5E+0_wp/, vx(10)/1.E+0_wp/,                  &
        vx(11)/1.E+0_wp/, vx(14)/0.1E+0_wp/, vx(15)/1.E+0_wp/,                 &
        vx(16)/1.E+0_wp/, vx(19)/1.E+0_wp/, vx(23)/1.E+0_wp/,                  &
        vx(24)/1.E+0_wp/, vx(25)/1.E+0_wp/, vx(26)/1.E+0_wp/,                  &
        vx(27)/1.E+10_wp/, vx(29)/1.E+0_wp/, vx(31)/1.E+0_wp/,                 &
        vx(32)/1.E+0_wp/, vx(33)/1.E+0_wp/, vx(34)/1.E+0_wp/
      Data cngd(1), cngd(2), cngd(3)/'---C', 'HANG', 'ED V'/, dflt(1),         &
        dflt(2), dflt(3)/'NOND', 'EFAU', 'LT V'/
      Data ijmp/33/, jlim(1)/0/, jlim(2)/24/, jlim(3)/0/, jlim(4)/24/,         &
        ndflt(1)/32/, ndflt(2)/25/, ndflt(3)/32/, ndflt(4)/25/
      Data miniv(1)/82/, miniv(2)/59/, miniv(3)/103/, miniv(4)/103/
!     .. Executable Statements ..
!
!     ...............................  BODY  ................................
!
      pu = 0
      If (prunit<=liv) pu = iv(prunit)
      If (algsav>liv) Go To 100
      If (alg==iv(algsav)) Go To 100
!         IF (PU .NE. 0) WRITE(PU,10) ALG, IV(ALGSAV)
!     10      FORMAT(/40H THE FIRST PARAMETER TO DIVSET SHOULD BE,I3,
!     1          12H RATHER THAN,I3)
      iv(1) = 67
      Go To 300
100   If (alg<1 .Or. alg>4) Go To 280
      miv1 = miniv(alg)
      If (iv(1)==15) Go To 290
      alg1 = mod(alg-1,2) + 1
      If (iv(1)==0) Call divset(alg,iv,liv,lv,v)
      iv1 = iv(1)
      If (iv1/=13 .And. iv1/=12) Go To 110
      If (perm<=liv) miv1 = max(miv1,iv(perm)-1)
      If (ivneed<=liv) miv2 = miv1 + max(iv(ivneed),0)
      If (lastiv<=liv) iv(lastiv) = miv2
      If (liv<miv1) Go To 260
      iv(ivneed) = 0
      iv(lastv) = max(iv(vneed),0) + iv(lmat) - 1
      iv(vneed) = 0
      If (liv<miv2) Go To 260
      If (lv<iv(lastv)) Go To 270
110   If (iv1<12 .Or. iv1>14) Go To 130
      If (n>=1) Go To 120
      iv(1) = 81
      If (pu==0) Go To 300
!              WRITE(PU,40) VARNM(ALG1), N
!     40           FORMAT(/8H /// BAD,A1,2H =,I5)
      Go To 300
120   If (iv1/=14) iv(nextiv) = iv(perm)
      If (iv1/=14) iv(nextv) = iv(lmat)
      If (iv1==13) Go To 300
      k = iv(parsav) - epslon
      Call dv7dfl(alg1,lv-k,v(k+1))
      iv(dtype0) = 2 - alg1
      iv(oldn) = n
      which(1) = dflt(1)
      which(2) = dflt(2)
      which(3) = dflt(3)
      Go To 160
130   If (n==iv(oldn)) Go To 140
      iv(1) = 17
      If (pu==0) Go To 300
!         WRITE(PU,70) VARNM(ALG1), IV(OLDN), N
!     70      FORMAT(/5H /// ,1A1,14H CHANGED FROM ,I5,4H TO ,I5)
      Go To 300
!
140   If (iv1<=11 .And. iv1>=1) Go To 150
      iv(1) = 80
!         IF (PU .NE. 0) WRITE(PU,90) IV1
!     90      FORMAT(/13H ///  IV(1) =,I5,28H SHOULD BE BETWEEN 0 AND 14.)
      Go To 300
!
150   which(1) = cngd(1)
      which(2) = cngd(2)
      which(3) = cngd(3)
!
160   If (iv1==14) iv1 = 12
      If (big>tiny) Go To 170
      tiny = dr7mdc(1)
      machep = dr7mdc(3)
      big = dr7mdc(6)
      vm(12) = machep
      vx(12) = big
      vx(13) = big
      vm(14) = machep
      vm(17) = tiny
      vx(17) = big
      vm(18) = tiny
      vx(18) = big
      vx(20) = big
      vx(21) = big
      vx(22) = big
      vm(24) = machep
      vm(25) = machep
      vm(26) = machep
      vx(28) = dr7mdc(5)
      vm(29) = machep
      vx(30) = big
      vm(33) = machep
170   m = 0
      i = 1
      j = jlim(alg1)
      k = epslon
      ndfalt = ndflt(alg1)
      Do l = 1, ndfalt
        vk = v(k)
        If (vk>=vm(i) .And. vk<=vx(i)) Go To 180
        m = k
!              IF (PU .NE. 0) WRITE(PU,130) VN(1,I), VN(2,I), K, VK,
!       1                                    VM(I), VX(I)
!       130          FORMAT(/6H ///  ,2A4,5H.. V(,I2,3H) =,D11.3,7H SHOULD,
!       1               11H BE BETWEEN,D11.3,4H AND,D11.3)
180     k = k + 1
        i = i + 1
        If (i==j) i = ijmp
      End Do
!
      If (iv(nvdflt)==ndfalt) Go To 190
      iv(1) = 51
      If (pu==0) Go To 300
!         WRITE(PU,160) IV(NVDFLT), NDFALT
!     160     FORMAT(/13H IV(NVDFLT) =,I5,13H RATHER THAN ,I5)
      Go To 300
190   If ((iv(dtype)>0 .Or. v(dinit)>zero) .And. iv1==12) Go To 210
      Do i = 1, n
        If (d(i)>zero) Go To 200
        m = 18
!              IF (PU .NE. 0) WRITE(PU,180) I, D(I)
!       180     FORMAT(/8H ///  D(,I3,3H) =,D11.3,19H SHOULD BE POSITIVE)
200   End Do
210   If (m==0) Go To 220
      iv(1) = m
      Go To 300
!
220   If (pu==0 .Or. iv(parprt)==0) Go To 300
      If (iv1/=12 .Or. iv(inits)==alg1-1) Go To 230
      m = 1
!         WRITE(PU,220) SH(ALG1), IV(INITS)
!     220     FORMAT(/22H NONDEFAULT VALUES..../5H INIT,A1,14H..... IV(25) =,
!     1          I3)
230   If (iv(dtype)==iv(dtype0)) Go To 240
!         IF (M .EQ. 0) WRITE(PU,260) WHICH
      m = 1
!         WRITE(PU,240) IV(DTYPE)
!     240     FORMAT(20H DTYPE..... IV(16) =,I3)
240   i = 1
      j = jlim(alg1)
      k = epslon
      l = iv(parsav)
      ndfalt = ndflt(alg1)
      Do ii = 1, ndfalt
        If (v(k)==v(l)) Go To 250
!              IF (M .EQ. 0) WRITE(PU,260) WHICH
!       260          FORMAT(/1H ,3A4,9HALUES..../)
        m = 1
!              WRITE(PU,270) VN(1,I), VN(2,I), K, V(K)
!       270          FORMAT(1X,2A4,5H.. V(,I2,3H) =,D15.7)
250     k = k + 1
        l = l + 1
        i = i + 1
        If (i==j) i = ijmp
      End Do
!
      iv(dtype0) = iv(dtype)
      parsv1 = iv(parsav)
      Call dv7cpy(iv(nvdflt),v(parsv1),v(epslon))
      Go To 300
!
260   iv(1) = 15
      If (pu==0) Go To 300
!      WRITE(PU,310) LIV, MIV2
!     310  FORMAT(/10H /// LIV =,I5,17H MUST BE AT LEAST,I5)
      If (liv<miv1) Go To 300
      If (lv<iv(lastv)) Go To 270
      Go To 300
!
270   iv(1) = 16
!      IF (PU .NE. 0) WRITE(PU,330) LV, IV(LASTV)
!     330  FORMAT(/9H /// LV =,I5,17H MUST BE AT LEAST,I5)
      Go To 300
!
280   iv(1) = 67
!      IF (PU .NE. 0) WRITE(PU,350) ALG
!     350  FORMAT(/10H /// ALG =,I5,21H MUST BE 1 2, 3, OR 4)
      Go To 300
290   Continue
!     360  IF (PU .NE. 0) WRITE(PU,370) LIV, MIV1
!     370  FORMAT(/10H /// LIV =,I5,17H MUST BE AT LEAST,I5,
!     1       37H TO COMPUTE TRUE MIN. LIV AND MIN. LV)
      If (lastiv<=liv) iv(lastiv) = miv1
      If (lastv<=liv) iv(lastv) = 0
!
300   Return
!     ***  LAST LINE OF DPARCK FOLLOWS  ***
    End Subroutine dparck

    Subroutine dq7apl(nn,n,p,j,r,ierr)
!     *****PARAMETERS.
!
!     ..................................................................
!
!     *****PURPOSE.
!     THIS SUBROUTINE APPLIES TO R THE ORTHOGONAL TRANSFORMATIONS
!     STORED IN J BY QRFACT
!
!     *****PARAMETER DESCRIPTION.
!     ON INPUT.
!
!        NN IS THE ROW DIMENSION OF THE MATRIX J AS DECLARED IN
!             THE CALLING PROGRAM DIMENSION STATEMENT
!
!        N IS THE NUMBER OF ROWS OF J AND THE SIZE OF THE VECTOR R
!
!        P IS THE NUMBER OF COLUMNS OF J AND THE SIZE OF SIGMA
!
!        J CONTAINS ON AND BELOW ITS DIAGONAL THE COLUMN VECTORS
!             U WHICH DETERMINE THE HOUSEHOLDER TRANSFORMATIONS
!             IDENT - U*U.TRANSPOSE
!
!        R IS THE RIGHT HAND SIDE VECTOR TO WHICH THE ORTHOGONAL
!             TRANSFORMATIONS WILL BE APPLIED
!
!        IERR IF NON-ZERO INDICATES THAT NOT ALL THE TRANSFORMATIONS
!             WERE SUCCESSFULLY DETERMINED AND ONLY THE FIRST
!             ABS(IERR) - 1 TRANSFORMATIONS WILL BE USED
!
!     ON OUTPUT.
!
!        R HAS BEEN OVERWRITTEN BY ITS TRANSFORMED IMAGE
!
!     *****APPLICATION AND USAGE RESTRICTIONS.
!     NONE
!
!     *****ALGORITHM NOTES.
!     THE VECTORS U WHICH DETERMINE THE HOUSEHOLDER TRANSFORMATIONS
!     ARE NORMALIZED SO THAT THEIR 2-NORM SQUARED IS 2.  THE USE OF
!     THESE TRANSFORMATIONS HERE IS IN THE SPIRIT OF (1).
!
!     *****SUBROUTINES AND FUNCTIONS CALLED.
!
!     DD7TPR - FUNCTION, RETURNS THE INNER PRODUCT OF VECTORS
!
!     *****REFERENCES.
!     (1) BUSINGER, P. A., AND GOLUB, G. H. (1965), LINEAR LEAST SQUARES
!        SOLUTIONS BY HOUSEHOLDER TRANSFORMATIONS, NUMER. MATH. 7,
!        PP. 269-276.
!
!     *****HISTORY.
!     DESIGNED BY DAVID M. GAY, CODED BY STEPHEN C. PETERS (WINTER 1977)
!     CALL ON DV2AXY SUBSTITUTED FOR DO LOOP, FALL 1983.
!
!     *****GENERAL.
!
!     THIS SUBROUTINE WAS WRITTEN IN CONNECTION WITH RESEARCH
!     SUPPORTED BY THE NATIONAL SCIENCE FOUNDATION UNDER GRANTS
!     MCS-7600324, DCR75-10143, 76-14311DSS, AND MCS76-11989.
!
!     ..................................................................
!
!     *****LOCAL VARIABLES.
!     *****FUNCTIONS.

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: ierr, n, nn, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: j(nn,p), r(n)
!     .. Local Scalars ..
      Integer                          :: k, l, nl1
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr
      External                         :: dv2axy
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs
!     .. Executable Statements ..
!
!     ***  BODY  ***
!
      k = p
      If (ierr/=0) k = abs(ierr) - 1
      If (k==0) Go To 100
!
      Do l = 1, k
        nl1 = n - l + 1
        Call dv2axy(nl1,r(l),-dd7tpr(nl1,j(l,l),r(l)),j(l,l),r(l))
      End Do
!
100   Return
!     ***  LAST LINE OF DQ7APL FOLLOWS  ***
    End Subroutine dq7apl

    Subroutine dv7dfl(alg,lv,v)
!
!     ***  SUPPLY ***SOL (VERSION 2.3) DEFAULT VALUES TO V  ***
!
!     ***  ALG = 1 MEANS REGRESSION CONSTANTS.
!     ***  ALG = 2 MEANS GENERAL UNCONSTRAINED OPTIMIZATION CONSTANTS.
!
!
!     DR7MDC... RETURNS MACHINE-DEPENDENT CONSTANTS
!
!
!     ***  SUBSCRIPTS FOR V  ***
!
!
!
!     ***  V SUBSCRIPT VALUES  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: three = 3.E+0_wp
      Integer, Parameter               :: afctol = 31, bias = 43, cosmin = 47, &
                                          d0init = 40, decfac = 22,            &
                                          delta0 = 44, dfac = 41, dinit = 38,  &
                                          dltfdc = 42, dltfdj = 43,            &
                                          dtinit = 39, epslon = 19, eta0 = 42, &
                                          fuzz = 45, incfac = 23, lmax0 = 35,  &
                                          lmaxs = 36, phmnfc = 20,             &
                                          phmxfc = 21, rdfcmn = 24,            &
                                          rdfcmx = 25, rfctol = 32,            &
                                          rlimit = 46, rsptol = 49,            &
                                          sctol = 37, sigmin = 50,             &
                                          tuner1 = 26, tuner2 = 27,            &
                                          tuner3 = 28, tuner4 = 29,            &
                                          tuner5 = 30, xctol = 33, xftol = 34
!     .. Scalar Arguments ..
      Integer                          :: alg, lv
!     .. Array Arguments ..
      Real (Kind=wp)                   :: v(lv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: machep, mepcrt, sqteps
!     .. External Procedures ..
      Real (Kind=wp), External         :: dr7mdc
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max
!     .. Executable Statements ..
!
!     -------------------------------  BODY  --------------------------------
!
      machep = dr7mdc(3)
      If (machep>1.E-10_wp) Then
        v(afctol) = machep**2
      Else
        v(afctol) = 1.E-20_wp
      End If

      v(decfac) = 0.5E+0_wp
      sqteps = dr7mdc(4)
      v(dfac) = 0.6E+0_wp
      v(dtinit) = 1.E-6_wp
      mepcrt = machep**(one/three)
      v(d0init) = 1.E+0_wp
      v(epslon) = 0.1E+0_wp
      v(incfac) = 2.E+0_wp
      v(lmax0) = 1.E+0_wp
      v(lmaxs) = 1.E+0_wp
      v(phmnfc) = -0.1E+0_wp
      v(phmxfc) = 0.1E+0_wp
      v(rdfcmn) = 0.1E+0_wp
      v(rdfcmx) = 4.E+0_wp
      v(rfctol) = max(1.E-10_wp,mepcrt**2)
      v(sctol) = v(rfctol)
      v(tuner1) = 0.1E+0_wp
      v(tuner2) = 1.E-4_wp
      v(tuner3) = 0.75E+0_wp
      v(tuner4) = 0.5E+0_wp
      v(tuner5) = 0.75E+0_wp
      v(xctol) = sqteps
      v(xftol) = 1.E+2_wp*machep
!
      If (alg==1) Then
!
!       ***  REGRESSION  VALUES (nls)
!
        v(cosmin) = max(1.E-6_wp,1.E+2_wp*machep)
        v(dinit) = 0.E+0_wp
        v(delta0) = sqteps
        v(dltfdc) = mepcrt
        v(dltfdj) = sqteps
        v(fuzz) = 1.5E+0_wp
        v(rlimit) = dr7mdc(5)
        v(rsptol) = 1.E-3_wp
        v(sigmin) = 1.E-4_wp
      Else
!
!       ***  GENERAL OPTIMIZATION VALUES (nlminb)
!
        v(bias) = 0.8E+0_wp
        v(dinit) = -1.0E+0_wp
        v(eta0) = 1.0E+3_wp*machep
!
      End If
!     ***  LAST CARD OF DV7DFL FOLLOWS  ***
    End Subroutine dv7dfl

    Function dr7mdc(k)
!
!     ***  RETURN MACHINE DEPENDENT CONSTANTS USED BY NL2SOL  ***
!
!
!     ***  THE CONSTANT RETURNED DEPENDS ON K...
!
!     ***        K = 1... SMALLEST POS. ETA SUCH THAT -ETA EXISTS.
!     ***        K = 2... SQUARE ROOT OF ETA.
!     ***        K = 3... UNIT ROUNDOFF = SMALLEST POS. NO. MACHEP SUCH
!     ***                 THAT 1 + MACHEP .GT. 1 .AND. 1 - MACHEP .LT. 1.
!     ***        K = 4... SQUARE ROOT OF MACHEP.
!     ***        K = 5... SQUARE ROOT OF BIG (SEE K = 6).
!     ***        K = 6... LARGEST MACHINE NO. BIG SUCH THAT -BIG EXISTS.
!
!     /+
!/
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Function Return Value ..
      Real (Kind=wp)                   :: dr7mdc
!     .. Scalar Arguments ..
      Integer                          :: k
!     .. Local Scalars ..
      Real (Kind=wp), Save             :: big, eta, machep, zero
!     .. External Procedures ..
      Real (Kind=wp), External         :: d1mach
!     .. Intrinsic Procedures ..
      Intrinsic                        :: sqrt
!     .. Data Statements ..
      Data big/0.E+0_wp/, eta/0.E+0_wp/, machep/0.E+0_wp/, zero/0.E+0_wp/
!     .. Executable Statements ..
      If (big>zero) Go To 100
      big = d1mach(2)
      eta = d1mach(1)
      machep = d1mach(4)
100   Continue
!
!     -------------------------------  BODY  --------------------------------
!
      Go To (110,120,130,140,150,160) k
!
110   dr7mdc = eta
      Go To 170
!
120   dr7mdc = sqrt(256.E+0_wp*eta)/16.E+0_wp
      Go To 170
!
130   dr7mdc = machep
      Go To 170
!
140   dr7mdc = sqrt(machep)
      Go To 170
!
150   dr7mdc = sqrt(big/256.E+0_wp)*16.E+0_wp
      Go To 170
!
160   dr7mdc = big
!
170   Return
!     ***  LAST CARD OF DR7MDC FOLLOWS  ***
    End Function dr7mdc
    Subroutine dg7itb(b,d,g,iv,liv,lv,p,ps,v,x,y)
!
!     ***  CARRY OUT NL2SOL-LIKE ITERATIONS FOR GENERALIZED LINEAR   ***
!     ***  REGRESSION PROBLEMS (AND OTHERS OF SIMILAR STRUCTURE)     ***
!     ***  HAVING SIMPLE BOUNDS ON THE PARAMETERS BEING ESTIMATED.   ***
!
!     ***  PARAMETER DECLARATIONS  ***
!
!
!     --------------------------  PARAMETER USAGE  --------------------------
!
!     B.... VECTOR OF LOWER AND UPPER BOUNDS ON X.
!     D.... SCALE VECTOR.
!     IV... INTEGER VALUE ARRAY.
!     LIV.. LENGTH OF IV.  MUST BE AT LEAST 80.
!     LH... LENGTH OF H = P*(P+1)/2.
!     LV... LENGTH OF V.  MUST BE AT LEAST P*(3*P + 19)/2 + 7.
!     G.... GRADIENT AT X (WHEN IV(1) = 2).
!     HC... GAUSS-NEWTON HESSIAN AT X (WHEN IV(1) = 2).
!     P.... NUMBER OF PARAMETERS (COMPONENTS IN X).
!     PS... NUMBER OF NONZERO ROWS AND COLUMNS IN S.
!     V.... FLOATING-POINT VALUE ARRAY.
!     X.... PARAMETER VECTOR.
!     Y.... PART OF YIELD VECTOR (WHEN IV(1)= 2, SCRATCH OTHERWISE).
!
!     ***  DISCUSSION  ***
!
!        DG7ITB IS SIMILAR TO DG7LIT, EXCEPT FOR THE EXTRA PARAMETER B
!     -- DG7ITB ENFORCES THE BOUNDS  B(1,I) .LE. X(I) .LE. B(2,I),
!     I = 1(1)P.
!        DG7ITB PERFORMS NL2SOL-LIKE ITERATIONS FOR A VARIETY OF
!     REGRESSION PROBLEMS THAT ARE SIMILAR TO NONLINEAR LEAST-SQUARES
!     IN THAT THE HESSIAN IS THE SUM OF TWO TERMS, A READILY-COMPUTED
!     FIRST-ORDER TERM AND A SECOND-ORDER TERM.  THE CALLER SUPPLIES
!     THE FIRST-ORDER TERM OF THE HESSIAN IN HC (LOWER TRIANGLE, STORED
!     COMPACTLY BY ROWS), AND DG7ITB BUILDS AN APPROXIMATION, S, TO THE
!     SECOND-ORDER TERM.  THE CALLER ALSO PROVIDES THE FUNCTION VALUE,
!     GRADIENT, AND PART OF THE YIELD VECTOR USED IN UPDATING S.
!     DG7ITB DECIDES DYNAMICALLY WHETHER OR NOT TO USE S WHEN CHOOSING
!     THE NEXT STEP TO TRY...  THE HESSIAN APPROXIMATION USED IS EITHER
!     HC ALONE (GAUSS-NEWTON MODEL) OR HC + S (AUGMENTED MODEL).
!     IF PS .LT. P, THEN ROWS AND COLUMNS PS+1...P OF S ARE KEPT
!     CONSTANT.  THEY WILL BE ZERO UNLESS THE CALLER SETS IV(INITS) TO
!     1 OR 2 AND SUPPLIES NONZERO VALUES FOR THEM, OR THE CALLER SETS
!     IV(INITS) TO 3 OR 4 AND THE FINITE-DIFFERENCE INITIAL S THEN
!     COMPUTED HAS NONZERO VALUES IN THESE ROWS.
!
!        IF IV(INITS) IS 3 OR 4, THEN THE INITIAL S IS COMPUTED BY
!     FINITE DIFFERENCES.  3 MEANS USE FUNCTION DIFFERENCES, 4 MEANS
!     USE GRADIENT DIFFERENCES.  FINITE DIFFERENCING IS DONE THE SAME
!     WAY AS IN COMPUTING A COVARIANCE MATRIX (WITH IV(COVREQ) = -1, -2,
!     1, OR 2).
!
!        FOR UPDATING S, DG7ITB ASSUMES THAT THE GRADIENT HAS THE FORM
!     OF A SUM OVER I OF RHO(I,X)*GRAD(R(I,X)), WHERE GRAD DENOTES THE
!     GRADIENT WITH RESPECT TO X.  THE TRUE SECOND-ORDER TERM THEN IS
!     THE SUM OVER I OF RHO(I,X)*HESSIAN(R(I,X)).  IF X = X0 + STEP,
!     THEN WE WISH TO UPDATE S SO THAT S*STEP IS THE SUM OVER I OF
!     RHO(I,X)*(GRAD(R(I,X)) - GRAD(R(I,X0))).  THE CALLER MUST SUPPLY
!     PART OF THIS IN Y, NAMELY THE SUM OVER I OF
!     RHO(I,X)*GRAD(R(I,X0)), WHEN CALLING DG7ITB WITH IV(1) = 2 AND
!     IV(MODE) = 0 (WHERE MODE = 38).  G THEN CONTANS THE OTHER PART,
!     SO THAT THE DESIRED YIELD VECTOR IS G - Y.  IF PS .LT. P, THEN
!     THE ABOVE DISCUSSION APPLIES ONLY TO THE FIRST PS COMPONENTS OF
!     GRAD(R(I,X)), STEP, AND Y.
!
!        PARAMETERS IV, P, V, AND X ARE THE SAME AS THE CORRESPONDING
!     ONES TO  DN2GB (AND NL2SOL), EXCEPT THAT V CAN BE SHORTER
!     (SINCE THE PART OF V THAT  DN2GB USES FOR STORING D, J, AND R IS
!     NOT NEEDED).  MOREOVER, COMPARED WITH  DN2GB (AND NL2SOL), IV(1)
!     MAY HAVE THE TWO ADDITIONAL OUTPUT VALUES 1 AND 2, WHICH ARE
!     EXPLAINED BELOW, AS IS THE USE OF IV(TOOBIG) AND IV(NFGCAL).
!     THE VALUES IV(D), IV(J), AND IV(R), WHICH ARE OUTPUT VALUES FROM
!      DN2GB (AND  DN2FB), ARE NOT REFERENCED BY DG7ITB OR THE
!     SUBROUTINES IT CALLS.
!
!        WHEN DG7ITB IS FIRST CALLED, I.E., WHEN DG7ITB IS CALLED WITH
!     IV(1) = 0 OR 12, V(F), G, AND HC NEED NOT BE INITIALIZED.  TO
!     OBTAIN THESE STARTING VALUES, DG7ITB RETURNS FIRST WITH IV(1) = 1,
!     THEN WITH IV(1) = 2, WITH IV(MODE) = -1 IN BOTH CASES.  ON
!     SUBSEQUENT RETURNS WITH IV(1) = 2, IV(MODE) = 0 IMPLIES THAT
!     Y MUST ALSO BE SUPPLIED.  (NOTE THAT Y IS USED FOR SCRATCH -- ITS
!     INPUT CONTENTS ARE LOST.  BY CONTRAST, HC IS NEVER CHANGED.)
!     ONCE CONVERGENCE HAS BEEN OBTAINED, IV(RDREQ) AND IV(COVREQ) MAY
!     IMPLY THAT A FINITE-DIFFERENCE HESSIAN SHOULD BE COMPUTED FOR USE
!     IN COMPUTING A COVARIANCE MATRIX.  IN THIS CASE DG7ITB WILL MAKE
!     A NUMBER OF RETURNS WITH IV(1) = 1 OR 2 AND IV(MODE) POSITIVE.
!     WHEN IV(MODE) IS POSITIVE, Y SHOULD NOT BE CHANGED.
!
!     IV(1) = 1 MEANS THE CALLER SHOULD SET V(F) (I.E., V(10)) TO F(X), THE
!             FUNCTION VALUE AT X, AND CALL DG7ITB AGAIN, HAVING CHANGED
!             NONE OF THE OTHER PARAMETERS.  AN EXCEPTION OCCURS IF F(X)
!             CANNOT BE EVALUATED (E.G. IF OVERFLOW WOULD OCCUR), WHICH
!             MAY HAPPEN BECAUSE OF AN OVERSIZED STEP.  IN THIS CASE
!             THE CALLER SHOULD SET IV(TOOBIG) = IV(2) TO 1, WHICH WILL
!             CAUSE DG7ITB TO IGNORE V(F) AND TRY A SMALLER STEP.  NOTE
!             THAT THE CURRENT FUNCTION EVALUATION COUNT IS AVAILABLE
!             IN IV(NFCALL) = IV(6).  THIS MAY BE USED TO IDENTIFY
!             WHICH COPY OF SAVED INFORMATION SHOULD BE USED IN COM-
!             PUTING G, HC, AND Y THE NEXT TIME DG7ITB RETURNS WITH
!             IV(1) = 2.  SEE MLPIT FOR AN EXAMPLE OF THIS.
!     IV(1) = 2 MEANS THE CALLER SHOULD SET G TO G(X), THE GRADIENT OF F AT
!             X.  THE CALLER SHOULD ALSO SET HC TO THE GAUSS-NEWTON
!             HESSIAN AT X.  IF IV(MODE) = 0, THEN THE CALLER SHOULD
!             ALSO COMPUTE THE PART OF THE YIELD VECTOR DESCRIBED ABOVE.
!             THE CALLER SHOULD THEN CALL DG7ITB AGAIN (WITH IV(1) = 2).
!             THE CALLER MAY ALSO CHANGE D AT THIS TIME, BUT SHOULD NOT
!             CHANGE X.  NOTE THAT IV(NFGCAL) = IV(7) CONTAINS THE
!             VALUE THAT IV(NFCALL) HAD DURING THE RETURN WITH
!             IV(1) = 1 IN WHICH X HAD THE SAME VALUE AS IT NOW HAS.
!             IV(NFGCAL) IS EITHER IV(NFCALL) OR IV(NFCALL) - 1.  MLPIT
!             IS AN EXAMPLE WHERE THIS INFORMATION IS USED.  IF G OR HC
!             CANNOT BE EVALUATED AT X, THEN THE CALLER MAY SET
!             IV(NFGCAL) TO 0, IN WHICH CASE DG7ITB WILL RETURN WITH
!             IV(1) = 15.
!
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY.
!
!        (SEE NL2SOL FOR REFERENCES.)
!
!     +++++++++++++++++++++++++++  DECLARATIONS  ++++++++++++++++++++++++++++
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  CONSTANTS  ***
!
!
!     ***  EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!
!     DA7SST.... ASSESSES CANDIDATE STEP.
!     DD7TPR... RETURNS INNER PRODUCT OF TWO VECTORS.
!     DF7DHB... COMPUTE FINITE-DIFFERENCE HESSIAN (FOR INIT. S MATRIX).
!     DG7QSB... COMPUTES GOLDFELD-QUANDT-TROTTER STEP (AUGMENTED MODEL).
!     I7COPY.... COPIES ONE INTEGER VECTOR TO ANOTHER.
!     I7PNVR... INVERTS PERMUTATION ARRAY.
!     I7SHFT... SHIFTS AN INTEGER VECTOR.
!     DITSUM.... PRINTS ITERATION SUMMARY AND INFO ON INITIAL AND FINAL X.
!     DL7MSB... COMPUTES LEVENBERG-MARQUARDT STEP (GAUSS-NEWTON MODEL).
!     DL7SQR... COMPUTES L * L**T FROM LOWER TRIANGULAR MATRIX L.
!     DL7TVM... COMPUTES L**T * V, V = VECTOR, L = LOWER TRIANGULAR MATRIX.
!     DL7VML.... COMPUTES L * V, V = VECTOR, L = LOWER TRIANGULAR MATRIX.
!     DPARCK.... CHECK VALIDITY OF IV AND V INPUT COMPONENTS.
!     DQ7RSH... SHIFTS A QR FACTORIZATION.
!     DRLDST... COMPUTES V(RELDX) = RELATIVE STEP SIZE.
!     DS7DMP... MULTIPLIES A SYM. MATRIX FORE AND AFT BY A DIAG. MATRIX.
!     DS7IPR... APPLIES PERMUTATION TO (LOWER TRIANG. OF) SYM. MATRIX.
!     DS7LUP... PERFORMS QUASI-NEWTON UPDATE ON COMPACTLY STORED LOWER TRI-
!             ANGLE OF A SYMMETRIC MATRIX.
!     DS7LVM... MULTIPLIES COMPACTLY STORED SYM. MATRIX TIMES VECTOR.
!     STOPX.... RETURNS .TRUE. IF THE BREAK KEY HAS BEEN PRESSED.
!     DV2NRM... RETURNS THE 2-NORM OF A VECTOR.
!     DV2AXY.... COMPUTES SCALAR TIMES ONE VECTOR PLUS ANOTHER.
!     DV7CPY.... COPIES ONE VECTOR TO ANOTHER.
!     DV7IPR... APPLIES A PERMUTATION TO A VECTOR.
!     DV7SCP... SETS ALL ELEMENTS OF A VECTOR TO A SCALAR.
!     DV7VMP... MULTIPLIES (DIVIDES) VECTORS COMPONENTWISE.
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!
!     ***  IV SUBSCRIPT VALUES  ***
!
!     ***  (NOTE THAT P0 AND PC ARE STORED IN IV(G0) AND IV(STLSTG) RESP.)
!
!
!     ***  V SUBSCRIPT VALUES  ***
!
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: negone = -1.E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: onep2 = 1.2E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: cnvcod = 55, cosmin = 47,            &
                                          covmat = 26, covreq = 15,            &
                                          dgnorm = 1, dig = 37, dstnrm = 2,    &
                                          f = 10, f0 = 13, fdh = 74,           &
                                          fdif = 11, fuzz = 45, gtstep = 4,    &
                                          h = 56, hc = 71, ierr = 75,          &
                                          incfac = 23, inits = 25,             &
                                          ipivot = 76, irc = 29, ivneed = 3,   &
                                          kagqt = 33, kalm = 34, lmat = 42,    &
                                          lmax0 = 35, lmaxs = 36, mode = 35,   &
                                          model = 5, mxfcal = 17, mxiter = 18, &
                                          nextiv = 46, nextv = 47, nfcall = 6, &
                                          nfcov = 52, nfgcal = 7, ngcall = 30, &
                                          ngcov = 53, niter = 31, nvsave = 9,  &
                                          p0 = 48, pc = 41, perm = 58,         &
                                          phmxfc = 21, preduc = 7, qtr = 77,   &
                                          rad0 = 9, radfac = 16, radinc = 8,   &
                                          radius = 8, rdreq = 57, regd = 67,   &
                                          reldx = 17, restor = 9, rmat = 78,   &
                                          s = 62, size = 55, step = 40,        &
                                          stglim = 11, stppar = 5, sused = 64, &
                                          switch = 12, toobig = 2,             &
                                          tuner4 = 29, tuner5 = 30, vneed = 4, &
                                          vsave = 60, w = 65, wscale = 56,     &
                                          x0 = 43, xirc = 13
!     .. Scalar Arguments ..
      Integer                          :: liv, lv, p, ps
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(2,p), d(p), g(p), v(lv), x(p),     &
                                          y(p)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: e, gi, sttsst, t, t1, xi
      Integer                          :: dig1, dummy, g01, h1, hc1, i, i1,    &
                                          ipi, ipiv0, ipiv1, ipiv2, ipn, j, k, &
                                          l, lmat1, lstgst, p1, p1len, pp1,    &
                                          pp1o2, qtr1, rmat1, rstrst, s1,      &
                                          step1, stpmod, td1, temp1, temp2,    &
                                          tg1, w1, wlm1, x01
      Logical                          :: havqtr, havrm
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, drldst, dv2nrm
      Logical, External                :: stopx
      External                         :: da7sst, df7dhb, dg7qsb, ditsum,      &
                                          dl7msb, dl7sqr, dl7tvm, dl7vml,      &
                                          dparck, dq7rsh, ds7dmp, ds7ipr,      &
                                          ds7lup, ds7lvm, dv2axy, dv7cpy,      &
                                          dv7ipr, dv7scp, dv7vmp, i7copy,      &
                                          i7pnvr, i7shft
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      i = iv(1)
      If (i==1) Go To 130
      If (i==2) Go To 140
!
      If (i<12) Go To 100
      If (i>13) Go To 100
      iv(vneed) = iv(vneed) + p*(3*p+25)/2 + 7
      iv(ivneed) = iv(ivneed) + 4*p
100   Call dparck(1,d,iv,liv,lv,p,v)
      i = iv(1) - 2
      If (i>12) Go To 760
      Go To (420,420,420,420,420,420,300,260,300,110,110,120) i
!
!     ***  STORAGE ALLOCATION  ***
!
110   pp1o2 = p*(p+1)/2
      iv(s) = iv(lmat) + pp1o2
      iv(x0) = iv(s) + pp1o2
      iv(step) = iv(x0) + 2*p
      iv(dig) = iv(step) + 3*p
      iv(w) = iv(dig) + 2*p
      iv(h) = iv(w) + 4*p + 7
      iv(nextv) = iv(h) + pp1o2
      iv(ipivot) = iv(perm) + 3*p
      iv(nextiv) = iv(ipivot) + p
      If (iv(1)/=13) Go To 120
      iv(1) = 14
      Go To 760
!
!     ***  INITIALIZATION  ***
!
120   iv(niter) = 0
      iv(nfcall) = 1
      iv(ngcall) = 1
      iv(nfgcal) = 1
      iv(mode) = -1
      iv(stglim) = 2
      iv(toobig) = 0
      iv(cnvcod) = 0
      iv(covmat) = 0
      iv(nfcov) = 0
      iv(ngcov) = 0
      iv(radinc) = 0
      iv(pc) = p
      v(rad0) = zero
      v(stppar) = zero
      v(radius) = v(lmax0)/(one+v(phmxfc))
!
!     ***  CHECK CONSISTENCY OF B AND INITIALIZE IP ARRAY  ***
!
      ipi = iv(ipivot)
      Do i = 1, p
        iv(ipi) = i
        ipi = ipi + 1
        If (b(1,i)>b(2,i)) Go To 730
      End Do
!
!     ***  SET INITIAL MODEL AND S MATRIX  ***
!
      iv(model) = 1
      iv(1) = 1
      If (iv(s)<0) Go To 750
      If (iv(inits)>1) iv(model) = 2
      s1 = iv(s)
      If (iv(inits)==0 .Or. iv(inits)>2) Call dv7scp(p*(p+1)/2,v(s1),zero)
      Go To 750
!
!     ***  NEW FUNCTION VALUE  ***
!
130   If (iv(mode)==0) Go To 420
      If (iv(mode)>0) Go To 640
!
      If (iv(toobig)==0) Go To 740
      iv(1) = 63
      Go To 760
!
!     ***  MAKE SURE GRADIENT COULD BE COMPUTED  ***
!
140   If (iv(toobig)==0) Go To 150
      iv(1) = 65
      Go To 760
!
!     ***  NEW GRADIENT  ***
!
150   iv(kalm) = -1
      iv(kagqt) = -1
      iv(fdh) = 0
      If (iv(mode)>0) Go To 640
      If (iv(hc)<=0 .And. iv(rmat)<=0) Go To 720
!
!     ***  CHOOSE INITIAL PERMUTATION  ***
!
      ipi = iv(ipivot)
      ipn = ipi + p - 1
      ipiv2 = iv(perm) - 1
      k = iv(pc)
      p1 = p
      pp1 = p + 1
      rmat1 = iv(rmat)
      havrm = rmat1 > 0
      qtr1 = iv(qtr)
      havqtr = qtr1 > 0
!     *** MAKE SURE V(QTR1) IS LEGAL (EVEN WHEN NOT REFERENCED) ***
      w1 = iv(w)
      If (.Not. havqtr) qtr1 = w1 + p
!
      Do i = 1, p
        i1 = iv(ipn)
        ipn = ipn - 1
        If (b(1,i1)>=b(2,i1)) Go To 160
        xi = x(i1)
        gi = g(i1)
        If (xi<=b(1,i1) .And. gi>zero) Go To 160
        If (xi>=b(2,i1) .And. gi<zero) Go To 160
!           *** DISALLOW CONVERGENCE IF X(I1) HAS JUST BEEN FREED ***
        j = ipiv2 + i1
        If (iv(j)>k) iv(cnvcod) = 0
        Go To 180
160     If (i1>=p1) Go To 170
        i1 = pp1 - i
        Call i7shft(p1,i1,iv(ipi))
        If (havrm) Call dq7rsh(i1,p1,havqtr,v(qtr1),v(rmat1),v(w1))
170     p1 = p1 - 1
180   End Do
      iv(pc) = p1
!
!     ***  COMPUTE V(DGNORM) (AN OUTPUT VALUE IF WE STOP NOW)  ***
!
      v(dgnorm) = zero
      If (p1<=0) Go To 190
      dig1 = iv(dig)
      Call dv7vmp(p,v(dig1),g,d,-1)
      Call dv7ipr(p,iv(ipi),v(dig1))
      v(dgnorm) = dv2nrm(p1,v(dig1))
190   If (iv(cnvcod)/=0) Go To 630
      If (iv(mode)==0) Go To 570
      iv(mode) = 0
      v(f0) = v(f)
      If (iv(inits)<=2) Go To 240
!
!     ***  ARRANGE FOR FINITE-DIFFERENCE INITIAL S  ***
!
      iv(xirc) = iv(covreq)
      iv(covreq) = -1
      If (iv(inits)>3) iv(covreq) = 1
      iv(cnvcod) = 70
      Go To 650
!
!     ***  COME TO NEXT STMT AFTER COMPUTING F.D. HESSIAN FOR INIT. S  ***
!
200   h1 = iv(fdh)
      If (h1<=0) Go To 710
      iv(cnvcod) = 0
      iv(mode) = 0
      iv(nfcov) = 0
      iv(ngcov) = 0
      iv(covreq) = iv(xirc)
      s1 = iv(s)
      pp1o2 = ps*(ps+1)/2
      hc1 = iv(hc)
      If (hc1<=0) Go To 210
      Call dv2axy(pp1o2,v(s1),negone,v(hc1),v(h1))
      Go To 220
210   rmat1 = iv(rmat)
      lmat1 = iv(lmat)
      Call dl7sqr(p,v(lmat1),v(rmat1))
      ipi = iv(ipivot)
      ipiv1 = iv(perm) + p
      Call i7pnvr(p,iv(ipiv1),iv(ipi))
      Call ds7ipr(p,iv(ipiv1),v(lmat1))
      Call dv2axy(pp1o2,v(s1),negone,v(lmat1),v(h1))
!
!     *** ZERO PORTION OF S CORRESPONDING TO FIXED X COMPONENTS ***
!
220   Do i = 1, p
        If (b(1,i)<b(2,i)) Go To 230
        k = s1 + i*(i-1)/2
        Call dv7scp(i,v(k),zero)
        If (i>=p) Go To 240
        k = k + 2*i - 1
        i1 = i + 1
        Do j = i1, p
          v(k) = zero
          k = k + j
        End Do
230   End Do
!
240   iv(1) = 2
!
!
!     -----------------------------  MAIN LOOP  -----------------------------
!
!
!     ***  PRINT ITERATION SUMMARY, CHECK ITERATION LIMIT  ***
!
250   Call ditsum(d,g,iv,liv,lv,p,v,x)
260   k = iv(niter)
      If (k<iv(mxiter)) Go To 270
      iv(1) = 10
      Go To 760
270   iv(niter) = k + 1
!
!     ***  UPDATE RADIUS  ***
!
      If (k==0) Go To 280
      step1 = iv(step)
      Do i = 1, p
        v(step1) = d(i)*v(step1)
        step1 = step1 + 1
      End Do
      step1 = iv(step)
      t = v(radfac)*dv2nrm(p,v(step1))
      If (v(radfac)<one .Or. t>v(radius)) v(radius) = t
!
!     ***  INITIALIZE FOR START OF NEXT ITERATION  ***
!
280   x01 = iv(x0)
      v(f0) = v(f)
      iv(irc) = 4
      iv(h) = -abs(iv(h))
      iv(sused) = iv(model)
!
!     ***  COPY X TO X0  ***
!
      Call dv7cpy(p,v(x01),x)
!
!     ***  CHECK STOPX AND FUNCTION EVALUATION LIMIT  ***
!
290   If (.Not. stopx(dummy)) Go To 310
      iv(1) = 11
      Go To 320
!
!     ***  COME HERE WHEN RESTARTING AFTER FUNC. EVAL. LIMIT OR STOPX.
!
300   If (v(f)>=v(f0)) Go To 310
      v(radfac) = one
      k = iv(niter)
      Go To 270
!
310   If (iv(nfcall)<iv(mxfcal)+iv(nfcov)) Go To 330
      iv(1) = 9
320   If (v(f)>=v(f0)) Go To 760
!
!        ***  IN CASE OF STOPX OR FUNCTION EVALUATION LIMIT WITH
!        ***  IMPROVED V(F), EVALUATE THE GRADIENT AT X.
!
      iv(cnvcod) = iv(1)
      Go To 560
!
!     . . . . . . . . . . . . .  COMPUTE CANDIDATE STEP  . . . . . . . . . .
!
330   step1 = iv(step)
      tg1 = iv(dig)
      td1 = tg1 + p
      x01 = iv(x0)
      w1 = iv(w)
      h1 = iv(h)
      p1 = iv(pc)
      ipi = iv(perm)
      ipiv1 = ipi + p
      ipiv2 = ipiv1 + p
      ipiv0 = iv(ipivot)
      If (iv(model)==2) Go To 340
!
!        ***  COMPUTE LEVENBERG-MARQUARDT STEP IF POSSIBLE...
!
      rmat1 = iv(rmat)
      If (rmat1<=0) Go To 340
      qtr1 = iv(qtr)
      If (qtr1<=0) Go To 340
      lmat1 = iv(lmat)
      wlm1 = w1 + p
      Call dl7msb(b,d,g,iv(ierr),iv(ipiv0),iv(ipiv1),iv(ipiv2),iv(kalm),       &
        v(lmat1),lv,p,iv(p0),iv(pc),v(qtr1),v(rmat1),v(step1),v(td1),v(tg1),v, &
        v(w1),v(wlm1),x,v(x01))
!        *** H IS STORED IN THE END OF W AND HAS JUST BEEN OVERWRITTEN,
!        *** SO WE MARK IT INVALID...
      iv(h) = -abs(h1)
!        *** EVEN IF H WERE STORED ELSEWHERE, IT WOULD BE NECESSARY TO
!        *** MARK INVALID THE INFORMATION DG7QTS MAY HAVE STORED IN V...
      iv(kagqt) = -1
      Go To 390
!
340   If (h1>0) Go To 380
!
!     ***  SET H TO  D**-1 * (HC + T1*S) * D**-1.  ***
!
      p1len = p1*(p1+1)/2
      h1 = -h1
      iv(h) = h1
      iv(fdh) = 0
      If (p1<=0) Go To 380
!        *** MAKE TEMPORARY PERMUTATION ARRAY ***
      Call i7copy(p,iv(ipi),iv(ipiv0))
      j = iv(hc)
      If (j>0) Go To 350
      j = h1
      rmat1 = iv(rmat)
      Call dl7sqr(p1,v(h1),v(rmat1))
      Go To 360
350   Call dv7cpy(p*(p+1)/2,v(h1),v(j))
      Call ds7ipr(p,iv(ipi),v(h1))
360   If (iv(model)==1) Go To 370
      lmat1 = iv(lmat)
      s1 = iv(s)
      Call dv7cpy(p*(p+1)/2,v(lmat1),v(s1))
      Call ds7ipr(p,iv(ipi),v(lmat1))
      Call dv2axy(p1len,v(h1),one,v(lmat1),v(h1))
370   Call dv7cpy(p,v(td1),d)
      Call dv7ipr(p,iv(ipi),v(td1))
      Call ds7dmp(p1,v(h1),v(h1),v(td1),-1)
      iv(kagqt) = -1
!
!     ***  COMPUTE ACTUAL GOLDFELD-QUANDT-TROTTER STEP  ***
!
380   lmat1 = iv(lmat)
      Call dg7qsb(b,d,v(h1),g,iv(ipi),iv(ipiv1),iv(ipiv2),iv(kagqt),v(lmat1),  &
        lv,p,iv(p0),p1,v(step1),v(td1),v(tg1),v,v(w1),x,v(x01))
      If (iv(kalm)>0) iv(kalm) = 0
!
390   If (iv(irc)/=6) Go To 400
      If (iv(restor)/=2) Go To 420
      rstrst = 2
      Go To 430
!
!     ***  CHECK WHETHER EVALUATING F(X0 + STEP) LOOKS WORTHWHILE  ***
!
400   iv(toobig) = 0
      If (v(dstnrm)<=zero) Go To 420
      If (iv(irc)/=5) Go To 410
      If (v(radfac)<=one) Go To 410
      If (v(preduc)>onep2*v(fdif)) Go To 410
      step1 = iv(step)
      x01 = iv(x0)
      Call dv2axy(p,v(step1),negone,v(x01),x)
      If (iv(restor)/=2) Go To 420
      rstrst = 0
      Go To 430
!
!     ***  COMPUTE F(X0 + STEP)  ***
!
410   x01 = iv(x0)
      step1 = iv(step)
      Call dv2axy(p,x,one,v(step1),v(x01))
      iv(nfcall) = iv(nfcall) + 1
      iv(1) = 1
      Go To 750
!
!     . . . . . . . . . . . . .  ASSESS CANDIDATE STEP  . . . . . . . . . . .
!
420   rstrst = 3
430   x01 = iv(x0)
      v(reldx) = drldst(p,d,x,v(x01))
      Call da7sst(iv,liv,lv,v)
      step1 = iv(step)
      lstgst = x01 + p
      i = iv(restor) + 1
      Go To (470,440,450,460) i
440   Call dv7cpy(p,x,v(x01))
      Go To 470
450   Call dv7cpy(p,v(lstgst),v(step1))
      Go To 470
460   Call dv7cpy(p,v(step1),v(lstgst))
      Call dv2axy(p,x,one,v(step1),v(x01))
      v(reldx) = drldst(p,d,x,v(x01))
      iv(restor) = rstrst
!
!     ***  IF NECESSARY, SWITCH MODELS  ***
!
470   If (iv(switch)==0) Go To 480
      iv(h) = -abs(iv(h))
      iv(sused) = iv(sused) + 2
      l = iv(vsave)
      Call dv7cpy(nvsave,v,v(l))
480   l = iv(irc) - 4
      stpmod = iv(model)
      If (l>0) Go To (500,510,520,520,520,520,520,520,620,570) l
!
!     ***  DECIDE WHETHER TO CHANGE MODELS  ***
!
      e = v(preduc) - v(fdif)
      s1 = iv(s)
      Call ds7lvm(ps,y,v(s1),v(step1))
      sttsst = half*dd7tpr(ps,v(step1),y)
      If (iv(model)==1) sttsst = -sttsst
      If (abs(e+sttsst)*v(fuzz)>=abs(e)) Go To 490
!
!     ***  SWITCH MODELS  ***
!
      iv(model) = 3 - iv(model)
      If (-2<l) Go To 530
      iv(h) = -abs(iv(h))
      iv(sused) = iv(sused) + 2
      l = iv(vsave)
      Call dv7cpy(nvsave,v(l),v)
      Go To 290
!
490   If (-3<l) Go To 530
!
!     ***  RECOMPUTE STEP WITH DIFFERENT RADIUS  ***
!
500   v(radius) = v(radfac)*v(dstnrm)
      Go To 290
!
!     ***  COMPUTE STEP OF LENGTH V(LMAXS) FOR SINGULAR CONVERGENCE TEST
!
510   v(radius) = v(lmaxs)
      Go To 330
!
!     ***  CONVERGENCE OR FALSE CONVERGENCE  ***
!
520   iv(cnvcod) = l
      If (v(f)>=v(f0)) Go To 630
      If (iv(xirc)==14) Go To 630
      iv(xirc) = 14
!
!     . . . . . . . . . . . .  PROCESS ACCEPTABLE STEP  . . . . . . . . . . .
!
530   iv(covmat) = 0
      iv(regd) = 0
!
!     ***  SEE WHETHER TO SET V(RADFAC) BY GRADIENT TESTS  ***
!
      If (iv(irc)/=3) Go To 560
      step1 = iv(step)
      temp1 = step1 + p
      temp2 = iv(x0)
!
!     ***  SET  TEMP1 = HESSIAN * STEP  FOR USE IN GRADIENT TESTS  ***
!
      hc1 = iv(hc)
      If (hc1<=0) Go To 540
      Call ds7lvm(p,v(temp1),v(hc1),v(step1))
      Go To 550
540   rmat1 = iv(rmat)
      ipiv0 = iv(ipivot)
      Call dv7cpy(p,v(temp1),v(step1))
      Call dv7ipr(p,iv(ipiv0),v(temp1))
      Call dl7tvm(p,v(temp1),v(rmat1),v(temp1))
      Call dl7vml(p,v(temp1),v(rmat1),v(temp1))
      ipiv1 = iv(perm) + p
      Call i7pnvr(p,iv(ipiv1),iv(ipiv0))
      Call dv7ipr(p,iv(ipiv1),v(temp1))
!
550   If (stpmod==1) Go To 560
      s1 = iv(s)
      Call ds7lvm(ps,v(temp2),v(s1),v(step1))
      Call dv2axy(ps,v(temp1),one,v(temp2),v(temp1))
!
!     ***  SAVE OLD GRADIENT AND COMPUTE NEW ONE  ***
!
560   iv(ngcall) = iv(ngcall) + 1
      g01 = iv(w)
      Call dv7cpy(p,v(g01),g)
      Go To 740
!
!     ***  INITIALIZATIONS -- G0 = G - G0, ETC.  ***
!
570   g01 = iv(w)
      Call dv2axy(p,v(g01),negone,v(g01),g)
      step1 = iv(step)
      temp1 = step1 + p
      temp2 = iv(x0)
      If (iv(irc)/=3) Go To 590
!
!     ***  SET V(RADFAC) BY GRADIENT TESTS  ***
!
!     ***  SET  TEMP1 = D**-1 * (HESSIAN * STEP  +  (G(X0) - G(X)))  ***
!
      k = temp1
      l = g01
      Do i = 1, p
        v(k) = (v(k)-v(l))/d(i)
        k = k + 1
        l = l + 1
      End Do
!
!        ***  DO GRADIENT TESTS  ***
!
      If (dv2nrm(p,v(temp1))<=v(dgnorm)*v(tuner4)) Go To 580
      If (dd7tpr(p,g,v(step1))>=v(gtstep)*v(tuner5)) Go To 590
580   v(radfac) = v(incfac)
!
!     ***  COMPUTE Y VECTOR NEEDED FOR UPDATING S  ***
!
590   Call dv2axy(ps,y,negone,y,g)
!
!     ***  DETERMINE SIZING FACTOR V(SIZE)  ***
!
!     ***  SET TEMP1 = S * STEP  ***
      s1 = iv(s)
      Call ds7lvm(ps,v(temp1),v(s1),v(step1))
!
      t1 = abs(dd7tpr(ps,v(step1),v(temp1)))
      t = abs(dd7tpr(ps,v(step1),y))
      v(size) = one
      If (t<t1) v(size) = t/t1
!
!     ***  SET G0 TO WCHMTD CHOICE OF FLETCHER AND AL-BAALI  ***
!
      hc1 = iv(hc)
      If (hc1<=0) Go To 600
      Call ds7lvm(ps,v(g01),v(hc1),v(step1))
      Go To 610
!
600   rmat1 = iv(rmat)
      ipiv0 = iv(ipivot)
      Call dv7cpy(p,v(g01),v(step1))
      i = g01 + ps
      If (ps<p) Call dv7scp(p-ps,v(i),zero)
      Call dv7ipr(p,iv(ipiv0),v(g01))
      Call dl7tvm(p,v(g01),v(rmat1),v(g01))
      Call dl7vml(p,v(g01),v(rmat1),v(g01))
      ipiv1 = iv(perm) + p
      Call i7pnvr(p,iv(ipiv1),iv(ipiv0))
      Call dv7ipr(p,iv(ipiv1),v(g01))
!
610   Call dv2axy(ps,v(g01),one,y,v(g01))
!
!     ***  UPDATE S  ***
!
      Call ds7lup(v(s1),v(cosmin),ps,v(size),v(step1),v(temp1),v(temp2),       &
        v(g01),v(wscale),y)
      iv(1) = 2
      Go To 250
!
!     . . . . . . . . . . . . . .  MISC. DETAILS  . . . . . . . . . . . . . .
!
!     ***  BAD PARAMETERS TO ASSESS  ***
!
620   iv(1) = 64
      Go To 760
!
!
!     ***  CONVERGENCE OBTAINED -- SEE WHETHER TO COMPUTE COVARIANCE  ***
!
630   If (iv(rdreq)==0) Go To 710
      If (iv(fdh)/=0) Go To 710
      If (iv(cnvcod)>=7) Go To 710
      If (iv(regd)>0) Go To 710
      If (iv(covmat)>0) Go To 710
      If (abs(iv(covreq))>=3) Go To 690
      If (iv(restor)==0) iv(restor) = 2
      Go To 650
!
!     ***  COMPUTE FINITE-DIFFERENCE HESSIAN FOR COMPUTING COVARIANCE  ***
!
640   iv(restor) = 0
650   Call df7dhb(b,d,g,i,iv,liv,lv,p,v,x)
      Go To (660,670,680) i
660   iv(nfcov) = iv(nfcov) + 1
      iv(nfcall) = iv(nfcall) + 1
      iv(1) = 1
      Go To 750
!
670   iv(ngcov) = iv(ngcov) + 1
      iv(ngcall) = iv(ngcall) + 1
      iv(nfgcal) = iv(nfcall) + iv(ngcov)
      Go To 740
!
680   If (iv(cnvcod)==70) Go To 200
      Go To 710
!
690   h1 = abs(iv(h))
      iv(fdh) = h1
      iv(h) = -h1
      hc1 = iv(hc)
      If (hc1<=0) Go To 700
      Call dv7cpy(p*(p+1)/2,v(h1),v(hc1))
      Go To 710
700   rmat1 = iv(rmat)
      Call dl7sqr(p,v(h1),v(rmat1))
!
710   iv(mode) = 0
      iv(1) = iv(cnvcod)
      iv(cnvcod) = 0
      Go To 760
!
!     ***  SPECIAL RETURN FOR MISSING HESSIAN INFORMATION -- BOTH
!     ***  IV(HC) .LE. 0 AND IV(RMAT) .LE. 0
!
720   iv(1) = 1400
      Go To 760
!
!     ***  INCONSISTENT B  ***
!
730   iv(1) = 82
      Go To 760
!
!     *** SAVE, THEN INITIALIZE IPIVOT ARRAY BEFORE COMPUTING G ***
!
740   iv(1) = 2
      j = iv(ipivot)
      ipi = iv(perm)
      Call i7pnvr(p,iv(ipi),iv(j))
      Do i = 1, p
        iv(j) = i
        j = j + 1
      End Do
!
!     ***  PROJECT X INTO FEASIBLE REGION (PRIOR TO COMPUTING F OR G)  ***
!
750   Do i = 1, p
        If (x(i)<b(1,i)) x(i) = b(1,i)
        If (x(i)>b(2,i)) x(i) = b(2,i)
      End Do
      iv(toobig) = 0
!
760   Return
!
!     ***  LAST LINE OF DG7ITB FOLLOWS  ***
    End Subroutine dg7itb
    Subroutine drnsgb(a,alf,b,c,da,in,iv,l,l1,la,liv,lv,n,nda,p,v,y)
!
!     ***  ITERATION DRIVER FOR SEPARABLE NONLINEAR LEAST SQUARES,
!     ***  WITH SIMPLE BOUNDS ON THE NONLINEAR VARIABLES.
!
!     ***  PARAMETER DECLARATIONS  ***
!
!     DIMENSION UIPARM(*)
!
!     ***  PURPOSE  ***
!
!     GIVEN A SET OF N OBSERVATIONS Y(1)....Y(N) OF A DEPENDENT VARIABLE
!     T(1)...T(N), DRNSGB ATTEMPTS TO COMPUTE A LEAST SQUARES FIT
!     TO A FUNCTION  ETA  (THE MODEL) WHICH IS A LINEAR COMBINATION
!
!                  L
!     ETA(C,ALF,T) =  SUM C * PHI(ALF,T) +PHI   (ALF,T)
!                 J=1  J     J           L+1
!
!     OF NONLINEAR FUNCTIONS PHI(J) DEPENDENT ON T AND ALF(1),...,ALF(P)
!     (.E.G. A SUM OF EXPONENTIALS OR GAUSSIANS).  THAT IS, IT DETERMINES
!     NONLINEAR PARAMETERS ALF WHICH MINIMIZE
!
!                   2    N                      2
!     NORM(RESIDUAL)  = SUM  (Y - ETA(C,ALF,T )) ,
!                       I=1    I             I
!
!     SUBJECT TO THE SIMPLE BOUND CONSTRAINTS
!     B(1,I) .LE. ALF(I) .LE. B(2,I), I = 1(1)P.
!
!     THE (L+1)ST TERM IS OPTIONAL.
!
!
!     ***  PARAMETERS  ***
!
!      A (IN)  MATRIX PHI(ALF,T) OF THE MODEL.
!     ALF (I/O) NONLINEAR PARAMETERS.
!                 INPUT = INITIAL GUESS,
!                 OUTPUT = BEST ESTIMATE FOUND.
!      C (OUT) LINEAR PARAMETERS (ESTIMATED).
!     DA (IN)  DERIVATIVES OF COLUMNS OF A WITH RESPECT TO COMPONENTS
!                 OF ALF, AS SPECIFIED BY THE IN ARRAY...
!     IN (IN)  WHEN DRNSGB IS CALLED WITH IV(1) = 2 OR -2, THEN FOR
!                 I = 1(1)NDA, COLUMN I OF DA IS THE PARTIAL
!                 DERIVATIVE WITH RESPECT TO ALF(IN(1,I)) OF COLUMN
!                 IN(2,I) OF A, UNLESS IV(1,I) IS NOT POSITIVE (IN
!                 WHICH CASE COLUMN I OF DA IS IGNORED.  IV(1) = -2
!                 MEANS THERE ARE MORE COLUMNS OF DA TO COME AND
!                 DRNSGB SHOULD RETURN FOR THEM.
!     IV (I/O) INTEGER PARAMETER AND SCRATCH VECTOR.  DRNSGB RETURNS
!                 WITH IV(1) = 1 WHEN IT WANTS A TO BE EVALUATED AT
!                 ALF AND WITH IV(1) = 2 WHEN IT WANTS DA TO BE
!                 EVALUATED AT ALF.  WHEN CALLED WITH IV(1) = -2
!                 (AFTER A RETURN WITH IV(1) = 2), DRNSGB RETURNS
!                 WITH IV(1) = -2 TO GET MORE COLUMNS OF DA.
!      L (IN)  NUMBER OF LINEAR PARAMETERS TO BE ESTIMATED.
!     L1 (IN)  L+1 IF PHI(L+1) IS IN THE MODEL, L IF NOT.
!     LA (IN)  LEAD DIMENSION OF A.  MUST BE AT LEAST N.
!     LIV (IN)  LENGTH OF IV.  MUST BE AT LEAST 110 + L + 4*P.
!     LV (IN)  LENGTH OF V.  MUST BE AT LEAST
!                 105 + 2*N + L*(L+3)/2 + P*(2*P + 21 + N).
!      N (IN)  NUMBER OF OBSERVATIONS.
!     NDA (IN)  NUMBER OF COLUMNS IN DA AND IN.
!      P (IN)  NUMBER OF NONLINEAR PARAMETERS TO BE ESTIMATED.
!      V (I/O) FLOATING-POINT PARAMETER AND SCRATCH VECTOR.
!      Y (IN)  RIGHT-HAND SIDE VECTOR.
!
!
!     ***  EXTERNAL SUBROUTINES  ***
!
!
!     DIVSET.... SUPPLIES DEFAULT PARAMETER VALUES.
!     DITSUM.... PRINTS ITERATION SUMMARY, INITIAL AND FINAL ALF.
!     DL7ITV... APPLIES INVERSE-TRANSPOSE OF COMPACT LOWER TRIANG. MATRIX.
!     DL7SVX... ESTIMATES LARGEST SING. VALUE OF LOWER TRIANG. MATRIX.
!     DL7SVN... ESTIMATES SMALLEST SING. VALUE OF LOWER TRIANG. MATRIX.
!     DRN2GB... UNDERLYING NONLINEAR LEAST-SQUARES SOLVER.
!     DQ7APL... APPLIES HOUSEHOLDER TRANSFORMS STORED BY DQ7RFH.
!     DQ7RFH.... COMPUTES QR FACT. VIA HOUSEHOLDER TRANSFORMS WITH PIVOTING.
!     DR7MDC... RETURNS MACHINE-DEP. CONSTANTS.
!     DS7CPR... PRINTS LINEAR PARAMETERS AT SOLUTION.
!     DV2AXY.... ADDS MULTIPLE OF ONE VECTOR TO ANOTHER.
!     DV7CPY.... COPIES ONE VECTOR TO ANOTHER.
!     DV7PRM.... PERMUTES VECTOR.
!     DV7SCL... SCALES AND COPIES ONE VECTOR TO ANOTHER.
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!
!     ***  IV SUBSCRIPT VALUES  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Integer, Parameter               :: ar = 110, csave = 105, d = 27,       &
                                          iers = 108, ipivs = 109,             &
                                          iv1sav = 104, ivneed = 3, j = 70,    &
                                          mode = 35, nextiv = 46, nextv = 47,  &
                                          nfcall = 6, nfgcal = 7, perm = 58,   &
                                          r = 61, regd = 67, regd0 = 82,       &
                                          restor = 9, toobig = 2, vneed = 4
!     .. Scalar Arguments ..
      Integer                          :: l, l1, la, liv, lv, n, nda, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(la,l1), alf(p), b(2,p), c(l),      &
                                          da(la,nda), v(lv), y(n)
      Integer                          :: in(2,nda), iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp), Save             :: machep, negone, sngfac, zero
      Real (Kind=wp)                   :: singtl, t
      Integer                          :: ar1, csave1, d1, dr1, dr1l, i, i1,   &
                                          ier, ipiv1, iv1, j1, jlen, k, ll1o2, &
                                          md, n1, n2, nml, nran, r1, r1l, rd1
!     .. External Procedures ..
      Real (Kind=wp), External         :: dl7svn, dl7svx, dr7mdc
      External                         :: ditsum, divset, dl7itv, dq7apl,      &
                                          dq7rfh, drn2gb, ds7cpr, dv2axy,      &
                                          dv7cpy, dv7prm, dv7scp
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max, real
!     .. Data Statements ..
      Data machep/ -1.E+0_wp/, negone/ -1.E+0_wp/, sngfac/1.E+2_wp/,           &
        zero/0.E+0_wp/
!     .. Executable Statements ..
!
!     ++++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++
!
!
      If (iv(1)==0) Call divset(1,iv,liv,lv,v)
      n1 = 1
      nml = n
      iv1 = iv(1)
      If (iv1<=2) Go To 110
!
!     ***  CHECK INPUT INTEGERS  ***
!
      If (p<=0) Go To 300
      If (l<0) Go To 300
      If (n<=l) Go To 300
      If (la<n) Go To 300
      If (iv1<12) Go To 110
      If (iv1==14) Go To 110
      If (iv1==12) iv(1) = 13
!
!     ***  FRESH START -- COMPUTE STORAGE REQUIREMENTS  ***
!
      If (iv(1)>16) Go To 300
      ll1o2 = l*(l+1)/2
      jlen = n*p
      i = l + p
      If (iv(1)/=13) Go To 100
      iv(ivneed) = iv(ivneed) + l
      iv(vneed) = iv(vneed) + p + 2*n + jlen + ll1o2 + l
100   If (iv(perm)<=ar) iv(perm) = ar + 1
      Call drn2gb(b,v,v,iv,liv,lv,n,n,n1,nml,p,v,v,v,alf)
      If (iv(1)/=14) Go To 310
!
!     ***  STORAGE ALLOCATION  ***
!
      iv(ipivs) = iv(nextiv)
      iv(nextiv) = iv(nextiv) + l
      iv(d) = iv(nextv)
      iv(regd0) = iv(d) + p
      iv(ar) = iv(regd0) + n
      iv(csave) = iv(ar) + ll1o2
      iv(j) = iv(csave) + l
      iv(r) = iv(j) + jlen
      iv(nextv) = iv(r) + n
      iv(iers) = 0
      If (iv1==13) Go To 310
!
!     ***  SET POINTERS INTO IV AND V  ***
!
110   ar1 = iv(ar)
      d1 = iv(d)
      dr1 = iv(j)
      dr1l = dr1 + l
      r1 = iv(r)
      r1l = r1 + l
      rd1 = iv(regd0)
      csave1 = iv(csave)
      nml = n - l
      If (iv1<=2) Go To 130
!
120   n2 = nml
      Call drn2gb(b,v(d1),v(dr1l),iv,liv,lv,nml,n,n1,n2,p,v(r1l),v(rd1),v,alf)
      If (abs(iv(restor)-2)==1 .And. l>0) Call dv7cpy(l,c,v(csave1))
      iv1 = iv(1)
      If (iv1==2) Go To 220
      If (iv1>2) Go To 290
!
!     ***  NEW FUNCTION VALUE (RESIDUAL) NEEDED  ***
!
      iv(iv1sav) = iv(1)
      iv(1) = abs(iv1)
      If (iv(restor)==2 .And. l>0) Call dv7cpy(l,v(csave1),c)
      Go To 310
!
!     ***  COMPUTE NEW RESIDUAL OR GRADIENT  ***
!
130   iv(1) = iv(iv1sav)
      md = iv(mode)
      If (md<=0) Go To 140
      nml = n
      dr1l = dr1
      r1l = r1
140   If (iv(toobig)/=0) Go To 120
      If (abs(iv1)==2) Go To 240
!
!     ***  COMPUTE NEW RESIDUAL  ***
!
      If (l1<=l) Call dv7cpy(n,v(r1),y)
      If (l1>l) Call dv2axy(n,v(r1),negone,a(1,l1),y)
      If (md>0) Go To 200
      ier = 0
      If (l<=0) Go To 190
      ll1o2 = l*(l+1)/2
      ipiv1 = iv(ipivs)
      Call dq7rfh(ier,iv(ipiv1),n,la,0,l,a,v(ar1),ll1o2,c)
!
!     *** DETERMINE NUMERICAL RANK OF A ***
!
      If (machep<=zero) machep = dr7mdc(3)
      singtl = sngfac*real(max(l,n),kind=wp)*machep
      k = l
      If (ier/=0) k = ier - 1
150   If (k<=0) Go To 170
      t = dl7svx(k,v(ar1),c,c)
      If (t>zero) t = dl7svn(k,v(ar1),c,c)/t
      If (t>singtl) Go To 160
      k = k - 1
      Go To 150
!
!     *** RECORD RANK IN IV(IERS)... IV(IERS) = 0 MEANS FULL RANK,
!     *** IV(IERS) .GT. 0 MEANS RANK IV(IERS) - 1.
!
160   If (k>=l) Go To 180
170   ier = k + 1
      Call dv7scp(l-k,c(k+1),zero)
180   iv(iers) = ier
      If (k<=0) Go To 190
!
!     *** APPLY HOUSEHOLDER TRANSFORMATONS TO RESIDUALS...
!
      Call dq7apl(la,n,k,a,v(r1),ier)
!
!     *** COMPUTING C NOW MAY SAVE A FUNCTION EVALUATION AT
!     *** THE LAST ITERATION.
!
      Call dl7itv(k,c,v(ar1),v(r1))
      Call dv7prm(l,iv(ipiv1),c)
!
190   If (iv(1)<2) Go To 280
      Go To 310
!
!
!     ***  RESIDUAL COMPUTATION FOR F.D. HESSIAN  ***
!
200   If (l<=0) Go To 210
      Do i = 1, l
        Call dv2axy(n,v(r1),-c(i),a(1,i),v(r1))
      End Do
210   If (iv(1)>0) Go To 120
      iv(1) = 2
      Go To 230
!
!     ***  NEW GRADIENT (JACOBIAN) NEEDED  ***
!
220   iv(iv1sav) = iv1
      If (iv(nfgcal)/=iv(nfcall)) iv(1) = 1
230   Call dv7scp(n*p,v(dr1),zero)
      Go To 310
!
!     ***  COMPUTE NEW JACOBIAN  ***
!
240   If (nda<=0) Go To 300
      Do i = 1, nda
        i1 = in(1,i) - 1
        If (i1<0) Go To 250
        j1 = in(2,i)
        k = dr1 + i1*n
        t = negone
        If (j1<=l) t = -c(j1)
        Call dv2axy(n,v(k),t,da(1,i),v(k))
250   End Do
      If (iv1==2) Go To 260
      iv(1) = iv1
      Go To 310
260   If (l<=0) Go To 120
      If (md>0) Go To 120
      k = dr1
      ier = iv(iers)
      nran = l
      If (ier>0) nran = ier - 1
      If (nran<=0) Go To 270
      Do i = 1, p
        Call dq7apl(la,n,nran,a,v(k),ier)
        k = k + n
      End Do
270   Call dv7cpy(l,v(csave1),c)
280   If (ier==0) Go To 120
!
!     *** ADJUST SUBSCRIPTS DESCRIBING R AND DR...
!
      nran = ier - 1
      dr1l = dr1 + nran
      nml = n - nran
      r1l = r1 + nran
      Go To 120
!
!     ***  CONVERGENCE OR LIMIT REACHED  ***
!
290   If (iv(regd)==1) iv(regd) = rd1
      If (iv(1)<=11) Call ds7cpr(c,iv,l,liv)
      Go To 310
!
300   iv(1) = 66
      Call ditsum(v,v,iv,liv,lv,p,v,alf)
!
310   Return
!
!     ***  LAST CARD OF DRNSGB FOLLOWS  ***
    End Subroutine drnsgb
    Subroutine ds7lup(a,cosmin,p,size,step,u,w,wchmtd,wscale,y)
!
!     ***  UPDATE SYMMETRIC  A  SO THAT  A * STEP = Y  ***
!     ***  (LOWER TRIANGLE OF  A  STORED ROWWISE       ***
!
!     ***  PARAMETER DECLARATIONS  ***
!
!     DIMENSION A(P*(P+1)/2)
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  CONSTANTS  ***
!
!     ***  EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: cosmin, size, wscale
      Integer                          :: p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(*), step(p), u(p), w(p),           &
                                          wchmtd(p), y(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: denmin, sdotwm, t, ui, wi
      Integer                          :: i, j, k
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, dv2nrm
      External                         :: ds7lvm
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, min
!     .. Executable Statements ..
!
!-----------------------------------------------------------------------
!
      sdotwm = dd7tpr(p,step,wchmtd)
      denmin = cosmin*dv2nrm(p,step)*dv2nrm(p,wchmtd)
      wscale = one
      If (denmin/=zero) wscale = min(one,abs(sdotwm/denmin))
      t = zero
      If (sdotwm/=zero) t = wscale/sdotwm
      Do i = 1, p
        w(i) = t*wchmtd(i)
      End Do
      Call ds7lvm(p,u,a,step)
      t = half*(size*dd7tpr(p,step,u)-dd7tpr(p,step,y))
      Do i = 1, p
        u(i) = t*w(i) + y(i) - size*u(i)
      End Do
!
!     ***  SET  A = A + U*(W**T) + W*(U**T)  ***
!
      k = 1
      Do i = 1, p
        ui = u(i)
        wi = w(i)
        Do j = 1, i
          a(k) = size*a(k) + ui*w(j) + wi*u(j)
          k = k + 1
        End Do
      End Do
!
      Return
!     ***  LAST CARD OF DS7LUP FOLLOWS  ***
    End Subroutine ds7lup
    Subroutine dl7mst(d,g,ierr,ipivot,ka,p,qtr,r,step,v,w)
!
!     ***  COMPUTE LEVENBERG-MARQUARDT STEP USING MORE-HEBDEN TECHNIQUE  **
!     ***  NL2SOL VERSION 2.2.  ***
!
!     ***  PARAMETER DECLARATIONS  ***
!
!     DIMENSION W(P*(P+5)/2 + 4)
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!     ***  PURPOSE  ***
!
!        GIVEN THE R MATRIX FROM THE QR DECOMPOSITION OF A JACOBIAN
!     MATRIX, J, AS WELL AS Q-TRANSPOSE TIMES THE CORRESPONDING
!     RESIDUAL VECTOR, RESID, THIS SUBROUTINE COMPUTES A LEVENBERG-
!     MARQUARDT STEP OF APPROXIMATE LENGTH V(RADIUS) BY THE MORE-
!     TECHNIQUE.
!
!     ***  PARAMETER DESCRIPTION  ***
!
!      D (IN)  = THE SCALE VECTOR.
!      G (IN)  = THE GRADIENT VECTOR (J**T)*R.
!     IERR (I/O) = RETURN CODE FROM QRFACT OR DQ7RGS -- 0 MEANS R HAS
!             FULL RANK.
!     IPIVOT (I/O) = PERMUTATION ARRAY FROM QRFACT OR DQ7RGS, WHICH COMPUTE
!             QR DECOMPOSITIONS WITH COLUMN PIVOTING.
!     KA (I/O).  KA .LT. 0 ON INPUT MEANS THIS IS THE FIRST CALL ON
!             DL7MST FOR THE CURRENT R AND QTR.  ON OUTPUT KA CON-
!             TAINS THE NUMBER OF HEBDEN ITERATIONS NEEDED TO DETERMINE
!             STEP.  KA = 0 MEANS A GAUSS-NEWTON STEP.
!      P (IN)  = NUMBER OF PARAMETERS.
!     QTR (IN)  = (Q**T)*RESID = Q-TRANSPOSE TIMES THE RESIDUAL VECTOR.
!      R (IN)  = THE R MATRIX, STORED COMPACTLY BY COLUMNS.
!     STEP (OUT) = THE LEVENBERG-MARQUARDT STEP COMPUTED.
!      V (I/O) CONTAINS VARIOUS CONSTANTS AND VARIABLES DESCRIBED BELOW.
!      W (I/O) = WORKSPACE OF LENGTH P*(P+5)/2 + 4.
!
!     ***  ENTRIES IN V  ***
!
!     V(DGNORM) (I/O) = 2-NORM OF (D**-1)*G.
!     V(DSTNRM) (I/O) = 2-NORM OF D*STEP.
!     V(DST0)   (I/O) = 2-NORM OF GAUSS-NEWTON STEP (FOR NONSING. J).
!     V(EPSLON) (IN) = MAX. REL. ERROR ALLOWED IN TWONORM(R)**2 MINUS
!             TWONORM(R - J*STEP)**2.  (SEE ALGORITHM NOTES BELOW.)
!     V(GTSTEP) (OUT) = INNER PRODUCT BETWEEN G AND STEP.
!     V(NREDUC) (OUT) = HALF THE REDUCTION IN THE SUM OF SQUARES PREDICTED
!             FOR A GAUSS-NEWTON STEP.
!     V(PHMNFC) (IN)  = TOL. (TOGETHER WITH V(PHMXFC)) FOR ACCEPTING STEP
!             (MORE*S SIGMA).  THE ERROR V(DSTNRM) - V(RADIUS) MUST LIE
!             BETWEEN V(PHMNFC)*V(RADIUS) AND V(PHMXFC)*V(RADIUS).
!     V(PHMXFC) (IN)  (SEE V(PHMNFC).)
!     V(PREDUC) (OUT) = HALF THE REDUCTION IN THE SUM OF SQUARES PREDICTED
!             BY THE STEP RETURNED.
!     V(RADIUS) (IN)  = RADIUS OF CURRENT (SCALED) TRUST REGION.
!     V(RAD0)   (I/O) = VALUE OF V(RADIUS) FROM PREVIOUS CALL.
!     V(STPPAR) (I/O) = MARQUARDT PARAMETER (OR ITS NEGATIVE IF THE SPECIAL
!             CASE MENTIONED BELOW IN THE ALGORITHM NOTES OCCURS).
!
!     NOTE -- SEE DATA STATEMENT BELOW FOR VALUES OF ABOVE SUBSCRIPTS.
!
!     ***  USAGE NOTES  ***
!
!     IF IT IS DESIRED TO RECOMPUTE STEP USING A DIFFERENT VALUE OF
!     V(RADIUS), THEN THIS ROUTINE MAY BE RESTARTED BY CALLING IT
!     WITH ALL PARAMETERS UNCHANGED EXCEPT V(RADIUS).  (THIS EXPLAINS
!     WHY MANY PARAMETERS ARE LISTED AS I/O).  ON AN INTIIAL CALL (ONE
!     WITH KA = -1), THE CALLER NEED ONLY HAVE INITIALIZED D, G, KA, P,
!     QTR, R, V(EPSLON), V(PHMNFC), V(PHMXFC), V(RADIUS), AND V(RAD0).
!
!     ***  APPLICATION AND USAGE RESTRICTIONS  ***
!
!     THIS ROUTINE IS CALLED AS PART OF THE NL2SOL (NONLINEAR LEAST-
!     SQUARES) PACKAGE (REF. 1).
!
!     ***  ALGORITHM NOTES  ***
!
!     THIS CODE IMPLEMENTS THE STEP COMPUTATION SCHEME DESCRIBED IN
!     REFS. 2 AND 4.  FAST GIVENS TRANSFORMATIONS (SEE REF. 3, PP. 60-
!     62) ARE USED TO COMPUTE STEP WITH A NONZERO MARQUARDT PARAMETER.
!        A SPECIAL CASE OCCURS IF J IS (NEARLY) SINGULAR AND V(RADIUS)
!     IS SUFFICIENTLY LARGE.  IN THIS CASE THE STEP RETURNED IS SUCH
!     THAT  TWONORM(R)**2 - TWONORM(R - J*STEP)**2  DIFFERS FROM ITS
!     OPTIMAL VALUE BY LESS THAN V(EPSLON) TIMES THIS OPTIMAL VALUE,
!     WHERE J AND R DENOTE THE ORIGINAL JACOBIAN AND RESIDUAL.  (SEE
!     REF. 2 FOR MORE DETAILS.)
!
!     ***  FUNCTIONS AND SUBROUTINES CALLED  ***
!
!     DD7TPR - RETURNS INNER PRODUCT OF TWO VECTORS.
!     DL7ITV - APPLY INVERSE-TRANSPOSE OF COMPACT LOWER TRIANG. MATRIX.
!     DL7IVM - APPLY INVERSE OF COMPACT LOWER TRIANG. MATRIX.
!     DV7CPY  - COPIES ONE VECTOR TO ANOTHER.
!     DV2NRM - RETURNS 2-NORM OF A VECTOR.
!
!     ***  REFERENCES  ***
!
!     1.  DENNIS, J.E., GAY, D.M., AND WELSCH, R.E. (1981), AN ADAPTIVE
!             NONLINEAR LEAST-SQUARES ALGORITHM, ACM TRANS. MATH.
!             SOFTWARE, VOL. 7, NO. 3.
!     2.  GAY, D.M. (1981), COMPUTING OPTIMAL LOCALLY CONSTRAINED STEPS,
!             SIAM J. SCI. STATIST. COMPUTING, VOL. 2, NO. 2, PP.
!             186-197.
!     3.  LAWSON, C.L., AND HANSON, R.J. (1974), SOLVING LEAST SQUARES
!             PROBLEMS, PRENTICE-HALL, ENGLEWOOD CLIFFS, N.J.
!     4.  MORE, J.J. (1978), THE LEVENBERG-MARQUARDT ALGORITHM, IMPLEMEN-
!             TATION AND THEORY, PP.105-116 OF SPRINGER LECTURE NOTES
!             IN MATHEMATICS NO. 630, EDITED BY G.A. WATSON, SPRINGER-
!             VERLAG, BERLIN AND NEW YORK.
!
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY.
!     THIS SUBROUTINE WAS WRITTEN IN CONNECTION WITH RESEARCH
!     SUPPORTED BY THE NATIONAL SCIENCE FOUNDATION UNDER GRANTS
!     MCS-7600324, DCR75-10143, 76-14311DSS, MCS76-11989, AND
!     MCS-7906671.
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  CONSTANTS  ***
!
!     ***  INTRINSIC FUNCTIONS  ***
!     /+
!/
!     ***  EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!
!     ***  SUBSCRIPTS FOR V  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: dfac = 256.E+0_wp
      Real (Kind=wp), Parameter        :: eight = 8.E+0_wp
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: negone = -1.E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: p001 = 1.E-3_wp
      Real (Kind=wp), Parameter        :: three = 3.E+0_wp
      Real (Kind=wp), Parameter        :: ttol = 2.5E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: dgnorm = 1, dst0 = 3, dstnrm = 2,    &
                                          epslon = 19, gtstep = 4, nreduc = 6, &
                                          phmnfc = 20, phmxfc = 21,            &
                                          preduc = 7, rad0 = 9, radius = 8,    &
                                          stppar = 5
!     .. Scalar Arguments ..
      Integer                          :: ierr, ka, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(p), g(p), qtr(p), r(*), step(p),   &
                                          v(21), w(*)
      Integer                          :: ipivot(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: a, adi, alphak, b, d1, d2, dfacsq,   &
                                          dst, dtol, lk, oldphi, phi, phimax,  &
                                          phimin, psifac, rad, si, sj, sqrtak, &
                                          t, twopsi, uk, wl
      Real (Kind=wp), Save             :: big
      Integer                          :: dstsav, i, i1, ip1, j1, k, kalim, l, &
                                          lk0, phipin, pp1o2, res, res0, rmat, &
                                          rmat0, uk0
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, dl7svn, dr7mdc, dv2nrm
      External                         :: dl7itv, dl7ivm, dv7cpy
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max, min, sqrt
!     .. Data Statements ..
      Data big/0.E+0_wp/
!     .. Executable Statements ..
!
!     ***  BODY  ***
!
!     ***  FOR USE IN RECOMPUTING STEP, THE FINAL VALUES OF LK AND UK,
!     ***  THE INVERSE DERIVATIVE OF MORE*S PHI AT 0 (FOR NONSING. J)
!     ***  AND THE VALUE RETURNED AS V(DSTNRM) ARE STORED AT W(LK0),
!     ***  W(UK0), W(PHIPIN), AND W(DSTSAV) RESPECTIVELY.
      lk0 = p + 1
      phipin = lk0 + 1
      uk0 = phipin + 1
      dstsav = uk0 + 1
      rmat0 = dstsav
!     ***  A COPY OF THE R-MATRIX FROM THE QR DECOMPOSITION OF J IS
!     ***  STORED IN W STARTING AT W(RMAT), AND A COPY OF THE RESIDUAL
!     ***  VECTOR IS STORED IN W STARTING AT W(RES).  THE LOOPS BELOW
!     ***  THAT UPDATE THE QR DECOMP. FOR A NONZERO MARQUARDT PARAMETER
!     ***  WORK ON THESE COPIES.
      rmat = rmat0 + 1
      pp1o2 = p*(p+1)/2
      res0 = pp1o2 + rmat0
      res = res0 + 1
      rad = v(radius)
      If (rad>zero) psifac = v(epslon)/((eight*(v(phmnfc)+one)+three)*rad**2)
      If (big<=zero) big = dr7mdc(6)
      phimax = v(phmxfc)*rad
      phimin = v(phmnfc)*rad
!     ***  DTOL, DFAC, AND DFACSQ ARE USED IN RESCALING THE FAST GIVENS
!     ***  REPRESENTATION OF THE UPDATED QR DECOMPOSITION.
      dtol = one/dfac
      dfacsq = dfac*dfac
!     ***  OLDPHI IS USED TO DETECT LIMITS OF NUMERICAL ACCURACY.  IF
!     ***  WE RECOMPUTE STEP AND IT DOES NOT CHANGE, THEN WE ACCEPT IT.
      oldphi = zero
      lk = zero
      uk = zero
      kalim = ka + 12
!
!     ***  START OR RESTART, DEPENDING ON KA  ***
!
      If (ka==0) Go To 100
      If (ka>0) Go To 260
!
!     ***  FRESH START -- COMPUTE V(NREDUC)  ***
!
      ka = 0
      kalim = 12
      k = p
      If (ierr/=0) k = abs(ierr) - 1
      v(nreduc) = half*dd7tpr(k,qtr,qtr)
!
!     ***  SET UP TO TRY INITIAL GAUSS-NEWTON STEP  ***
!
100   v(dst0) = negone
      If (ierr/=0) Go To 120
      t = dl7svn(p,r,step,w(res))
      If (t>=one) Go To 110
      If (dv2nrm(p,qtr)>=big*t) Go To 120
!
!     ***  COMPUTE GAUSS-NEWTON STEP  ***
!
!     ***  NOTE -- THE R-MATRIX IS STORED COMPACTLY BY COLUMNS IN
!     ***  R(1), R(2), R(3), ...  IT IS THE TRANSPOSE OF A
!     ***  LOWER TRIANGULAR MATRIX STORED COMPACTLY BY ROWS, AND WE
!     ***  TREAT IT AS SUCH WHEN USING DL7ITV AND DL7IVM.
110   Call dl7itv(p,w,r,qtr)
!     ***  TEMPORARILY STORE PERMUTED -D*STEP IN STEP.
      Do i = 1, p
        j1 = ipivot(i)
        step(i) = d(j1)*w(i)
      End Do
      dst = dv2nrm(p,step)
      v(dst0) = dst
      phi = dst - rad
      If (phi<=phimax) Go To 290
!     ***  IF THIS IS A RESTART, GO TO 110  ***
      If (ka>0) Go To 130
!
!     ***  GAUSS-NEWTON STEP WAS UNACCEPTABLE.  COMPUTE L0  ***
!
      Do i = 1, p
        j1 = ipivot(i)
        step(i) = d(j1)*(step(i)/dst)
      End Do
      Call dl7ivm(p,step,r,step)
      t = one/dv2nrm(p,step)
      w(phipin) = (t/rad)*t
      lk = phi*w(phipin)
!
!     ***  COMPUTE U0  ***
!
120   Do i = 1, p
        w(i) = g(i)/d(i)
      End Do
      v(dgnorm) = dv2nrm(p,w)
      uk = v(dgnorm)/rad
      If (uk<=zero) Go To 280
!
!     ***  ALPHAK WILL BE USED AS THE CURRENT MARQUARDT PARAMETER.  WE
!     ***  USE MORE*S SCHEME FOR INITIALIZING IT.
!
      alphak = abs(v(stppar))*v(rad0)/rad
      alphak = min(uk,max(alphak,lk))
!
!
!     ***  TOP OF LOOP -- INCREMENT KA, COPY R TO RMAT, QTR TO RES  ***
!
130   ka = ka + 1
      Call dv7cpy(pp1o2,w(rmat),r)
      Call dv7cpy(p,w(res),qtr)
!
!     ***  SAFEGUARD ALPHAK AND INITIALIZE FAST GIVENS SCALE VECTOR.  ***
!
      If (alphak<=zero .Or. alphak<lk .Or. alphak>=uk) alphak = uk*            &
        max(p001,sqrt(lk/uk))
      If (alphak<=zero) alphak = half*uk
      sqrtak = sqrt(alphak)
      Do i = 1, p
        w(i) = one
      End Do
!
!     ***  ADD ALPHAK*D AND UPDATE QR DECOMP. USING FAST GIVENS TRANS.  ***
!
      Do i = 1, p
!        ***  GENERATE, APPLY 1ST GIVENS TRANS. FOR ROW I OF ALPHAK*D.
!        ***  (USE STEP TO STORE TEMPORARY ROW)  ***
        l = i*(i+1)/2 + rmat0
        wl = w(l)
        d2 = one
        d1 = w(i)
        j1 = ipivot(i)
        adi = sqrtak*d(j1)
        If (adi>=abs(wl)) Go To 150
140     a = adi/wl
        b = d2*a/d1
        t = a*b + one
        If (t>ttol) Go To 150
        w(i) = d1/t
        d2 = d2/t
        w(l) = t*wl
        a = -a
        Do j1 = i, p
          l = l + j1
          step(j1) = a*w(l)
        End Do
        Go To 160
!
150     b = wl/adi
        a = d1*b/d2
        t = a*b + one
        If (t>ttol) Go To 140
        w(i) = d2/t
        d2 = d1/t
        w(l) = t*adi
        Do j1 = i, p
          l = l + j1
          wl = w(l)
          step(j1) = -wl
          w(l) = a*wl
        End Do
!
160     If (i==p) Go To 220
!
!        ***  NOW USE GIVENS TRANS. TO ZERO ELEMENTS OF TEMP. ROW  ***
!
        ip1 = i + 1
        Do i1 = ip1, p
          si = step(i1-1)
          If (si==zero) Go To 210
          l = i1*(i1+1)/2 + rmat0
          wl = w(l)
          d1 = w(i1)
!
!             ***  RESCALE ROW I1 IF NECESSARY  ***
!
          If (d1>=dtol) Go To 170
          d1 = d1*dfacsq
          wl = wl/dfac
          k = l
          Do j1 = i1, p
            k = k + j1
            w(k) = w(k)/dfac
          End Do
!
!             ***  USE GIVENS TRANS. TO ZERO NEXT ELEMENT OF TEMP. ROW
!
170       If (abs(si)>abs(wl)) Go To 190
180       a = si/wl
          b = d2*a/d1
          t = a*b + one
          If (t>ttol) Go To 190
          w(l) = t*wl
          w(i1) = d1/t
          d2 = d2/t
          Do j1 = i1, p
            l = l + j1
            wl = w(l)
            sj = step(j1)
            w(l) = wl + b*sj
            step(j1) = sj - a*wl
          End Do
          Go To 200
!
190       b = wl/si
          a = d1*b/d2
          t = a*b + one
          If (t>ttol) Go To 180
          w(i1) = d2/t
          d2 = d1/t
          w(l) = t*si
          Do j1 = i1, p
            l = l + j1
            wl = w(l)
            sj = step(j1)
            w(l) = a*wl + sj
            step(j1) = b*sj - wl
          End Do
!
!             ***  RESCALE TEMP. ROW IF NECESSARY  ***
!
200       If (d2>=dtol) Go To 210
          d2 = d2*dfacsq
          Do k = i1, p
            step(k) = step(k)/dfac
          End Do
210     End Do
      End Do
!
!     ***  COMPUTE STEP  ***
!
220   Call dl7itv(p,w(res),w(rmat),w(res))
!     ***  RECOVER STEP AND STORE PERMUTED -D*STEP AT W(RES)  ***
      Do i = 1, p
        j1 = ipivot(i)
        k = res0 + i
        t = w(k)
        step(j1) = -t
        w(k) = t*d(j1)
      End Do
      dst = dv2nrm(p,w(res))
      phi = dst - rad
      If (phi<=phimax .And. phi>=phimin) Go To 300
      If (oldphi==phi) Go To 300
      oldphi = phi
!
!     ***  CHECK FOR (AND HANDLE) SPECIAL CASE  ***
!
      If (phi>zero) Go To 240
      If (ka>=kalim) Go To 300
      twopsi = alphak*dst*dst - dd7tpr(p,step,g)
      If (alphak>=twopsi*psifac) Go To 240
      v(stppar) = -alphak
      Go To 310
!
!     ***  UNACCEPTABLE STEP -- UPDATE LK, UK, ALPHAK, AND TRY AGAIN  ***
!
230   If (phi<zero) uk = min(uk,alphak)
      Go To 250
240   If (phi<zero) uk = alphak
250   Do i = 1, p
        j1 = ipivot(i)
        k = res0 + i
        step(i) = d(j1)*(w(k)/dst)
      End Do
      Call dl7ivm(p,step,w(rmat),step)
      Do i = 1, p
        step(i) = step(i)/sqrt(w(i))
      End Do
      t = one/dv2nrm(p,step)
      alphak = alphak + t*phi*t/rad
      lk = max(lk,alphak)
      alphak = lk
      Go To 130
!
!     ***  RESTART  ***
!
260   lk = w(lk0)
      uk = w(uk0)
      If (v(dst0)>zero .And. v(dst0)-rad<=phimax) Go To 100
      alphak = abs(v(stppar))
      dst = w(dstsav)
      phi = dst - rad
      t = v(dgnorm)/rad
      If (rad>v(rad0)) Go To 270
!
!        ***  SMALLER RADIUS  ***
      uk = t
      If (alphak<=zero) lk = zero
      If (v(dst0)>zero) lk = max(lk,(v(dst0)-rad)*w(phipin))
      Go To 230
!
!     ***  BIGGER RADIUS  ***
270   If (alphak<=zero .Or. uk>t) uk = t
      lk = zero
      If (v(dst0)>zero) lk = max(lk,(v(dst0)-rad)*w(phipin))
      Go To 230
!
!     ***  SPECIAL CASE -- RAD .LE. 0 OR (G = 0 AND J IS SINGULAR)  ***
!
280   v(stppar) = zero
      dst = zero
      lk = zero
      uk = zero
      v(gtstep) = zero
      v(preduc) = zero
      Do i = 1, p
        step(i) = zero
      End Do
      Go To 320
!
!     ***  ACCEPTABLE GAUSS-NEWTON STEP -- RECOVER STEP FROM W  ***
!
290   alphak = zero
      Do i = 1, p
        j1 = ipivot(i)
        step(j1) = -w(i)
      End Do
!
!     ***  SAVE VALUES FOR USE IN A POSSIBLE RESTART  ***
!
300   v(stppar) = alphak
310   v(gtstep) = min(dd7tpr(p,step,g),zero)
      v(preduc) = half*(alphak*dst*dst-v(gtstep))
320   v(dstnrm) = dst
      w(dstsav) = dst
      w(lk0) = lk
      w(uk0) = uk
      v(rad0) = rad
!
      Return
!
!     ***  LAST CARD OF DL7MST FOLLOWS  ***
    End Subroutine dl7mst
    Subroutine drmnfb(b,d,fx,iv,liv,lv,p,v,x)
!
!     ***  ITERATION DRIVER FOR  DMNF...
!     ***  MINIMIZE GENERAL UNCONSTRAINED OBJECTIVE FUNCTION USING
!     ***  FINITE-DIFFERENCE GRADIENTS AND SECANT HESSIAN APPROXIMATIONS.
!
!     DIMENSION IV(59 + P), V(77 + P*(P+23)/2)
!
!     ***  PURPOSE  ***
!
!        THIS ROUTINE INTERACTS WITH SUBROUTINE  DRMNGB  IN AN ATTEMPT
!     TO FIND AN P-VECTOR  X*  THAT MINIMIZES THE (UNCONSTRAINED)
!     OBJECTIVE FUNCTION  FX = F(X)  COMPUTED BY THE CALLER.  (OFTEN
!     THE  X*  FOUND IS A LOCAL MINIMIZER RATHER THAN A GLOBAL ONE.)
!
!     ***  PARAMETERS  ***
!
!        THE PARAMETERS FOR DRMNFB ARE THE SAME AS THOSE FOR  DMNG
!     (WHICH SEE), EXCEPT THAT CALCF, CALCG, UIPARM, URPARM, AND UFPARM
!     ARE OMITTED, AND A PARAMETER  FX  FOR THE OBJECTIVE FUNCTION
!     VALUE AT X IS ADDED.  INSTEAD OF CALLING CALCG TO OBTAIN THE
!     GRADIENT OF THE OBJECTIVE FUNCTION AT X, DRMNFB CALLS DS3GRD,
!     WHICH COMPUTES AN APPROXIMATION TO THE GRADIENT BY FINITE
!     (FORWARD AND CENTRAL) DIFFERENCES USING THE METHOD OF REF. 1.
!     THE FOLLOWING INPUT COMPONENT IS OF INTEREST IN THIS REGARD
!     (AND IS NOT DESCRIBED IN  DMNG).
!
!     V(ETA0)..... V(42) IS AN ESTIMATED BOUND ON THE RELATIVE ERROR IN THE
!             OBJECTIVE FUNCTION VALUE COMPUTED BY CALCF...
!                  (TRUE VALUE) = (COMPUTED VALUE) * (1 + E),
!             WHERE ABS(E) .LE. V(ETA0).  DEFAULT = MACHEP * 10**3,
!             WHERE MACHEP IS THE UNIT ROUNDOFF.
!
!        THE OUTPUT VALUES IV(NFCALL) AND IV(NGCALL) HAVE DIFFERENT
!     MEANINGS FOR  DMNF THAN FOR  DMNG...
!
!     IV(NFCALL)... IV(6) IS THE NUMBER OF CALLS SO FAR MADE ON CALCF (I.E.,
!             FUNCTION EVALUATIONS) EXCLUDING THOSE MADE ONLY FOR
!             COMPUTING GRADIENTS.  THE INPUT VALUE IV(MXFCAL) IS A
!             LIMIT ON IV(NFCALL).
!     IV(NGCALL)... IV(30) IS THE NUMBER OF FUNCTION EVALUATIONS MADE ONLY
!             FOR COMPUTING GRADIENTS.  THE TOTAL NUMBER OF FUNCTION
!             EVALUATIONS IS THUS  IV(NFCALL) + IV(NGCALL).
!
!     ***  REFERENCES  ***
!
!     1. STEWART, G.W. (1967), A MODIFICATION OF DAVIDON*S MINIMIZATION
!        METHOD TO ACCEPT DIFFERENCE APPROXIMATIONS OF DERIVATIVES,
!        J. ASSOC. COMPUT. MACH. 14, PP. 72-83.
!.
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY (AUGUST 1982).
!
!     ----------------------------  DECLARATIONS  ---------------------------
!
!
!     DIVSET.... SUPPLIES DEFAULT PARAMETER VALUES.
!     DD7TPR... RETURNS INNER PRODUCT OF TWO VECTORS.
!     DS3GRD... COMPUTES FINITE-DIFFERENCE GRADIENT APPROXIMATION.
!     DRMNGB... REVERSE-COMMUNICATION ROUTINE THAT DOES  DMNGB ALGORITHM.
!     DV7SCP... SETS ALL ELEMENTS OF A VECTOR TO A SCALAR.
!
!
!     ***  SUBSCRIPTS FOR IV   ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: eta0 = 42, f = 10, g = 28,           &
                                          lmat = 42, nextv = 47, ngcall = 30,  &
                                          niter = 31, perm = 58, sgirc = 57,   &
                                          toobig = 2, vneed = 4
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: fx
      Integer                          :: liv, lv, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(2,p), d(p), v(lv), x(p)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Integer                          :: alpha, alpha0, g1, i, ipi, iv1, j,   &
                                          k, w
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr
      External                         :: divset, drmngb, ds3grd, dv7scp
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      iv1 = iv(1)
      If (iv1==1) Go To 100
      If (iv1==2) Go To 120
      If (iv(1)==0) Call divset(2,iv,liv,lv,v)
      iv1 = iv(1)
      If (iv1==12 .Or. iv1==13) iv(vneed) = iv(vneed) + 2*p + 6
      If (iv1==14) Go To 100
      If (iv1>2 .And. iv1<12) Go To 100
      g1 = 1
      If (iv1==12) iv(1) = 13
      Go To 110
!
100   g1 = iv(g)
!
110   Call drmngb(b,d,fx,v(g1),iv,liv,lv,p,v,x)
      If (iv(1)<2) Go To 160
      If (iv(1)>2) Go To 150
!
!     ***  COMPUTE GRADIENT  ***
!
      If (iv(niter)==0) Call dv7scp(p,v(g1),zero)
      j = iv(lmat)
      alpha0 = g1 - p - 1
      ipi = iv(perm)
      Do i = 1, p
        k = alpha0 + iv(ipi)
        v(k) = dd7tpr(i,v(j),v(j))
        ipi = ipi + 1
        j = j + i
      End Do
!     ***  UNDO INCREMENT OF IV(NGCALL) DONE BY DRMNGB  ***
      iv(ngcall) = iv(ngcall) - 1
!     ***  STORE RETURN CODE FROM DS3GRD IN IV(SGIRC)  ***
      iv(sgirc) = 0
!     ***  X MAY HAVE BEEN RESTORED, SO COPY BACK FX... ***
      fx = v(f)
      Go To 130
!
!     ***  GRADIENT LOOP  ***
!
120   If (iv(toobig)/=0) Go To 100
!
130   g1 = iv(g)
      alpha = g1 - p
      w = alpha - 6
      Call ds3grd(v(alpha),b,d,v(eta0),fx,v(g1),iv(sgirc),p,v(w),x)
      i = iv(sgirc)
      If (i==0) Go To 100
      If (i<=p) Go To 140
      iv(toobig) = 1
      Go To 100
!
140   iv(ngcall) = iv(ngcall) + 1
      Go To 160
!
150   If (iv(1)/=14) Go To 160
!
!     ***  STORAGE ALLOCATION  ***
!
      iv(g) = iv(nextv) + p + 6
      iv(nextv) = iv(g) + p
      If (iv1/=13) Go To 100
!
160   Return
!     ***  LAST CARD OF DRMNFB FOLLOWS  ***
    End Subroutine drmnfb
    Subroutine d7egr(n,indrow,jpntr,indcol,ipntr,ndeg,iwa,bwa)
!     **********
!
!     SUBROUTINE D7EGR
!
!     GIVEN THE SPARSITY PATTERN OF AN M BY N MATRIX A,
!     THIS SUBROUTINE DETERMINES THE DEGREE SEQUENCE FOR
!     THE INTERSECTION GRAPH OF THE COLUMNS OF A.
!
!     IN GRAPH-THEORY TERMINOLOGY, THE INTERSECTION GRAPH OF
!     THE COLUMNS OF A IS THE LOOPLESS GRAPH G WITH VERTICES
!     A(J), J = 1,2,...,N WHERE A(J) IS THE J-TH COLUMN OF A
!     AND WITH EDGE (A(I),A(J)) IF AND ONLY IF COLUMNS I AND J
!     HAVE A NON-ZERO IN THE SAME ROW POSITION.
!
!     NOTE THAT THE VALUE OF M IS NOT NEEDED BY D7EGR AND IS
!     THEREFORE NOT PRESENT IN THE SUBROUTINE STATEMENT.
!
!     THE SUBROUTINE STATEMENT IS
!
!       SUBROUTINE D7EGR(N,INDROW,JPNTR,INDCOL,IPNTR,NDEG,IWA,BWA)
!
!     WHERE
!
!       N IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE NUMBER
!         OF COLUMNS OF A.
!
!       INDROW IS AN INTEGER INPUT ARRAY WHICH CONTAINS THE ROW
!         INDICES FOR THE NON-ZEROES IN THE MATRIX A.
!
!       JPNTR IS AN INTEGER INPUT ARRAY OF LENGTH N + 1 WHICH
!         SPECIFIES THE LOCATIONS OF THE ROW INDICES IN INDROW.
!         THE ROW INDICES FOR COLUMN J ARE
!
!               INDROW(K), K = JPNTR(J),...,JPNTR(J+1)-1.
!
!         NOTE THAT JPNTR(N+1)-1 IS THEN THE NUMBER OF NON-ZERO
!         ELEMENTS OF THE MATRIX A.
!
!       INDCOL IS AN INTEGER INPUT ARRAY WHICH CONTAINS THE
!         COLUMN INDICES FOR THE NON-ZEROES IN THE MATRIX A.
!
!       IPNTR IS AN INTEGER INPUT ARRAY OF LENGTH M + 1 WHICH
!         SPECIFIES THE LOCATIONS OF THE COLUMN INDICES IN INDCOL.
!         THE COLUMN INDICES FOR ROW I ARE
!
!               INDCOL(K), K = IPNTR(I),...,IPNTR(I+1)-1.
!
!         NOTE THAT IPNTR(M+1)-1 IS THEN THE NUMBER OF NON-ZERO
!         ELEMENTS OF THE MATRIX A.
!
!       NDEG IS AN INTEGER OUTPUT ARRAY OF LENGTH N WHICH
!         SPECIFIES THE DEGREE SEQUENCE. THE DEGREE OF THE
!         J-TH COLUMN OF A IS NDEG(J).
!
!       IWA IS AN INTEGER WORK ARRAY OF LENGTH N.
!
!       BWA IS A LOGICAL WORK ARRAY OF LENGTH N.
!
!     ARGONNE NATIONAL LABORATORY. MINPACK PROJECT. JUNE 1982.
!     THOMAS F. COLEMAN, BURTON S. GARBOW, JORGE J. MORE
!
!     **********

!     .. Implicit None Statement ..
      Implicit None
!     .. Scalar Arguments ..
      Integer                          :: n
!     .. Array Arguments ..
      Integer                          :: indcol(1), indrow(1), ipntr(1),      &
                                          iwa(n), jpntr(1), ndeg(n)
      Logical                          :: bwa(n)
!     .. Local Scalars ..
      Integer                          :: deg, ic, ip, ipl, ipu, ir, jcol, jp, &
                                          jpl, jpu
!     .. Executable Statements ..
!
!     INITIALIZATION BLOCK.
!
      Do jp = 1, n
        ndeg(jp) = 0
        bwa(jp) = .False.
      End Do
!
!     COMPUTE THE DEGREE SEQUENCE BY DETERMINING THE CONTRIBUTIONS
!     TO THE DEGREES FROM THE CURRENT(JCOL) COLUMN AND FURTHER
!     COLUMNS WHICH HAVE NOT YET BEEN CONSIDERED.
!
      If (n<2) Go To 130
      Do jcol = 2, n
        bwa(jcol) = .True.
        deg = 0
!
!        DETERMINE ALL POSITIONS (IR,JCOL) WHICH CORRESPOND
!        TO NON-ZEROES IN THE MATRIX.
!
        jpl = jpntr(jcol)
        jpu = jpntr(jcol+1) - 1
        If (jpu<jpl) Go To 110
        Do jp = jpl, jpu
          ir = indrow(jp)
!
!           FOR EACH ROW IR, DETERMINE ALL POSITIONS (IR,IC)
!           WHICH CORRESPOND TO NON-ZEROES IN THE MATRIX.
!
          ipl = ipntr(ir)
          ipu = ipntr(ir+1) - 1
          Do ip = ipl, ipu
            ic = indcol(ip)
!
!              ARRAY BWA MARKS COLUMNS WHICH HAVE CONTRIBUTED TO
!              THE DEGREE COUNT OF COLUMN JCOL. UPDATE THE DEGREE
!              COUNTS OF THESE COLUMNS. ARRAY IWA RECORDS THE
!              MARKED COLUMNS.
!
            If (bwa(ic)) Go To 100
            bwa(ic) = .True.
            ndeg(ic) = ndeg(ic) + 1
            deg = deg + 1
            iwa(deg) = ic
100         Continue
          End Do
        End Do
110     Continue
!
!        UN-MARK THE COLUMNS RECORDED BY IWA AND FINALIZE THE
!        DEGREE COUNT OF COLUMN JCOL.
!
        If (deg<1) Go To 120
        Do jp = 1, deg
          ic = iwa(jp)
          bwa(ic) = .False.
        End Do
        ndeg(jcol) = ndeg(jcol) + deg
120     Continue
      End Do
130   Continue
      Return
!
!     LAST CARD OF SUBROUTINE D7EGR.
!
    End Subroutine d7egr
    Subroutine drmng(d,fx,g,iv,liv,lv,n,v,x)
!
!     ***  CARRY OUT  DMNG (UNCONSTRAINED MINIMIZATION) ITERATIONS, USING
!     ***  DOUBLE-DOGLEG/BFGS STEPS.
!
!     ***  PARAMETER DECLARATIONS  ***
!
!
!     --------------------------  PARAMETER USAGE  --------------------------
!
!     D.... SCALE VECTOR.
!     FX... FUNCTION VALUE.
!     G.... GRADIENT VECTOR.
!     IV... INTEGER VALUE ARRAY.
!     LIV.. LENGTH OF IV (AT LEAST 60).
!     LV... LENGTH OF V (AT LEAST 71 + N*(N+13)/2).
!     N.... NUMBER OF VARIABLES (COMPONENTS IN X AND G).
!     V.... FLOATING-POINT VALUE ARRAY.
!     X.... VECTOR OF PARAMETERS TO BE OPTIMIZED.
!
!     ***  DISCUSSION  ***
!
!        PARAMETERS IV, N, V, AND X ARE THE SAME AS THE CORRESPONDING
!     ONES TO  DMNG (WHICH SEE), EXCEPT THAT V CAN BE SHORTER (SINCE
!     THE PART OF V THAT  DMNG USES FOR STORING G IS NOT NEEDED).
!     MOREOVER, COMPARED WITH  DMNG, IV(1) MAY HAVE THE TWO ADDITIONAL
!     OUTPUT VALUES 1 AND 2, WHICH ARE EXPLAINED BELOW, AS IS THE USE
!     OF IV(TOOBIG) AND IV(NFGCAL).  THE VALUE IV(G), WHICH IS AN
!     OUTPUT VALUE FROM  DMNG (AND  DMNF), IS NOT REFERENCED BY
!     DRMNG OR THE SUBROUTINES IT CALLS.
!        FX AND G NEED NOT HAVE BEEN INITIALIZED WHEN DRMNG IS CALLED
!     WITH IV(1) = 12, 13, OR 14.
!
!     IV(1) = 1 MEANS THE CALLER SHOULD SET FX TO F(X), THE FUNCTION VALUE
!             AT X, AND CALL DRMNG AGAIN, HAVING CHANGED NONE OF THE
!             OTHER PARAMETERS.  AN EXCEPTION OCCURS IF F(X) CANNOT BE
!             (E.G. IF OVERFLOW WOULD OCCUR), WHICH MAY HAPPEN BECAUSE
!             OF AN OVERSIZED STEP.  IN THIS CASE THE CALLER SHOULD SET
!             IV(TOOBIG) = IV(2) TO 1, WHICH WILL CAUSE DRMNG TO IG-
!             NORE FX AND TRY A SMALLER STEP.  THE PARAMETER NF THAT
!              DMNG PASSES TO CALCF (FOR POSSIBLE USE BY CALCG) IS A
!             COPY OF IV(NFCALL) = IV(6).
!     IV(1) = 2 MEANS THE CALLER SHOULD SET G TO G(X), THE GRADIENT VECTOR
!             OF F AT X, AND CALL DRMNG AGAIN, HAVING CHANGED NONE OF
!             THE OTHER PARAMETERS EXCEPT POSSIBLY THE SCALE VECTOR D
!             WHEN IV(DTYPE) = 0.  THE PARAMETER NF THAT  DMNG PASSES
!             TO CALCG IS IV(NFGCAL) = IV(7).  IF G(X) CANNOT BE
!             EVALUATED, THEN THE CALLER MAY SET IV(TOOBIG) TO 0, IN
!             WHICH CASE DRMNG WILL RETURN WITH IV(1) = 65.
!.
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY (DECEMBER 1979).  REVISED SEPT. 1982.
!     THIS SUBROUTINE WAS WRITTEN IN CONNECTION WITH RESEARCH SUPPORTED
!     IN PART BY THE NATIONAL SCIENCE FOUNDATION UNDER GRANTS
!     MCS-7600324 AND MCS-7906671.
!
!        (SEE  DMNG FOR REFERENCES.)
!
!     +++++++++++++++++++++++++++  DECLARATIONS  ++++++++++++++++++++++++++++
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  CONSTANTS  ***
!
!
!     ***  NO INTRINSIC FUNCTIONS  ***
!
!     ***  EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!
!     DA7SST.... ASSESSES CANDIDATE STEP.
!     DD7DOG.... COMPUTES DOUBLE-DOGLEG (CANDIDATE) STEP.
!     DIVSET.... SUPPLIES DEFAULT IV AND V INPUT COMPONENTS.
!     DD7TPR... RETURNS INNER PRODUCT OF TWO VECTORS.
!     DITSUM.... PRINTS ITERATION SUMMARY AND INFO ON INITIAL AND FINAL X.
!     DL7ITV... MULTIPLIES INVERSE TRANSPOSE OF LOWER TRIANGLE TIMES VECTOR.
!     DL7IVM... MULTIPLIES INVERSE OF LOWER TRIANGLE TIMES VECTOR.
!     DL7TVM... MULTIPLIES TRANSPOSE OF LOWER TRIANGLE TIMES VECTOR.
!     LUPDT.... UPDATES CHOLESKY FACTOR OF HESSIAN APPROXIMATION.
!     DL7VML.... MULTIPLIES LOWER TRIANGLE TIMES VECTOR.
!     DPARCK.... CHECKS VALIDITY OF INPUT IV AND V VALUES.
!     DRLDST... COMPUTES V(RELDX) = RELATIVE STEP SIZE.
!     STOPX.... RETURNS .TRUE. IF THE BREAK KEY HAS BEEN PRESSED.
!     DV2AXY.... COMPUTES SCALAR TIMES ONE VECTOR PLUS ANOTHER.
!     DV7CPY.... COPIES ONE VECTOR TO ANOTHER.
!     DV7SCP... SETS ALL ELEMENTS OF A VECTOR TO A SCALAR.
!     DV7VMP... MULTIPLIES VECTOR BY VECTOR RAISED TO POWER (COMPONENTWISE).
!     DV2NRM... RETURNS THE 2-NORM OF A VECTOR.
!     DW7ZBF... COMPUTES W AND Z FOR DL7UPD CORRESPONDING TO BFGS UPDATE.
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!
!     ***  IV SUBSCRIPT VALUES  ***
!
!
!     ***  V SUBSCRIPT VALUES  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: negone = -1.E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: onep2 = 1.2E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: cnvcod = 55, dg = 37, dgnorm = 1,    &
                                          dinit = 38, dst0 = 3, dstnrm = 2,    &
                                          f = 10, f0 = 13, fdif = 11, g0 = 48, &
                                          gthg = 44, gtstep = 4, incfac = 23,  &
                                          inith = 25, irc = 29, kagqt = 33,    &
                                          lmat = 42, lmax0 = 35, lmaxs = 36,   &
                                          mode = 35, model = 5, mxfcal = 17,   &
                                          mxiter = 18, nextv = 47, nfcall = 6, &
                                          nfgcal = 7, ngcall = 30, niter = 31, &
                                          nreduc = 6, nwtstp = 34, preduc = 7, &
                                          rad0 = 9, radfac = 16, radinc = 8,   &
                                          radius = 8, reldx = 17, restor = 9,  &
                                          step = 40, stglim = 11, stlstg = 41, &
                                          toobig = 2, tuner4 = 29,             &
                                          tuner5 = 30, vneed = 4, x0 = 43,     &
                                          xirc = 13
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: fx
      Integer                          :: liv, lv, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(n), g(n), v(lv), x(n)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t
      Integer                          :: dg1, dummy, g01, i, k, l, lstgst,    &
                                          nwtst1, rstrst, step1, temp1, w,     &
                                          x01, z
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, drldst, dv2nrm
      Logical, External                :: stopx
      External                         :: da7sst, dd7dog, ditsum, divset,      &
                                          dl7itv, dl7ivm, dl7tvm, dl7upd,      &
                                          dl7vml, dparck, dv2axy, dv7cpy,      &
                                          dv7scp, dv7vmp, dw7zbf
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      i = iv(1)
      If (i==1) Go To 130
      If (i==2) Go To 140
!
!     ***  CHECK VALIDITY OF IV AND V INPUT VALUES  ***
!
      If (iv(1)==0) Call divset(2,iv,liv,lv,v)
      If (iv(1)==12 .Or. iv(1)==13) iv(vneed) = iv(vneed) + n*(n+13)/2
      Call dparck(2,d,iv,liv,lv,n,v)
      i = iv(1) - 2
      If (i>12) Go To 440
      Go To (270,270,270,270,270,270,200,170,200,100,100,110) i
!
!     ***  STORAGE ALLOCATION  ***
!
100   l = iv(lmat)
      iv(x0) = l + n*(n+1)/2
      iv(step) = iv(x0) + n
      iv(stlstg) = iv(step) + n
      iv(g0) = iv(stlstg) + n
      iv(nwtstp) = iv(g0) + n
      iv(dg) = iv(nwtstp) + n
      iv(nextv) = iv(dg) + n
      If (iv(1)/=13) Go To 110
      iv(1) = 14
      Go To 440
!
!     ***  INITIALIZATION  ***
!
110   iv(niter) = 0
      iv(nfcall) = 1
      iv(ngcall) = 1
      iv(nfgcal) = 1
      iv(mode) = -1
      iv(model) = 1
      iv(stglim) = 1
      iv(toobig) = 0
      iv(cnvcod) = 0
      iv(radinc) = 0
      v(rad0) = zero
      If (v(dinit)>=zero) Call dv7scp(n,d,v(dinit))
      If (iv(inith)/=1) Go To 120
!
!     ***  SET THE INITIAL HESSIAN APPROXIMATION TO DIAG(D)**-2  ***
!
      l = iv(lmat)
      Call dv7scp(n*(n+1)/2,v(l),zero)
      k = l - 1
      Do i = 1, n
        k = k + i
        t = d(i)
        If (t<=zero) t = one
        v(k) = t
      End Do
!
!     ***  COMPUTE INITIAL FUNCTION VALUE  ***
!
120   iv(1) = 1
      Go To 440
!
130   v(f) = fx
      If (iv(mode)>=0) Go To 270
      v(f0) = fx
      iv(1) = 2
      If (iv(toobig)==0) Go To 440
      iv(1) = 63
      Go To 430
!
!     ***  MAKE SURE GRADIENT COULD BE COMPUTED  ***
!
140   If (iv(toobig)==0) Go To 150
      iv(1) = 65
      Go To 430
!
150   dg1 = iv(dg)
      Call dv7vmp(n,v(dg1),g,d,-1)
      v(dgnorm) = dv2nrm(n,v(dg1))
!
      If (iv(cnvcod)/=0) Go To 420
      If (iv(mode)==0) Go To 380
!
!     ***  ALLOW FIRST STEP TO HAVE SCALED 2-NORM AT MOST V(LMAX0)  ***
!
      v(radius) = v(lmax0)
!
      iv(mode) = 0
!
!
!     -----------------------------  MAIN LOOP  -----------------------------
!
!
!     ***  PRINT ITERATION SUMMARY, CHECK ITERATION LIMIT  ***
!
160   Call ditsum(d,g,iv,liv,lv,n,v,x)
170   k = iv(niter)
      If (k<iv(mxiter)) Go To 180
      iv(1) = 10
      Go To 430
!
!     ***  UPDATE RADIUS  ***
!
180   iv(niter) = k + 1
      If (k>0) v(radius) = v(radfac)*v(dstnrm)
!
!     ***  INITIALIZE FOR START OF NEXT ITERATION  ***
!
      g01 = iv(g0)
      x01 = iv(x0)
      v(f0) = v(f)
      iv(irc) = 4
      iv(kagqt) = -1
!
!     ***  COPY X TO X0, G TO G0  ***
!
      Call dv7cpy(n,v(x01),x)
      Call dv7cpy(n,v(g01),g)
!
!     ***  CHECK STOPX AND FUNCTION EVALUATION LIMIT  ***
!
190   If (.Not. stopx(dummy)) Go To 210
      iv(1) = 11
      Go To 220
!
!     ***  COME HERE WHEN RESTARTING AFTER FUNC. EVAL. LIMIT OR STOPX.
!
200   If (v(f)>=v(f0)) Go To 210
      v(radfac) = one
      k = iv(niter)
      Go To 180
!
210   If (iv(nfcall)<iv(mxfcal)) Go To 230
      iv(1) = 9
220   If (v(f)>=v(f0)) Go To 430
!
!        ***  IN CASE OF STOPX OR FUNCTION EVALUATION LIMIT WITH
!        ***  IMPROVED V(F), EVALUATE THE GRADIENT AT X.
!
      iv(cnvcod) = iv(1)
      Go To 370
!
!     . . . . . . . . . . . . .  COMPUTE CANDIDATE STEP  . . . . . . . . . .
!
230   step1 = iv(step)
      dg1 = iv(dg)
      nwtst1 = iv(nwtstp)
      If (iv(kagqt)>=0) Go To 240
      l = iv(lmat)
      Call dl7ivm(n,v(nwtst1),v(l),g)
      v(nreduc) = half*dd7tpr(n,v(nwtst1),v(nwtst1))
      Call dl7itv(n,v(nwtst1),v(l),v(nwtst1))
      Call dv7vmp(n,v(step1),v(nwtst1),d,1)
      v(dst0) = dv2nrm(n,v(step1))
      Call dv7vmp(n,v(dg1),v(dg1),d,-1)
      Call dl7tvm(n,v(step1),v(l),v(dg1))
      v(gthg) = dv2nrm(n,v(step1))
      iv(kagqt) = 0
240   Call dd7dog(v(dg1),lv,n,v(nwtst1),v(step1),v)
      If (iv(irc)/=6) Go To 250
      If (iv(restor)/=2) Go To 270
      rstrst = 2
      Go To 280
!
!     ***  CHECK WHETHER EVALUATING F(X0 + STEP) LOOKS WORTHWHILE  ***
!
250   iv(toobig) = 0
      If (v(dstnrm)<=zero) Go To 270
      If (iv(irc)/=5) Go To 260
      If (v(radfac)<=one) Go To 260
      If (v(preduc)>onep2*v(fdif)) Go To 260
      If (iv(restor)/=2) Go To 270
      rstrst = 0
      Go To 280
!
!     ***  COMPUTE F(X0 + STEP)  ***
!
260   x01 = iv(x0)
      step1 = iv(step)
      Call dv2axy(n,x,one,v(step1),v(x01))
      iv(nfcall) = iv(nfcall) + 1
      iv(1) = 1
      Go To 440
!
!     . . . . . . . . . . . . .  ASSESS CANDIDATE STEP  . . . . . . . . . . .
!
270   rstrst = 3
280   x01 = iv(x0)
      v(reldx) = drldst(n,d,x,v(x01))
      Call da7sst(iv,liv,lv,v)
      step1 = iv(step)
      lstgst = iv(stlstg)
      i = iv(restor) + 1
      Go To (320,290,300,310) i
290   Call dv7cpy(n,x,v(x01))
      Go To 320
300   Call dv7cpy(n,v(lstgst),v(step1))
      Go To 320
310   Call dv7cpy(n,v(step1),v(lstgst))
      Call dv2axy(n,x,one,v(step1),v(x01))
      v(reldx) = drldst(n,d,x,v(x01))
      iv(restor) = rstrst
!
320   k = iv(irc)
      Go To (330,360,360,360,330,340,350,350,350,350,350,350,410,380) k
!
!     ***  RECOMPUTE STEP WITH CHANGED RADIUS  ***
!
330   v(radius) = v(radfac)*v(dstnrm)
      Go To 190
!
!     ***  COMPUTE STEP OF LENGTH V(LMAXS) FOR SINGULAR CONVERGENCE TEST.
!
340   v(radius) = v(lmaxs)
      Go To 230
!
!     ***  CONVERGENCE OR FALSE CONVERGENCE  ***
!
350   iv(cnvcod) = k - 4
      If (v(f)>=v(f0)) Go To 420
      If (iv(xirc)==14) Go To 420
      iv(xirc) = 14
!
!     . . . . . . . . . . . .  PROCESS ACCEPTABLE STEP  . . . . . . . . . . .
!
360   If (iv(irc)/=3) Go To 370
      step1 = iv(step)
      temp1 = iv(stlstg)
!
!     ***  SET  TEMP1 = HESSIAN * STEP  FOR USE IN GRADIENT TESTS  ***
!
      l = iv(lmat)
      Call dl7tvm(n,v(temp1),v(l),v(step1))
      Call dl7vml(n,v(temp1),v(l),v(temp1))
!
!     ***  COMPUTE GRADIENT  ***
!
370   iv(ngcall) = iv(ngcall) + 1
      iv(1) = 2
      Go To 440
!
!     ***  INITIALIZATIONS -- G0 = G - G0, ETC.  ***
!
380   g01 = iv(g0)
      Call dv2axy(n,v(g01),negone,v(g01),g)
      step1 = iv(step)
      temp1 = iv(stlstg)
      If (iv(irc)/=3) Go To 400
!
!     ***  SET V(RADFAC) BY GRADIENT TESTS  ***
!
!     ***  SET  TEMP1 = DIAG(D)**-1 * (HESSIAN*STEP + (G(X0)-G(X)))  ***
!
      Call dv2axy(n,v(temp1),negone,v(g01),v(temp1))
      Call dv7vmp(n,v(temp1),v(temp1),d,-1)
!
!        ***  DO GRADIENT TESTS  ***
!
      If (dv2nrm(n,v(temp1))<=v(dgnorm)*v(tuner4)) Go To 390
      If (dd7tpr(n,g,v(step1))>=v(gtstep)*v(tuner5)) Go To 400
390   v(radfac) = v(incfac)
!
!     ***  UPDATE H, LOOP  ***
!
400   w = iv(nwtstp)
      z = iv(x0)
      l = iv(lmat)
      Call dw7zbf(v(l),n,v(step1),v(w),v(g01),v(z))
!
!     ** USE THE N-VECTORS STARTING AT V(STEP1) AND V(G01) FOR SCRATCH..
      Call dl7upd(v(temp1),v(step1),v(l),v(g01),v(l),n,v(w),v(z))
      iv(1) = 2
      Go To 160
!
!     . . . . . . . . . . . . . .  MISC. DETAILS  . . . . . . . . . . . . . .
!
!     ***  BAD PARAMETERS TO ASSESS  ***
!
410   iv(1) = 64
      Go To 430
!
!     ***  PRINT SUMMARY OF FINAL ITERATION AND OTHER REQUESTED ITEMS  ***
!
420   iv(1) = iv(cnvcod)
      iv(cnvcod) = 0
430   Call ditsum(d,g,iv,liv,lv,n,v,x)
!
440   Return
!
!     ***  LAST LINE OF DRMNG FOLLOWS  ***
    End Subroutine drmng
    Subroutine i7do(m,n,indrow,jpntr,indcol,ipntr,ndeg,list,maxclq,iwa1,iwa2,  &
      iwa3,iwa4,bwa)
!     **********
!
!     SUBROUTINE I7DO
!
!     GIVEN THE SPARSITY PATTERN OF AN M BY N MATRIX A, THIS
!     SUBROUTINE DETERMINES AN INCIDENCE-DEGREE ORDERING OF THE
!     COLUMNS OF A.
!
!     THE INCIDENCE-DEGREE ORDERING IS DEFINED FOR THE LOOPLESS
!     GRAPH G WITH VERTICES A(J), J = 1,2,...,N WHERE A(J) IS THE
!     J-TH COLUMN OF A AND WITH EDGE (A(I),A(J)) IF AND ONLY IF
!     COLUMNS I AND J HAVE A NON-ZERO IN THE SAME ROW POSITION.
!
!     AT EACH STAGE OF I7DO, A COLUMN OF MAXIMAL INCIDENCE IS
!     CHOSEN AND ORDERED. IF JCOL IS AN UN-ORDERED COLUMN, THEN
!     THE INCIDENCE OF JCOL IS THE NUMBER OF ORDERED COLUMNS
!     ADJACENT TO JCOL IN THE GRAPH G. AMONG ALL THE COLUMNS OF
!     MAXIMAL INCIDENCE,I7DO CHOOSES A COLUMN OF MAXIMAL DEGREE.
!
!     THE SUBROUTINE STATEMENT IS
!
!       SUBROUTINE I7DO(M,N,INDROW,JPNTR,INDCOL,IPNTR,NDEG,LIST,
!                      MAXCLQ,IWA1,IWA2,IWA3,IWA4,BWA)
!
!     WHERE
!
!       M IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE NUMBER
!         OF ROWS OF A.
!
!       N IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE NUMBER
!         OF COLUMNS OF A.
!
!       INDROW IS AN INTEGER INPUT ARRAY WHICH CONTAINS THE ROW
!         INDICES FOR THE NON-ZEROES IN THE MATRIX A.
!
!       JPNTR IS AN INTEGER INPUT ARRAY OF LENGTH N + 1 WHICH
!         SPECIFIES THE LOCATIONS OF THE ROW INDICES IN INDROW.
!         THE ROW INDICES FOR COLUMN J ARE
!
!               INDROW(K), K = JPNTR(J),...,JPNTR(J+1)-1.
!
!         NOTE THAT JPNTR(N+1)-1 IS THEN THE NUMBER OF NON-ZERO
!         ELEMENTS OF THE MATRIX A.
!
!       INDCOL IS AN INTEGER INPUT ARRAY WHICH CONTAINS THE
!         COLUMN INDICES FOR THE NON-ZEROES IN THE MATRIX A.
!
!       IPNTR IS AN INTEGER INPUT ARRAY OF LENGTH M + 1 WHICH
!         SPECIFIES THE LOCATIONS OF THE COLUMN INDICES IN INDCOL.
!         THE COLUMN INDICES FOR ROW I ARE
!
!               INDCOL(K), K = IPNTR(I),...,IPNTR(I+1)-1.
!
!         NOTE THAT IPNTR(M+1)-1 IS THEN THE NUMBER OF NON-ZERO
!         ELEMENTS OF THE MATRIX A.
!
!       NDEG IS AN INTEGER INPUT ARRAY OF LENGTH N WHICH SPECIFIES
!         THE DEGREE SEQUENCE. THE DEGREE OF THE J-TH COLUMN
!         OF A IS NDEG(J).
!
!       LIST IS AN INTEGER OUTPUT ARRAY OF LENGTH N WHICH SPECIFIES
!         THE INCIDENCE-DEGREE ORDERING OF THE COLUMNS OF A. THE J-TH
!         COLUMN IN THIS ORDER IS LIST(J).
!
!       MAXCLQ IS AN INTEGER OUTPUT VARIABLE SET TO THE SIZE
!         OF THE LARGEST CLIQUE FOUND DURING THE ORDERING.
!
!       IWA1,IWA2,IWA3, AND IWA4 ARE INTEGER WORK ARRAYS OF LENGTH N.
!
!       BWA IS A LOGICAL WORK ARRAY OF LENGTH N.
!
!     SUBPROGRAMS CALLED
!
!       MINPACK-SUPPLIED ... N7MSRT
!
!       FORTRAN-SUPPLIED ... MAX0
!
!     ARGONNE NATIONAL LABORATORY. MINPACK PROJECT. JUNE 1982.
!     THOMAS F. COLEMAN, BURTON S. GARBOW, JORGE J. MORE
!
!     **********

!     .. Implicit None Statement ..
      Implicit None
!     .. Scalar Arguments ..
      Integer                          :: m, maxclq, n
!     .. Array Arguments ..
      Integer                          :: indcol(1), indrow(1), ipntr(1),      &
                                          iwa1(n), iwa2(n), iwa3(n), iwa4(n),  &
                                          jpntr(1), list(n), ndeg(n)
      Logical                          :: bwa(n)
!     .. Local Scalars ..
      Integer                          :: deg, head, ic, ip, ipl, ipu, ir,     &
                                          jcol, jp, jpl, jpu, l, maxinc,       &
                                          maxlst, ncomp, numinc, numlst,       &
                                          numord, numwgt
!     .. External Procedures ..
      External                         :: n7msrt
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max
!     .. Executable Statements ..
!
!     SORT THE DEGREE SEQUENCE.
!
      Call n7msrt(n,n-1,ndeg,-1,iwa4,iwa1,iwa3)
!
!     INITIALIZATION BLOCK.
!
!     CREATE A DOUBLY-LINKED LIST TO ACCESS THE INCIDENCES OF THE
!     COLUMNS. THE POINTERS FOR THE LINKED LIST ARE AS FOLLOWS.
!
!     EACH UN-ORDERED COLUMN JCOL IS IN A LIST (THE INCIDENCE LIST)
!     OF COLUMNS WITH THE SAME INCIDENCE.
!
!     IWA1(NUMINC+1) IS THE FIRST COLUMN IN THE NUMINC LIST
!     UNLESS IWA1(NUMINC+1) = 0. IN THIS CASE THERE ARE
!     NO COLUMNS IN THE NUMINC LIST.
!
!     IWA2(JCOL) IS THE COLUMN BEFORE JCOL IN THE INCIDENCE LIST
!     UNLESS IWA2(JCOL) = 0. IN THIS CASE JCOL IS THE FIRST
!     COLUMN IN THIS INCIDENCE LIST.
!
!     IWA3(JCOL) IS THE COLUMN AFTER JCOL IN THE INCIDENCE LIST
!     UNLESS IWA3(JCOL) = 0. IN THIS CASE JCOL IS THE LAST
!     COLUMN IN THIS INCIDENCE LIST.
!
!     IF JCOL IS AN UN-ORDERED COLUMN, THEN LIST(JCOL) IS THE
!     INCIDENCE OF JCOL IN THE GRAPH. IF JCOL IS AN ORDERED COLUMN,
!     THEN LIST(JCOL) IS THE INCIDENCE-DEGREE ORDER OF COLUMN JCOL.
!
      maxinc = 0
      Do jp = 1, n
        list(jp) = 0
        bwa(jp) = .False.
        iwa1(jp) = 0
        l = iwa4(jp)
        If (jp/=1) iwa2(l) = iwa4(jp-1)
        If (jp/=n) iwa3(l) = iwa4(jp+1)
      End Do
      iwa1(1) = iwa4(1)
      l = iwa4(1)
      iwa2(l) = 0
      l = iwa4(n)
      iwa3(l) = 0
!
!     DETERMINE THE MAXIMAL SEARCH LENGTH FOR THE LIST
!     OF COLUMNS OF MAXIMAL INCIDENCE.
!
      maxlst = 0
      Do ir = 1, m
        maxlst = maxlst + (ipntr(ir+1)-ipntr(ir))**2
      End Do
      maxlst = maxlst/n
      maxclq = 1
!
!     BEGINNING OF ITERATION LOOP.
!
      Do numord = 1, n
!
!        CHOOSE A COLUMN JCOL OF MAXIMAL DEGREE AMONG THE
!        COLUMNS OF MAXIMAL INCIDENCE.
!
        jp = iwa1(maxinc+1)
        numlst = 1
        numwgt = -1
100     Continue
        If (ndeg(jp)<=numwgt) Go To 110
        numwgt = ndeg(jp)
        jcol = jp
110     Continue
        jp = iwa3(jp)
        numlst = numlst + 1
        If (jp>0 .And. numlst<=maxlst) Go To 100
        list(jcol) = numord
!
!        DELETE COLUMN JCOL FROM THE LIST OF COLUMNS OF
!        MAXIMAL INCIDENCE.
!
        l = iwa2(jcol)
        If (l==0) iwa1(maxinc+1) = iwa3(jcol)
        If (l>0) iwa3(l) = iwa3(jcol)
        l = iwa3(jcol)
        If (l>0) iwa2(l) = iwa2(jcol)
!
!        UPDATE THE SIZE OF THE LARGEST CLIQUE
!        FOUND DURING THE ORDERING.
!
        If (maxinc==0) ncomp = 0
        ncomp = ncomp + 1
        If (maxinc+1==ncomp) maxclq = max(maxclq,ncomp)
!
!        UPDATE THE MAXIMAL INCIDENCE COUNT.
!
120     Continue
        If (iwa1(maxinc+1)>0) Go To 130
        maxinc = maxinc - 1
        If (maxinc>=0) Go To 120
130     Continue
!
!        FIND ALL COLUMNS ADJACENT TO COLUMN JCOL.
!
        bwa(jcol) = .True.
        deg = 0
!
!        DETERMINE ALL POSITIONS (IR,JCOL) WHICH CORRESPOND
!        TO NON-ZEROES IN THE MATRIX.
!
        jpl = jpntr(jcol)
        jpu = jpntr(jcol+1) - 1
        If (jpu<jpl) Go To 150
        Do jp = jpl, jpu
          ir = indrow(jp)
!
!           FOR EACH ROW IR, DETERMINE ALL POSITIONS (IR,IC)
!           WHICH CORRESPOND TO NON-ZEROES IN THE MATRIX.
!
          ipl = ipntr(ir)
          ipu = ipntr(ir+1) - 1
          Do ip = ipl, ipu
            ic = indcol(ip)
!
!              ARRAY BWA MARKS COLUMNS WHICH ARE ADJACENT TO
!              COLUMN JCOL. ARRAY IWA4 RECORDS THE MARKED COLUMNS.
!
            If (bwa(ic)) Go To 140
            bwa(ic) = .True.
            deg = deg + 1
            iwa4(deg) = ic
140         Continue
          End Do
        End Do
150     Continue
!
!        UPDATE THE POINTERS TO THE INCIDENCE LISTS.
!
        If (deg<1) Go To 170
        Do jp = 1, deg
          ic = iwa4(jp)
          If (list(ic)>0) Go To 160
          numinc = -list(ic) + 1
          list(ic) = -numinc
          maxinc = max(maxinc,numinc)
!
!           DELETE COLUMN IC FROM THE NUMINC-1 LIST.
!
          l = iwa2(ic)
          If (l==0) iwa1(numinc) = iwa3(ic)
          If (l>0) iwa3(l) = iwa3(ic)
          l = iwa3(ic)
          If (l>0) iwa2(l) = iwa2(ic)
!
!           ADD COLUMN IC TO THE NUMINC LIST.
!
          head = iwa1(numinc+1)
          iwa1(numinc+1) = ic
          iwa2(ic) = 0
          iwa3(ic) = head
          If (head>0) iwa2(head) = ic
160       Continue
!
!           UN-MARK COLUMN IC IN THE ARRAY BWA.
!
          bwa(ic) = .False.
        End Do
170     Continue
        bwa(jcol) = .False.
!
!        END OF ITERATION LOOP.
!
      End Do
!
!     INVERT THE ARRAY LIST.
!
      Do jcol = 1, n
        numord = list(jcol)
        iwa1(numord) = jcol
      End Do
      Do jp = 1, n
        list(jp) = iwa1(jp)
      End Do
      Return
!
!     LAST CARD OF SUBROUTINE I7DO.
!
    End Subroutine i7do
    Subroutine m7slo(n,indrow,jpntr,indcol,ipntr,ndeg,list,maxclq,iwa1,iwa2,   &
      iwa3,iwa4,bwa)
!     **********
!
!     SUBROUTINE M7SLO
!
!     GIVEN THE SPARSITY PATTERN OF AN M BY N MATRIX A, THIS
!     SUBROUTINE DETERMINES THE SMALLEST-LAST ORDERING OF THE
!     COLUMNS OF A.
!
!     THE SMALLEST-LAST ORDERING IS DEFINED FOR THE LOOPLESS
!     GRAPH G WITH VERTICES A(J), J = 1,2,...,N WHERE A(J) IS THE
!     J-TH COLUMN OF A AND WITH EDGE (A(I),A(J)) IF AND ONLY IF
!     COLUMNS I AND J HAVE A NON-ZERO IN THE SAME ROW POSITION.
!
!     THE SMALLEST-LAST ORDERING IS DETERMINED RECURSIVELY BY
!     LETTING LIST(K), K = N,...,1 BE A COLUMN WITH LEAST DEGREE
!     IN THE SUBGRAPH SPANNED BY THE UN-ORDERED COLUMNS.
!
!     NOTE THAT THE VALUE OF M IS NOT NEEDED BY M7SLO AND IS
!     THEREFORE NOT PRESENT IN THE SUBROUTINE STATEMENT.
!
!     THE SUBROUTINE STATEMENT IS
!
!       SUBROUTINE M7SLO(N,INDROW,JPNTR,INDCOL,IPNTR,NDEG,LIST,
!                      MAXCLQ,IWA1,IWA2,IWA3,IWA4,BWA)
!
!     WHERE
!
!       N IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE NUMBER
!         OF COLUMNS OF A.
!
!       INDROW IS AN INTEGER INPUT ARRAY WHICH CONTAINS THE ROW
!         INDICES FOR THE NON-ZEROES IN THE MATRIX A.
!
!       JPNTR IS AN INTEGER INPUT ARRAY OF LENGTH N + 1 WHICH
!         SPECIFIES THE LOCATIONS OF THE ROW INDICES IN INDROW.
!         THE ROW INDICES FOR COLUMN J ARE
!
!               INDROW(K), K = JPNTR(J),...,JPNTR(J+1)-1.
!
!         NOTE THAT JPNTR(N+1)-1 IS THEN THE NUMBER OF NON-ZERO
!         ELEMENTS OF THE MATRIX A.
!
!       INDCOL IS AN INTEGER INPUT ARRAY WHICH CONTAINS THE
!         COLUMN INDICES FOR THE NON-ZEROES IN THE MATRIX A.
!
!       IPNTR IS AN INTEGER INPUT ARRAY OF LENGTH M + 1 WHICH
!         SPECIFIES THE LOCATIONS OF THE COLUMN INDICES IN INDCOL.
!         THE COLUMN INDICES FOR ROW I ARE
!
!               INDCOL(K), K = IPNTR(I),...,IPNTR(I+1)-1.
!
!         NOTE THAT IPNTR(M+1)-1 IS THEN THE NUMBER OF NON-ZERO
!         ELEMENTS OF THE MATRIX A.
!
!       NDEG IS AN INTEGER INPUT ARRAY OF LENGTH N WHICH SPECIFIES
!         THE DEGREE SEQUENCE. THE DEGREE OF THE J-TH COLUMN
!         OF A IS NDEG(J).
!
!       LIST IS AN INTEGER OUTPUT ARRAY OF LENGTH N WHICH SPECIFIES
!         THE SMALLEST-LAST ORDERING OF THE COLUMNS OF A. THE J-TH
!         COLUMN IN THIS ORDER IS LIST(J).
!
!       MAXCLQ IS AN INTEGER OUTPUT VARIABLE SET TO THE SIZE
!         OF THE LARGEST CLIQUE FOUND DURING THE ORDERING.
!
!       IWA1,IWA2,IWA3, AND IWA4 ARE INTEGER WORK ARRAYS OF LENGTH N.
!
!       BWA IS A LOGICAL WORK ARRAY OF LENGTH N.
!
!     SUBPROGRAMS CALLED
!
!       FORTRAN-SUPPLIED ... MIN0
!
!     ARGONNE NATIONAL LABORATORY. MINPACK PROJECT. JUNE 1982.
!     THOMAS F. COLEMAN, BURTON S. GARBOW, JORGE J. MORE
!
!     **********

!     .. Implicit None Statement ..
      Implicit None
!     .. Scalar Arguments ..
      Integer                          :: maxclq, n
!     .. Array Arguments ..
      Integer                          :: indcol(1), indrow(1), ipntr(1),      &
                                          iwa1(n), iwa2(n), iwa3(n), iwa4(n),  &
                                          jpntr(1), list(n), ndeg(n)
      Logical                          :: bwa(n)
!     .. Local Scalars ..
      Integer                          :: deg, head, ic, ip, ipl, ipu, ir,     &
                                          jcol, jp, jpl, jpu, l, mindeg,       &
                                          numdeg, numord
!     .. Intrinsic Procedures ..
      Intrinsic                        :: min
!     .. Executable Statements ..
!
!     INITIALIZATION BLOCK.
!
      mindeg = n
      Do jp = 1, n
        iwa1(jp) = 0
        bwa(jp) = .False.
        list(jp) = ndeg(jp)
        mindeg = min(mindeg,ndeg(jp))
      End Do
!
!     CREATE A DOUBLY-LINKED LIST TO ACCESS THE DEGREES OF THE
!     COLUMNS. THE POINTERS FOR THE LINKED LIST ARE AS FOLLOWS.
!
!     EACH UN-ORDERED COLUMN JCOL IS IN A LIST (THE DEGREE
!     LIST) OF COLUMNS WITH THE SAME DEGREE.
!
!     IWA1(NUMDEG+1) IS THE FIRST COLUMN IN THE NUMDEG LIST
!     UNLESS IWA1(NUMDEG+1) = 0. IN THIS CASE THERE ARE
!     NO COLUMNS IN THE NUMDEG LIST.
!
!     IWA2(JCOL) IS THE COLUMN BEFORE JCOL IN THE DEGREE LIST
!     UNLESS IWA2(JCOL) = 0. IN THIS CASE JCOL IS THE FIRST
!     COLUMN IN THIS DEGREE LIST.
!
!     IWA3(JCOL) IS THE COLUMN AFTER JCOL IN THE DEGREE LIST
!     UNLESS IWA3(JCOL) = 0. IN THIS CASE JCOL IS THE LAST
!     COLUMN IN THIS DEGREE LIST.
!
!     IF JCOL IS AN UN-ORDERED COLUMN, THEN LIST(JCOL) IS THE
!     DEGREE OF JCOL IN THE GRAPH INDUCED BY THE UN-ORDERED
!     COLUMNS. IF JCOL IS AN ORDERED COLUMN, THEN LIST(JCOL)
!     IS THE SMALLEST-LAST ORDER OF COLUMN JCOL.
!
      Do jp = 1, n
        numdeg = ndeg(jp)
        head = iwa1(numdeg+1)
        iwa1(numdeg+1) = jp
        iwa2(jp) = 0
        iwa3(jp) = head
        If (head>0) iwa2(head) = jp
      End Do
      maxclq = 0
      numord = n
!
!     BEGINNING OF ITERATION LOOP.
!
100   Continue
!
!        MARK THE SIZE OF THE LARGEST CLIQUE
!        FOUND DURING THE ORDERING.
!
      If (mindeg+1==numord .And. maxclq==0) maxclq = numord
!
!        CHOOSE A COLUMN JCOL OF MINIMAL DEGREE MINDEG.
!
110   Continue
      jcol = iwa1(mindeg+1)
      If (jcol>0) Go To 120
      mindeg = mindeg + 1
      Go To 110
120   Continue
      list(jcol) = numord
      numord = numord - 1
!
!        TERMINATION TEST.
!
      If (numord==0) Go To 160
!
!        DELETE COLUMN JCOL FROM THE MINDEG LIST.
!
      l = iwa3(jcol)
      iwa1(mindeg+1) = l
      If (l>0) iwa2(l) = 0
!
!        FIND ALL COLUMNS ADJACENT TO COLUMN JCOL.
!
      bwa(jcol) = .True.
      deg = 0
!
!        DETERMINE ALL POSITIONS (IR,JCOL) WHICH CORRESPOND
!        TO NON-ZEROES IN THE MATRIX.
!
      jpl = jpntr(jcol)
      jpu = jpntr(jcol+1) - 1
      If (jpu<jpl) Go To 140
      Do jp = jpl, jpu
        ir = indrow(jp)
!
!           FOR EACH ROW IR, DETERMINE ALL POSITIONS (IR,IC)
!           WHICH CORRESPOND TO NON-ZEROES IN THE MATRIX.
!
        ipl = ipntr(ir)
        ipu = ipntr(ir+1) - 1
        Do ip = ipl, ipu
          ic = indcol(ip)
!
!              ARRAY BWA MARKS COLUMNS WHICH ARE ADJACENT TO
!              COLUMN JCOL. ARRAY IWA4 RECORDS THE MARKED COLUMNS.
!
          If (bwa(ic)) Go To 130
          bwa(ic) = .True.
          deg = deg + 1
          iwa4(deg) = ic
130       Continue
        End Do
      End Do
140   Continue
!
!        UPDATE THE POINTERS TO THE CURRENT DEGREE LISTS.
!
      If (deg<1) Go To 150
      Do jp = 1, deg
        ic = iwa4(jp)
        numdeg = list(ic)
        list(ic) = list(ic) - 1
        mindeg = min(mindeg,list(ic))
!
!           DELETE COLUMN IC FROM THE NUMDEG LIST.
!
        l = iwa2(ic)
        If (l==0) iwa1(numdeg+1) = iwa3(ic)
        If (l>0) iwa3(l) = iwa3(ic)
        l = iwa3(ic)
        If (l>0) iwa2(l) = iwa2(ic)
!
!           ADD COLUMN IC TO THE NUMDEG-1 LIST.
!
        head = iwa1(numdeg)
        iwa1(numdeg) = ic
        iwa2(ic) = 0
        iwa3(ic) = head
        If (head>0) iwa2(head) = ic
!
!           UN-MARK COLUMN IC IN THE ARRAY BWA.
!
        bwa(ic) = .False.
      End Do
150   Continue
!
!        END OF ITERATION LOOP.
!
      Go To 100
160   Continue
!
!     INVERT THE ARRAY LIST.
!
      Do jcol = 1, n
        numord = list(jcol)
        iwa1(numord) = jcol
      End Do
      Do jp = 1, n
        list(jp) = iwa1(jp)
      End Do
      Return
!
!     LAST CARD OF SUBROUTINE M7SLO.
!
    End Subroutine m7slo
    Subroutine ds7dmp(n,x,y,z,k)
!
!     ***  SET X = DIAG(Z)**K * Y * DIAG(Z)**K
!     ***  FOR X, Y = COMPACTLY STORED LOWER TRIANG. MATRICES
!     ***  K = 1 OR -1.
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: k, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: x(*), y(*), z(n)
!     .. Local Scalars ..
      Real (Kind=wp), Save             :: one
      Real (Kind=wp)                   :: t
      Integer                          :: i, j, l
!     .. Data Statements ..
      Data one/1.E+0_wp/
!     .. Executable Statements ..
!
      l = 1
      If (k>=0) Go To 100
      Do i = 1, n
        t = one/z(i)
        Do j = 1, i
          x(l) = t*y(l)/z(j)
          l = l + 1
        End Do
      End Do
      Go To 110
!
100   Do i = 1, n
        t = z(i)
        Do j = 1, i
          x(l) = t*y(l)*z(j)
          l = l + 1
        End Do
      End Do
110   Return
!     ***  LAST CARD OF DS7DMP FOLLOWS  ***
    End Subroutine ds7dmp
    Subroutine ds7bqn(b,d,dst,ipiv,ipiv1,ipiv2,kb,l,lv,ns,p,p1,step,td,tg,v,w, &
      x,x0)
!
!     ***  COMPUTE BOUNDED MODIFIED NEWTON STEP  ***
!
!     DIMENSION L(P*(P+1)/2)
!
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  V SUBSCRIPTS  ***
!
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Integer, Parameter               :: dstnrm = 2, gtstep = 4, phmnfc = 20, &
                                          phmxfc = 21, preduc = 7, radius = 8, &
                                          stppar = 5
!     .. Scalar Arguments ..
      Integer                          :: kb, lv, ns, p, p1
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(2,p), d(p), dst(p), l(*), step(p), &
                                          td(p), tg(p), v(lv), w(p), x(p),     &
                                          x0(p)
      Integer                          :: ipiv(p), ipiv1(p), ipiv2(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: alpha, dst0, dst1, dstmax, dstmin,   &
                                          dx, gts, t, t1, ti, xi
      Real (Kind=wp), Save             :: fudge, half, meps2, one, two, zero
      Integer                          :: i, j, k, p0, p1m1
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, dr7mdc, dv2nrm
      External                         :: dl7itv, dl7ivm, dq7rsh, dv2axy,      &
                                          dv7cpy, dv7ipr, dv7scp, dv7shf,      &
                                          i7shft
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max
!     .. Data Statements ..
      Data fudge/1.0001E+0_wp/, half/0.5E+0_wp/, meps2/0.E+0_wp/,              &
        one/1.0E+0_wp/, two/2.E+0_wp/, zero/0.E+0_wp/
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      dstmax = fudge*(one+v(phmxfc))*v(radius)
      dstmin = (one+v(phmnfc))*v(radius)
      dst1 = zero
      If (meps2<=zero) meps2 = two*dr7mdc(3)
      p0 = p1
      ns = 0
      Do i = 1, p
        ipiv1(i) = i
        ipiv2(i) = i
      End Do
      Do i = 1, p1
        w(i) = -step(i)*td(i)
      End Do
      alpha = abs(v(stppar))
      v(preduc) = zero
      gts = -v(gtstep)
      If (kb<0) Call dv7scp(p,dst,zero)
      kb = 1
!
!     ***  -W = D TIMES RESTRICTED NEWTON STEP FROM X + DST/D.
!
!     ***  FIND T SUCH THAT X - T*W IS STILL FEASIBLE.
!
100   t = one
      k = 0
      Do i = 1, p1
        j = ipiv(i)
        dx = w(i)/d(j)
        xi = x(j) - dx
        If (xi<b(1,j)) Go To 110
        If (xi<=b(2,j)) Go To 130
        ti = (x(j)-b(2,j))/dx
        k = i
        Go To 120
110     ti = (x(j)-b(1,j))/dx
        k = -i
120     If (t<=ti) Go To 130
        t = ti
130   End Do
!
      If (p>p1) Call dv7cpy(p-p1,step(p1+1),dst(p1+1))
      Call dv2axy(p1,step,-t,w,dst)
      dst0 = dst1
      dst1 = dv2nrm(p,step)
!
!     ***  CHECK FOR OVERSIZE STEP  ***
!
      If (dst1<=dstmax) Go To 150
      If (p1>=p0) Go To 140
      If (dst0<dstmin) kb = 0
      Go To 170
!
140   k = 0
!
!     ***  UPDATE DST, TG, AND V(PREDUC)  ***
!
150   v(dstnrm) = dst1
      Call dv7cpy(p1,dst,step)
      t1 = one - t
      Do i = 1, p1
        tg(i) = t1*tg(i)
      End Do
      If (alpha>zero) Call dv2axy(p1,tg,t*alpha,w,tg)
      v(preduc) = v(preduc) + t*((one-half*t)*gts+half*alpha*t*dd7tpr(p1,w,w))
      If (k==0) Go To 170
!
!     ***  PERMUTE L, ETC. IF NECESSARY  ***
!
      p1m1 = p1 - 1
      j = abs(k)
      If (j==p1) Go To 160
      ns = ns + 1
      ipiv2(p1) = j
      Call dq7rsh(j,p1,.False.,tg,l,w)
      Call i7shft(p1,j,ipiv)
      Call i7shft(p1,j,ipiv1)
      Call dv7shf(p1,j,tg)
      Call dv7shf(p1,j,dst)
160   If (k<0) ipiv(p1) = -ipiv(p1)
      p1 = p1m1
      If (p1<=0) Go To 170
      Call dl7ivm(p1,w,l,tg)
      gts = dd7tpr(p1,w,w)
      Call dl7itv(p1,w,l,w)
      Go To 100
!
!     ***  UNSCALE STEP  ***
!
170   Do i = 1, p
        j = abs(ipiv(i))
        step(j) = dst(i)/d(j)
      End Do
!
!     ***  FUDGE STEP TO ENSURE THAT IT FORCES APPROPRIATE COMPONENTS
!     ***  TO THEIR BOUNDS  ***
!
      If (p1>=p0) Go To 190
      k = p1 + 1
      Do i = k, p0
        j = ipiv(i)
        t = meps2
        If (j>0) Go To 180
        t = -t
        j = -j
        ipiv(i) = j
180     t = t*max(abs(x(j)),abs(x0(j)))
        step(j) = step(j) + t
      End Do
!
190   Call dv2axy(p,x,one,step,x0)
      If (ns>0) Call dv7ipr(p0,ipiv1,td)
      Return
!     ***  LAST LINE OF DS7BQN FOLLOWS  ***
    End Subroutine ds7bqn
    Subroutine n7msrt(n,nmax,num,mode,index,last,next)
!     **********.
!
!     SUBROUTINE N7MSRT
!
!     GIVEN A SEQUENCE OF INTEGERS, THIS SUBROUTINE GROUPS
!     TOGETHER THOSE INDICES WITH THE SAME SEQUENCE VALUE
!     AND, OPTIONALLY, SORTS THE SEQUENCE INTO EITHER
!     ASCENDING OR DESCENDING ORDER.
!
!     THE SEQUENCE OF INTEGERS IS DEFINED BY THE ARRAY NUM,
!     AND IT IS ASSUMED THAT THE INTEGERS ARE EACH FROM THE SET
!     0,1,...,NMAX. ON OUTPUT THE INDICES K SUCH THAT NUM(K) = L
!     FOR ANY L = 0,1,...,NMAX CAN BE OBTAINED FROM THE ARRAYS
!     LAST AND NEXT AS FOLLOWS.
!
!           K = LAST(L+1)
!           WHILE (K .NE. 0) K = NEXT(K)
!
!     OPTIONALLY, THE SUBROUTINE PRODUCES AN ARRAY INDEX SO THAT
!     THE SEQUENCE NUM(INDEX(I)), I = 1,2,...,N IS SORTED.
!
!     THE SUBROUTINE STATEMENT IS
!
!       SUBROUTINE N7MSRT(N,NMAX,NUM,MODE,INDEX,LAST,NEXT)
!
!     WHERE
!
!       N IS A POSITIVE INTEGER INPUT VARIABLE.
!
!       NMAX IS A POSITIVE INTEGER INPUT VARIABLE.
!
!       NUM IS AN INPUT ARRAY OF LENGTH N WHICH CONTAINS THE
!         SEQUENCE OF INTEGERS TO BE GROUPED AND SORTED. IT
!         IS ASSUMED THAT THE INTEGERS ARE EACH FROM THE SET
!         0,1,...,NMAX.
!
!       MODE IS AN INTEGER INPUT VARIABLE. THE SEQUENCE NUM IS
!         SORTED IN ASCENDING ORDER IF MODE IS POSITIVE AND IN
!         DESCENDING ORDER IF MODE IS NEGATIVE. IF MODE IS 0,
!         NO SORTING IS DONE.
!
!       INDEX IS AN INTEGER OUTPUT ARRAY OF LENGTH N SET SO
!         THAT THE SEQUENCE
!
!               NUM(INDEX(I)), I = 1,2,...,N
!
!         IS SORTED ACCORDING TO THE SETTING OF MODE. IF MODE
!         IS 0, INDEX IS NOT REFERENCED.
!
!       LAST IS AN INTEGER OUTPUT ARRAY OF LENGTH NMAX + 1. THE
!         INDEX OF NUM FOR THE LAST OCCURRENCE OF L IS LAST(L+1)
!         FOR ANY L = 0,1,...,NMAX UNLESS LAST(L+1) = 0. IN
!         THIS CASE L DOES NOT APPEAR IN NUM.
!
!       NEXT IS AN INTEGER OUTPUT ARRAY OF LENGTH N. IF
!         NUM(K) = L, THEN THE INDEX OF NUM FOR THE PREVIOUS
!         OCCURRENCE OF L IS NEXT(K) FOR ANY L = 0,1,...,NMAX
!         UNLESS NEXT(K) = 0. IN THIS CASE THERE IS NO PREVIOUS
!         OCCURRENCE OF L IN NUM.
!
!     ARGONNE NATIONAL LABORATORY. MINPACK PROJECT. JUNE 1982.
!     THOMAS F. COLEMAN, BURTON S. GARBOW, JORGE J. MORE
!
!     **********

!     .. Implicit None Statement ..
      Implicit None
!     .. Scalar Arguments ..
      Integer                          :: mode, n, nmax
!     .. Array Arguments ..
      Integer                          :: index(n), last(1), next(n), num(n)
!     .. Local Scalars ..
      Integer                          :: i, j, jp, k, l, nmaxp1, nmaxp2
!     .. Executable Statements ..
!
!     DETERMINE THE ARRAYS NEXT AND LAST.
!
      nmaxp1 = nmax + 1
      Do i = 1, nmaxp1
        last(i) = 0
      End Do
      Do k = 1, n
        l = num(k)
        next(k) = last(l+1)
        last(l+1) = k
      End Do
      If (mode==0) Go To 120
!
!     STORE THE POINTERS TO THE SORTED ARRAY IN INDEX.
!
      i = 1
      nmaxp2 = nmaxp1 + 1
      Do j = 1, nmaxp1
        jp = j
        If (mode<0) jp = nmaxp2 - j
        k = last(jp)
100     Continue
        If (k==0) Go To 110
        index(i) = k
        i = i + 1
        k = next(k)
        Go To 100
110     Continue
      End Do
120   Continue
      Return
!
!     LAST CARD OF SUBROUTINE N7MSRT.
!
    End Subroutine n7msrt
    Subroutine dg7lit(d,g,iv,liv,lv,p,ps,v,x,y)
!
!     ***  CARRY OUT NL2SOL-LIKE ITERATIONS FOR GENERALIZED LINEAR   ***
!     ***  REGRESSION PROBLEMS (AND OTHERS OF SIMILAR STRUCTURE)     ***
!
!     ***  PARAMETER DECLARATIONS  ***
!
!
!     --------------------------  PARAMETER USAGE  --------------------------
!
!     D.... SCALE VECTOR.
!     IV... INTEGER VALUE ARRAY.
!     LIV.. LENGTH OF IV.  MUST BE AT LEAST 82.
!     LH... LENGTH OF H = P*(P+1)/2.
!     LV... LENGTH OF V.  MUST BE AT LEAST P*(3*P + 19)/2 + 7.
!     G.... GRADIENT AT X (WHEN IV(1) = 2).
!     P.... NUMBER OF PARAMETERS (COMPONENTS IN X).
!     PS... NUMBER OF NONZERO ROWS AND COLUMNS IN S.
!     V.... FLOATING-POINT VALUE ARRAY.
!     X.... PARAMETER VECTOR.
!     Y.... PART OF YIELD VECTOR (WHEN IV(1)= 2, SCRATCH OTHERWISE).
!
!     ***  DISCUSSION  ***
!
!       DG7LIT PERFORMS NL2SOL-LIKE ITERATIONS FOR A VARIETY OF
!     REGRESSION PROBLEMS THAT ARE SIMILAR TO NONLINEAR LEAST-SQUARES
!     IN THAT THE HESSIAN IS THE SUM OF TWO TERMS, A READILY-COMPUTED
!     FIRST-ORDER TERM AND A SECOND-ORDER TERM.  THE CALLER SUPPLIES
!     THE FIRST-ORDER TERM OF THE HESSIAN IN HC (LOWER TRIANGLE, STORED
!     COMPACTLY BY ROWS IN V, STARTING AT IV(HC)), AND DG7LIT BUILDS AN
!     APPROXIMATION, S, TO THE SECOND-ORDER TERM.  THE CALLER ALSO
!     PROVIDES THE FUNCTION VALUE, GRADIENT, AND PART OF THE YIELD
!     VECTOR USED IN UPDATING S. DG7LIT DECIDES DYNAMICALLY WHETHER OR
!     NOT TO USE S WHEN CHOOSING THE NEXT STEP TO TRY...  THE HESSIAN
!     APPROXIMATION USED IS EITHER HC ALONE (GAUSS-NEWTON MODEL) OR
!     HC + S (AUGMENTED MODEL).
!
!        IF PS .LT. P, THEN ROWS AND COLUMNS PS+1...P OF S ARE KEPT
!     CONSTANT.  THEY WILL BE ZERO UNLESS THE CALLER SETS IV(INITS) TO
!     1 OR 2 AND SUPPLIES NONZERO VALUES FOR THEM, OR THE CALLER SETS
!     IV(INITS) TO 3 OR 4 AND THE FINITE-DIFFERENCE INITIAL S THEN
!     COMPUTED HAS NONZERO VALUES IN THESE ROWS.
!
!        IF IV(INITS) IS 3 OR 4, THEN THE INITIAL S IS COMPUTED BY
!     FINITE DIFFERENCES.  3 MEANS USE FUNCTION DIFFERENCES, 4 MEANS
!     USE GRADIENT DIFFERENCES.  FINITE DIFFERENCING IS DONE THE SAME
!     WAY AS IN COMPUTING A COVARIANCE MATRIX (WITH IV(COVREQ) = -1, -2,
!     1, OR 2).
!
!        FOR UPDATING S,DG7LIT ASSUMES THAT THE GRADIENT HAS THE FORM
!     OF A SUM OVER I OF RHO(I,X)*GRAD(R(I,X)), WHERE GRAD DENOTES THE
!     GRADIENT WITH RESPECT TO X.  THE TRUE SECOND-ORDER TERM THEN IS
!     THE SUM OVER I OF RHO(I,X)*HESSIAN(R(I,X)).  IF X = X0 + STEP,
!     THEN WE WISH TO UPDATE S SO THAT S*STEP IS THE SUM OVER I OF
!     RHO(I,X)*(GRAD(R(I,X)) - GRAD(R(I,X0))).  THE CALLER MUST SUPPLY
!     PART OF THIS IN Y, NAMELY THE SUM OVER I OF
!     RHO(I,X)*GRAD(R(I,X0)), WHEN CALLING DG7LIT WITH IV(1) = 2 AND
!     IV(MODE) = 0 (WHERE MODE = 38).  G THEN CONTANS THE OTHER PART,
!     SO THAT THE DESIRED YIELD VECTOR IS G - Y.  IF PS .LT. P, THEN
!     THE ABOVE DISCUSSION APPLIES ONLY TO THE FIRST PS COMPONENTS OF
!     GRAD(R(I,X)), STEP, AND Y.
!
!        PARAMETERS IV, P, V, AND X ARE THE SAME AS THE CORRESPONDING
!     ONES TO NL2SOL (WHICH SEE), EXCEPT THAT V CAN BE SHORTER
!     (SINCE THE PART OF V THAT NL2SOL USES FOR STORING D, J, AND R IS
!     NOT NEEDED).  MOREOVER, COMPARED WITH NL2SOL, IV(1) MAY HAVE THE
!     TWO ADDITIONAL OUTPUT VALUES 1 AND 2, WHICH ARE EXPLAINED BELOW,
!     AS IS THE USE OF IV(TOOBIG) AND IV(NFGCAL).  THE VALUES IV(D),
!     IV(J), AND IV(R), WHICH ARE OUTPUT VALUES FROM NL2SOL (AND
!     NL2SNO), ARE NOT REFERENCED BY DG7LIT OR THE SUBROUTINES IT CALLS.
!
!        WHEN DG7LIT IS FIRST CALLED, I.E., WHEN DG7LIT IS CALLED WITH
!     IV(1) = 0 OR 12, V(F), G, AND HC NEED NOT BE INITIALIZED.  TO
!     OBTAIN THESE STARTING VALUES,DG7LIT RETURNS FIRST WITH IV(1) = 1,
!     THEN WITH IV(1) = 2, WITH IV(MODE) = -1 IN BOTH CASES.  ON
!     SUBSEQUENT RETURNS WITH IV(1) = 2, IV(MODE) = 0 IMPLIES THAT
!     Y MUST ALSO BE SUPPLIED.  (NOTE THAT Y IS USED FOR SCRATCH -- ITS
!     INPUT CONTENTS ARE LOST.  BY CONTRAST, HC IS NEVER CHANGED.)
!     ONCE CONVERGENCE HAS BEEN OBTAINED, IV(RDREQ) AND IV(COVREQ) MAY
!     IMPLY THAT A FINITE-DIFFERENCE HESSIAN SHOULD BE COMPUTED FOR USE
!     IN COMPUTING A COVARIANCE MATRIX.  IN THIS CASE DG7LIT WILL MAKE A
!     NUMBER OF RETURNS WITH IV(1) = 1 OR 2 AND IV(MODE) POSITIVE.
!     WHEN IV(MODE) IS POSITIVE, Y SHOULD NOT BE CHANGED.
!
!     IV(1) = 1 MEANS THE CALLER SHOULD SET V(F) (I.E., V(10)) TO F(X), THE
!             FUNCTION VALUE AT X, AND CALL DG7LIT AGAIN, HAVING CHANGED
!             NONE OF THE OTHER PARAMETERS.  AN EXCEPTION OCCURS IF F(X)
!             CANNOT BE EVALUATED (E.G. IF OVERFLOW WOULD OCCUR), WHICH
!             MAY HAPPEN BECAUSE OF AN OVERSIZED STEP.  IN THIS CASE
!             THE CALLER SHOULD SET IV(TOOBIG) = IV(2) TO 1, WHICH WILL
!             CAUSE DG7LIT TO IGNORE V(F) AND TRY A SMALLER STEP.  NOTE
!             THAT THE CURRENT FUNCTION EVALUATION COUNT IS AVAILABLE
!             IN IV(NFCALL) = IV(6).  THIS MAY BE USED TO IDENTIFY
!             WHICH COPY OF SAVED INFORMATION SHOULD BE USED IN COM-
!             PUTING G, HC, AND Y THE NEXT TIME DG7LIT RETURNS WITH
!             IV(1) = 2.  SEE MLPIT FOR AN EXAMPLE OF THIS.
!     IV(1) = 2 MEANS THE CALLER SHOULD SET G TO G(X), THE GRADIENT OF F AT
!             X.  THE CALLER SHOULD ALSO SET HC TO THE GAUSS-NEWTON
!             HESSIAN AT X.  IF IV(MODE) = 0, THEN THE CALLER SHOULD
!             ALSO COMPUTE THE PART OF THE YIELD VECTOR DESCRIBED ABOVE.
!             THE CALLER SHOULD THEN CALL DG7LIT AGAIN (WITH IV(1) = 2).
!             THE CALLER MAY ALSO CHANGE D AT THIS TIME, BUT SHOULD NOT
!             CHANGE X.  NOTE THAT IV(NFGCAL) = IV(7) CONTAINS THE
!             VALUE THAT IV(NFCALL) HAD DURING THE RETURN WITH
!             IV(1) = 1 IN WHICH X HAD THE SAME VALUE AS IT NOW HAS.
!             IV(NFGCAL) IS EITHER IV(NFCALL) OR IV(NFCALL) - 1.  MLPIT
!             IS AN EXAMPLE WHERE THIS INFORMATION IS USED.  IF G OR HC
!             CANNOT BE EVALUATED AT X, THEN THE CALLER MAY SET
!             IV(TOOBIG) TO 1, IN WHICH CASE DG7LIT WILL RETURN WITH
!             IV(1) = 15.
!
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY.
!     THIS SUBROUTINE WAS WRITTEN IN CONNECTION WITH RESEARCH
!     SUPPORTED IN PART BY D.O.E. GRANT EX-76-A-01-2295 TO MIT/CCREMS.
!
!        (SEE NL2SOL FOR REFERENCES.)
!
!     +++++++++++++++++++++++++++  DECLARATIONS  ++++++++++++++++++++++++++++
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  CONSTANTS  ***
!
!
!     ***  EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!
!     DA7SST.... ASSESSES CANDIDATE STEP.
!     DD7TPR... RETURNS INNER PRODUCT OF TWO VECTORS.
!     DF7HES.... COMPUTE FINITE-DIFFERENCE HESSIAN (FOR COVARIANCE).
!     DG7QTS.... COMPUTES GOLDFELD-QUANDT-TROTTER STEP (AUGMENTED MODEL).
!     DITSUM.... PRINTS ITERATION SUMMARY AND INFO ON INITIAL AND FINAL X.
!     DL7MST... COMPUTES LEVENBERG-MARQUARDT STEP (GAUSS-NEWTON MODEL).
!     DL7SRT.... COMPUTES CHOLESKY FACTOR OF (LOWER TRIANG. OF) SYM. MATRIX.
!     DL7SQR... COMPUTES L * L**T FROM LOWER TRIANGULAR MATRIX L.
!     DL7TVM... COMPUTES L**T * V, V = VECTOR, L = LOWER TRIANGULAR MATRIX.
!     DL7SVX... ESTIMATES LARGEST SING. VALUE OF LOWER TRIANG. MATRIX.
!     DL7SVN... ESTIMATES SMALLEST SING. VALUE OF LOWER TRIANG. MATRIX.
!     DL7VML.... COMPUTES L * V, V = VECTOR, L = LOWER TRIANGULAR MATRIX.
!     DPARCK.... CHECK VALIDITY OF IV AND V INPUT COMPONENTS.
!     DRLDST... COMPUTES V(RELDX) = RELATIVE STEP SIZE.
!     DR7MDC... RETURNS MACHINE-DEPENDENT CONSTANTS.
!     DS7LUP... PERFORMS QUASI-NEWTON UPDATE ON COMPACTLY STORED LOWER TRI-
!             ANGLE OF A SYMMETRIC MATRIX.
!     STOPX.... RETURNS .TRUE. IF THE BREAK KEY HAS BEEN PRESSED.
!     DV2AXY.... COMPUTES SCALAR TIMES ONE VECTOR PLUS ANOTHER.
!     DV7CPY.... COPIES ONE VECTOR TO ANOTHER.
!     DV7SCP... SETS ALL ELEMENTS OF A VECTOR TO A SCALAR.
!     DV2NRM... RETURNS THE 2-NORM OF A VECTOR.
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!
!     ***  IV SUBSCRIPT VALUES  ***
!
!
!     ***  V SUBSCRIPT VALUES  ***
!
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: negone = -1.E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: onep2 = 1.2E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: cnvcod = 55, cosmin = 47,            &
                                          covmat = 26, covreq = 15,            &
                                          dgnorm = 1, dig = 37, dstnrm = 2,    &
                                          f = 10, f0 = 13, fdh = 74,           &
                                          fdif = 11, fuzz = 45, gtstep = 4,    &
                                          h = 56, hc = 71, ierr = 75,          &
                                          incfac = 23, inits = 25,             &
                                          ipivot = 76, irc = 29, kagqt = 33,   &
                                          kalm = 34, lmat = 42, lmax0 = 35,    &
                                          lmaxs = 36, mode = 35, model = 5,    &
                                          mxfcal = 17, mxiter = 18,            &
                                          nextv = 47, nfcall = 6, nfcov = 52,  &
                                          nfgcal = 7, ngcall = 30, ngcov = 53, &
                                          niter = 31, nvsave = 9, phmxfc = 21, &
                                          preduc = 7, qtr = 77, rad0 = 9,      &
                                          radfac = 16, radinc = 8, radius = 8, &
                                          rcond = 53, rdreq = 57, regd = 67,   &
                                          reldx = 17, restor = 9, rmat = 78,   &
                                          s = 62, size = 55, step = 40,        &
                                          stglim = 11, stlstg = 41,            &
                                          stppar = 5, sused = 64, switch = 12, &
                                          toobig = 2, tuner4 = 29,             &
                                          tuner5 = 30, vneed = 4, vsave = 60,  &
                                          w = 65, wscale = 56, x0 = 43,        &
                                          xirc = 13
!     .. Scalar Arguments ..
      Integer                          :: liv, lv, p, ps
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(p), g(p), v(lv), x(p), y(p)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: e, sttsst, t, t1
      Integer                          :: dig1, dummy, g01, h1, hc1, i, ipiv1, &
                                          j, k, l, lmat1, lstgst, pp1o2, qtr1, &
                                          rmat1, rstrst, s1, step1, stpmod,    &
                                          temp1, temp2, w1, x01
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, dl7svn, dl7svx, dr7mdc,      &
                                          drldst, dv2nrm
      Logical, External                :: stopx
      External                         :: da7sst, df7hes, dg7qts, ditsum,      &
                                          dl7mst, dl7sqr, dl7srt, dl7tvm,      &
                                          dl7vml, dparck, ds7lup, ds7lvm,      &
                                          dv2axy, dv7cpy, dv7scp
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      i = iv(1)
      If (i==1) Go To 120
      If (i==2) Go To 130
!
      If (i==12 .Or. i==13) iv(vneed) = iv(vneed) + p*(3*p+19)/2 + 7
      Call dparck(1,d,iv,liv,lv,p,v)
      i = iv(1) - 2
      If (i>12) Go To 650
      Go To (330,330,330,330,330,330,230,190,230,100,100,110) i
!
!     ***  STORAGE ALLOCATION  ***
!
100   pp1o2 = p*(p+1)/2
      iv(s) = iv(lmat) + pp1o2
      iv(x0) = iv(s) + pp1o2
      iv(step) = iv(x0) + p
      iv(stlstg) = iv(step) + p
      iv(dig) = iv(stlstg) + p
      iv(w) = iv(dig) + p
      iv(h) = iv(w) + 4*p + 7
      iv(nextv) = iv(h) + pp1o2
      If (iv(1)/=13) Go To 110
      iv(1) = 14
      Go To 650
!
!     ***  INITIALIZATION  ***
!
110   iv(niter) = 0
      iv(nfcall) = 1
      iv(ngcall) = 1
      iv(nfgcal) = 1
      iv(mode) = -1
      iv(stglim) = 2
      iv(toobig) = 0
      iv(cnvcod) = 0
      iv(covmat) = 0
      iv(nfcov) = 0
      iv(ngcov) = 0
      iv(radinc) = 0
      iv(restor) = 0
      iv(fdh) = 0
      v(rad0) = zero
      v(stppar) = zero
      v(radius) = v(lmax0)/(one+v(phmxfc))
!
!     ***  SET INITIAL MODEL AND S MATRIX  ***
!
      iv(model) = 1
      If (iv(s)<0) Go To 650
      If (iv(inits)>1) iv(model) = 2
      s1 = iv(s)
      If (iv(inits)==0 .Or. iv(inits)>2) Call dv7scp(p*(p+1)/2,v(s1),zero)
      iv(1) = 1
      j = iv(ipivot)
      If (j<=0) Go To 650
      Do i = 1, p
        iv(j) = i
        j = j + 1
      End Do
      Go To 650
!
!     ***  NEW FUNCTION VALUE  ***
!
120   If (iv(mode)==0) Go To 330
      If (iv(mode)>0) Go To 550
!
      iv(1) = 2
      If (iv(toobig)==0) Go To 650
      iv(1) = 63
      Go To 650
!
!     ***  NEW GRADIENT  ***
!
130   iv(kalm) = -1
      iv(kagqt) = -1
      iv(fdh) = 0
      If (iv(mode)>0) Go To 550
!
!     ***  MAKE SURE GRADIENT COULD BE COMPUTED  ***
!
      If (iv(toobig)==0) Go To 140
      iv(1) = 65
      Go To 650
140   If (iv(hc)<=0 .And. iv(rmat)<=0) Go To 640
!
!     ***  COMPUTE  D**-1 * GRADIENT  ***
!
      dig1 = iv(dig)
      k = dig1
      Do i = 1, p
        v(k) = g(i)/d(i)
        k = k + 1
      End Do
      v(dgnorm) = dv2nrm(p,v(dig1))
!
      If (iv(cnvcod)/=0) Go To 540
      If (iv(mode)==0) Go To 480
      iv(mode) = 0
      v(f0) = v(f)
      If (iv(inits)<=2) Go To 170
!
!     ***  ARRANGE FOR FINITE-DIFFERENCE INITIAL S  ***
!
      iv(xirc) = iv(covreq)
      iv(covreq) = -1
      If (iv(inits)>3) iv(covreq) = 1
      iv(cnvcod) = 70
      Go To 560
!
!     ***  COME TO NEXT STMT AFTER COMPUTING F.D. HESSIAN FOR INIT. S  ***
!
150   iv(cnvcod) = 0
      iv(mode) = 0
      iv(nfcov) = 0
      iv(ngcov) = 0
      iv(covreq) = iv(xirc)
      s1 = iv(s)
      pp1o2 = ps*(ps+1)/2
      hc1 = iv(hc)
      If (hc1<=0) Go To 160
      Call dv2axy(pp1o2,v(s1),negone,v(hc1),v(h1))
      Go To 170
160   rmat1 = iv(rmat)
      Call dl7sqr(ps,v(s1),v(rmat1))
      Call dv2axy(pp1o2,v(s1),negone,v(s1),v(h1))
170   iv(1) = 2
!
!
!     -----------------------------  MAIN LOOP  -----------------------------
!
!
!     ***  PRINT ITERATION SUMMARY, CHECK ITERATION LIMIT  ***
!
180   Call ditsum(d,g,iv,liv,lv,p,v,x)
190   k = iv(niter)
      If (k<iv(mxiter)) Go To 200
      iv(1) = 10
      Go To 650
200   iv(niter) = k + 1
!
!     ***  UPDATE RADIUS  ***
!
      If (k==0) Go To 210
      step1 = iv(step)
      Do i = 1, p
        v(step1) = d(i)*v(step1)
        step1 = step1 + 1
      End Do
      step1 = iv(step)
      t = v(radfac)*dv2nrm(p,v(step1))
      If (v(radfac)<one .Or. t>v(radius)) v(radius) = t
!
!     ***  INITIALIZE FOR START OF NEXT ITERATION  ***
!
210   x01 = iv(x0)
      v(f0) = v(f)
      iv(irc) = 4
      iv(h) = -abs(iv(h))
      iv(sused) = iv(model)
!
!     ***  COPY X TO X0  ***
!
      Call dv7cpy(p,v(x01),x)
!
!     ***  CHECK STOPX AND FUNCTION EVALUATION LIMIT  ***
!
220   If (.Not. stopx(dummy)) Go To 240
      iv(1) = 11
      Go To 250
!
!     ***  COME HERE WHEN RESTARTING AFTER FUNC. EVAL. LIMIT OR STOPX.
!
230   If (v(f)>=v(f0)) Go To 240
      v(radfac) = one
      k = iv(niter)
      Go To 200
!
240   If (iv(nfcall)<iv(mxfcal)+iv(nfcov)) Go To 260
      iv(1) = 9
250   If (v(f)>=v(f0)) Go To 650
!
!        ***  IN CASE OF STOPX OR FUNCTION EVALUATION LIMIT WITH
!        ***  IMPROVED V(F), EVALUATE THE GRADIENT AT X.
!
      iv(cnvcod) = iv(1)
      Go To 470
!
!     . . . . . . . . . . . . .  COMPUTE CANDIDATE STEP  . . . . . . . . . .
!
260   step1 = iv(step)
      w1 = iv(w)
      h1 = iv(h)
      t1 = one
      If (iv(model)==2) Go To 270
      t1 = zero
!
!        ***  COMPUTE LEVENBERG-MARQUARDT STEP IF POSSIBLE...
!
      rmat1 = iv(rmat)
      If (rmat1<=0) Go To 270
      qtr1 = iv(qtr)
      If (qtr1<=0) Go To 270
      ipiv1 = iv(ipivot)
      Call dl7mst(d,g,iv(ierr),iv(ipiv1),iv(kalm),p,v(qtr1),v(rmat1),v(step1), &
        v,v(w1))
!        *** H IS STORED IN THE END OF W AND HAS JUST BEEN OVERWRITTEN,
!        *** SO WE MARK IT INVALID...
      iv(h) = -abs(h1)
!        *** EVEN IF H WERE STORED ELSEWHERE, IT WOULD BE NECESSARY TO
!        *** MARK INVALID THE INFORMATION DG7QTS MAY HAVE STORED IN V...
      iv(kagqt) = -1
      Go To 300
!
270   If (h1>0) Go To 290
!
!     ***  SET H TO  D**-1 * (HC + T1*S) * D**-1.  ***
!
      h1 = -h1
      iv(h) = h1
      iv(fdh) = 0
      j = iv(hc)
      If (j>0) Go To 280
      j = h1
      rmat1 = iv(rmat)
      Call dl7sqr(p,v(h1),v(rmat1))
280   s1 = iv(s)
      Do i = 1, p
        t = one/d(i)
        Do k = 1, i
          v(h1) = t*(v(j)+t1*v(s1))/d(k)
          j = j + 1
          h1 = h1 + 1
          s1 = s1 + 1
        End Do
      End Do
      h1 = iv(h)
      iv(kagqt) = -1
!
!     ***  COMPUTE ACTUAL GOLDFELD-QUANDT-TROTTER STEP  ***
!
290   dig1 = iv(dig)
      lmat1 = iv(lmat)
      Call dg7qts(d,v(dig1),v(h1),iv(kagqt),v(lmat1),p,v(step1),v,v(w1))
      If (iv(kalm)>0) iv(kalm) = 0
!
300   If (iv(irc)/=6) Go To 310
      If (iv(restor)/=2) Go To 330
      rstrst = 2
      Go To 340
!
!     ***  CHECK WHETHER EVALUATING F(X0 + STEP) LOOKS WORTHWHILE  ***
!
310   iv(toobig) = 0
      If (v(dstnrm)<=zero) Go To 330
      If (iv(irc)/=5) Go To 320
      If (v(radfac)<=one) Go To 320
      If (v(preduc)>onep2*v(fdif)) Go To 320
      step1 = iv(step)
      x01 = iv(x0)
      Call dv2axy(p,v(step1),negone,v(x01),x)
      If (iv(restor)/=2) Go To 330
      rstrst = 0
      Go To 340
!
!     ***  COMPUTE F(X0 + STEP)  ***
!
320   x01 = iv(x0)
      step1 = iv(step)
      Call dv2axy(p,x,one,v(step1),v(x01))
      iv(nfcall) = iv(nfcall) + 1
      iv(1) = 1
      Go To 650
!
!     . . . . . . . . . . . . .  ASSESS CANDIDATE STEP  . . . . . . . . . . .
!
330   rstrst = 3
340   x01 = iv(x0)
      v(reldx) = drldst(p,d,x,v(x01))
      Call da7sst(iv,liv,lv,v)
      step1 = iv(step)
      lstgst = iv(stlstg)
      i = iv(restor) + 1
      Go To (380,350,360,370) i
350   Call dv7cpy(p,x,v(x01))
      Go To 380
360   Call dv7cpy(p,v(lstgst),v(step1))
      Go To 380
370   Call dv7cpy(p,v(step1),v(lstgst))
      Call dv2axy(p,x,one,v(step1),v(x01))
      v(reldx) = drldst(p,d,x,v(x01))
      iv(restor) = rstrst
!
!     ***  IF NECESSARY, SWITCH MODELS  ***
!
380   If (iv(switch)==0) Go To 390
      iv(h) = -abs(iv(h))
      iv(sused) = iv(sused) + 2
      l = iv(vsave)
      Call dv7cpy(nvsave,v,v(l))
390   l = iv(irc) - 4
      stpmod = iv(model)
      If (l>0) Go To (410,420,430,430,430,430,430,430,530,480) l
!
!     ***  DECIDE WHETHER TO CHANGE MODELS  ***
!
      e = v(preduc) - v(fdif)
      s1 = iv(s)
      Call ds7lvm(ps,y,v(s1),v(step1))
      sttsst = half*dd7tpr(ps,v(step1),y)
      If (iv(model)==1) sttsst = -sttsst
      If (abs(e+sttsst)*v(fuzz)>=abs(e)) Go To 400
!
!     ***  SWITCH MODELS  ***
!
      iv(model) = 3 - iv(model)
      If (-2<l) Go To 440
      iv(h) = -abs(iv(h))
      iv(sused) = iv(sused) + 2
      l = iv(vsave)
      Call dv7cpy(nvsave,v(l),v)
      Go To 220
!
400   If (-3<l) Go To 440
!
!     ***  RECOMPUTE STEP WITH NEW RADIUS  ***
!
410   v(radius) = v(radfac)*v(dstnrm)
      Go To 220
!
!     ***  COMPUTE STEP OF LENGTH V(LMAXS) FOR SINGULAR CONVERGENCE TEST
!
420   v(radius) = v(lmaxs)
      Go To 260
!
!     ***  CONVERGENCE OR FALSE CONVERGENCE  ***
!
430   iv(cnvcod) = l
      If (v(f)>=v(f0)) Go To 540
      If (iv(xirc)==14) Go To 540
      iv(xirc) = 14
!
!     . . . . . . . . . . . .  PROCESS ACCEPTABLE STEP  . . . . . . . . . . .
!
440   iv(covmat) = 0
      iv(regd) = 0
!
!     ***  SEE WHETHER TO SET V(RADFAC) BY GRADIENT TESTS  ***
!
      If (iv(irc)/=3) Go To 470
      step1 = iv(step)
      temp1 = iv(stlstg)
      temp2 = iv(w)
!
!     ***  SET  TEMP1 = HESSIAN * STEP  FOR USE IN GRADIENT TESTS  ***
!
      hc1 = iv(hc)
      If (hc1<=0) Go To 450
      Call ds7lvm(p,v(temp1),v(hc1),v(step1))
      Go To 460
450   rmat1 = iv(rmat)
      Call dl7tvm(p,v(temp1),v(rmat1),v(step1))
      Call dl7vml(p,v(temp1),v(rmat1),v(temp1))
!
460   If (stpmod==1) Go To 470
      s1 = iv(s)
      Call ds7lvm(ps,v(temp2),v(s1),v(step1))
      Call dv2axy(ps,v(temp1),one,v(temp2),v(temp1))
!
!     ***  SAVE OLD GRADIENT AND COMPUTE NEW ONE  ***
!
470   iv(ngcall) = iv(ngcall) + 1
      g01 = iv(w)
      Call dv7cpy(p,v(g01),g)
      iv(1) = 2
      iv(toobig) = 0
      Go To 650
!
!     ***  INITIALIZATIONS -- G0 = G - G0, ETC.  ***
!
480   g01 = iv(w)
      Call dv2axy(p,v(g01),negone,v(g01),g)
      step1 = iv(step)
      temp1 = iv(stlstg)
      temp2 = iv(w)
      If (iv(irc)/=3) Go To 500
!
!     ***  SET V(RADFAC) BY GRADIENT TESTS  ***
!
!     ***  SET  TEMP1 = D**-1 * (HESSIAN * STEP  +  (G(X0) - G(X)))  ***
!
      k = temp1
      l = g01
      Do i = 1, p
        v(k) = (v(k)-v(l))/d(i)
        k = k + 1
        l = l + 1
      End Do
!
!        ***  DO GRADIENT TESTS  ***
!
      If (dv2nrm(p,v(temp1))<=v(dgnorm)*v(tuner4)) Go To 490
      If (dd7tpr(p,g,v(step1))>=v(gtstep)*v(tuner5)) Go To 500
490   v(radfac) = v(incfac)
!
!     ***  COMPUTE Y VECTOR NEEDED FOR UPDATING S  ***
!
500   Call dv2axy(ps,y,negone,y,g)
!
!     ***  DETERMINE SIZING FACTOR V(SIZE)  ***
!
!     ***  SET TEMP1 = S * STEP  ***
      s1 = iv(s)
      Call ds7lvm(ps,v(temp1),v(s1),v(step1))
!
      t1 = abs(dd7tpr(ps,v(step1),v(temp1)))
      t = abs(dd7tpr(ps,v(step1),y))
      v(size) = one
      If (t<t1) v(size) = t/t1
!
!     ***  SET G0 TO WCHMTD CHOICE OF FLETCHER AND AL-BAALI  ***
!
      hc1 = iv(hc)
      If (hc1<=0) Go To 510
      Call ds7lvm(ps,v(g01),v(hc1),v(step1))
      Go To 520
!
510   rmat1 = iv(rmat)
      Call dl7tvm(ps,v(g01),v(rmat1),v(step1))
      Call dl7vml(ps,v(g01),v(rmat1),v(g01))
!
520   Call dv2axy(ps,v(g01),one,y,v(g01))
!
!     ***  UPDATE S  ***
!
      Call ds7lup(v(s1),v(cosmin),ps,v(size),v(step1),v(temp1),v(temp2),       &
        v(g01),v(wscale),y)
      iv(1) = 2
      Go To 180
!
!     . . . . . . . . . . . . . .  MISC. DETAILS  . . . . . . . . . . . . . .
!
!     ***  BAD PARAMETERS TO ASSESS  ***
!
530   iv(1) = 64
      Go To 650
!
!
!     ***  CONVERGENCE OBTAINED -- SEE WHETHER TO COMPUTE COVARIANCE  ***
!
540   If (iv(rdreq)==0) Go To 630
      If (iv(fdh)/=0) Go To 630
      If (iv(cnvcod)>=7) Go To 630
      If (iv(regd)>0) Go To 630
      If (iv(covmat)>0) Go To 630
      If (abs(iv(covreq))>=3) Go To 590
      If (iv(restor)==0) iv(restor) = 2
      Go To 560
!
!     ***  COMPUTE FINITE-DIFFERENCE HESSIAN FOR COMPUTING COVARIANCE  ***
!
550   iv(restor) = 0
560   Call df7hes(d,g,i,iv,liv,lv,p,v,x)
      Go To (570,580,610) i
570   iv(nfcov) = iv(nfcov) + 1
      iv(nfcall) = iv(nfcall) + 1
      iv(1) = 1
      Go To 650
!
580   iv(ngcov) = iv(ngcov) + 1
      iv(ngcall) = iv(ngcall) + 1
      iv(nfgcal) = iv(nfcall) + iv(ngcov)
      iv(1) = 2
      Go To 650
!
590   h1 = abs(iv(h))
      iv(h) = -h1
      pp1o2 = p*(p+1)/2
      rmat1 = iv(rmat)
      If (rmat1<=0) Go To 600
      lmat1 = iv(lmat)
      Call dv7cpy(pp1o2,v(lmat1),v(rmat1))
      v(rcond) = zero
      Go To 620
600   hc1 = iv(hc)
      iv(fdh) = h1
      Call dv7cpy(p*(p+1)/2,v(h1),v(hc1))
!
!     ***  COMPUTE CHOLESKY FACTOR OF FINITE-DIFFERENCE HESSIAN
!     ***  FOR USE IN CALLER*S COVARIANCE CALCULATION...
!
610   lmat1 = iv(lmat)
      h1 = iv(fdh)
      If (h1<=0) Go To 630
      If (iv(cnvcod)==70) Go To 150
      Call dl7srt(1,p,v(lmat1),v(h1),i)
      iv(fdh) = -1
      v(rcond) = zero
      If (i/=0) Go To 630
!
620   iv(fdh) = -1
      step1 = iv(step)
      t = dl7svn(p,v(lmat1),v(step1),v(step1))
      If (t<=zero) Go To 630
      t = t/dl7svx(p,v(lmat1),v(step1),v(step1))
      If (t>dr7mdc(4)) iv(fdh) = h1
      v(rcond) = t
!
630   iv(mode) = 0
      iv(1) = iv(cnvcod)
      iv(cnvcod) = 0
      Go To 650
!
!     ***  SPECIAL RETURN FOR MISSING HESSIAN INFORMATION -- BOTH
!     ***  IV(HC) .LE. 0 AND IV(RMAT) .LE. 0
!
640   iv(1) = 1400
!
650   Return
!
!     ***  LAST LINE OF DG7LIT FOLLOWS  ***
    End Subroutine dg7lit
    Subroutine dl7msb(b,d,g,ierr,ipiv,ipiv1,ipiv2,ka,lmat,lv,p,p0,pc,qtr,rmat, &
      step,td,tg,v,w,wlm,x,x0)
!
!     ***  COMPUTE HEURISTIC BOUNDED NEWTON STEP  ***
!
!     DIMENSION LMAT(P*(P+1)/2), RMAT(P*(P+1)/2), WLM(P*(P+5)/2 + 4)
!
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  V SUBSCRIPTS  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Integer, Parameter               :: dst0 = 3, dstnrm = 2, gtstep = 4,    &
                                          nreduc = 6, preduc = 7, radius = 8
!     .. Scalar Arguments ..
      Integer                          :: ierr, ka, lv, p, p0, pc
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(2,p), d(p), g(p), lmat(*), qtr(p), &
                                          rmat(*), step(p,3), td(p), tg(p),    &
                                          v(lv), w(p), wlm(*), x(p), x0(p)
      Integer                          :: ipiv(p), ipiv1(p), ipiv2(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: ds0, nred, pred, rad
      Real (Kind=wp), Save             :: one, zero
      Integer                          :: i, j, k, k0, kb, kinit, l, ns, p1,   &
                                          p10, p11
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr
      External                         :: dd7mlp, dl7mst, dl7tvm, dq7rsh,      &
                                          ds7bqn, dv2axy, dv7cpy, dv7ipr,      &
                                          dv7scp, dv7vmp
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max
!     .. Data Statements ..
      Data one/1.E+0_wp/, zero/0.E+0_wp/
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      p1 = pc
      If (ka<0) Go To 100
      nred = v(nreduc)
      ds0 = v(dst0)
      Go To 110
100   p0 = 0
      ka = -1
!
110   kinit = -1
      If (p0==p1) kinit = ka
      Call dv7cpy(p,x,x0)
      Call dv7cpy(p,td,d)
!     *** USE STEP(1,3) AS TEMP. COPY OF QTR ***
      Call dv7cpy(p,step(1,3),qtr)
      Call dv7ipr(p,ipiv,td)
      pred = zero
      rad = v(radius)
      kb = -1
      v(dstnrm) = zero
      If (p1>0) Go To 120
      nred = zero
      ds0 = zero
      Call dv7scp(p,step,zero)
      Go To 160
!
120   Call dv7vmp(p,tg,g,d,-1)
      Call dv7ipr(p,ipiv,tg)
      p10 = p1
130   k = kinit
      kinit = -1
      v(radius) = rad - v(dstnrm)
      Call dv7vmp(p1,tg,tg,td,1)
      Do i = 1, p1
        ipiv1(i) = i
      End Do
      k0 = max(0,k)
      Call dl7mst(td,tg,ierr,ipiv1,k,p1,step(1,3),rmat,step,v,wlm)
      Call dv7vmp(p1,tg,tg,td,-1)
      p0 = p1
      If (ka>=0) Go To 140
      nred = v(nreduc)
      ds0 = v(dst0)
!
140   ka = k
      v(radius) = rad
      l = p1 + 5
      If (k<=k0) Call dd7mlp(p1,lmat,td,rmat,-1)
      If (k>k0) Call dd7mlp(p1,lmat,td,wlm(l),-1)
      Call ds7bqn(b,d,step(1,2),ipiv,ipiv1,ipiv2,kb,lmat,lv,ns,p,p1,step,td,   &
        tg,v,w,x,x0)
      pred = pred + v(preduc)
      If (ns==0) Go To 150
      p0 = 0
!
!     ***  UPDATE RMAT AND QTR  ***
!
      p11 = p1 + 1
      l = p10 + p11
      Do k = p11, p10
        j = l - k
        i = ipiv2(j)
        If (i<j) Call dq7rsh(i,j,.True.,qtr,rmat,w)
      End Do
!
150   If (kb>0) Go To 160
!
!     ***  UPDATE LOCAL COPY OF QTR  ***
!
      Call dv7vmp(p10,w,step(1,2),td,-1)
      Call dl7tvm(p10,w,lmat,w)
      Call dv2axy(p10,step(1,3),one,w,qtr)
      Go To 130
!
160   v(dst0) = ds0
      v(nreduc) = nred
      v(preduc) = pred
      v(gtstep) = dd7tpr(p,g,step)
!
      Return
!     ***  LAST LINE OF DL7MSB FOLLOWS  ***
    End Subroutine dl7msb
    Subroutine dn2lrd(dr,iv,l,lh,liv,lv,nd,nn,p,r,rd,v)
!
!     ***  COMPUTE REGRESSION DIAGNOSTIC AND DEFAULT COVARIANCE MATRIX FOR
!        DRN2G  ***
!
!     ***  PARAMETERS  ***
!
!
!     ***  CODED BY DAVID M. GAY (WINTER 1982, FALL 1983)  ***
!
!     ***  EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  CONSTANTS  ***
!
!
!     ***  INTRINSIC FUNCTIONS  ***
!     /+
!/
!
!     ***  IV AND V SUBSCRIPTS  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: negone = -1.E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: f = 10, h = 56, mode = 35,           &
                                          rdreq = 57, step = 40
!     .. Scalar Arguments ..
      Integer                          :: lh, liv, lv, nd, nn, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: dr(nd,p), l(lh), r(nn), rd(nn),      &
                                          v(lv)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: a, ff, s, t
      Integer                          :: cov, i, j, m, step1
!     .. Local Arrays ..
      Real (Kind=wp), Save             :: onev(1)
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr
      External                         :: dl7itv, dl7ivm, do7prd, dv7scp
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, mod, sqrt
!     .. Data Statements ..
      Data onev(1)/1.E+0_wp/
!     .. Executable Statements ..
!
!     ++++++++++++++++++++++++++++++++  BODY  +++++++++++++++++++++++++++++++
!
      step1 = iv(step)
      i = iv(rdreq)
      If (i<=0) Go To 120
      If (mod(i,4)<2) Go To 110
      ff = one
      If (v(f)/=zero) ff = one/sqrt(abs(v(f)))
      Call dv7scp(nn,rd,negone)
      Do i = 1, nn
        a = r(i)**2
        m = step1
        Do j = 1, p
          v(m) = dr(i,j)
          m = m + 1
        End Do
        Call dl7ivm(p,v(step1),l,v(step1))
        s = dd7tpr(p,v(step1),v(step1))
        t = one - s
        If (t<=zero) Go To 100
        a = a*s/t
        rd(i) = sqrt(a)*ff
100   End Do
!
110   If (iv(mode)-p<2) Go To 120
!
!     ***  COMPUTE DEFAULT COVARIANCE MATRIX  ***
!
      cov = abs(iv(h))
      Do i = 1, nn
        m = step1
        Do j = 1, p
          v(m) = dr(i,j)
          m = m + 1
        End Do
        Call dl7ivm(p,v(step1),l,v(step1))
        Call dl7itv(p,v(step1),l,v(step1))
        Call do7prd(1,lh,p,v(cov),onev,v(step1),v(step1))
      End Do
!
120   Return
!     ***  LAST LINE OF DN2LRD FOLLOWS  ***
    End Subroutine dn2lrd
    Subroutine dr7tvm(n,p,y,d,u,x)
!
!     ***  SET Y TO R*X, WHERE R IS THE UPPER TRIANGULAR MATRIX WHOSE
!     ***  DIAGONAL IS IN D AND WHOSE STRICT UPPER TRIANGLE IS IN U.
!
!     ***  X AND Y MAY SHARE STORAGE.
!
!
!
!     ***  LOCAL VARIABLES  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: n, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(p), u(n,p), x(p), y(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t
      Integer                          :: i, ii, pl, pp1
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr
!     .. Intrinsic Procedures ..
      Intrinsic                        :: min
!     .. Executable Statements ..
!
!     ***  BODY  ***
!
      pl = min(n,p)
      pp1 = pl + 1
      Do ii = 1, pl
        i = pp1 - ii
        t = x(i)*d(i)
        If (i>1) t = t + dd7tpr(i-1,u(1,i),x)
        y(i) = t
      End Do
      Return
!     ***  LAST LINE OF DR7TVM FOLLOWS  ***
    End Subroutine dr7tvm
    Subroutine dq7rad(n,nn,p,qtr,qtrset,rmat,w,y)
!
!     ***  ADD ROWS W TO QR FACTORIZATION WITH R MATRIX RMAT AND
!     ***  Q**T * RESIDUAL = QTR.  Y = NEW COMPONENTS OF RESIDUAL
!     ***  CORRESPONDING TO W.  QTR, Y REFERENCED ONLY IF QTRSET = .TRUE.
!
!     DIMENSION RMAT(P*(P+1)/2)
!     /+
!/
!
!     ***  LOCAL VARIABLES  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: n, nn, p
      Logical                          :: qtrset
!     .. Array Arguments ..
      Real (Kind=wp)                   :: qtr(p), rmat(*), w(nn,p), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: ari, qri, ri, s, t, wi
      Real (Kind=wp), Save             :: big, bigrt, one, tiny, tinyrt, zero
      Integer                          :: i, ii, ij, ip1, j, k, nk
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, dr7mdc, dv2nrm
      External                         :: dv2axy, dv7scl
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, sqrt
!     .. Data Statements ..
      Data big/ -1.E+0_wp/, bigrt/ -1.E+0_wp/, one/1.E+0_wp/, tiny/0.E+0_wp/,  &
        tinyrt/0.E+0_wp/, zero/0.E+0_wp/
!     .. Executable Statements ..
!
!     ------------------------------ BODY -----------------------------------
!
      If (tiny>zero) Go To 100
      tiny = dr7mdc(1)
      big = dr7mdc(6)
      If (tiny*big<one) tiny = one/big
100   k = 1
      nk = n
      ii = 0
      Do i = 1, p
        ii = ii + i
        ip1 = i + 1
        ij = ii + i
        If (nk<=1) t = abs(w(k,i))
        If (nk>1) t = dv2nrm(nk,w(k,i))
        If (t<tiny) Go To 230
        ri = rmat(ii)
        If (ri/=zero) Go To 170
        If (nk>1) Go To 110
        ij = ii
        Do j = i, p
          rmat(ij) = w(k,j)
          ij = ij + j
        End Do
        If (qtrset) qtr(i) = y(k)
        w(k,i) = zero
        Go To 240
110     wi = w(k,i)
        If (bigrt>zero) Go To 120
        bigrt = dr7mdc(5)
        tinyrt = dr7mdc(2)
120     If (t<=tinyrt) Go To 130
        If (t>=bigrt) Go To 130
        If (wi<zero) t = -t
        wi = wi + t
        s = sqrt(t*wi)
        Go To 150
130     s = sqrt(t)
        If (wi<zero) Go To 140
        wi = wi + t
        s = s*sqrt(wi)
        Go To 150
140     t = -t
        wi = wi + t
        s = s*sqrt(-wi)
150     w(k,i) = wi
        Call dv7scl(nk,w(k,i),one/s,w(k,i))
        rmat(ii) = -t
        If (.Not. qtrset) Go To 160
        Call dv2axy(nk,y(k),-dd7tpr(nk,y(k),w(k,i)),w(k,i),y(k))
        qtr(i) = y(k)
160     If (ip1>p) Go To 240
        Do j = ip1, p
          Call dv2axy(nk,w(k,j),-dd7tpr(nk,w(k,j),w(k,i)),w(k,i),w(k,j))
          rmat(ij) = w(k,j)
          ij = ij + j
        End Do
        If (nk<=1) Go To 240
        k = k + 1
        nk = nk - 1
        Go To 230
!
170     ari = abs(ri)
        If (ari>t) Go To 180
        t = t*sqrt(one+(ari/t)**2)
        Go To 190
180     t = ari*sqrt(one+(t/ari)**2)
190     If (ri<zero) t = -t
        ri = ri + t
        rmat(ii) = -t
        s = -ri/t
        If (nk<=1) Go To 210
        Call dv7scl(nk,w(k,i),one/ri,w(k,i))
        If (.Not. qtrset) Go To 200
        qri = qtr(i)
        t = s*(qri+dd7tpr(nk,y(k),w(k,i)))
        qtr(i) = qri + t
200     If (ip1>p) Go To 240
        If (qtrset) Call dv2axy(nk,y(k),t,w(k,i),y(k))
        Do j = ip1, p
          ri = rmat(ij)
          t = s*(ri+dd7tpr(nk,w(k,j),w(k,i)))
          Call dv2axy(nk,w(k,j),t,w(k,i),w(k,j))
          rmat(ij) = ri + t
          ij = ij + j
        End Do
        Go To 230
!
210     wi = w(k,i)/ri
        w(k,i) = wi
        If (.Not. qtrset) Go To 220
        qri = qtr(i)
        t = s*(qri+y(k)*wi)
        qtr(i) = qri + t
220     If (ip1>p) Go To 240
        If (qtrset) y(k) = t*wi + y(k)
        Do j = ip1, p
          ri = rmat(ij)
          t = s*(ri+w(k,j)*wi)
          w(k,j) = w(k,j) + t*wi
          rmat(ij) = ri + t
          ij = ij + j
        End Do
230   End Do
!
240   Return
!     ***  LAST LINE OF DQ7RAD FOLLOWS  ***
    End Subroutine dq7rad
    Subroutine df7hes(d,g,irt,iv,liv,lv,p,v,x)
!
!     ***  COMPUTE FINITE-DIFFERENCE HESSIAN, STORE IT IN V STARTING
!     ***  AT V(IV(FDH)) = V(-IV(H)).
!
!     ***  IF IV(COVREQ) .GE. 0 THEN DF7HES USES GRADIENT DIFFERENCES,
!     ***  OTHERWISE FUNCTION DIFFERENCES.  STORAGE IN V IS AS IN DG7LIT.
!
!     IRT VALUES...
!     1 = COMPUTE FUNCTION VALUE, I.E., V(F).
!     2 = COMPUTE G.
!     3 = DONE.
!
!
!     ***  PARAMETER DECLARATIONS  ***
!
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  EXTERNAL SUBROUTINES  ***
!
!
!     DV7CPY.... COPY ONE VECTOR TO ANOTHER.
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: negpt5 = -0.5E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: two = 2.E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: covreq = 15, delta = 52,             &
                                          delta0 = 44, dltfdc = 42, f = 10,    &
                                          fdh = 74, fx = 53, h = 56,           &
                                          kagqt = 33, mode = 35, nfgcal = 7,   &
                                          savei = 63, switch = 12, toobig = 2, &
                                          w = 65, xmsave = 51
!     .. Scalar Arguments ..
      Integer                          :: irt, liv, lv, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(p), g(p), v(lv), x(p)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: del
      Integer                          :: gsave1, hes, hmi, hpi, hpm, i,       &
                                          ikind, k, l, m, mm1, mm1o2, pp1o2,   &
                                          stp0, stpi, stpm
!     .. External Procedures ..
      External                         :: dv7cpy
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      irt = 4
      ikind = iv(covreq)
      m = iv(mode)
      If (m>0) Go To 100
      iv(h) = -abs(iv(h))
      iv(fdh) = 0
      iv(kagqt) = -1
      v(fx) = v(f)
100   If (m>p) Go To 280
      If (ikind<0) Go To 170
!
!     ***  COMPUTE FINITE-DIFFERENCE HESSIAN USING BOTH FUNCTION AND
!     ***  GRADIENT VALUES.
!
      gsave1 = iv(w) + p
      If (m>0) Go To 110
!        ***  FIRST CALL ON DF7HES.  SET GSAVE = G, TAKE FIRST STEP  ***
      Call dv7cpy(p,v(gsave1),g)
      iv(switch) = iv(nfgcal)
      Go To 150
!
110   del = v(delta)
      x(m) = v(xmsave)
      If (iv(toobig)==0) Go To 130
!
!     ***  HANDLE OVERSIZE V(DELTA)  ***
!
      If (del*x(m)>zero) Go To 120
!             ***  WE ALREADY TRIED SHRINKING V(DELTA), SO QUIT  ***
      iv(fdh) = -2
      Go To 270
!
!        ***  TRY SHRINKING V(DELTA)  ***
120   del = negpt5*del
      Go To 160
!
130   hes = -iv(h)
!
!     ***  SET  G = (G - GSAVE)/DEL  ***
!
      Do i = 1, p
        g(i) = (g(i)-v(gsave1))/del
        gsave1 = gsave1 + 1
      End Do
!
!     ***  ADD G AS NEW COL. TO FINITE-DIFF. HESSIAN MATRIX  ***
!
      k = hes + m*(m-1)/2
      l = k + m - 2
      If (m==1) Go To 140
!
!     ***  SET  H(I,M) = 0.5 * (H(I,M) + G(I))  FOR I = 1 TO M-1  ***
!
      mm1 = m - 1
      Do i = 1, mm1
        v(k) = half*(v(k)+g(i))
        k = k + 1
      End Do
!
!     ***  ADD  H(I,M) = G(I)  FOR I = M TO P  ***
!
140   l = l + 1
      Do i = m, p
        v(l) = g(i)
        l = l + i
      End Do
!
150   m = m + 1
      iv(mode) = m
      If (m>p) Go To 260
!
!     ***  CHOOSE NEXT FINITE-DIFFERENCE STEP, RETURN TO GET G THERE  ***
!
      del = v(delta0)*max(one/d(m),abs(x(m)))
      If (x(m)<zero) del = -del
      v(xmsave) = x(m)
160   x(m) = x(m) + del
      v(delta) = del
      irt = 2
      Go To 280
!
!     ***  COMPUTE FINITE-DIFFERENCE HESSIAN USING FUNCTION VALUES ONLY.
!
170   stp0 = iv(w) + p - 1
      mm1 = m - 1
      mm1o2 = m*mm1/2
      If (m>0) Go To 180
!        ***  FIRST CALL ON DF7HES.  ***
      iv(savei) = 0
      Go To 250
!
180   i = iv(savei)
      hes = -iv(h)
      If (i>0) Go To 230
      If (iv(toobig)==0) Go To 200
!
!     ***  HANDLE OVERSIZE STEP  ***
!
      stpm = stp0 + m
      del = v(stpm)
      If (del*x(xmsave)>zero) Go To 190
!             ***  WE ALREADY TRIED SHRINKING THE STEP, SO QUIT  ***
      iv(fdh) = -2
      Go To 270
!
!        ***  TRY SHRINKING THE STEP  ***
190   del = negpt5*del
      x(m) = x(xmsave) + del
      v(stpm) = del
      irt = 1
      Go To 280
!
!     ***  SAVE F(X + STP(M)*E(M)) IN H(P,M)  ***
!
200   pp1o2 = p*(p-1)/2
      hpm = hes + pp1o2 + mm1
      v(hpm) = v(f)
!
!     ***  START COMPUTING ROW M OF THE FINITE-DIFFERENCE HESSIAN H.  ***
!
      hmi = hes + mm1o2
      If (mm1==0) Go To 210
      hpi = hes + pp1o2
      Do i = 1, mm1
        v(hmi) = v(fx) - (v(f)+v(hpi))
        hmi = hmi + 1
        hpi = hpi + 1
      End Do
210   v(hmi) = v(f) - two*v(fx)
!
!     ***  COMPUTE FUNCTION VALUES NEEDED TO COMPLETE ROW M OF H.  ***
!
      i = 1
!
220   iv(savei) = i
      stpi = stp0 + i
      v(delta) = x(i)
      x(i) = x(i) + v(stpi)
      If (i==m) x(i) = v(xmsave) - v(stpi)
      irt = 1
      Go To 280
!
230   x(i) = v(delta)
      If (iv(toobig)==0) Go To 240
!        ***  PUNT IN THE EVENT OF AN OVERSIZE STEP  ***
      iv(fdh) = -2
      Go To 270
!
!     ***  FINISH COMPUTING H(M,I)  ***
!
240   stpi = stp0 + i
      hmi = hes + mm1o2 + i - 1
      stpm = stp0 + m
      v(hmi) = (v(hmi)+v(f))/(v(stpi)*v(stpm))
      i = i + 1
      If (i<=m) Go To 220
      iv(savei) = 0
      x(m) = v(xmsave)
!
250   m = m + 1
      iv(mode) = m
      If (m>p) Go To 260
!
!     ***  PREPARE TO COMPUTE ROW M OF THE FINITE-DIFFERENCE HESSIAN H.
!     ***  COMPUTE M-TH STEP SIZE STP(M), THEN RETURN TO OBTAIN
!     ***  F(X + STP(M)*E(M)), WHERE E(M) = M-TH STD. UNIT VECTOR.
!
      del = v(dltfdc)*max(one/d(m),abs(x(m)))
      If (x(m)<zero) del = -del
      v(xmsave) = x(m)
      x(m) = x(m) + del
      stpm = stp0 + m
      v(stpm) = del
      irt = 1
      Go To 280
!
!     ***  RESTORE V(F), ETC.  ***
!
260   iv(fdh) = hes
270   v(f) = v(fx)
      irt = 3
      If (ikind<0) Go To 280
      iv(nfgcal) = iv(switch)
      gsave1 = iv(w) + p
      Call dv7cpy(p,g,v(gsave1))
      Go To 280
!
280   Return
!     ***  LAST CARD OF DF7HES FOLLOWS  ***
    End Subroutine df7hes
    Subroutine drnsg(a,alf,c,da,in,iv,l,l1,la,liv,lv,n,nda,p,v,y)
!
!     ***  ITERATION DRIVER FOR SEPARABLE NONLINEAR LEAST SQUARES.
!
!     ***  PARAMETER DECLARATIONS  ***
!
!     DIMENSION UIPARM(*)
!
!     ***  PURPOSE  ***
!
!     GIVEN A SET OF N OBSERVATIONS Y(1)....Y(N) OF A DEPENDENT VARIABLE
!     T(1)...T(N),  DRNSG ATTEMPTS TO COMPUTE A LEAST SQUARES FIT
!     TO A FUNCTION  ETA  (THE MODEL) WHICH IS A LINEAR COMBINATION
!
!                  L
!     ETA(C,ALF,T) =  SUM C * PHI(ALF,T) +PHI   (ALF,T)
!                 J=1  J     J           L+1
!
!     OF NONLINEAR FUNCTIONS PHI(J) DEPENDENT ON T AND ALF(1),...,ALF(P)
!     (.E.G. A SUM OF EXPONENTIALS OR GAUSSIANS).  THAT IS, IT DETERMINES
!     NONLINEAR PARAMETERS ALF WHICH MINIMIZE
!
!                   2    N                      2
!     NORM(RESIDUAL)  = SUM  (Y - ETA(C,ALF,T )).
!                       I=1    I             I
!
!     THE (L+1)ST TERM IS OPTIONAL.
!
!
!     ***  PARAMETERS  ***
!
!      A (IN)  MATRIX PHI(ALF,T) OF THE MODEL.
!     ALF (I/O) NONLINEAR PARAMETERS.
!                 INPUT = INITIAL GUESS,
!                 OUTPUT = BEST ESTIMATE FOUND.
!      C (OUT) LINEAR PARAMETERS (ESTIMATED).
!     DA (IN)  DERIVATIVES OF COLUMNS OF A WITH RESPECT TO COMPONENTS
!                 OF ALF, AS SPECIFIED BY THE IN ARRAY...
!     IN (IN)  WHEN  DRNSG IS CALLED WITH IV(1) = 2 OR -2, THEN FOR
!                 I = 1(1)NDA, COLUMN I OF DA IS THE PARTIAL
!                 DERIVATIVE WITH RESPECT TO ALF(IN(1,I)) OF COLUMN
!                 IN(2,I) OF A, UNLESS IV(1,I) IS NOT POSITIVE (IN
!                 WHICH CASE COLUMN I OF DA IS IGNORED.  IV(1) = -2
!                 MEANS THERE ARE MORE COLUMNS OF DA TO COME AND
!                  DRNSG SHOULD RETURN FOR THEM.
!     IV (I/O) INTEGER PARAMETER AND SCRATCH VECTOR.   DRNSG RETURNS
!                 WITH IV(1) = 1 WHEN IT WANTS A TO BE EVALUATED AT
!                 ALF AND WITH IV(1) = 2 WHEN IT WANTS DA TO BE
!                 EVALUATED AT ALF.  WHEN CALLED WITH IV(1) = -2
!                 (AFTER A RETURN WITH IV(1) = 2),  DRNSG RETURNS
!                 WITH IV(1) = -2 TO GET MORE COLUMNS OF DA.
!      L (IN)  NUMBER OF LINEAR PARAMETERS TO BE ESTIMATED.
!     L1 (IN)  L+1 IF PHI(L+1) IS IN THE MODEL, L IF NOT.
!     LA (IN)  LEAD DIMENSION OF A.  MUST BE AT LEAST N.
!     LIV (IN)  LENGTH OF IV.  MUST BE AT LEAST 110 + L + P.
!     LV (IN)  LENGTH OF V.  MUST BE AT LEAST
!                 105 + 2*N + JLEN + L*(L+3)/2 + P*(2*P + 17),
!                 WHERE  JLEN = (L+P)*(N+L+P+1),  UNLESS NEITHER A
!                 COVARIANCE MATRIX NOR REGRESSION DIAGNOSTICS ARE
!                 REQUESTED, IN WHICH CASE  JLEN = N*P.
!      N (IN)  NUMBER OF OBSERVATIONS.
!     NDA (IN)  NUMBER OF COLUMNS IN DA AND IN.
!      P (IN)  NUMBER OF NONLINEAR PARAMETERS TO BE ESTIMATED.
!      V (I/O) FLOATING-POINT PARAMETER AND SCRATCH VECTOR.
!              IF A COVARIANCE ESTIMATE IS REQUESTED, IT IS FOR
!              (ALF,C) -- NONLINEAR PARAMETERS ORDERED FIRST,
!              FOLLOWED BY LINEAR PARAMETERS.
!      Y (IN)  RIGHT-HAND SIDE VECTOR.
!
!
!     ***  EXTERNAL SUBROUTINES  ***
!
!
!     DC7VFN... FINISHES COVARIANCE COMPUTATION.
!     DIVSET.... SUPPLIES DEFAULT PARAMETER VALUES.
!     DD7TPR... RETURNS INNER PRODUCT OF TWO VECTORS.
!     DITSUM.... PRINTS ITERATION SUMMARY, INITIAL AND FINAL ALF.
!     DL7ITV... APPLIES INVERSE-TRANSPOSE OF COMPACT LOWER TRIANG. MATRIX.
!     DL7SRT.... COMPUTES (PARTIAL) CHOLESKY FACTORIZATION.
!     DL7SVX... ESTIMATES LARGEST SING. VALUE OF LOWER TRIANG. MATRIX.
!     DL7SVN... ESTIMATES SMALLEST SING. VALUE OF LOWER TRIANG. MATRIX.
!     DN2CVP... PRINTS COVARIANCE MATRIX.
!     DN2LRD... COMPUTES COVARIANCE AND REGRESSION DIAGNOSTICS.
!     DN2RDP... PRINTS REGRESSION DIAGNOSTICS.
!     DRN2G... UNDERLYING NONLINEAR LEAST-SQUARES SOLVER.
!     DQ7APL... APPLIES HOUSEHOLDER TRANSFORMS STORED BY DQ7RFH.
!     DQ7RFH.... COMPUTES QR FACT. VIA HOUSEHOLDER TRANSFORMS WITH PIVOTING.
!     DQ7RAD.... QR FACT., NO PIVOTING.
!     DR7MDC... RETURNS MACHINE-DEP. CONSTANTS.
!     DS7CPR... PRINTS LINEAR PARAMETERS AT SOLUTION.
!     DV2AXY.... ADDS MULTIPLE OF ONE VECTOR TO ANOTHER.
!     DV7CPY.... COPIES ONE VECTOR TO ANOTHER.
!     DV7PRM.... PERMUTES A VECTOR.
!     DV7SCL... SCALES AND COPIES ONE VECTOR TO ANOTHER.
!     DV7SCP... SETS ALL COMPONENTS OF A VECTOR TO A SCALAR.
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!
!     ***  IV SUBSCRIPT VALUES  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Integer, Parameter               :: ar = 110, cnvcod = 55, covmat = 26,  &
                                          covreq = 15, csave = 105,            &
                                          cvrqsv = 106, d = 27, fdh = 74,      &
                                          h = 56, iers = 108, ipivs = 109,     &
                                          iv1sav = 104, ivneed = 3, j = 70,    &
                                          lmat = 42, mode = 35, nextiv = 46,   &
                                          nextv = 47, nfcall = 6, nfcov = 52,  &
                                          nfgcal = 7, ngcall = 30, ngcov = 53, &
                                          perm = 58, r = 61, rcond = 53,       &
                                          rdreq = 57, rdrqsv = 107, regd = 67, &
                                          regd0 = 82, restor = 9, toobig = 2,  &
                                          vneed = 4
!     .. Scalar Arguments ..
      Integer                          :: l, l1, la, liv, lv, n, nda, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(la,l1), alf(p), c(l), da(la,nda),  &
                                          v(lv), y(n)
      Integer                          :: in(2,nda), iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp), Save             :: machep, negone, sngfac, zero
      Real (Kind=wp)                   :: singtl, t
      Integer                          :: ar1, csave1, d1, dr1, dr1l, dri,     &
                                          dri1, fdh0, hsave, i, i1, ier,       &
                                          ipiv1, iv1, j1, jlen, k, lh, li,     &
                                          ll1o2, md, n1, n2, nml, nran, pp,    &
                                          pp1, r1, r1l, rd1, temp1
      Logical                          :: nocov
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, dl7svn, dl7svx, dr7mdc
      External                         :: dc7vfn, ditsum, divset, dl7itv,      &
                                          dl7srt, dn2cvp, dn2lrd, dn2rdp,      &
                                          dq7apl, dq7rad, dq7rfh, drn2g,       &
                                          ds7cpr, dv2axy, dv7cpy, dv7prm,      &
                                          dv7scl, dv7scp
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max, mod, real
!     .. Data Statements ..
      Data machep/ -1.E+0_wp/, negone/ -1.E+0_wp/, sngfac/1.E+2_wp/,           &
        zero/0.E+0_wp/
!     .. Executable Statements ..
!
!     ++++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++
!
!
      If (iv(1)==0) Call divset(1,iv,liv,lv,v)
      n1 = 1
      nml = n
      iv1 = iv(1)
      If (iv1<=2) Go To 110
!
!     ***  CHECK INPUT INTEGERS  ***
!
      If (p<=0) Go To 400
      If (l<0) Go To 400
      If (n<=l) Go To 400
      If (la<n) Go To 400
      If (iv1<12) Go To 110
      If (iv1==14) Go To 110
      If (iv1==12) iv(1) = 13
!
!     ***  FRESH START -- COMPUTE STORAGE REQUIREMENTS  ***
!
      If (iv(1)>16) Go To 400
      ll1o2 = l*(l+1)/2
      jlen = n*p
      i = l + p
      If (iv(rdreq)>0 .And. iv(covreq)/=0) jlen = i*(n+i+1)
      If (iv(1)/=13) Go To 100
      iv(ivneed) = iv(ivneed) + l
      iv(vneed) = iv(vneed) + p + 2*n + jlen + ll1o2 + l
100   If (iv(perm)<=ar) iv(perm) = ar + 1
      Call drn2g(v,v,iv,liv,lv,n,n,n1,nml,p,v,v,v,alf)
      If (iv(1)/=14) Go To 410
!
!     ***  STORAGE ALLOCATION  ***
!
      iv(ipivs) = iv(nextiv)
      iv(nextiv) = iv(nextiv) + l
      iv(d) = iv(nextv)
      iv(regd0) = iv(d) + p
      iv(ar) = iv(regd0) + n
      iv(csave) = iv(ar) + ll1o2
      iv(j) = iv(csave) + l
      iv(r) = iv(j) + jlen
      iv(nextv) = iv(r) + n
      iv(iers) = 0
      If (iv1==13) Go To 410
!
!     ***  SET POINTERS INTO IV AND V  ***
!
110   ar1 = iv(ar)
      d1 = iv(d)
      dr1 = iv(j)
      dr1l = dr1 + l
      r1 = iv(r)
      r1l = r1 + l
      rd1 = iv(regd0)
      csave1 = iv(csave)
      nml = n - l
      If (iv1<=2) Go To 130
!
!     ***  IF F.D. HESSIAN WILL BE NEEDED (FOR COVARIANCE OR REG.
!     ***  DIAGNOSTICS), HAVE  DRN2G COMPUTE ONLY THE PART CORRESP.
!     ***  TO ALF WITH C FIXED...
!
      If (l<=0) Go To 120
      iv(cvrqsv) = iv(covreq)
      If (abs(iv(covreq))>=3) iv(covreq) = 0
      iv(rdrqsv) = iv(rdreq)
      If (iv(rdreq)>0) iv(rdreq) = -1
!
120   n2 = nml
      Call drn2g(v(d1),v(dr1l),iv,liv,lv,nml,n,n1,n2,p,v(r1l),v(rd1),v,alf)
      If (abs(iv(restor)-2)==1 .And. l>0) Call dv7cpy(l,c,v(csave1))
      iv1 = iv(1)
      If (iv1==2) Go To 220
      If (iv1>2) Go To 290
!
!     ***  NEW FUNCTION VALUE (RESIDUAL) NEEDED  ***
!
      iv(iv1sav) = iv(1)
      iv(1) = abs(iv1)
      If (iv(restor)==2 .And. l>0) Call dv7cpy(l,v(csave1),c)
      Go To 410
!
!     ***  COMPUTE NEW RESIDUAL OR GRADIENT  ***
!
130   iv(1) = iv(iv1sav)
      md = iv(mode)
      If (md<=0) Go To 140
      nml = n
      dr1l = dr1
      r1l = r1
140   If (iv(toobig)/=0) Go To 120
      If (abs(iv1)==2) Go To 240
!
!     ***  COMPUTE NEW RESIDUAL  ***
!
      If (l1<=l) Call dv7cpy(n,v(r1),y)
      If (l1>l) Call dv2axy(n,v(r1),negone,a(1,l1),y)
      If (md>0) Go To 200
      ier = 0
      If (l<=0) Go To 190
      ll1o2 = l*(l+1)/2
      ipiv1 = iv(ipivs)
      Call dq7rfh(ier,iv(ipiv1),n,la,0,l,a,v(ar1),ll1o2,c)
!
!     *** DETERMINE NUMERICAL RANK OF A ***
!
      If (machep<=zero) machep = dr7mdc(3)
      singtl = sngfac*real(max(l,n),kind=wp)*machep
      k = l
      If (ier/=0) k = ier - 1
150   If (k<=0) Go To 170
      t = dl7svx(k,v(ar1),c,c)
      If (t>zero) t = dl7svn(k,v(ar1),c,c)/t
      If (t>singtl) Go To 160
      k = k - 1
      Go To 150
!
!     *** RECORD RANK IN IV(IERS)... IV(IERS) = 0 MEANS FULL RANK,
!     *** IV(IERS) .GT. 0 MEANS RANK IV(IERS) - 1.
!
160   If (k>=l) Go To 180
170   ier = k + 1
      Call dv7scp(l-k,c(k+1),zero)
180   iv(iers) = ier
      If (k<=0) Go To 190
!
!     *** APPLY HOUSEHOLDER TRANSFORMATONS TO RESIDUALS...
!
      Call dq7apl(la,n,k,a,v(r1),ier)
!
!     *** COMPUTING C NOW MAY SAVE A FUNCTION EVALUATION AT
!     *** THE LAST ITERATION.
!
      Call dl7itv(k,c,v(ar1),v(r1))
      Call dv7prm(l,iv(ipiv1),c)
!
190   If (iv(1)<2) Go To 280
      Go To 410
!
!
!     ***  RESIDUAL COMPUTATION FOR F.D. HESSIAN  ***
!
200   If (l<=0) Go To 210
      Do i = 1, l
        Call dv2axy(n,v(r1),-c(i),a(1,i),v(r1))
      End Do
210   If (iv(1)>0) Go To 120
      iv(1) = 2
      Go To 230
!
!     ***  NEW GRADIENT (JACOBIAN) NEEDED  ***
!
220   iv(iv1sav) = iv1
      If (iv(nfgcal)/=iv(nfcall)) iv(1) = 1
230   Call dv7scp(n*p,v(dr1),zero)
      Go To 410
!
!     ***  COMPUTE NEW JACOBIAN  ***
!
240   nocov = md <= p .Or. abs(iv(covreq)) >= 3
      fdh0 = dr1 + n*(p+l)
      If (nda<=0) Go To 400
      Do i = 1, nda
        i1 = in(1,i) - 1
        If (i1<0) Go To 250
        j1 = in(2,i)
        k = dr1 + i1*n
        t = negone
        If (j1<=l) t = -c(j1)
        Call dv2axy(n,v(k),t,da(1,i),v(k))
        If (nocov) Go To 250
        If (j1>l) Go To 250
!        ***  ADD IN (L,P) PORTION OF SECOND-ORDER PART OF HESSIAN
!        ***  FOR COVARIANCE OR REG. DIAG. COMPUTATIONS...
        j1 = j1 + p
        k = fdh0 + j1*(j1-1)/2 + i1
        v(k) = v(k) - dd7tpr(n,v(r1),da(1,i))
250   End Do
      If (iv1==2) Go To 260
      iv(1) = iv1
      Go To 410
260   If (l<=0) Go To 120
      If (md>p) Go To 300
      If (md>0) Go To 120
      k = dr1
      ier = iv(iers)
      nran = l
      If (ier>0) nran = ier - 1
      If (nran<=0) Go To 270
      Do i = 1, p
        Call dq7apl(la,n,nran,a,v(k),ier)
        k = k + n
      End Do
270   Call dv7cpy(l,v(csave1),c)
280   If (ier==0) Go To 120
!
!     *** ADJUST SUBSCRIPTS DESCRIBING R AND DR...
!
      nran = ier - 1
      dr1l = dr1 + nran
      nml = n - nran
      r1l = r1 + nran
      Go To 120
!
!     ***  CONVERGENCE OR LIMIT REACHED  ***
!
290   If (l<=0) Go To 380
      iv(covreq) = iv(cvrqsv)
      iv(rdreq) = iv(rdrqsv)
      If (iv(1)>6) Go To 390
      If (mod(iv(rdreq),4)==0) Go To 390
      If (iv(fdh)<=0 .And. abs(iv(covreq))<3) Go To 390
      If (iv(regd)>0) Go To 390
      If (iv(covmat)>0) Go To 390
!
!     *** PREPARE TO FINISH COMPUTING COVARIANCE MATRIX AND REG. DIAG. ***
!
      pp = l + p
      i = 0
      If (mod(iv(rdreq),4)>=2) i = 1
      If (mod(iv(rdreq),2)==1 .And. abs(iv(covreq))==1) i = i + 2
      iv(mode) = pp + i
      i = dr1 + n*pp
      k = p*(p+1)/2
      i1 = iv(lmat)
      Call dv7cpy(k,v(i),v(i1))
      i = i + k
      Call dv7scp(pp*(pp+1)/2-k,v(i),zero)
      iv(nfcov) = iv(nfcov) + 1
      iv(nfcall) = iv(nfcall) + 1
      iv(nfgcal) = iv(nfcall)
      iv(cnvcod) = iv(1)
      iv(iv1sav) = -1
      iv(1) = 1
      iv(ngcall) = iv(ngcall) + 1
      iv(ngcov) = iv(ngcov) + 1
      Go To 410
!
!     ***  FINISH COVARIANCE COMPUTATION  ***
!
300   i = dr1 + n*p
      Do i1 = 1, l
        Call dv7scl(n,v(i),negone,a(1,i1))
        i = i + n
      End Do
      pp = l + p
      hsave = iv(h)
      k = dr1 + n*pp
      lh = pp*(pp+1)/2
      If (abs(iv(covreq))<3) Go To 320
      i = iv(mode) - 4
      If (i>=pp) Go To 310
      Call dv7scp(lh,v(k),zero)
      Call dq7rad(n,n,pp,v,.False.,v(k),v(dr1),v)
      iv(mode) = i + 8
      iv(1) = 2
      iv(ngcall) = iv(ngcall) + 1
      iv(ngcov) = iv(ngcov) + 1
      Go To 230
!
310   iv(mode) = i
      Go To 330
!
320   pp1 = p + 1
      dri = dr1 + n*p
      li = k + p*pp1/2
      Do i = pp1, pp
        dri1 = dr1
        Do i1 = 1, i
          v(li) = v(li) + dd7tpr(n,v(dri),v(dri1))
          li = li + 1
          dri1 = dri1 + n
        End Do
        dri = dri + n
      End Do
      Call dl7srt(pp1,pp,v(k),v(k),i)
      If (i/=0) Go To 340
330   temp1 = k + lh
      t = dl7svn(pp,v(k),v(temp1),v(temp1))
      If (t<=zero) Go To 340
      t = t/dl7svx(pp,v(k),v(temp1),v(temp1))
      v(rcond) = t
      If (t>dr7mdc(4)) Go To 350
340   iv(regd) = -1
      iv(covmat) = -1
      iv(fdh) = -1
      Go To 370
350   iv(h) = temp1
      iv(fdh) = abs(hsave)
      If (iv(mode)-pp<2) Go To 360
      i = iv(h)
      Call dv7scp(lh,v(i),zero)
360   Call dn2lrd(v(dr1),iv,v(k),lh,liv,lv,n,n,pp,v(r1),v(rd1),v)
370   Call dc7vfn(iv,v(k),lh,liv,lv,n,pp,v)
      iv(h) = hsave
!
380   If (iv(regd)==1) iv(regd) = rd1
390   If (iv(1)<=11) Call ds7cpr(c,iv,l,liv)
      If (iv(1)>6) Go To 410
      Call dn2cvp(iv,liv,lv,p+l,v)
      Call dn2rdp(iv,liv,lv,n,v(rd1),v)
      Go To 410
!
400   iv(1) = 66
      Call ditsum(v,v,iv,liv,lv,p,v,alf)
!
410   Return
!
!     ***  LAST CARD OF  DRNSG FOLLOWS  ***
    End Subroutine drnsg
    Subroutine dl7tvm(n,x,l,y)
!
!     ***  COMPUTE  X = (L**T)*Y, WHERE  L  IS AN  N X N  LOWER
!     ***  TRIANGULAR MATRIX STORED COMPACTLY BY ROWS.  X AND Y MAY
!     ***  OCCUPY THE SAME STORAGE.  ***
!
!     DIMENSION L(N*(N+1)/2)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
!     .. Scalar Arguments ..
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: l(*), x(n), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: yi
      Integer                          :: i, i0, ij, j
!     .. Executable Statements ..
!
      i0 = 0
      Do i = 1, n
        yi = y(i)
        x(i) = zero
        Do j = 1, i
          ij = i0 + j
          x(j) = x(j) + yi*l(ij)
        End Do
        i0 = i0 + i
      End Do
      Return
!     ***  LAST CARD OF DL7TVM FOLLOWS  ***
    End Subroutine dl7tvm
    Subroutine dl7itv(n,x,l,y)
!
!     ***  SOLVE  (L**T)*X = Y,  WHERE  L  IS AN  N X N  LOWER TRIANGULAR
!     ***  MATRIX STORED COMPACTLY BY ROWS.  X AND Y MAY OCCUPY THE SAME
!     ***  STORAGE.  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
!     .. Scalar Arguments ..
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: l(*), x(n), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: xi
      Integer                          :: i, i0, ii, ij, im1, j, np1
!     .. Executable Statements ..
!
      Do i = 1, n
        x(i) = y(i)
      End Do
      np1 = n + 1
      i0 = n*(n+1)/2
      Do ii = 1, n
        i = np1 - ii
        xi = x(i)/l(i0)
        x(i) = xi
        If (i<=1) Go To 110
        i0 = i0 - i
        If (xi==zero) Go To 100
        im1 = i - 1
        Do j = 1, im1
          ij = i0 + j
          x(j) = x(j) - xi*l(ij)
        End Do
100   End Do
110   Return
!     ***  LAST CARD OF DL7ITV FOLLOWS  ***
    End Subroutine dl7itv
    Subroutine drmngb(b,d,fx,g,iv,liv,lv,n,v,x)
!
!     ***  CARRY OUT  DMNGB (SIMPLY BOUNDED MINIMIZATION) ITERATIONS,
!     ***  USING DOUBLE-DOGLEG/BFGS STEPS.
!
!     ***  PARAMETER DECLARATIONS  ***
!
!
!     --------------------------  PARAMETER USAGE  --------------------------
!
!     B.... VECTOR OF LOWER AND UPPER BOUNDS ON X.
!     D.... SCALE VECTOR.
!     FX... FUNCTION VALUE.
!     G.... GRADIENT VECTOR.
!     IV... INTEGER VALUE ARRAY.
!     LIV.. LENGTH OF IV (AT LEAST 59) + N.
!     LV... LENGTH OF V (AT LEAST 71 + N*(N+19)/2).
!     N.... NUMBER OF VARIABLES (COMPONENTS IN X AND G).
!     V.... FLOATING-POINT VALUE ARRAY.
!     X.... VECTOR OF PARAMETERS TO BE OPTIMIZED.
!
!     ***  DISCUSSION  ***
!
!        PARAMETERS IV, N, V, AND X ARE THE SAME AS THE CORRESPONDING
!     ONES TO  DMNGB (WHICH SEE), EXCEPT THAT V CAN BE SHORTER (SINCE
!     THE PART OF V THAT  DMNGB USES FOR STORING G IS NOT NEEDED).
!     MOREOVER, COMPARED WITH  DMNGB, IV(1) MAY HAVE THE TWO ADDITIONAL
!     OUTPUT VALUES 1 AND 2, WHICH ARE EXPLAINED BELOW, AS IS THE USE
!     OF IV(TOOBIG) AND IV(NFGCAL).  THE VALUE IV(G), WHICH IS AN
!     OUTPUT VALUE FROM  DMNGB (AND SMSNOB), IS NOT REFERENCED BY
!     DRMNGB OR THE SUBROUTINES IT CALLS.
!        FX AND G NEED NOT HAVE BEEN INITIALIZED WHEN DRMNGB IS CALLED
!     WITH IV(1) = 12, 13, OR 14.
!
!     IV(1) = 1 MEANS THE CALLER SHOULD SET FX TO F(X), THE FUNCTION VALUE
!             AT X, AND CALL DRMNGB AGAIN, HAVING CHANGED NONE OF THE
!             OTHER PARAMETERS.  AN EXCEPTION OCCURS IF F(X) CANNOT BE
!             (E.G. IF OVERFLOW WOULD OCCUR), WHICH MAY HAPPEN BECAUSE
!             OF AN OVERSIZED STEP.  IN THIS CASE THE CALLER SHOULD SET
!             IV(TOOBIG) = IV(2) TO 1, WHICH WILL CAUSE DRMNGB TO IG-
!             NORE FX AND TRY A SMALLER STEP.  THE PARAMETER NF THAT
!              DMNGB PASSES TO CALCF (FOR POSSIBLE USE BY CALCG) IS A
!             COPY OF IV(NFCALL) = IV(6).
!     IV(1) = 2 MEANS THE CALLER SHOULD SET G TO G(X), THE GRADIENT VECTOR
!             OF F AT X, AND CALL DRMNGB AGAIN, HAVING CHANGED NONE OF
!             THE OTHER PARAMETERS EXCEPT POSSIBLY THE SCALE VECTOR D
!             WHEN IV(DTYPE) = 0.  THE PARAMETER NF THAT  DMNGB PASSES
!             TO CALCG IS IV(NFGCAL) = IV(7).  IF G(X) CANNOT BE
!             EVALUATED, THEN THE CALLER MAY SET IV(NFGCAL) TO 0, IN
!             WHICH CASE DRMNGB WILL RETURN WITH IV(1) = 65.
!.
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY (DECEMBER 1979).  REVISED SEPT. 1982.
!     THIS SUBROUTINE WAS WRITTEN IN CONNECTION WITH RESEARCH SUPPORTED
!     IN PART BY THE NATIONAL SCIENCE FOUNDATION UNDER GRANTS
!     MCS-7600324 AND MCS-7906671.
!
!        (SEE  DMNG FOR REFERENCES.)
!
!     +++++++++++++++++++++++++++  DECLARATIONS  ++++++++++++++++++++++++++++
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  CONSTANTS  ***
!
!
!     ***  NO INTRINSIC FUNCTIONS  ***
!
!     ***  EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!
!     DA7SST.... ASSESSES CANDIDATE STEP.
!     DD7DGB... COMPUTES SIMPLY BOUNDED DOUBLE-DOGLEG (CANDIDATE) STEP.
!     DIVSET.... SUPPLIES DEFAULT IV AND V INPUT COMPONENTS.
!     DD7TPR... RETURNS INNER PRODUCT OF TWO VECTORS.
!     I7SHFT... CYCLICALLLY SHIFTS AN ARRAY OF INTEGERS.
!     DITSUM.... PRINTS ITERATION SUMMARY AND INFO ON INITIAL AND FINAL X.
!     DL7TVM... MULTIPLIES TRANSPOSE OF LOWER TRIANGLE TIMES VECTOR.
!     LUPDT.... UPDATES CHOLESKY FACTOR OF HESSIAN APPROXIMATION.
!     DL7VML.... MULTIPLIES LOWER TRIANGLE TIMES VECTOR.
!     DPARCK.... CHECKS VALIDITY OF INPUT IV AND V VALUES.
!     DQ7RSH... CYCLICALLY SHIFTS CHOLESKY FACTOR.
!     DRLDST... COMPUTES V(RELDX) = RELATIVE STEP SIZE.
!     STOPX.... RETURNS .TRUE. IF THE BREAK KEY HAS BEEN PRESSED.
!     DV2NRM... RETURNS THE 2-NORM OF A VECTOR.
!     DV2AXY.... COMPUTES SCALAR TIMES ONE VECTOR PLUS ANOTHER.
!     DV7CPY.... COPIES ONE VECTOR TO ANOTHER.
!     DV7IPR... CYCLICALLY SHIFTS A FLOATING-POINT ARRAY.
!     DV7SCP... SETS ALL ELEMENTS OF A VECTOR TO A SCALAR.
!     DV7VMP... MULTIPLIES VECTOR BY VECTOR RAISED TO POWER (COMPONENTWISE).
!     DW7ZBF... COMPUTES W AND Z FOR DL7UPD CORRESPONDING TO BFGS UPDATE.
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!
!     ***  IV SUBSCRIPT VALUES  ***
!
!     ***  (NOTE THAT NC IS STORED IN IV(G0)) ***
!
!
!     ***  V SUBSCRIPT VALUES  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: negone = -1.E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: onep2 = 1.2E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: cnvcod = 55, dg = 37, dgnorm = 1,    &
                                          dinit = 38, dstnrm = 2, f = 10,      &
                                          f0 = 13, fdif = 11, gtstep = 4,      &
                                          incfac = 23, inith = 25, irc = 29,   &
                                          ivneed = 3, kagqt = 33, lmat = 42,   &
                                          lmax0 = 35, lmaxs = 36, mode = 35,   &
                                          model = 5, mxfcal = 17, mxiter = 18, &
                                          nc = 48, nextiv = 46, nextv = 47,    &
                                          nfcall = 6, nfgcal = 7, ngcall = 30, &
                                          niter = 31, nwtstp = 34, perm = 58,  &
                                          preduc = 7, rad0 = 9, radfac = 16,   &
                                          radinc = 8, radius = 8, reldx = 17,  &
                                          restor = 9, step = 40, stglim = 11,  &
                                          stlstg = 41, toobig = 2,             &
                                          tuner4 = 29, tuner5 = 30, vneed = 4, &
                                          x0 = 43, xirc = 13
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: fx
      Integer                          :: liv, lv, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(2,n), d(n), g(n), v(lv), x(n)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: gi, t, xi
      Integer                          :: dg1, dstep1, dummy, g01, i, i1, ipi, &
                                          ipn, j, k, l, lstgst, n1, np1,       &
                                          nwtst1, rstrst, step1, td1, temp0,   &
                                          temp1, tg1, w1, x01, z
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, drldst, dv2nrm
      Logical, External                :: stopx
      External                         :: da7sst, dd7dgb, ditsum, divset,      &
                                          dl7tvm, dl7upd, dl7vml, dparck,      &
                                          dq7rsh, dv2axy, dv7cpy, dv7ipr,      &
                                          dv7scp, dv7vmp, dw7zbf, i7shft
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      i = iv(1)
      If (i==1) Go To 140
      If (i==2) Go To 150
!
!     ***  CHECK VALIDITY OF IV AND V INPUT VALUES  ***
!
      If (iv(1)==0) Call divset(2,iv,liv,lv,v)
      If (iv(1)<12) Go To 100
      If (iv(1)>13) Go To 100
      iv(vneed) = iv(vneed) + n*(n+19)/2
      iv(ivneed) = iv(ivneed) + n
100   Call dparck(2,d,iv,liv,lv,n,v)
      i = iv(1) - 2
      If (i>12) Go To 510
      Go To (320,320,320,320,320,320,260,220,260,110,110,120) i
!
!     ***  STORAGE ALLOCATION  ***
!
110   l = iv(lmat)
      iv(x0) = l + n*(n+1)/2
      iv(step) = iv(x0) + 2*n
      iv(stlstg) = iv(step) + 2*n
      iv(nwtstp) = iv(stlstg) + n
      iv(dg) = iv(nwtstp) + 2*n
      iv(nextv) = iv(dg) + 2*n
      iv(nextiv) = iv(perm) + n
      If (iv(1)/=13) Go To 120
      iv(1) = 14
      Go To 510
!
!     ***  INITIALIZATION  ***
!
120   iv(niter) = 0
      iv(nfcall) = 1
      iv(ngcall) = 1
      iv(nfgcal) = 1
      iv(mode) = -1
      iv(model) = 1
      iv(stglim) = 1
      iv(toobig) = 0
      iv(cnvcod) = 0
      iv(radinc) = 0
      iv(nc) = n
      v(rad0) = zero
!
!     ***  CHECK CONSISTENCY OF B AND INITIALIZE IP ARRAY  ***
!
      ipi = iv(perm)
      Do i = 1, n
        iv(ipi) = i
        ipi = ipi + 1
        If (b(1,i)>b(2,i)) Go To 470
      End Do
!
      If (v(dinit)>=zero) Call dv7scp(n,d,v(dinit))
      If (iv(inith)/=1) Go To 130
!
!     ***  SET THE INITIAL HESSIAN APPROXIMATION TO DIAG(D)**-2  ***
!
      l = iv(lmat)
      Call dv7scp(n*(n+1)/2,v(l),zero)
      k = l - 1
      Do i = 1, n
        k = k + i
        t = d(i)
        If (t<=zero) t = one
        v(k) = t
      End Do
!
!     ***  GET INITIAL FUNCTION VALUE  ***
!
130   iv(1) = 1
      Go To 500
!
140   v(f) = fx
      If (iv(mode)>=0) Go To 320
      v(f0) = fx
      iv(1) = 2
      If (iv(toobig)==0) Go To 510
      iv(1) = 63
      Go To 490
!
!     ***  MAKE SURE GRADIENT COULD BE COMPUTED  ***
!
150   If (iv(toobig)==0) Go To 160
      iv(1) = 65
      Go To 490
!
!     ***  CHOOSE INITIAL PERMUTATION  ***
!
160   ipi = iv(perm)
      ipn = ipi + n
      n1 = n
      np1 = n + 1
      l = iv(lmat)
      w1 = iv(nwtstp) + n
      k = n - iv(nc)
      Do i = 1, n
        ipn = ipn - 1
        j = iv(ipn)
        If (b(1,j)>=b(2,j)) Go To 170
        xi = x(j)
        gi = g(j)
        If (xi<=b(1,j) .And. gi>zero) Go To 170
        If (xi>=b(2,j) .And. gi<zero) Go To 170
!           *** DISALLOW CONVERGENCE IF X(J) HAS JUST BEEN FREED ***
        If (i<=k) iv(cnvcod) = 0
        Go To 190
170     i1 = np1 - i
        If (i1>=n1) Go To 180
        Call i7shft(n1,i1,iv(ipi))
        Call dq7rsh(i1,n1,.False.,g,v(l),v(w1))
180     n1 = n1 - 1
190   End Do
!
      iv(nc) = n1
      v(dgnorm) = zero
      If (n1<=0) Go To 200
      dg1 = iv(dg)
      Call dv7vmp(n,v(dg1),g,d,-1)
      Call dv7ipr(n,iv(ipi),v(dg1))
      v(dgnorm) = dv2nrm(n1,v(dg1))
200   If (iv(cnvcod)/=0) Go To 480
      If (iv(mode)==0) Go To 430
!
!     ***  ALLOW FIRST STEP TO HAVE SCALED 2-NORM AT MOST V(LMAX0)  ***
!
      v(radius) = v(lmax0)
!
      iv(mode) = 0
!
!
!     -----------------------------  MAIN LOOP  -----------------------------
!
!
!     ***  PRINT ITERATION SUMMARY, CHECK ITERATION LIMIT  ***
!
210   Call ditsum(d,g,iv,liv,lv,n,v,x)
220   k = iv(niter)
      If (k<iv(mxiter)) Go To 230
      iv(1) = 10
      Go To 490
!
!     ***  UPDATE RADIUS  ***
!
230   iv(niter) = k + 1
      If (k==0) Go To 240
      t = v(radfac)*v(dstnrm)
      If (v(radfac)<one .Or. t>v(radius)) v(radius) = t
!
!     ***  INITIALIZE FOR START OF NEXT ITERATION  ***
!
240   x01 = iv(x0)
      v(f0) = v(f)
      iv(irc) = 4
      iv(kagqt) = -1
!
!     ***  COPY X TO X0  ***
!
      Call dv7cpy(n,v(x01),x)
!
!     ***  CHECK STOPX AND FUNCTION EVALUATION LIMIT  ***
!
250   If (.Not. stopx(dummy)) Go To 270
      iv(1) = 11
      Go To 280
!
!     ***  COME HERE WHEN RESTARTING AFTER FUNC. EVAL. LIMIT OR STOPX.
!
260   If (v(f)>=v(f0)) Go To 270
      v(radfac) = one
      k = iv(niter)
      Go To 230
!
270   If (iv(nfcall)<iv(mxfcal)) Go To 290
      iv(1) = 9
280   If (v(f)>=v(f0)) Go To 490
!
!        ***  IN CASE OF STOPX OR FUNCTION EVALUATION LIMIT WITH
!        ***  IMPROVED V(F), EVALUATE THE GRADIENT AT X.
!
      iv(cnvcod) = iv(1)
      Go To 420
!
!     . . . . . . . . . . . . .  COMPUTE CANDIDATE STEP  . . . . . . . . . .
!
290   step1 = iv(step)
      dg1 = iv(dg)
      nwtst1 = iv(nwtstp)
      w1 = nwtst1 + n
      dstep1 = step1 + n
      ipi = iv(perm)
      l = iv(lmat)
      tg1 = dg1 + n
      x01 = iv(x0)
      td1 = x01 + n
      Call dd7dgb(b,d,v(dg1),v(dstep1),g,iv(ipi),iv(kagqt),v(l),lv,n,iv(nc),   &
        v(nwtst1),v(step1),v(td1),v(tg1),v,v(w1),v(x01))
      If (iv(irc)/=6) Go To 300
      If (iv(restor)/=2) Go To 320
      rstrst = 2
      Go To 330
!
!     ***  CHECK WHETHER EVALUATING F(X0 + STEP) LOOKS WORTHWHILE  ***
!
300   iv(toobig) = 0
      If (v(dstnrm)<=zero) Go To 320
      If (iv(irc)/=5) Go To 310
      If (v(radfac)<=one) Go To 310
      If (v(preduc)>onep2*v(fdif)) Go To 310
      If (iv(restor)/=2) Go To 320
      rstrst = 0
      Go To 330
!
!     ***  COMPUTE F(X0 + STEP)  ***
!
310   Call dv2axy(n,x,one,v(step1),v(x01))
      iv(nfcall) = iv(nfcall) + 1
      iv(1) = 1
      Go To 500
!
!     . . . . . . . . . . . . .  ASSESS CANDIDATE STEP  . . . . . . . . . . .
!
320   rstrst = 3
330   x01 = iv(x0)
      v(reldx) = drldst(n,d,x,v(x01))
      Call da7sst(iv,liv,lv,v)
      step1 = iv(step)
      lstgst = iv(stlstg)
      i = iv(restor) + 1
      Go To (370,340,350,360) i
340   Call dv7cpy(n,x,v(x01))
      Go To 370
350   Call dv7cpy(n,v(lstgst),x)
      Go To 370
360   Call dv7cpy(n,x,v(lstgst))
      Call dv2axy(n,v(step1),negone,v(x01),x)
      v(reldx) = drldst(n,d,x,v(x01))
      iv(restor) = rstrst
!
370   k = iv(irc)
      Go To (380,410,410,410,380,390,400,400,400,400,400,400,460,430) k
!
!     ***  RECOMPUTE STEP WITH CHANGED RADIUS  ***
!
380   v(radius) = v(radfac)*v(dstnrm)
      Go To 250
!
!     ***  COMPUTE STEP OF LENGTH V(LMAXS) FOR SINGULAR CONVERGENCE TEST.
!
390   v(radius) = v(lmaxs)
      Go To 290
!
!     ***  CONVERGENCE OR FALSE CONVERGENCE  ***
!
400   iv(cnvcod) = k - 4
      If (v(f)>=v(f0)) Go To 480
      If (iv(xirc)==14) Go To 480
      iv(xirc) = 14
!
!     . . . . . . . . . . . .  PROCESS ACCEPTABLE STEP  . . . . . . . . . . .
!
410   x01 = iv(x0)
      step1 = iv(step)
      Call dv2axy(n,v(step1),negone,v(x01),x)
      If (iv(irc)/=3) Go To 420
!
!     ***  SET  TEMP1 = HESSIAN * STEP  FOR USE IN GRADIENT TESTS  ***
!
!     ***  USE X0 AS TEMPORARY...
!
      ipi = iv(perm)
      Call dv7cpy(n,v(x01),v(step1))
      Call dv7ipr(n,iv(ipi),v(x01))
      l = iv(lmat)
      Call dl7tvm(n,v(x01),v(l),v(x01))
      Call dl7vml(n,v(x01),v(l),v(x01))
!
!        *** UNPERMUTE X0 INTO TEMP1 ***
!
      temp1 = iv(stlstg)
      temp0 = temp1 - 1
      Do i = 1, n
        j = iv(ipi)
        ipi = ipi + 1
        k = temp0 + j
        v(k) = v(x01)
        x01 = x01 + 1
      End Do
!
!     ***  SAVE OLD GRADIENT, COMPUTE NEW ONE  ***
!
420   g01 = iv(nwtstp) + n
      Call dv7cpy(n,v(g01),g)
      iv(ngcall) = iv(ngcall) + 1
      iv(toobig) = 0
      iv(1) = 2
      Go To 510
!
!     ***  INITIALIZATIONS -- G0 = G - G0, ETC.  ***
!
430   g01 = iv(nwtstp) + n
      Call dv2axy(n,v(g01),negone,v(g01),g)
      step1 = iv(step)
      temp1 = iv(stlstg)
      If (iv(irc)/=3) Go To 450
!
!     ***  SET V(RADFAC) BY GRADIENT TESTS  ***
!
!     ***  SET  TEMP1 = DIAG(D)**-1 * (HESSIAN*STEP + (G(X0)-G(X)))  ***
!
      Call dv2axy(n,v(temp1),negone,v(g01),v(temp1))
      Call dv7vmp(n,v(temp1),v(temp1),d,-1)
!
!        ***  DO GRADIENT TESTS  ***
!
      If (dv2nrm(n,v(temp1))<=v(dgnorm)*v(tuner4)) Go To 440
      If (dd7tpr(n,g,v(step1))>=v(gtstep)*v(tuner5)) Go To 450
440   v(radfac) = v(incfac)
!
!     ***  UPDATE H, LOOP  ***
!
450   w1 = iv(nwtstp)
      z = iv(x0)
      l = iv(lmat)
      ipi = iv(perm)
      Call dv7ipr(n,iv(ipi),v(step1))
      Call dv7ipr(n,iv(ipi),v(g01))
      Call dw7zbf(v(l),n,v(step1),v(w1),v(g01),v(z))
!
!     ** USE THE N-VECTORS STARTING AT V(STEP1) AND V(G01) FOR SCRATCH..
      Call dl7upd(v(temp1),v(step1),v(l),v(g01),v(l),n,v(w1),v(z))
      iv(1) = 2
      Go To 210
!
!     . . . . . . . . . . . . . .  MISC. DETAILS  . . . . . . . . . . . . . .
!
!     ***  BAD PARAMETERS TO ASSESS  ***
!
460   iv(1) = 64
      Go To 490
!
!     ***  INCONSISTENT B  ***
!
470   iv(1) = 82
      Go To 490
!
!     ***  PRINT SUMMARY OF FINAL ITERATION AND OTHER REQUESTED ITEMS  ***
!
480   iv(1) = iv(cnvcod)
      iv(cnvcod) = 0
490   Call ditsum(d,g,iv,liv,lv,n,v,x)
      Go To 510
!
!     ***  PROJECT X INTO FEASIBLE REGION (PRIOR TO COMPUTING F OR G)  ***
!
500   Do i = 1, n
        If (x(i)<b(1,i)) x(i) = b(1,i)
        If (x(i)>b(2,i)) x(i) = b(2,i)
      End Do
!
510   Return
!
!     ***  LAST CARD OF DRMNGB FOLLOWS  ***
    End Subroutine drmngb
    Subroutine ds7grd(alpha,d,eta0,fx,g,irc,n,w,x)
!
!     ***  COMPUTE FINITE DIFFERENCE GRADIENT BY STWEART*S SCHEME  ***
!
!     ***  PARAMETERS  ***
!
!
!.......................................................................
!
!     ***  PURPOSE  ***
!
!        THIS SUBROUTINE USES AN EMBELLISHED FORM OF THE FINITE-DIFFER-
!     ENCE SCHEME PROPOSED BY STEWART (REF. 1) TO APPROXIMATE THE
!     GRADIENT OF THE FUNCTION F(X), WHOSE VALUES ARE SUPPLIED BY
!     REVERSE COMMUNICATION.
!
!     ***  PARAMETER DESCRIPTION  ***
!
!     ALPHA IN  (APPROXIMATE) DIAGONAL ELEMENTS OF THE HESSIAN OF F(X).
!      D IN  SCALE VECTOR SUCH THAT D(I)*X(I), I = 1,...,N, ARE IN
!             COMPARABLE UNITS.
!     ETA0 IN  ESTIMATED BOUND ON RELATIVE ERROR IN THE FUNCTION VALUE...
!             (TRUE VALUE) = (COMPUTED VALUE)*(1+E),   WHERE
!             ABS(E) .LE. ETA0.
!     FX I/O ON INPUT,  FX  MUST BE THE COMPUTED VALUE OF F(X).  ON
!             OUTPUT WITH IRC = 0, FX HAS BEEN RESTORED TO ITS ORIGINAL
!             VALUE, THE ONE IT HAD WHEN DS7GRD WAS LAST CALLED WITH
!             IRC = 0.
!      G I/O ON INPUT WITH IRC = 0, G SHOULD CONTAIN AN APPROXIMATION
!             TO THE GRADIENT OF F NEAR X, E.G., THE GRADIENT AT THE
!             PREVIOUS ITERATE.  WHEN DS7GRD RETURNS WITH IRC = 0, G IS
!             THE DESIRED FINITE-DIFFERENCE APPROXIMATION TO THE
!             GRADIENT AT X.
!     IRC I/O INPUT/RETURN CODE... BEFORE THE VERY FIRST CALL ON DS7GRD,
!             THE CALLER MUST SET IRC TO 0.  WHENEVER DS7GRD RETURNS A
!             NONZERO VALUE FOR IRC, IT HAS PERTURBED SOME COMPONENT OF
!             X... THE CALLER SHOULD EVALUATE F(X) AND CALL DS7GRD
!             AGAIN WITH FX = F(X).
!      N IN  THE NUMBER OF VARIABLES (COMPONENTS OF X) ON WHICH F
!             DEPENDS.
!      X I/O ON INPUT WITH IRC = 0, X IS THE POINT AT WHICH THE
!             GRADIENT OF F IS DESIRED.  ON OUTPUT WITH IRC NONZERO, X
!             IS THE POINT AT WHICH F SHOULD BE EVALUATED.  ON OUTPUT
!             WITH IRC = 0, X HAS BEEN RESTORED TO ITS ORIGINAL VALUE
!             (THE ONE IT HAD WHEN DS7GRD WAS LAST CALLED WITH IRC = 0)
!             AND G CONTAINS THE DESIRED GRADIENT APPROXIMATION.
!      W I/O WORK VECTOR OF LENGTH 6 IN WHICH DS7GRD SAVES CERTAIN
!             QUANTITIES WHILE THE CALLER IS EVALUATING F(X) AT A
!             PERTURBED X.
!
!     ***  APPLICATION AND USAGE RESTRICTIONS  ***
!
!        THIS ROUTINE IS INTENDED FOR USE WITH QUASI-NEWTON ROUTINES
!     FOR UNCONSTRAINED MINIMIZATION (IN WHICH CASE  ALPHA  COMES FROM
!     THE DIAGONAL OF THE QUASI-NEWTON HESSIAN APPROXIMATION).
!
!     ***  ALGORITHM NOTES  ***
!
!        THIS CODE DEPARTS FROM THE SCHEME PROPOSED BY STEWART (REF. 1)
!     IN ITS GUARDING AGAINST OVERLY LARGE OR SMALL STEP SIZES AND ITS
!     HANDLING OF SPECIAL CASES (SUCH AS ZERO COMPONENTS OF ALPHA OR G).
!
!     ***  REFERENCES  ***
!
!     1. STEWART, G.W. (1967), A MODIFICATION OF DAVIDON*S MINIMIZATION
!        METHOD TO ACCEPT DIFFERENCE APPROXIMATIONS OF DERIVATIVES,
!        J. ASSOC. COMPUT. MACH. 14, PP. 72-83.
!
!     ***  HISTORY  ***
!
!     DESIGNED AND CODED BY DAVID M. GAY (SUMMER 1977/SUMMER 1980).
!
!     ***  GENERAL  ***
!
!        THIS ROUTINE WAS PREPARED IN CONNECTION WITH WORK SUPPORTED BY
!     THE NATIONAL SCIENCE FOUNDATION UNDER GRANTS MCS76-00324 AND
!     MCS-7906671.
!
!.......................................................................
!
!     *****  EXTERNAL FUNCTION  *****
!
!     DR7MDC... RETURNS MACHINE-DEPENDENT CONSTANTS.
!
!     ***** INTRINSIC FUNCTIONS *****
!     /+
!/
!     ***** LOCAL VARIABLES *****
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: c2000 = 2.0E+3_wp
      Real (Kind=wp), Parameter        :: four = 4.0E+0_wp
      Real (Kind=wp), Parameter        :: hmax0 = 0.02E+0_wp
      Real (Kind=wp), Parameter        :: hmin0 = 5.0E+1_wp
      Real (Kind=wp), Parameter        :: one = 1.0E+0_wp
      Real (Kind=wp), Parameter        :: p002 = 0.002E+0_wp
      Real (Kind=wp), Parameter        :: three = 3.0E+0_wp
      Real (Kind=wp), Parameter        :: two = 2.0E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.0E+0_wp
      Integer, Parameter               :: fh = 3, fx0 = 4, hsave = 5,          &
                                          xisave = 6
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: eta0, fx
      Integer                          :: irc, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: alpha(n), d(n), g(n), w(6), x(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: aai, afx, afxeta, agi, alphai, axi,  &
                                          axibar, discon, eta, gi, h, h0,      &
                                          hmin, machep
      Integer                          :: i
!     .. External Procedures ..
      Real (Kind=wp), External         :: dr7mdc
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max, sqrt
!     .. Executable Statements ..
!
!     ---------------------------------  BODY  ------------------------------
!
      If (irc<0) Go To 130
      If (irc>0) Go To 190
!
!     ***  FRESH START -- GET MACHINE-DEPENDENT CONSTANTS  ***
!
!     STORE MACHEP IN W(1) AND H0 IN W(2), WHERE MACHEP IS THE UNIT
!     ROUNDOFF (THE SMALLEST POSITIVE NUMBER SUCH THAT
!     1 + MACHEP .GT. 1  AND  1 - MACHEP .LT. 1),  AND  H0 IS THE
!     SQUARE-ROOT OF MACHEP.
!
      w(1) = dr7mdc(3)
      w(2) = sqrt(w(1))
!
      w(fx0) = fx
!
!     ***  INCREMENT  I  AND START COMPUTING  G(I)  ***
!
100   i = abs(irc) + 1
      If (i>n) Go To 200
      irc = i
      afx = abs(w(fx0))
      machep = w(1)
      h0 = w(2)
      hmin = hmin0*machep
      w(xisave) = x(i)
      axi = abs(x(i))
      axibar = max(axi,one/d(i))
      gi = g(i)
      agi = abs(gi)
      eta = abs(eta0)
      If (afx>zero) eta = max(eta,agi*axi*machep/afx)
      alphai = alpha(i)
      If (alphai==zero) Go To 160
      If (gi==zero .Or. fx==zero) Go To 170
      afxeta = afx*eta
      aai = abs(alphai)
!
!        *** COMPUTE H = STEWART*S FORWARD-DIFFERENCE STEP SIZE.
!
      If (gi**2<=afxeta*aai) Go To 110
      h = two*sqrt(afxeta/aai)
      h = h*(one-aai*h/(three*aai*h+four*agi))
      Go To 120
!     120     H = TWO*(AFXETA*AGI/(AAI**2))**(ONE/THREE)
110   h = two*(afxeta*agi)**(one/three)*aai**(-two/three)
      h = h*(one-two*agi/(three*aai*h+four*agi))
!
!        ***  ENSURE THAT  H  IS NOT INSIGNIFICANTLY SMALL  ***
!
120   h = max(h,hmin*axibar)
!
!        *** USE FORWARD DIFFERENCE IF BOUND ON TRUNCATION ERROR IS AT
!        *** MOST 10**-3.
!
      If (aai*h<=p002*agi) Go To 150
!
!        *** COMPUTE H = STEWART*S STEP FOR CENTRAL DIFFERENCE.
!
      discon = c2000*afxeta
      h = discon/(agi+sqrt(gi**2+aai*discon))
!
!        ***  ENSURE THAT  H  IS NEITHER TOO SMALL NOR TOO BIG  ***
!
      h = max(h,hmin*axibar)
      If (h>=hmax0*axibar) h = axibar*h0**(two/three)
!
!        ***  COMPUTE CENTRAL DIFFERENCE  ***
!
      irc = -i
      Go To 180
!
130   h = -w(hsave)
      i = abs(irc)
      If (h>zero) Go To 140
      w(fh) = fx
      Go To 180
!
140   g(i) = (w(fh)-fx)/(two*h)
      x(i) = w(xisave)
      Go To 100
!
!     ***  COMPUTE FORWARD DIFFERENCES IN VARIOUS CASES  ***
!
150   If (h>=hmax0*axibar) h = h0*axibar
      If (alphai*gi<zero) h = -h
      Go To 180
160   h = axibar
      Go To 180
170   h = h0*axibar
!
180   x(i) = w(xisave) + h
      w(hsave) = h
      Go To 210
!
!     ***  COMPUTE ACTUAL FORWARD DIFFERENCE  ***
!
190   g(irc) = (fx-w(fx0))/w(hsave)
      x(irc) = w(xisave)
      Go To 100
!
!     ***  RESTORE FX AND INDICATE THAT G HAS BEEN COMPUTED  ***
!
200   fx = w(fx0)
      irc = 0
!
210   Return
!     ***  LAST CARD OF DS7GRD FOLLOWS  ***
    End Subroutine ds7grd
    Subroutine dg7qts(d,dig,dihdi,ka,l,p,step,v,w)
!
!     *** COMPUTE GOLDFELD-QUANDT-TROTTER STEP BY MORE-HEBDEN TECHNIQUE ***
!     ***  (NL2SOL VERSION 2.2), MODIFIED A LA MORE AND SORENSEN  ***
!
!     ***  PARAMETER DECLARATIONS  ***
!
!     DIMENSION DIHDI(P*(P+1)/2), L(P*(P+1)/2), W(4*P+7)
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!     ***  PURPOSE  ***
!
!        GIVEN THE (COMPACTLY STORED) LOWER TRIANGLE OF A SCALED
!     HESSIAN (APPROXIMATION) AND A NONZERO SCALED GRADIENT VECTOR,
!     THIS SUBROUTINE COMPUTES A GOLDFELD-QUANDT-TROTTER STEP OF
!     APPROXIMATE LENGTH V(RADIUS) BY THE MORE-HEBDEN TECHNIQUE.  IN
!     OTHER WORDS, STEP IS COMPUTED TO (APPROXIMATELY) MINIMIZE
!     PSI(STEP) = (G**T)*STEP + 0.5*(STEP**T)*H*STEP  SUCH THAT THE
!     2-NORM OF D*STEP IS AT MOST (APPROXIMATELY) V(RADIUS), WHERE
!     G  IS THE GRADIENT,  H  IS THE HESSIAN, AND  D  IS A DIAGONAL
!     SCALE MATRIX WHOSE DIAGONAL IS STORED IN THE PARAMETER D.
!     (DG7QTS ASSUMES  DIG = D**-1 * G  AND  DIHDI = D**-1 * H * D**-1.)
!
!     ***  PARAMETER DESCRIPTION  ***
!
!     D (IN)  = THE SCALE VECTOR, I.E. THE DIAGONAL OF THE SCALE
!              MATRIX  D  MENTIONED ABOVE UNDER PURPOSE.
!     DIG (IN)  = THE SCALED GRADIENT VECTOR, D**-1 * G.  IF G = 0, THEN
!              STEP = 0  AND  V(STPPAR) = 0  ARE RETURNED.
!     DIHDI (IN)  = LOWER TRIANGLE OF THE SCALED HESSIAN (APPROXIMATION),
!              I.E., D**-1 * H * D**-1, STORED COMPACTLY BY ROWS., I.E.,
!              IN THE ORDER (1,1), (2,1), (2,2), (3,1), (3,2), ETC.
!     KA (I/O) = THE NUMBER OF HEBDEN ITERATIONS (SO FAR) TAKEN TO DETER-
!              MINE STEP.  KA .LT. 0 ON INPUT MEANS THIS IS THE FIRST
!              ATTEMPT TO DETERMINE STEP (FOR THE PRESENT DIG AND DIHDI)
!              -- KA IS INITIALIZED TO 0 IN THIS CASE.  OUTPUT WITH
!              KA = 0  (OR V(STPPAR) = 0)  MEANS  STEP = -(H**-1)*G.
!     L (I/O) = WORKSPACE OF LENGTH P*(P+1)/2 FOR CHOLESKY FACTORS.
!     P (IN)  = NUMBER OF PARAMETERS -- THE HESSIAN IS A  P X P  MATRIX.
!     STEP (I/O) = THE STEP COMPUTED.
!     V (I/O) CONTAINS VARIOUS CONSTANTS AND VARIABLES DESCRIBED BELOW.
!     W (I/O) = WORKSPACE OF LENGTH 4*P + 6.
!
!     ***  ENTRIES IN V  ***
!
!     V(DGNORM) (I/O) = 2-NORM OF (D**-1)*G.
!     V(DSTNRM) (OUTPUT) = 2-NORM OF D*STEP.
!     V(DST0)   (I/O) = 2-NORM OF D*(H**-1)*G (FOR POS. DEF. H ONLY), OR
!             OVERESTIMATE OF SMALLEST EIGENVALUE OF (D**-1)*H*(D**-1).
!     V(EPSLON) (IN)  = MAX. REL. ERROR ALLOWED FOR PSI(STEP).  FOR THE
!             STEP RETURNED, PSI(STEP) WILL EXCEED ITS OPTIMAL VALUE
!             BY LESS THAN -V(EPSLON)*PSI(STEP).  SUGGESTED VALUE = 0.1.
!     V(GTSTEP) (OUT) = INNER PRODUCT BETWEEN G AND STEP.
!     V(NREDUC) (OUT) = PSI(-(H**-1)*G) = PSI(NEWTON STEP)  (FOR POS. DEF.
!             H ONLY -- V(NREDUC) IS SET TO ZERO OTHERWISE).
!     V(PHMNFC) (IN)  = TOL. (TOGETHER WITH V(PHMXFC)) FOR ACCEPTING STEP
!             (MORE*S SIGMA).  THE ERROR V(DSTNRM) - V(RADIUS) MUST LIE
!             BETWEEN V(PHMNFC)*V(RADIUS) AND V(PHMXFC)*V(RADIUS).
!     V(PHMXFC) (IN)  (SEE V(PHMNFC).)
!             SUGGESTED VALUES -- V(PHMNFC) = -0.25, V(PHMXFC) = 0.5.
!     V(PREDUC) (OUT) = PSI(STEP) = PREDICTED OBJ. FUNC. REDUCTION FOR STEP.
!     V(RADIUS) (IN)  = RADIUS OF CURRENT (SCALED) TRUST REGION.
!     V(RAD0)   (I/O) = VALUE OF V(RADIUS) FROM PREVIOUS CALL.
!     V(STPPAR) (I/O) IS NORMALLY THE MARQUARDT PARAMETER, I.E. THE ALPHA
!             DESCRIBED BELOW UNDER ALGORITHM NOTES.  IF H + ALPHA*D**2
!             (SEE ALGORITHM NOTES) IS (NEARLY) SINGULAR, HOWEVER,
!             THEN V(STPPAR) = -ALPHA.
!
!     ***  USAGE NOTES  ***
!
!     IF IT IS DESIRED TO RECOMPUTE STEP USING A DIFFERENT VALUE OF
!     V(RADIUS), THEN THIS ROUTINE MAY BE RESTARTED BY CALLING IT
!     WITH ALL PARAMETERS UNCHANGED EXCEPT V(RADIUS).  (THIS EXPLAINS
!     WHY STEP AND W ARE LISTED AS I/O).  ON AN INITIAL CALL (ONE WITH
!     KA .LT. 0), STEP AND W NEED NOT BE INITIALIZED AND ONLY COMPO-
!     NENTS V(EPSLON), V(STPPAR), V(PHMNFC), V(PHMXFC), V(RADIUS), AND
!     V(RAD0) OF V MUST BE INITIALIZED.
!
!     ***  ALGORITHM NOTES  ***
!
!        THE DESIRED G-Q-T STEP (REF. 2, 3, 4, 6) SATISFIES
!     (H + ALPHA*D**2)*STEP = -G  FOR SOME NONNEGATIVE ALPHA SUCH THAT
!     H + ALPHA*D**2 IS POSITIVE SEMIDEFINITE.  ALPHA AND STEP ARE
!     COMPUTED BY A SCHEME ANALOGOUS TO THE ONE DESCRIBED IN REF. 5.
!     ESTIMATES OF THE SMALLEST AND LARGEST EIGENVALUES OF THE HESSIAN
!     ARE OBTAINED FROM THE GERSCHGORIN CIRCLE THEOREM ENHANCED BY A
!     SIMPLE FORM OF THE SCALING DESCRIBED IN REF. 7.  CASES IN WHICH
!     H + ALPHA*D**2 IS NEARLY (OR EXACTLY) SINGULAR ARE HANDLED BY
!     THE TECHNIQUE DISCUSSED IN REF. 2.  IN THESE CASES, A STEP OF
!     (EXACT) LENGTH V(RADIUS) IS RETURNED FOR WHICH PSI(STEP) EXCEEDS
!     ITS OPTIMAL VALUE BY LESS THAN -V(EPSLON)*PSI(STEP).  THE TEST
!     SUGGESTED IN REF. 6 FOR DETECTING THE SPECIAL CASE IS PERFORMED
!     ONCE TWO MATRIX FACTORIZATIONS HAVE BEEN DONE -- DOING SO SOONER
!     SEEMS TO DEGRADE THE PERFORMANCE OF OPTIMIZATION ROUTINES THAT
!     CALL THIS ROUTINE.
!
!     ***  FUNCTIONS AND SUBROUTINES CALLED  ***
!
!     DD7TPR - RETURNS INNER PRODUCT OF TWO VECTORS.
!     DL7ITV - APPLIES INVERSE-TRANSPOSE OF COMPACT LOWER TRIANG. MATRIX.
!     DL7IVM - APPLIES INVERSE OF COMPACT LOWER TRIANG. MATRIX.
!     DL7SRT  - FINDS CHOLESKY FACTOR (OF COMPACTLY STORED LOWER TRIANG.).
!     DL7SVN - RETURNS APPROX. TO MIN. SING. VALUE OF LOWER TRIANG. MATRIX.
!     DR7MDC - RETURNS MACHINE-DEPENDENT CONSTANTS.
!     DV2NRM - RETURNS 2-NORM OF A VECTOR.
!
!     ***  REFERENCES  ***
!
!     1.  DENNIS, J.E., GAY, D.M., AND WELSCH, R.E. (1981), AN ADAPTIVE
!             NONLINEAR LEAST-SQUARES ALGORITHM, ACM TRANS. MATH.
!             SOFTWARE, VOL. 7, NO. 3.
!     2.  GAY, D.M. (1981), COMPUTING OPTIMAL LOCALLY CONSTRAINED STEPS,
!             SIAM J. SCI. STATIST. COMPUTING, VOL. 2, NO. 2, PP.
!             186-197.
!     3.  GOLDFELD, S.M., QUANDT, R.E., AND TROTTER, H.F. (1966),
!             MAXIMIZATION BY QUADRATIC HILL-CLIMBING, ECONOMETRICA 34,
!             PP. 541-551.
!     4.  HEBDEN, M.D. (1973), AN ALGORITHM FOR MINIMIZATION USING EXACT
!             SECOND DERIVATIVES, REPORT T.P. 515, THEORETICAL PHYSICS
!             DIV., A.E.R.E. HARWELL, OXON., ENGLAND.
!     5.  MORE, J.J. (1978), THE LEVENBERG-MARQUARDT ALGORITHM, IMPLEMEN-
!             TATION AND THEORY, PP.105-116 OF SPRINGER LECTURE NOTES
!             IN MATHEMATICS NO. 630, EDITED BY G.A. WATSON, SPRINGER-
!             VERLAG, BERLIN AND NEW YORK.
!     6.  MORE, J.J., AND SORENSEN, D.C. (1981), COMPUTING A TRUST REGION
!             STEP, TECHNICAL REPORT ANL-81-83, ARGONNE NATIONAL LAB.
!     7.  VARGA, R.S. (1965), MINIMAL GERSCHGORIN SETS, PACIFIC J. MATH. 15,
!             PP. 719-729.
!
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY.
!     THIS SUBROUTINE WAS WRITTEN IN CONNECTION WITH RESEARCH
!     SUPPORTED BY THE NATIONAL SCIENCE FOUNDATION UNDER GRANTS
!     MCS-7600324, DCR75-10143, 76-14311DSS, MCS76-11989, AND
!     MCS-7906671.
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  CONSTANTS  ***
!
!     ***  INTRINSIC FUNCTIONS  ***
!     /+
!/
!     ***  EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!
!     ***  SUBSCRIPTS FOR V  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: epsfac = 50.0E+0_wp
      Real (Kind=wp), Parameter        :: four = 4.0E+0_wp
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: kappa = 2.0E+0_wp
      Real (Kind=wp), Parameter        :: negone = -1.0E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.0E+0_wp
      Real (Kind=wp), Parameter        :: p001 = 1.0E-3_wp
      Real (Kind=wp), Parameter        :: six = 6.0E+0_wp
      Real (Kind=wp), Parameter        :: three = 3.0E+0_wp
      Real (Kind=wp), Parameter        :: two = 2.0E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.0E+0_wp
      Integer, Parameter               :: dgnorm = 1, dst0 = 3, dstnrm = 2,    &
                                          epslon = 19, gtstep = 4, nreduc = 6, &
                                          phmnfc = 20, phmxfc = 21,            &
                                          preduc = 7, rad0 = 9, radius = 8,    &
                                          stppar = 5
!     .. Scalar Arguments ..
      Integer                          :: ka, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(p), dig(p), dihdi(*), l(*),        &
                                          step(p), v(21), w(*)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: aki, akk, alphak, delta, dst, eps,   &
                                          gtsta, lk, oldphi, phi, phimax,      &
                                          phimin, psifac, rad, radsq, root,    &
                                          si, sk, sw, t, t1, t2, twopsi, uk,   &
                                          wi
      Real (Kind=wp), Save             :: big, dgxfac
      Integer                          :: dggdmx, diag, diag0, dstsav, emax,   &
                                          emin, i, im1, inc, irc, j, k, k1,    &
                                          kalim, kamin, lk0, phipin, q, q0,    &
                                          uk0, x
      Logical                          :: restrt
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, dl7svn, dr7mdc, dv2nrm
      External                         :: dl7itv, dl7ivm, dl7srt
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max, min, sqrt
!     .. Data Statements ..
      Data big/0.E+0_wp/, dgxfac/0.E+0_wp/
!     .. Executable Statements ..
!
!     ***  BODY  ***
!
      If (big<=zero) big = dr7mdc(6)
!
!     ***  STORE LARGEST ABS. ENTRY IN (D**-1)*H*(D**-1) AT W(DGGDMX).
      dggdmx = p + 1
!     ***  STORE GERSCHGORIN OVER- AND UNDERESTIMATES OF THE LARGEST
!     ***  AND SMALLEST EIGENVALUES OF (D**-1)*H*(D**-1) AT W(EMAX)
!     ***  AND W(EMIN) RESPECTIVELY.
      emax = dggdmx + 1
      emin = emax + 1
!     ***  FOR USE IN RECOMPUTING STEP, THE FINAL VALUES OF LK, UK, DST,
!     ***  AND THE INVERSE DERIVATIVE OF MORE*S PHI AT 0 (FOR POS. DEF.
!     ***  H) ARE STORED IN W(LK0), W(UK0), W(DSTSAV), AND W(PHIPIN)
!     ***  RESPECTIVELY.
      lk0 = emin + 1
      phipin = lk0 + 1
      uk0 = phipin + 1
      dstsav = uk0 + 1
!     ***  STORE DIAG OF (D**-1)*H*(D**-1) IN W(DIAG),...,W(DIAG0+P).
      diag0 = dstsav
      diag = diag0 + 1
!     ***  STORE -D*STEP IN W(Q),...,W(Q0+P).
      q0 = diag0 + p
      q = q0 + 1
!     ***  ALLOCATE STORAGE FOR SCRATCH VECTOR X  ***
      x = q + p
      rad = v(radius)
      radsq = rad**2
!     ***  PHITOL = MAX. ERROR ALLOWED IN DST = V(DSTNRM) = 2-NORM OF
!     ***  D*STEP.
      phimax = v(phmxfc)*rad
      phimin = v(phmnfc)*rad
      psifac = big
      t1 = two*v(epslon)/(three*(four*(v(phmnfc)+one)*(kappa+                  &
        one)+kappa+two)*rad)
      If (t1<big*min(rad,one)) psifac = t1/rad
!     ***  OLDPHI IS USED TO DETECT LIMITS OF NUMERICAL ACCURACY.  IF
!     ***  WE RECOMPUTE STEP AND IT DOES NOT CHANGE, THEN WE ACCEPT IT.
      oldphi = zero
      eps = v(epslon)
      irc = 0
      restrt = .False.
      kalim = ka + 50
!
!     ***  START OR RESTART, DEPENDING ON KA  ***
!
      If (ka>=0) Go To 280
!
!     ***  FRESH START  ***
!
      k = 0
      uk = negone
      ka = 0
      kalim = 50
      v(dgnorm) = dv2nrm(p,dig)
      v(nreduc) = zero
      v(dst0) = zero
      kamin = 3
      If (v(dgnorm)==zero) kamin = 0
!
!     ***  STORE DIAG(DIHDI) IN W(DIAG0+1),...,W(DIAG0+P)  ***
!
      j = 0
      Do i = 1, p
        j = j + i
        k1 = diag0 + i
        w(k1) = dihdi(j)
      End Do
!
!     ***  DETERMINE W(DGGDMX), THE LARGEST ELEMENT OF DIHDI  ***
!
      t1 = zero
      j = p*(p+1)/2
      Do i = 1, j
        t = abs(dihdi(i))
        If (t1<t) t1 = t
      End Do
      w(dggdmx) = t1
!
!     ***  TRY ALPHA = 0  ***
!
100   Call dl7srt(1,p,l,dihdi,irc)
      If (irc==0) Go To 110
!        ***  INDEF. H -- UNDERESTIMATE SMALLEST EIGENVALUE, USE THIS
!        ***  ESTIMATE TO INITIALIZE LOWER BOUND LK ON ALPHA.
      j = irc*(irc+1)/2
      t = l(j)
      l(j) = one
      Do i = 1, irc
        w(i) = zero
      End Do
      w(irc) = one
      Call dl7itv(irc,w,l,w)
      t1 = dv2nrm(irc,w)
      lk = -t/t1/t1
      v(dst0) = -lk
      If (restrt) Go To 230
      Go To 130
!
!     ***  POSITIVE DEFINITE H -- COMPUTE UNMODIFIED NEWTON STEP.  ***
110   lk = zero
      t = dl7svn(p,l,w(q),w(q))
      If (t>=one) Go To 120
      If (v(dgnorm)>=t*t*big) Go To 130
120   Call dl7ivm(p,w(q),l,dig)
      gtsta = dd7tpr(p,w(q),w(q))
      v(nreduc) = half*gtsta
      Call dl7itv(p,w(q),l,w(q))
      dst = dv2nrm(p,w(q))
      v(dst0) = dst
      phi = dst - rad
      If (phi<=phimax) Go To 260
      If (restrt) Go To 230
!
!     ***  PREPARE TO COMPUTE GERSCHGORIN ESTIMATES OF LARGEST (AND
!     ***  SMALLEST) EIGENVALUES.  ***
!
130   k = 0
      Do i = 1, p
        wi = zero
        If (i==1) Go To 140
        im1 = i - 1
        Do j = 1, im1
          k = k + 1
          t = abs(dihdi(k))
          wi = wi + t
          w(j) = w(j) + t
        End Do
140     w(i) = wi
        k = k + 1
      End Do
!
!     ***  (UNDER-)ESTIMATE SMALLEST EIGENVALUE OF (D**-1)*H*(D**-1)  ***
!
      k = 1
      t1 = w(diag) - w(1)
      If (p<=1) Go To 160
      Do i = 2, p
        j = diag0 + i
        t = w(j) - w(i)
        If (t>=t1) Go To 150
        t1 = t
        k = i
150   End Do
!
160   sk = w(k)
      j = diag0 + k
      akk = w(j)
      k1 = k*(k-1)/2 + 1
      inc = 1
      t = zero
      Do i = 1, p
        If (i==k) Go To 170
        aki = abs(dihdi(k1))
        si = w(i)
        j = diag0 + i
        t1 = half*(akk-w(j)+si-aki)
        t1 = t1 + sqrt(t1*t1+sk*aki)
        If (t<t1) t = t1
        If (i<k) Go To 180
170     inc = i
180     k1 = k1 + inc
      End Do
!
      w(emin) = akk - t
      uk = v(dgnorm)/rad - w(emin)
      If (v(dgnorm)==zero) uk = uk + p001 + p001*uk
      If (uk<=zero) uk = p001
!
!     ***  COMPUTE GERSCHGORIN (OVER-)ESTIMATE OF LARGEST EIGENVALUE  ***
!
      k = 1
      t1 = w(diag) + w(1)
      If (p<=1) Go To 200
      Do i = 2, p
        j = diag0 + i
        t = w(j) + w(i)
        If (t<=t1) Go To 190
        t1 = t
        k = i
190   End Do
!
200   sk = w(k)
      j = diag0 + k
      akk = w(j)
      k1 = k*(k-1)/2 + 1
      inc = 1
      t = zero
      Do i = 1, p
        If (i==k) Go To 210
        aki = abs(dihdi(k1))
        si = w(i)
        j = diag0 + i
        t1 = half*(w(j)+si-aki-akk)
        t1 = t1 + sqrt(t1*t1+sk*aki)
        If (t<t1) t = t1
        If (i<k) Go To 220
210     inc = i
220     k1 = k1 + inc
      End Do
!
      w(emax) = akk + t
      lk = max(lk,v(dgnorm)/rad-w(emax))
!
!     ***  ALPHAK = CURRENT VALUE OF ALPHA (SEE ALG. NOTES ABOVE).  WE
!     ***  USE MORE*S SCHEME FOR INITIALIZING IT.
      alphak = abs(v(stppar))*v(rad0)/rad
      alphak = min(uk,max(alphak,lk))
!
      If (irc/=0) Go To 230
!
!     ***  COMPUTE L0 FOR POSITIVE DEFINITE H  ***
!
      Call dl7ivm(p,w,l,w(q))
      t = dv2nrm(p,w)
      w(phipin) = rad/t/t
      lk = max(lk,phi*w(phipin))
!
!     ***  SAFEGUARD ALPHAK AND ADD ALPHAK*I TO (D**-1)*H*(D**-1)  ***
!
230   ka = ka + 1
      If (-v(dst0)>=alphak .Or. alphak<lk .Or. alphak>=uk) alphak = uk*        &
        max(p001,sqrt(lk/uk))
      If (alphak<=zero) alphak = half*uk
      If (alphak<=zero) alphak = uk
      k = 0
      Do i = 1, p
        k = k + i
        j = diag0 + i
        dihdi(k) = w(j) + alphak
      End Do
!
!     ***  TRY COMPUTING CHOLESKY DECOMPOSITION  ***
!
      Call dl7srt(1,p,l,dihdi,irc)
      If (irc==0) Go To 240
!
!     ***  (D**-1)*H*(D**-1) + ALPHAK*I  IS INDEFINITE -- OVERESTIMATE
!     ***  SMALLEST EIGENVALUE FOR USE IN UPDATING LK  ***
!
      j = (irc*(irc+1))/2
      t = l(j)
      l(j) = one
      Do i = 1, irc
        w(i) = zero
      End Do
      w(irc) = one
      Call dl7itv(irc,w,l,w)
      t1 = dv2nrm(irc,w)
      lk = alphak - t/t1/t1
      v(dst0) = -lk
      If (uk<lk) uk = lk
      If (alphak<lk) Go To 230
!
!     ***  NASTY CASE -- EXACT GERSCHGORIN BOUNDS.  FUDGE LK, UK...
!
      t = p001*alphak
      If (t<=zero) t = p001
      lk = alphak + t
      If (uk<=lk) uk = lk + t
      Go To 230
!
!     ***  ALPHAK MAKES (D**-1)*H*(D**-1) POSITIVE DEFINITE.
!     ***  COMPUTE Q = -D*STEP, CHECK FOR CONVERGENCE.  ***
!
240   Call dl7ivm(p,w(q),l,dig)
      gtsta = dd7tpr(p,w(q),w(q))
      Call dl7itv(p,w(q),l,w(q))
      dst = dv2nrm(p,w(q))
      phi = dst - rad
      If (phi<=phimax .And. phi>=phimin) Go To 270
      If (phi==oldphi) Go To 270
      oldphi = phi
      If (phi<zero) Go To 310
!
!     ***  UNACCEPTABLE ALPHAK -- UPDATE LK, UK, ALPHAK  ***
!
250   If (ka>=kalim) Go To 270
!     ***  THE FOLLOWING DMIN1 IS NECESSARY BECAUSE OF RESTARTS  ***
      If (phi<zero) uk = min(uk,alphak)
!     *** KAMIN = 0 ONLY IFF THE GRADIENT VANISHES  ***
      If (kamin==0) Go To 230
      Call dl7ivm(p,w,l,w(q))
!     *** THE FOLLOWING, COMMENTED CALCULATION OF ALPHAK IS SOMETIMES
!     *** SAFER BUT WORSE IN PERFORMANCE...
!     T1 = DST / DV2NRM(P, W)
!     ALPHAK = ALPHAK  +  T1 * (PHI/RAD) * T1
      t1 = dv2nrm(p,w)
      alphak = alphak + (phi/t1)*(dst/t1)*(dst/rad)
      lk = max(lk,alphak)
      alphak = lk
      Go To 230
!
!     ***  ACCEPTABLE STEP ON FIRST TRY  ***
!
260   alphak = zero
!
!     ***  SUCCESSFUL STEP IN GENERAL.  COMPUTE STEP = -(D**-1)*Q  ***
!
270   Do i = 1, p
        j = q0 + i
        step(i) = -w(j)/d(i)
      End Do
      v(gtstep) = -gtsta
      v(preduc) = half*(abs(alphak)*dst*dst+gtsta)
      Go To 360
!
!
!     ***  RESTART WITH NEW RADIUS  ***
!
280   If (v(dst0)<=zero .Or. v(dst0)-rad>phimax) Go To 290
!
!     ***  PREPARE TO RETURN NEWTON STEP  ***
!
      restrt = .True.
      ka = ka + 1
      k = 0
      Do i = 1, p
        k = k + i
        j = diag0 + i
        dihdi(k) = w(j)
      End Do
      uk = negone
      Go To 100
!
290   kamin = ka + 3
      If (v(dgnorm)==zero) kamin = 0
      If (ka==0) Go To 110
!
      dst = w(dstsav)
      alphak = abs(v(stppar))
      phi = dst - rad
      t = v(dgnorm)/rad
      uk = t - w(emin)
      If (v(dgnorm)==zero) uk = uk + p001 + p001*uk
      If (uk<=zero) uk = p001
      If (rad>v(rad0)) Go To 300
!
!        ***  SMALLER RADIUS  ***
      lk = zero
      If (alphak>zero) lk = w(lk0)
      lk = max(lk,t-w(emax))
      If (v(dst0)>zero) lk = max(lk,(v(dst0)-rad)*w(phipin))
      Go To 250
!
!     ***  BIGGER RADIUS  ***
300   If (alphak>zero) uk = min(uk,w(uk0))
      lk = max(zero,-v(dst0),t-w(emax))
      If (v(dst0)>zero) lk = max(lk,(v(dst0)-rad)*w(phipin))
      Go To 250
!
!     ***  DECIDE WHETHER TO CHECK FOR SPECIAL CASE... IN PRACTICE (FROM
!     ***  THE STANDPOINT OF THE CALLING OPTIMIZATION CODE) IT SEEMS BEST
!     ***  NOT TO CHECK UNTIL A FEW ITERATIONS HAVE FAILED -- HENCE THE
!     ***  TEST ON KAMIN BELOW.
!
310   delta = alphak + min(zero,v(dst0))
      twopsi = alphak*dst*dst + gtsta
      If (ka>=kamin) Go To 320
!     *** IF THE TEST IN REF. 2 IS SATISFIED, FALL THROUGH TO HANDLE
!     *** THE SPECIAL CASE (AS SOON AS THE MORE-SORENSEN TEST DETECTS
!     *** IT).
      If (psifac>=big) Go To 320
      If (delta>=psifac*twopsi) Go To 330
!
!     ***  CHECK FOR THE SPECIAL CASE OF  H + ALPHA*D**2  (NEARLY)
!     ***  SINGULAR.  USE ONE STEP OF INVERSE POWER METHOD WITH START
!     ***  FROM DL7SVN TO OBTAIN APPROXIMATE EIGENVECTOR CORRESPONDING
!     ***  TO SMALLEST EIGENVALUE OF (D**-1)*H*(D**-1).  DL7SVN RETURNS
!     ***  X AND W WITH  L*W = X.
!
320   t = dl7svn(p,l,w(x),w)
!
!     ***  NORMALIZE W  ***
      Do i = 1, p
        w(i) = t*w(i)
      End Do
!     ***  COMPLETE CURRENT INV. POWER ITER. -- REPLACE W BY (L**-T)*W.
      Call dl7itv(p,w,l,w)
      t2 = one/dv2nrm(p,w)
      Do i = 1, p
        w(i) = t2*w(i)
      End Do
      t = t2*t
!
!     ***  NOW W IS THE DESIRED APPROXIMATE (UNIT) EIGENVECTOR AND
!     ***  T*X = ((D**-1)*H*(D**-1) + ALPHAK*I)*W.
!
      sw = dd7tpr(p,w(q),w)
      t1 = (rad+dst)*(rad-dst)
      root = sqrt(sw*sw+t1)
      If (sw<zero) root = -root
      si = t1/(sw+root)
!
!     ***  THE ACTUAL TEST FOR THE SPECIAL CASE...
!
      If ((t2*si)**2<=eps*(dst**2+alphak*radsq)) Go To 340
!
!     ***  UPDATE UPPER BOUND ON SMALLEST EIGENVALUE (WHEN NOT POSITIVE)
!     ***  (AS RECOMMENDED BY MORE AND SORENSEN) AND CONTINUE...
!
      If (v(dst0)<=zero) v(dst0) = min(v(dst0),t2**2-alphak)
      lk = max(lk,-v(dst0))
!
!     ***  CHECK WHETHER WE CAN HOPE TO DETECT THE SPECIAL CASE IN
!     ***  THE AVAILABLE ARITHMETIC.  ACCEPT STEP AS IT IS IF NOT.
!
!     ***  IF NOT YET AVAILABLE, OBTAIN MACHINE DEPENDENT VALUE DGXFAC.
330   If (dgxfac==zero) dgxfac = epsfac*dr7mdc(3)
!
      If (delta>dgxfac*w(dggdmx)) Go To 250
      Go To 270
!
!     ***  SPECIAL CASE DETECTED... NEGATE ALPHAK TO INDICATE SPECIAL CASE
!
340   alphak = -alphak
      v(preduc) = half*twopsi
!
!     ***  ACCEPT CURRENT STEP IF ADDING SI*W WOULD LEAD TO A
!     ***  FURTHER RELATIVE REDUCTION IN PSI OF LESS THAN V(EPSLON)/3.
!
      t1 = zero
      t = si*(alphak*sw-half*si*(alphak+t*dd7tpr(p,w(x),w)))
      If (t<eps*twopsi/six) Go To 350
      v(preduc) = v(preduc) + t
      dst = rad
      t1 = -si
350   Do i = 1, p
        j = q0 + i
        w(j) = t1*w(i) - w(j)
        step(i) = w(j)/d(i)
      End Do
      v(gtstep) = dd7tpr(p,dig,w(q))
!
!     ***  SAVE VALUES FOR USE IN A POSSIBLE RESTART  ***
!
360   v(dstnrm) = dst
      v(stppar) = alphak
      w(lk0) = lk
      w(uk0) = uk
      v(rad0) = rad
      w(dstsav) = dst
!
!     ***  RESTORE DIAGONAL OF DIHDI  ***
!
      j = 0
      Do i = 1, p
        j = j + i
        k = diag0 + i
        dihdi(j) = w(k)
      End Do
!
      Return
!
!     ***  LAST CARD OF DG7QTS FOLLOWS  ***
    End Subroutine dg7qts
    Subroutine dw7zbf(l,n,s,w,y,z)
!
!     ***  COMPUTE  Y  AND  Z  FOR  DL7UPD  CORRESPONDING TO BFGS UPDATE.
!
!     DIMENSION L(N*(N+1)/2)
!
!     --------------------------  PARAMETER USAGE  --------------------------
!
!     L (I/O) CHOLESKY FACTOR OF HESSIAN, A LOWER TRIANG. MATRIX STORED
!             COMPACTLY BY ROWS.
!     N (INPUT) ORDER OF  L  AND LENGTH OF  S,  W,  Y,  Z.
!     S (INPUT) THE STEP JUST TAKEN.
!     W (OUTPUT) RIGHT SINGULAR VECTOR OF RANK 1 CORRECTION TO L.
!     Y (INPUT) CHANGE IN GRADIENTS CORRESPONDING TO S.
!     Z (OUTPUT) LEFT SINGULAR VECTOR OF RANK 1 CORRECTION TO L.
!
!     -------------------------------  NOTES  -------------------------------
!
!     ***  ALGORITHM NOTES  ***
!
!        WHEN  S  IS COMPUTED IN CERTAIN WAYS, E.G. BY  GQTSTP  OR
!     DBLDOG,  IT IS POSSIBLE TO SAVE N**2/2 OPERATIONS SINCE  (L**T)*S
!     OR  L*(L**T)*S IS THEN KNOWN.
!        IF THE BFGS UPDATE TO L*(L**T) WOULD REDUCE ITS DETERMINANT TO
!     LESS THAN EPS TIMES ITS OLD VALUE, THEN THIS ROUTINE IN EFFECT
!     REPLACES  Y  BY  THETA*Y + (1 - THETA)*L*(L**T)*S,  WHERE  THETA
!     (BETWEEN 0 AND 1) IS CHOSEN TO MAKE THE REDUCTION FACTOR = EPS.
!
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY (FALL 1979).
!     THIS SUBROUTINE WAS WRITTEN IN CONNECTION WITH RESEARCH SUPPORTED
!     BY THE NATIONAL SCIENCE FOUNDATION UNDER GRANTS MCS-7600324 AND
!     MCS-7906671.
!
!     ------------------------  EXTERNAL QUANTITIES  ------------------------
!
!     ***  FUNCTIONS AND SUBROUTINES CALLED  ***
!
!     DD7TPR RETURNS INNER PRODUCT OF TWO VECTORS.
!     DL7IVM MULTIPLIES L**-1 TIMES A VECTOR.
!     DL7TVM MULTIPLIES L**T TIMES A VECTOR.
!
!     ***  INTRINSIC FUNCTIONS  ***
!     /+
!/
!     --------------------------  LOCAL VARIABLES  --------------------------
!
!
!     ***  DATA INITIALIZATIONS  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: eps = 0.1E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
!     .. Scalar Arguments ..
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: l(*), s(n), w(n), y(n), z(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: cs, cy, epsrt, shs, theta, ys
      Integer                          :: i
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr
      External                         :: dl7ivm, dl7tvm
!     .. Intrinsic Procedures ..
      Intrinsic                        :: sqrt
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      Call dl7tvm(n,w,l,s)
      shs = dd7tpr(n,w,w)
      ys = dd7tpr(n,y,s)
      If (ys>=eps*shs) Go To 100
      theta = (one-eps)*shs/(shs-ys)
      epsrt = sqrt(eps)
      cy = theta/(shs*epsrt)
      cs = (one+(theta-one)/epsrt)/shs
      Go To 110
100   cy = one/(sqrt(ys)*sqrt(shs))
      cs = one/shs
110   Call dl7ivm(n,z,l,y)
      Do i = 1, n
        z(i) = cy*z(i) - cs*w(i)
      End Do
!
      Return
!     ***  LAST CARD OF DW7ZBF FOLLOWS  ***
    End Subroutine dw7zbf
    Subroutine dc7vfn(iv,l,lh,liv,lv,n,p,v)
!
!     ***  FINISH COVARIANCE COMPUTATION FOR  DRN2G,  DRNSG  ***
!
!
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Integer, Parameter               :: cnvcod = 55, covmat = 26, f = 10,    &
                                          fdh = 74, h = 56, mode = 35,         &
                                          rdreq = 57, regd = 67
!     .. Scalar Arguments ..
      Integer                          :: lh, liv, lv, n, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: l(lh), v(lv)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp), Save             :: half
      Integer                          :: cov, i
!     .. External Procedures ..
      External                         :: dl7nvr, dl7tsq, dv7scl
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max, mod, real
!     .. Data Statements ..
      Data half/0.5E+0_wp/
!     .. Executable Statements ..
!
!     ***  BODY  ***
!
      iv(1) = iv(cnvcod)
      i = iv(mode) - p
      iv(mode) = 0
      iv(cnvcod) = 0
      If (iv(fdh)<=0) Go To 110
      If ((i-2)**2==1) iv(regd) = 1
      If (mod(iv(rdreq),2)/=1) Go To 110
!
!     ***  FINISH COMPUTING COVARIANCE MATRIX = INVERSE OF F.D. HESSIAN.
!
      cov = abs(iv(h))
      iv(fdh) = 0
!
      If (iv(covmat)/=0) Go To 110
      If (i>=2) Go To 100
      Call dl7nvr(p,v(cov),l)
      Call dl7tsq(p,v(cov),v(cov))
!
100   Call dv7scl(lh,v(cov),v(f)/(half*real(max(1,n-p),kind=wp)),v(cov))
      iv(covmat) = cov
!
110   Return
!     ***  LAST LINE OF DC7VFN FOLLOWS  ***
    End Subroutine dc7vfn
    Subroutine dd7mlp(n,x,y,z,k)
!
!     ***  SET X = DIAG(Y)**K * Z
!     ***  FOR X, Z = LOWER TRIANG. MATRICES STORED COMPACTLY BY ROW
!     ***  K = 1 OR -1.
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: k, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: x(*), y(n), z(*)
!     .. Local Scalars ..
      Real (Kind=wp), Save             :: one
      Real (Kind=wp)                   :: t
      Integer                          :: i, j, l
!     .. Data Statements ..
      Data one/1.E+0_wp/
!     .. Executable Statements ..
!
      l = 1
      If (k>=0) Go To 100
      Do i = 1, n
        t = one/y(i)
        Do j = 1, i
          x(l) = t*z(l)
          l = l + 1
        End Do
      End Do
      Go To 110
!
100   Do i = 1, n
        t = y(i)
        Do j = 1, i
          x(l) = t*z(l)
          l = l + 1
        End Do
      End Do
110   Return
!     ***  LAST CARD OF DD7MLP FOLLOWS  ***
    End Subroutine dd7mlp
    Subroutine dl7ivm(n,x,l,y)
!
!     ***  SOLVE  L*X = Y, WHERE  L  IS AN  N X N  LOWER TRIANGULAR
!     ***  MATRIX STORED COMPACTLY BY ROWS.  X AND Y MAY OCCUPY THE SAME
!     ***  STORAGE.  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
!     .. Scalar Arguments ..
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: l(*), x(n), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t
      Integer                          :: i, j, k
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr
!     .. Executable Statements ..
!
      Do k = 1, n
        If (y(k)/=zero) Go To 100
        x(k) = zero
      End Do
      Go To 110
100   j = k*(k+1)/2
      x(k) = y(k)/l(j)
      If (k>=n) Go To 110
      k = k + 1
      Do i = k, n
        t = dd7tpr(i-1,l(j+1),x)
        j = j + i
        x(i) = (y(i)-t)/l(j)
      End Do
110   Return
!     ***  LAST CARD OF DL7IVM FOLLOWS  ***
    End Subroutine dl7ivm
    Subroutine dd7upd(d,dr,iv,liv,lv,n,nd,nn,n2,p,v)
!
!     ***  UPDATE SCALE VECTOR D FOR NL2IT  ***
!
!     ***  PARAMETER DECLARATIONS  ***
!
!     DIMENSION V(*)
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  CONSTANTS  ***
!
!
!     ***  INTRINSIC FUNCTIONS  ***
!     /+
!/
!     ***  EXTERNAL SUBROUTINE  ***
!
!
!     DV7SCP... SETS ALL COMPONENTS OF A VECTOR TO A SCALAR.
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: dfac = 41, dtype = 16, jcn = 66,     &
                                          jtol = 59, niter = 31, s = 62
!     .. Scalar Arguments ..
      Integer                          :: liv, lv, n, n2, nd, nn, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(p), dr(nd,p), v(lv)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t, vdfac
      Integer                          :: d0, i, jcn0, jcn1, jcni, jtol0,      &
                                          jtoli, k, sii
!     .. External Procedures ..
      External                         :: dv7scp
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max, sqrt
!     .. Executable Statements ..
!
!     -------------------------------  BODY  --------------------------------
!
      If (iv(dtype)/=1 .And. iv(niter)>0) Go To 110
      jcn1 = iv(jcn)
      jcn0 = abs(jcn1) - 1
      If (jcn1<0) Go To 100
      iv(jcn) = -jcn1
      Call dv7scp(p,v(jcn1),zero)
100   Do i = 1, p
        jcni = jcn0 + i
        t = v(jcni)
        Do k = 1, nn
          t = max(t,abs(dr(k,i)))
        End Do
        v(jcni) = t
      End Do
      If (n2<n) Go To 110
      vdfac = v(dfac)
      jtol0 = iv(jtol) - 1
      d0 = jtol0 + p
      sii = iv(s) - 1
      Do i = 1, p
        sii = sii + i
        jcni = jcn0 + i
        t = v(jcni)
        If (v(sii)>zero) t = max(sqrt(v(sii)),t)
        jtoli = jtol0 + i
        d0 = d0 + 1
        If (t<v(jtoli)) t = max(v(d0),v(jtoli))
        d(i) = max(vdfac*d(i),t)
      End Do
!
110   Return
!     ***  LAST CARD OF DD7UPD FOLLOWS  ***
    End Subroutine dd7upd
    Subroutine dv7shf(n,k,x)
!
!     ***  SHIFT X(K),...,X(N) LEFT CIRCULARLY ONE POSITION  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: k, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: x(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t
      Integer                          :: i, nm1
!     .. Executable Statements ..
!
      If (k>=n) Go To 100
      nm1 = n - 1
      t = x(k)
      Do i = k, nm1
        x(i) = x(i+1)
      End Do
      x(n) = t
100   Return
    End Subroutine dv7shf
    Subroutine ds3grd(alpha,b,d,eta0,fx,g,irc,p,w,x)
!
!     ***  COMPUTE FINITE DIFFERENCE GRADIENT BY STWEART*S SCHEME  ***
!
!     ***  PARAMETERS  ***
!
!
!.......................................................................
!
!     ***  PURPOSE  ***
!
!        THIS SUBROUTINE USES AN EMBELLISHED FORM OF THE FINITE-DIFFER-
!     ENCE SCHEME PROPOSED BY STEWART (REF. 1) TO APPROXIMATE THE
!     GRADIENT OF THE FUNCTION F(X), WHOSE VALUES ARE SUPPLIED BY
!     REVERSE COMMUNICATION.
!
!     ***  PARAMETER DESCRIPTION  ***
!
!     ALPHA IN  (APPROXIMATE) DIAGONAL ELEMENTS OF THE HESSIAN OF F(X).
!      B IN  ARRAY OF SIMPLE LOWER AND UPPER BOUNDS ON X.  X MUST
!             SATISFY B(1,I) .LE. X(I) .LE. B(2,I), I = 1(1)P.
!             FOR ALL I WITH B(1,I) .GE. B(2,I), DS3GRD SIMPLY
!             SETS G(I) TO 0.
!      D IN  SCALE VECTOR SUCH THAT D(I)*X(I), I = 1,...,P, ARE IN
!             COMPARABLE UNITS.
!     ETA0 IN  ESTIMATED BOUND ON RELATIVE ERROR IN THE FUNCTION VALUE...
!             (TRUE VALUE) = (COMPUTED VALUE)*(1+E),   WHERE
!             ABS(E) .LE. ETA0.
!     FX I/O ON INPUT,  FX  MUST BE THE COMPUTED VALUE OF F(X).  ON
!             OUTPUT WITH IRC = 0, FX HAS BEEN RESTORED TO ITS ORIGINAL
!             VALUE, THE ONE IT HAD WHEN DS3GRD WAS LAST CALLED WITH
!             IRC = 0.
!      G I/O ON INPUT WITH IRC = 0, G SHOULD CONTAIN AN APPROXIMATION
!             TO THE GRADIENT OF F NEAR X, E.G., THE GRADIENT AT THE
!             PREVIOUS ITERATE.  WHEN DS3GRD RETURNS WITH IRC = 0, G IS
!             THE DESIRED FINITE-DIFFERENCE APPROXIMATION TO THE
!             GRADIENT AT X.
!     IRC I/O INPUT/RETURN CODE... BEFORE THE VERY FIRST CALL ON DS3GRD,
!             THE CALLER MUST SET IRC TO 0.  WHENEVER DS3GRD RETURNS A
!             NONZERO VALUE (OF AT MOST P) FOR IRC, IT HAS PERTURBED
!             SOME COMPONENT OF X... THE CALLER SHOULD EVALUATE F(X)
!             AND CALL DS3GRD AGAIN WITH FX = F(X).  IF B PREVENTS
!             ESTIMATING G(I) I.E., IF THERE IS AN I WITH
!             B(1,I) .LT. B(2,I) BUT WITH B(1,I) SO CLOSE TO B(2,I)
!             THAT THE FINITE-DIFFERENCING STEPS CANNOT BE CHOSEN,
!             THEN DS3GRD RETURNS WITH IRC .GT. P.
!      P IN  THE NUMBER OF VARIABLES (COMPONENTS OF X) ON WHICH F
!             DEPENDS.
!      X I/O ON INPUT WITH IRC = 0, X IS THE POINT AT WHICH THE
!             GRADIENT OF F IS DESIRED.  ON OUTPUT WITH IRC NONZERO, X
!             IS THE POINT AT WHICH F SHOULD BE EVALUATED.  ON OUTPUT
!             WITH IRC = 0, X HAS BEEN RESTORED TO ITS ORIGINAL VALUE
!             (THE ONE IT HAD WHEN DS3GRD WAS LAST CALLED WITH IRC = 0)
!             AND G CONTAINS THE DESIRED GRADIENT APPROXIMATION.
!      W I/O WORK VECTOR OF LENGTH 6 IN WHICH DS3GRD SAVES CERTAIN
!             QUANTITIES WHILE THE CALLER IS EVALUATING F(X) AT A
!             PERTURBED X.
!
!     ***  APPLICATION AND USAGE RESTRICTIONS  ***
!
!        THIS ROUTINE IS INTENDED FOR USE WITH QUASI-NEWTON ROUTINES
!     FOR UNCONSTRAINED MINIMIZATION (IN WHICH CASE  ALPHA  COMES FROM
!     THE DIAGONAL OF THE QUASI-NEWTON HESSIAN APPROXIMATION).
!
!     ***  ALGORITHM NOTES  ***
!
!        THIS CODE DEPARTS FROM THE SCHEME PROPOSED BY STEWART (REF. 1)
!     IN ITS GUARDING AGAINST OVERLY LARGE OR SMALL STEP SIZES AND ITS
!     HANDLING OF SPECIAL CASES (SUCH AS ZERO COMPONENTS OF ALPHA OR G).
!
!     ***  REFERENCES  ***
!
!     1. STEWART, G.W. (1967), A MODIFICATION OF DAVIDON*S MINIMIZATION
!        METHOD TO ACCEPT DIFFERENCE APPROXIMATIONS OF DERIVATIVES,
!        J. ASSOC. COMPUT. MACH. 14, PP. 72-83.
!
!     ***  HISTORY  ***
!
!     DESIGNED AND CODED BY DAVID M. GAY (SUMMER 1977/SUMMER 1980).
!
!     ***  GENERAL  ***
!
!        THIS ROUTINE WAS PREPARED IN CONNECTION WITH WORK SUPPORTED BY
!     THE NATIONAL SCIENCE FOUNDATION UNDER GRANTS MCS76-00324 AND
!     MCS-7906671.
!
!.......................................................................
!
!     *****  EXTERNAL FUNCTION  *****
!
!     DR7MDC... RETURNS MACHINE-DEPENDENT CONSTANTS.
!
!     ***** INTRINSIC FUNCTIONS *****
!     /+
!/
!     ***** LOCAL VARIABLES *****
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: c2000 = 2.0E+3_wp
      Real (Kind=wp), Parameter        :: four = 4.0E+0_wp
      Real (Kind=wp), Parameter        :: hmax0 = 0.02E+0_wp
      Real (Kind=wp), Parameter        :: hmin0 = 5.0E+1_wp
      Real (Kind=wp), Parameter        :: one = 1.0E+0_wp
      Real (Kind=wp), Parameter        :: p002 = 0.002E+0_wp
      Real (Kind=wp), Parameter        :: three = 3.0E+0_wp
      Real (Kind=wp), Parameter        :: two = 2.0E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.0E+0_wp
      Integer, Parameter               :: fh = 3, fx0 = 4, hsave = 5,          &
                                          xisave = 6
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: eta0, fx
      Integer                          :: irc, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: alpha(p), b(2,p), d(p), g(p), w(6),  &
                                          x(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: aai, afx, afxeta, agi, alphai, axi,  &
                                          axibar, discon, eta, gi, h, h0,      &
                                          hmin, machep, xi, xih
      Integer                          :: i
      Logical                          :: hit
!     .. External Procedures ..
      Real (Kind=wp), External         :: dr7mdc
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max, sqrt
!     .. Executable Statements ..
!
!     ---------------------------------  BODY  ------------------------------
!
      If (irc<0) Go To 160
      If (irc>0) Go To 290
!
!     ***  FRESH START -- GET MACHINE-DEPENDENT CONSTANTS  ***
!
!     STORE MACHEP IN W(1) AND H0 IN W(2), WHERE MACHEP IS THE UNIT
!     ROUNDOFF (THE SMALLEST POSITIVE NUMBER SUCH THAT
!     1 + MACHEP .GT. 1  AND  1 - MACHEP .LT. 1),  AND  H0 IS THE
!     SQUARE-ROOT OF MACHEP.
!
      w(1) = dr7mdc(3)
      w(2) = sqrt(w(1))
!
      w(fx0) = fx
!
!     ***  INCREMENT  I  AND START COMPUTING  G(I)  ***
!
100   i = abs(irc) + 1
      If (i>p) Go To 300
      irc = i
      If (b(1,i)<b(2,i)) Go To 110
      g(i) = zero
      Go To 100
110   afx = abs(w(fx0))
      machep = w(1)
      h0 = w(2)
      hmin = hmin0*machep
      xi = x(i)
      w(xisave) = xi
      axi = abs(xi)
      axibar = max(axi,one/d(i))
      gi = g(i)
      agi = abs(gi)
      eta = abs(eta0)
      If (afx>zero) eta = max(eta,agi*axi*machep/afx)
      alphai = alpha(i)
      If (alphai==zero) Go To 210
      If (gi==zero .Or. fx==zero) Go To 220
      afxeta = afx*eta
      aai = abs(alphai)
!
!        *** COMPUTE H = STEWART*S FORWARD-DIFFERENCE STEP SIZE.
!
      If (gi**2<=afxeta*aai) Go To 120
      h = two*sqrt(afxeta/aai)
      h = h*(one-aai*h/(three*aai*h+four*agi))
      Go To 130
!     40      H = TWO*(AFXETA*AGI/(AAI**2))**(ONE/THREE)
120   h = two*(afxeta*agi)**(one/three)*aai**(-two/three)
      h = h*(one-two*agi/(three*aai*h+four*agi))
!
!        ***  ENSURE THAT  H  IS NOT INSIGNIFICANTLY SMALL  ***
!
130   h = max(h,hmin*axibar)
!
!        *** USE FORWARD DIFFERENCE IF BOUND ON TRUNCATION ERROR IS AT
!        *** MOST 10**-3.
!
      If (aai*h<=p002*agi) Go To 200
!
!        *** COMPUTE H = STEWART*S STEP FOR CENTRAL DIFFERENCE.
!
      discon = c2000*afxeta
      h = discon/(agi+sqrt(gi**2+aai*discon))
!
!        ***  ENSURE THAT  H  IS NEITHER TOO SMALL NOR TOO BIG  ***
!
      h = max(h,hmin*axibar)
      If (h>=hmax0*axibar) h = axibar*h0**(two/three)
!
!        ***  COMPUTE CENTRAL DIFFERENCE  ***
!
      xih = xi + h
      If (xi-h<b(1,i)) Go To 140
      irc = -i
      If (xih<=b(2,i)) Go To 280
      h = -h
      xih = xi + h
      If (xi+two*h<b(1,i)) Go To 270
      Go To 150
140   If (xi+two*h>b(2,i)) Go To 270
!        *** MUST DO OFF-SIDE CENTRAL DIFFERENCE ***
150   irc = -(i+p)
      Go To 280
!
160   i = -irc
      If (i<=p) Go To 180
      i = i - p
      If (i>p) Go To 170
      w(fh) = fx
      h = two*w(hsave)
      xih = w(xisave) + h
      irc = irc - p
      Go To 280
!
!     *** FINISH OFF-SIDE CENTRAL DIFFERENCE ***
!
170   i = i - p
      g(i) = (four*w(fh)-fx-three*w(fx0))/w(hsave)
      irc = i
      x(i) = w(xisave)
      Go To 100
!
180   h = -w(hsave)
      If (h>zero) Go To 190
      w(fh) = fx
      xih = w(xisave) + h
      Go To 280
!
190   g(i) = (w(fh)-fx)/(two*h)
      x(i) = w(xisave)
      Go To 100
!
!     ***  COMPUTE FORWARD DIFFERENCES IN VARIOUS CASES  ***
!
200   If (h>=hmax0*axibar) h = h0*axibar
      If (alphai*gi<zero) h = -h
      Go To 230
210   h = axibar
      Go To 230
220   h = h0*axibar
!
230   hit = .False.
240   xih = xi + h
      If (h>zero) Go To 250
      If (xih>=b(1,i)) Go To 280
      Go To 260
250   If (xih<=b(2,i)) Go To 280
260   If (hit) Go To 270
      hit = .True.
      h = -h
      Go To 240
!
!        *** ERROR RETURN...
270   irc = i + p
      Go To 310
!
!        *** RETURN FOR NEW FUNCTION VALUE...
280   x(i) = xih
      w(hsave) = h
      Go To 320
!
!     ***  COMPUTE ACTUAL FORWARD DIFFERENCE  ***
!
290   g(irc) = (fx-w(fx0))/w(hsave)
      x(irc) = w(xisave)
      Go To 100
!
!     ***  RESTORE FX AND INDICATE THAT G HAS BEEN COMPUTED  ***
!
300   irc = 0
310   fx = w(fx0)
!
320   Return
!     ***  LAST LINE OF DS3GRD FOLLOWS  ***
    End Subroutine ds3grd
    Subroutine dl7upd(beta,gamma,l,lambda,lplus,n,w,z)
!
!     ***  COMPUTE LPLUS = SECANT UPDATE OF L  ***
!
!     ***  PARAMETER DECLARATIONS  ***
!
!     DIMENSION L(N*(N+1)/2), LPLUS(N*(N+1)/2)
!
!     --------------------------  PARAMETER USAGE  --------------------------
!
!     BETA = SCRATCH VECTOR.
!     GAMMA = SCRATCH VECTOR.
!      L (INPUT) LOWER TRIANGULAR MATRIX, STORED ROWWISE.
!     LAMBDA = SCRATCH VECTOR.
!     LPLUS (OUTPUT) LOWER TRIANGULAR MATRIX, STORED ROWWISE, WHICH MAY
!             OCCUPY THE SAME STORAGE AS  L.
!      N (INPUT) LENGTH OF VECTOR PARAMETERS AND ORDER OF MATRICES.
!      W (INPUT, DESTROYED ON OUTPUT) RIGHT SINGULAR VECTOR OF RANK 1
!             CORRECTION TO  L.
!      Z (INPUT, DESTROYED ON OUTPUT) LEFT SINGULAR VECTOR OF RANK 1
!             CORRECTION TO  L.
!
!     -------------------------------  NOTES  -------------------------------
!
!     ***  APPLICATION AND USAGE RESTRICTIONS  ***
!
!        THIS ROUTINE UPDATES THE CHOLESKY FACTOR  L  OF A SYMMETRIC
!     POSITIVE DEFINITE MATRIX TO WHICH A SECANT UPDATE IS BEING
!     APPLIED -- IT COMPUTES A CHOLESKY FACTOR  LPLUS  OF
!     L * (I + Z*W**T) * (I + W*Z**T) * L**T.  IT IS ASSUMED THAT  W
!     AND  Z  HAVE BEEN CHOSEN SO THAT THE UPDATED MATRIX IS STRICTLY
!     POSITIVE DEFINITE.
!
!     ***  ALGORITHM NOTES  ***
!
!        THIS CODE USES RECURRENCE 3 OF REF. 1 (WITH D(J) = 1 FOR ALL J)
!     TO COMPUTE  LPLUS  OF THE FORM  L * (I + Z*W**T) * Q,  WHERE  Q
!     IS AN ORTHOGONAL MATRIX THAT MAKES THE RESULT LOWER TRIANGULAR.
!        LPLUS MAY HAVE SOME NEGATIVE DIAGONAL ELEMENTS.
!
!     ***  REFERENCES  ***
!
!     1.  GOLDFARB, D. (1976), FACTORIZED VARIABLE METRIC METHODS FOR UNCON-
!             STRAINED OPTIMIZATION, MATH. COMPUT. 30, PP. 796-811.
!
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY (FALL 1979).
!     THIS SUBROUTINE WAS WRITTEN IN CONNECTION WITH RESEARCH SUPPORTED
!     BY THE NATIONAL SCIENCE FOUNDATION UNDER GRANTS MCS-7600324 AND
!     MCS-7906671.
!
!     ------------------------  EXTERNAL QUANTITIES  ------------------------
!
!     ***  INTRINSIC FUNCTIONS  ***
!     /+
!/
!     --------------------------  LOCAL VARIABLES  --------------------------
!
!
!     ***  DATA INITIALIZATIONS  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
!     .. Scalar Arguments ..
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: beta(n), gamma(n), l(*), lambda(n),  &
                                          lplus(*), w(n), z(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: a, b, bj, eta, gj, lij, lj, ljj, nu, &
                                          s, theta, wj, zj
      Integer                          :: i, ij, j, jj, jp1, k, nm1, np1
!     .. Intrinsic Procedures ..
      Intrinsic                        :: sqrt
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      nu = one
      eta = zero
      If (n<=1) Go To 100
      nm1 = n - 1
!
!     ***  TEMPORARILY STORE S(J) = SUM OVER K = J+1 TO N OF W(K)**2 IN
!     ***  LAMBDA(J).
!
      s = zero
      Do i = 1, nm1
        j = n - i
        s = s + w(j+1)**2
        lambda(j) = s
      End Do
!
!     ***  COMPUTE LAMBDA, GAMMA, AND BETA BY GOLDFARB*S RECURRENCE 3.
!
      Do j = 1, nm1
        wj = w(j)
        a = nu*z(j) - eta*wj
        theta = one + a*wj
        s = a*lambda(j)
        lj = sqrt(theta**2+a*s)
        If (theta>zero) lj = -lj
        lambda(j) = lj
        b = theta*wj + s
        gamma(j) = b*nu/lj
        beta(j) = (a-b*eta)/lj
        nu = -nu/lj
        eta = -(eta+(a**2)/(theta-lj))/lj
      End Do
100   lambda(n) = one + (nu*z(n)-eta*w(n))*w(n)
!
!     ***  UPDATE L, GRADUALLY OVERWRITING  W  AND  Z  WITH  L*W  AND  L*Z.
!
      np1 = n + 1
      jj = n*(n+1)/2
      Do k = 1, n
        j = np1 - k
        lj = lambda(j)
        ljj = l(jj)
        lplus(jj) = lj*ljj
        wj = w(j)
        w(j) = ljj*wj
        zj = z(j)
        z(j) = ljj*zj
        If (k==1) Go To 110
        bj = beta(j)
        gj = gamma(j)
        ij = jj + j
        jp1 = j + 1
        Do i = jp1, n
          lij = l(ij)
          lplus(ij) = lj*lij + bj*w(i) + gj*z(i)
          w(i) = w(i) + lij*wj
          z(i) = z(i) + lij*zj
          ij = ij + i
        End Do
110     jj = jj - j
      End Do
!
      Return
!     ***  LAST CARD OF DL7UPD FOLLOWS  ***
    End Subroutine dl7upd
    Subroutine do7prd(l,ls,p,s,w,y,z)
!
!     ***  FOR I = 1..L, SET S = S + W(I)*Y(.,I)*(Z(.,I)**T), I.E.,
!     ***        ADD W(I) TIMES THE OUTER PRODUCT OF Y(.,I) AND Z(.,I).
!
!     DIMENSION S(P*(P+1)/2)
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: l, ls, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: s(ls), w(l), y(p,l), z(p,l)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: wk, yi
      Real (Kind=wp), Save             :: zero
      Integer                          :: i, j, k, m
!     .. Data Statements ..
      Data zero/0.E+0_wp/
!     .. Executable Statements ..
!
      Do k = 1, l
        wk = w(k)
        If (wk==zero) Go To 100
        m = 1
        Do i = 1, p
          yi = wk*y(i,k)
          Do j = 1, i
            s(m) = s(m) + yi*z(j,k)
            m = m + 1
          End Do
        End Do
100   End Do
!
      Return
!     ***  LAST CARD OF DO7PRD FOLLOWS  ***
    End Subroutine do7prd
    Subroutine dv7vmp(n,x,y,z,k)
!
!     ***  SET X(I) = Y(I) * Z(I)**K, 1 .LE. I .LE. N (FOR K = 1 OR -1)  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: k, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: x(n), y(n), z(n)
!     .. Local Scalars ..
      Integer                          :: i
!     .. Executable Statements ..
!
      If (k>=0) Go To 100
      Do i = 1, n
        x(i) = y(i)/z(i)
      End Do
      Go To 110
!
100   Do i = 1, n
        x(i) = y(i)*z(i)
      End Do
110   Return
!     ***  LAST CARD OF DV7VMP FOLLOWS  ***
    End Subroutine dv7vmp
    Subroutine dsm(m,n,npairs,indrow,indcol,ngrp,maxgrp,mingrp,info,ipntr,     &
      jpntr,iwa,liwa,bwa)
!     **********
!
!     SUBROUTINE DSM
!
!     THE PURPOSE OF DSM IS TO DETERMINE AN OPTIMAL OR NEAR-
!     OPTIMAL CONSISTENT PARTITION OF THE COLUMNS OF A SPARSE
!     M BY N MATRIX A.
!
!     THE SPARSITY PATTERN OF THE MATRIX A IS SPECIFIED BY
!     THE ARRAYS INDROW AND INDCOL. ON INPUT THE INDICES
!     FOR THE NON-ZERO ELEMENTS OF A ARE
!
!           INDROW(K),INDCOL(K), K = 1,2,...,NPAIRS.
!
!     THE (INDROW,INDCOL) PAIRS MAY BE SPECIFIED IN ANY ORDER.
!     DUPLICATE INPUT PAIRS ARE PERMITTED, BUT THE SUBROUTINE
!     ELIMINATES THEM.
!
!     THE SUBROUTINE PARTITIONS THE COLUMNS OF A INTO GROUPS
!     SUCH THAT COLUMNS IN THE SAME GROUP DO NOT HAVE A
!     NON-ZERO IN THE SAME ROW POSITION. A PARTITION OF THE
!     COLUMNS OF A WITH THIS PROPERTY IS CONSISTENT WITH THE
!     DIRECT DETERMINATION OF A.
!
!     THE SUBROUTINE STATEMENT IS
!
!       SUBROUTINE DSM(M,N,NPAIRS,INDROW,INDCOL,NGRP,MAXGRP,MINGRP,
!                      INFO,IPNTR,JPNTR,IWA,LIWA,BWA)
!
!     WHERE
!
!       M IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE NUMBER
!         OF ROWS OF A.
!
!       N IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE NUMBER
!         OF COLUMNS OF A.
!
!       NPAIRS IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE
!         NUMBER OF (INDROW,INDCOL) PAIRS USED TO DESCRIBE THE
!         SPARSITY PATTERN OF A.
!
!       INDROW IS AN INTEGER ARRAY OF LENGTH NPAIRS. ON INPUT INDROW
!         MUST CONTAIN THE ROW INDICES OF THE NON-ZERO ELEMENTS OF A.
!         ON OUTPUT INDROW IS PERMUTED SO THAT THE CORRESPONDING
!         COLUMN INDICES ARE IN NON-DECREASING ORDER. THE COLUMN
!         INDICES CAN BE RECOVERED FROM THE ARRAY JPNTR.
!
!       INDCOL IS AN INTEGER ARRAY OF LENGTH NPAIRS. ON INPUT INDCOL
!         MUST CONTAIN THE COLUMN INDICES OF THE NON-ZERO ELEMENTS OF
!         A. ON OUTPUT INDCOL IS PERMUTED SO THAT THE CORRESPONDING
!         ROW INDICES ARE IN NON-DECREASING ORDER. THE ROW INDICES
!         CAN BE RECOVERED FROM THE ARRAY IPNTR.
!
!       NGRP IS AN INTEGER OUTPUT ARRAY OF LENGTH N WHICH SPECIFIES
!         THE PARTITION OF THE COLUMNS OF A. COLUMN JCOL BELONGS
!         TO GROUP NGRP(JCOL).
!
!       MAXGRP IS AN INTEGER OUTPUT VARIABLE WHICH SPECIFIES THE
!         NUMBER OF GROUPS IN THE PARTITION OF THE COLUMNS OF A.
!
!       MINGRP IS AN INTEGER OUTPUT VARIABLE WHICH SPECIFIES A LOWER
!         BOUND FOR THE NUMBER OF GROUPS IN ANY CONSISTENT PARTITION
!         OF THE COLUMNS OF A.
!
!       INFO IS AN INTEGER OUTPUT VARIABLE SET AS FOLLOWS. FOR
!         NORMAL TERMINATION INFO = 1. IF M, N, OR NPAIRS IS NOT
!         POSITIVE OR LIWA IS LESS THAN MAX(M,6*N), THEN INFO = 0.
!         IF THE K-TH ELEMENT OF INDROW IS NOT AN INTEGER BETWEEN
!         1 AND M OR THE K-TH ELEMENT OF INDCOL IS NOT AN INTEGER
!         BETWEEN 1 AND N, THEN INFO = -K.
!
!       IPNTR IS AN INTEGER OUTPUT ARRAY OF LENGTH M + 1 WHICH
!         SPECIFIES THE LOCATIONS OF THE COLUMN INDICES IN INDCOL.
!         THE COLUMN INDICES FOR ROW I ARE
!
!               INDCOL(K), K = IPNTR(I),...,IPNTR(I+1)-1.
!
!         NOTE THAT IPNTR(M+1)-1 IS THEN THE NUMBER OF NON-ZERO
!         ELEMENTS OF THE MATRIX A.
!
!       JPNTR IS AN INTEGER OUTPUT ARRAY OF LENGTH N + 1 WHICH
!         SPECIFIES THE LOCATIONS OF THE ROW INDICES IN INDROW.
!         THE ROW INDICES FOR COLUMN J ARE
!
!               INDROW(K), K = JPNTR(J),...,JPNTR(J+1)-1.
!
!         NOTE THAT JPNTR(N+1)-1 IS THEN THE NUMBER OF NON-ZERO
!         ELEMENTS OF THE MATRIX A.
!
!       IWA IS AN INTEGER WORK ARRAY OF LENGTH LIWA.
!
!       LIWA IS A POSITIVE INTEGER INPUT VARIABLE NOT LESS THAN
!         MAX(M,6*N).
!
!       BWA IS A LOGICAL WORK ARRAY OF LENGTH N.
!
!     SUBPROGRAMS CALLED
!
!       MINPACK-SUPPLIED ...D7EGR,I7DO,N7MSRT,M7SEQ,S7ETR,M7SLO,S7RTDT
!
!       FORTRAN-SUPPLIED ... MAX0
!
!     ARGONNE NATIONAL LABORATORY. MINPACK PROJECT. JUNE 1982.
!     THOMAS F. COLEMAN, BURTON S. GARBOW, JORGE J. MORE
!
!     **********

!     .. Implicit None Statement ..
      Implicit None
!     .. Scalar Arguments ..
      Integer                          :: info, liwa, m, maxgrp, mingrp, n,    &
                                          npairs
!     .. Array Arguments ..
      Integer                          :: indcol(npairs), indrow(npairs),      &
                                          ipntr(1), iwa(liwa), jpntr(1),       &
                                          ngrp(n)
      Logical                          :: bwa(n)
!     .. Local Scalars ..
      Integer                          :: i, ir, j, jp, jpl, jpu, k, maxclq,   &
                                          nnz, numgrp
!     .. External Procedures ..
      External                         :: d7egr, i7do, m7seq, m7slo, n7msrt,   &
                                          s7etr, s7rtdt
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max
!     .. Executable Statements ..
!
!     CHECK THE INPUT DATA.
!
      info = 0
      If (m<1 .Or. n<1 .Or. npairs<1 .Or. liwa<max(m,6*n)) Go To 140
      Do k = 1, npairs
        info = -k
        If (indrow(k)<1 .Or. indrow(k)>m .Or. indcol(k)<1 .Or. indcol(k)>n)    &
          Go To 140
      End Do
      info = 1
!
!     SORT THE DATA STRUCTURE BY COLUMNS.
!
      Call s7rtdt(n,npairs,indrow,indcol,jpntr,iwa(1))
!
!     COMPRESS THE DATA AND DETERMINE THE NUMBER OF
!     NON-ZERO ELEMENTS OF A.
!
      Do i = 1, m
        iwa(i) = 0
      End Do
      nnz = 0
      Do j = 1, n
        jpl = jpntr(j)
        jpu = jpntr(j+1) - 1
        jpntr(j) = nnz + 1
        If (jpu<jpl) Go To 110
        Do jp = jpl, jpu
          ir = indrow(jp)
          If (iwa(ir)/=0) Go To 100
          nnz = nnz + 1
          indrow(nnz) = ir
          iwa(ir) = 1
100       Continue
        End Do
        jpl = jpntr(j)
        Do jp = jpl, nnz
          ir = indrow(jp)
          iwa(ir) = 0
        End Do
110     Continue
      End Do
      jpntr(n+1) = nnz + 1
!
!     EXTEND THE DATA STRUCTURE TO ROWS.
!
      Call s7etr(m,n,indrow,jpntr,indcol,ipntr,iwa(1))
!
!     DETERMINE A LOWER BOUND FOR THE NUMBER OF GROUPS.
!
      mingrp = 0
      Do i = 1, m
        mingrp = max(mingrp,ipntr(i+1)-ipntr(i))
      End Do
!
!     DETERMINE THE DEGREE SEQUENCE FOR THE INTERSECTION
!     GRAPH OF THE COLUMNS OF A.
!
      Call d7egr(n,indrow,jpntr,indcol,ipntr,iwa(5*n+1),iwa(n+1),bwa)
!
!     COLOR THE INTERSECTION GRAPH OF THE COLUMNS OF A
!     WITH THE SMALLEST-LAST (SL) ORDERING.
!
      Call m7slo(n,indrow,jpntr,indcol,ipntr,iwa(5*n+1),iwa(4*n+1),maxclq,     &
        iwa(1),iwa(n+1),iwa(2*n+1),iwa(3*n+1),bwa)
      Call m7seq(n,indrow,jpntr,indcol,ipntr,iwa(4*n+1),ngrp,maxgrp,iwa(n+1),  &
        bwa)
      mingrp = max(mingrp,maxclq)
      If (maxgrp==mingrp) Go To 140
!
!     COLOR THE INTERSECTION GRAPH OF THE COLUMNS OF A
!     WITH THE INCIDENCE-DEGREE (ID) ORDERING.
!
      Call i7do(m,n,indrow,jpntr,indcol,ipntr,iwa(5*n+1),iwa(4*n+1),maxclq,    &
        iwa(1),iwa(n+1),iwa(2*n+1),iwa(3*n+1),bwa)
      Call m7seq(n,indrow,jpntr,indcol,ipntr,iwa(4*n+1),iwa(1),numgrp,         &
        iwa(n+1),bwa)
      mingrp = max(mingrp,maxclq)
      If (numgrp>=maxgrp) Go To 120
      maxgrp = numgrp
      Do j = 1, n
        ngrp(j) = iwa(j)
      End Do
      If (maxgrp==mingrp) Go To 140
120   Continue
!
!     COLOR THE INTERSECTION GRAPH OF THE COLUMNS OF A
!     WITH THE LARGEST-FIRST (LF) ORDERING.
!
      Call n7msrt(n,n-1,iwa(5*n+1),-1,iwa(4*n+1),iwa(2*n+1),iwa(n+1))
      Call m7seq(n,indrow,jpntr,indcol,ipntr,iwa(4*n+1),iwa(1),numgrp,         &
        iwa(n+1),bwa)
      If (numgrp>=maxgrp) Go To 130
      maxgrp = numgrp
      Do j = 1, n
        ngrp(j) = iwa(j)
      End Do
130   Continue
!
!     EXIT FROM PROGRAM.
!
140   Continue
      Return
!
!     LAST CARD OF SUBROUTINE DSM.
!
    End Subroutine dsm
    Subroutine m7seq(n,indrow,jpntr,indcol,ipntr,list,ngrp,maxgrp,iwa,bwa)
!     **********
!
!     SUBROUTINE M7SEQ
!
!     GIVEN THE SPARSITY PATTERN OF AN M BY N MATRIX A, THIS
!     SUBROUTINE DETERMINES A CONSISTENT PARTITION OF THE
!     COLUMNS OF A BY A SEQUENTIAL ALGORITHM.
!
!     A CONSISTENT PARTITION IS DEFINED IN TERMS OF THE LOOPLESS
!     GRAPH G WITH VERTICES A(J), J = 1,2,...,N WHERE A(J) IS THE
!     J-TH COLUMN OF A AND WITH EDGE (A(I),A(J)) IF AND ONLY IF
!     COLUMNS I AND J HAVE A NON-ZERO IN THE SAME ROW POSITION.
!
!     A PARTITION OF THE COLUMNS OF A INTO GROUPS IS CONSISTENT
!     IF THE COLUMNS IN ANY GROUP ARE NOT ADJACENT IN THE GRAPH G.
!     IN GRAPH-THEORY TERMINOLOGY, A CONSISTENT PARTITION OF THE
!     COLUMNS OF A CORRESPONDS TO A COLORING OF THE GRAPH G.
!
!     THE SUBROUTINE EXAMINES THE COLUMNS IN THE ORDER SPECIFIED
!     BY THE ARRAY LIST, AND ASSIGNS THE CURRENT COLUMN TO THE
!     GROUP WITH THE SMALLEST POSSIBLE NUMBER.
!
!     NOTE THAT THE VALUE OF M IS NOT NEEDED BY M7SEQ AND IS
!     THEREFORE NOT PRESENT IN THE SUBROUTINE STATEMENT.
!
!     THE SUBROUTINE STATEMENT IS
!
!       SUBROUTINE M7SEQ(N,INDROW,JPNTR,INDCOL,IPNTR,LIST,NGRP,MAXGRP,
!                      IWA,BWA)
!
!     WHERE
!
!       N IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE NUMBER
!         OF COLUMNS OF A.
!
!       INDROW IS AN INTEGER INPUT ARRAY WHICH CONTAINS THE ROW
!         INDICES FOR THE NON-ZEROES IN THE MATRIX A.
!
!       JPNTR IS AN INTEGER INPUT ARRAY OF LENGTH N + 1 WHICH
!         SPECIFIES THE LOCATIONS OF THE ROW INDICES IN INDROW.
!         THE ROW INDICES FOR COLUMN J ARE
!
!               INDROW(K), K = JPNTR(J),...,JPNTR(J+1)-1.
!
!         NOTE THAT JPNTR(N+1)-1 IS THEN THE NUMBER OF NON-ZERO
!         ELEMENTS OF THE MATRIX A.
!
!       INDCOL IS AN INTEGER INPUT ARRAY WHICH CONTAINS THE
!         COLUMN INDICES FOR THE NON-ZEROES IN THE MATRIX A.
!
!       IPNTR IS AN INTEGER INPUT ARRAY OF LENGTH M + 1 WHICH
!         SPECIFIES THE LOCATIONS OF THE COLUMN INDICES IN INDCOL.
!         THE COLUMN INDICES FOR ROW I ARE
!
!               INDCOL(K), K = IPNTR(I),...,IPNTR(I+1)-1.
!
!         NOTE THAT IPNTR(M+1)-1 IS THEN THE NUMBER OF NON-ZERO
!         ELEMENTS OF THE MATRIX A.
!
!       LIST IS AN INTEGER INPUT ARRAY OF LENGTH N WHICH SPECIFIES
!         THE ORDER TO BE USED BY THE SEQUENTIAL ALGORITHM.
!         THE J-TH COLUMN IN THIS ORDER IS LIST(J).
!
!       NGRP IS AN INTEGER OUTPUT ARRAY OF LENGTH N WHICH SPECIFIES
!         THE PARTITION OF THE COLUMNS OF A. COLUMN JCOL BELONGS
!         TO GROUP NGRP(JCOL).
!
!       MAXGRP IS AN INTEGER OUTPUT VARIABLE WHICH SPECIFIES THE
!         NUMBER OF GROUPS IN THE PARTITION OF THE COLUMNS OF A.
!
!       IWA IS AN INTEGER WORK ARRAY OF LENGTH N.
!
!       BWA IS A LOGICAL WORK ARRAY OF LENGTH N.
!
!     ARGONNE NATIONAL LABORATORY. MINPACK PROJECT. JUNE 1982.
!     THOMAS F. COLEMAN, BURTON S. GARBOW, JORGE J. MORE
!
!     **********

!     .. Implicit None Statement ..
      Implicit None
!     .. Scalar Arguments ..
      Integer                          :: maxgrp, n
!     .. Array Arguments ..
      Integer                          :: indcol(1), indrow(1), ipntr(1),      &
                                          iwa(n), jpntr(1), list(n), ngrp(n)
      Logical                          :: bwa(n)
!     .. Local Scalars ..
      Integer                          :: deg, ic, ip, ipl, ipu, ir, j, jcol,  &
                                          jp, jpl, jpu, l, numgrp
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max
!     .. Executable Statements ..
!
!     INITIALIZATION BLOCK.
!
      maxgrp = 0
      Do jp = 1, n
        ngrp(jp) = n
        bwa(jp) = .False.
      End Do
      bwa(n) = .True.
!
!     BEGINNING OF ITERATION LOOP.
!
      Do j = 1, n
        jcol = list(j)
!
!        FIND ALL COLUMNS ADJACENT TO COLUMN JCOL.
!
        deg = 0
!
!        DETERMINE ALL POSITIONS (IR,JCOL) WHICH CORRESPOND
!        TO NON-ZEROES IN THE MATRIX.
!
        jpl = jpntr(jcol)
        jpu = jpntr(jcol+1) - 1
        If (jpu<jpl) Go To 110
        Do jp = jpl, jpu
          ir = indrow(jp)
!
!           FOR EACH ROW IR, DETERMINE ALL POSITIONS (IR,IC)
!           WHICH CORRESPOND TO NON-ZEROES IN THE MATRIX.
!
          ipl = ipntr(ir)
          ipu = ipntr(ir+1) - 1
          Do ip = ipl, ipu
            ic = indcol(ip)
            l = ngrp(ic)
!
!              ARRAY BWA MARKS THE GROUP NUMBERS OF THE
!              COLUMNS WHICH ARE ADJACENT TO COLUMN JCOL.
!              ARRAY IWA RECORDS THE MARKED GROUP NUMBERS.
!
            If (bwa(l)) Go To 100
            bwa(l) = .True.
            deg = deg + 1
            iwa(deg) = l
100         Continue
          End Do
        End Do
110     Continue
!
!        ASSIGN THE SMALLEST UN-MARKED GROUP NUMBER TO JCOL.
!
        Do jp = 1, n
          numgrp = jp
          If (.Not. bwa(jp)) Go To 120
        End Do
120     Continue
        ngrp(jcol) = numgrp
        maxgrp = max(maxgrp,numgrp)
!
!        UN-MARK THE GROUP NUMBERS.
!
        If (deg<1) Go To 130
        Do jp = 1, deg
          l = iwa(jp)
          bwa(l) = .False.
        End Do
130     Continue
      End Do
!
!        END OF ITERATION LOOP.
!
      Return
!
!     LAST CARD OF SUBROUTINE M7SEQ.
!
    End Subroutine m7seq
    Subroutine dl7tsq(n,a,l)
!
!     ***  SET A TO LOWER TRIANGLE OF (L**T) * L  ***
!
!     ***  L = N X N LOWER TRIANG. MATRIX STORED ROWWISE.  ***
!     ***  A IS ALSO STORED ROWWISE AND MAY SHARE STORAGE WITH L.  ***
!
!     DIMENSION A(N*(N+1)/2), L(N*(N+1)/2)
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(*), l(*)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: lii, lj
      Integer                          :: i, i1, ii, iim1, j, k, m
!     .. Executable Statements ..
!
      ii = 0
      Do i = 1, n
        i1 = ii + 1
        ii = ii + i
        m = 1
        If (i==1) Go To 100
        iim1 = ii - 1
        Do j = i1, iim1
          lj = l(j)
          Do k = i1, j
            a(m) = a(m) + lj*l(k)
            m = m + 1
          End Do
        End Do
100     lii = l(ii)
        Do j = i1, ii
          a(j) = lii*l(j)
        End Do
      End Do
!
      Return
!     ***  LAST CARD OF DL7TSQ FOLLOWS  ***
    End Subroutine dl7tsq
    Function drldst(p,d,x,x0)
!
!     ***  COMPUTE AND RETURN RELATIVE DIFFERENCE BETWEEN X AND X0  ***
!     ***  NL2SOL VERSION 2.2  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Function Return Value ..
      Real (Kind=wp)                   :: drldst
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
!     .. Scalar Arguments ..
      Integer                          :: p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: d(p), x(p), x0(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: emax, t, xmax
      Integer                          :: i
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs
!     .. Executable Statements ..
!
!     ***  BODY  ***
!
      emax = zero
      xmax = zero
      Do i = 1, p
        t = abs(d(i)*(x(i)-x0(i)))
        If (emax<t) emax = t
        t = d(i)*(abs(x(i))+abs(x0(i)))
        If (xmax<t) xmax = t
      End Do
      drldst = zero
      If (xmax>zero) drldst = emax/xmax
      Return
!     ***  LAST CARD OF DRLDST FOLLOWS  ***
    End Function drldst
    Subroutine drn2gb(b,d,dr,iv,liv,lv,n,nd,n1,n2,p,r,rd,v,x)
!
!     ***  REVISED ITERATION DRIVER FOR NL2SOL WITH SIMPLE BOUNDS  ***
!
!
!     --------------------------  PARAMETER USAGE  --------------------------
!
!     B........ BOUNDS ON X.
!     D........ SCALE VECTOR.
!     DR....... DERIVATIVES OF R AT X.
!     IV....... INTEGER VALUES ARRAY.
!     LIV...... LENGTH OF IV... LIV MUST BE AT LEAST 4*P + 82.
!     LV....... LENGTH OF V...  LV  MUST BE AT LEAST 105 + P*(2*P+20).
!     N........ TOTAL NUMBER OF RESIDUALS.
!     ND....... MAX. NO. OF RESIDUALS PASSED ON ONE CALL.
!     N1....... LOWEST  ROW INDEX FOR RESIDUALS SUPPLIED THIS TIME.
!     N2....... HIGHEST ROW INDEX FOR RESIDUALS SUPPLIED THIS TIME.
!     P........ NUMBER OF PARAMETERS (COMPONENTS OF X) BEING ESTIMATED.
!     R........ RESIDUALS.
!     V........ FLOATING-POINT VALUES ARRAY.
!     X........ PARAMETER VECTOR BEING ESTIMATED (INPUT = INITIAL GUESS,
!             OUTPUT = BEST VALUE FOUND).
!
!     ***  DISCUSSION  ***
!
!     THIS ROUTINE CARRIES OUT ITERATIONS FOR SOLVING NONLINEAR
!     LEAST SQUARES PROBLEMS.  IT IS SIMILAR TO  DRN2G, EXCEPT THAT
!     THIS ROUTINE ENFORCES THE BOUNDS  B(1,I) .LE. X(I) .LE. B(2,I),
!     I = 1(1)P.
!
!     ***  GENERAL  ***
!
!     CODED BY DAVID M. GAY.
!
!     +++++++++++++++++++++++++++++  DECLARATIONS  ++++++++++++++++++++++++++
!
!     ***  EXTERNAL FUNCTIONS AND SUBROUTINES  ***
!
!
!     DIVSET.... PROVIDES DEFAULT IV AND V INPUT COMPONENTS.
!     DD7TPR... COMPUTES INNER PRODUCT OF TWO VECTORS.
!     DD7UPD...  UPDATES SCALE VECTOR D.
!     DG7ITB... PERFORMS BASIC MINIMIZATION ALGORITHM.
!     DITSUM.... PRINTS ITERATION SUMMARY, INFO ABOUT INITIAL AND FINAL X.
!     DL7VML.... COMPUTES L * V, V = VECTOR, L = LOWER TRIANGULAR MATRIX.
!     DQ7APL... APPLIES QR TRANSFORMATIONS STORED BY DQ7RAD.
!     DQ7RAD.... ADDS A NEW BLOCK OF ROWS TO QR DECOMPOSITION.
!     DR7TVM... MULT. VECTOR BY TRANS. OF UPPER TRIANG. MATRIX FROM QR FACT.
!     DV7CPY.... COPIES ONE VECTOR TO ANOTHER.
!     DV7SCP... SETS ALL ELEMENTS OF A VECTOR TO A SCALAR.
!     DV2NRM... RETURNS THE 2-NORM OF A VECTOR.
!
!
!     ***  LOCAL VARIABLES  ***
!
!
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!
!     ***  IV SUBSCRIPT VALUES  ***
!
!
!     ***  V SUBSCRIPT VALUES  ***
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: d0init = 40, dinit = 38,             &
                                          dtinit = 39, dtype = 16, f = 10,     &
                                          g = 28, jcn = 66, jtol = 59,         &
                                          mode = 35, nextv = 47, nf0 = 68,     &
                                          nf00 = 81, nf1 = 69, nfcall = 6,     &
                                          nfcov = 52, nfgcal = 7, qtr = 77,    &
                                          rdreq = 57, regd = 67, restor = 9,   &
                                          rlimit = 46, rmat = 78, toobig = 2,  &
                                          vneed = 4
!     .. Scalar Arguments ..
      Integer                          :: liv, lv, n, n1, n2, nd, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(2,p), d(p), dr(nd,p), r(nd),       &
                                          rd(nd), v(lv), x(p)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t
      Integer                          :: g1, gi, i, iv1, ivmode, jtol1, l,    &
                                          lh, nn, qtr1, rd1, rmat1, y1, yi
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, dv2nrm
      External                         :: dd7upd, dg7itb, ditsum, divset,      &
                                          dl7vml, dq7apl, dq7rad, dr7tvm,      &
                                          dv7cpy, dv7scp
!     .. Intrinsic Procedures ..
      Intrinsic                        :: min
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      lh = p*(p+1)/2
      If (iv(1)==0) Call divset(1,iv,liv,lv,v)
      iv1 = iv(1)
      If (iv1>2) Go To 100
      nn = n2 - n1 + 1
      iv(restor) = 0
      i = iv1 + 4
      If (iv(toobig)==0) Go To (220,210,220,200,200,220) i
      If (i/=5) iv(1) = 2
      Go To 130
!
!     ***  FRESH START OR RESTART -- CHECK INPUT INTEGERS  ***
!
100   If (nd<=0) Go To 270
      If (p<=0) Go To 270
      If (n<=0) Go To 270
      If (iv1==14) Go To 120
      If (iv1>16) Go To 320
      If (iv1<12) Go To 130
      If (iv1==12) iv(1) = 13
      If (iv(1)/=13) Go To 110
      iv(vneed) = iv(vneed) + p*(p+15)/2
110   Call dg7itb(b,d,x,iv,liv,lv,p,p,v,x,x)
      If (iv(1)/=14) Go To 330
!
!     ***  STORAGE ALLOCATION  ***
!
      iv(g) = iv(nextv)
      iv(jcn) = iv(g) + 2*p
      iv(rmat) = iv(jcn) + p
      iv(qtr) = iv(rmat) + lh
      iv(jtol) = iv(qtr) + 2*p
      iv(nextv) = iv(jtol) + 2*p
!     ***  TURN OFF COVARIANCE COMPUTATION  ***
      iv(rdreq) = 0
      If (iv1==13) Go To 330
!
120   jtol1 = iv(jtol)
      If (v(dinit)>=zero) Call dv7scp(p,d,v(dinit))
      If (v(dtinit)>zero) Call dv7scp(p,v(jtol1),v(dtinit))
      i = jtol1 + p
      If (v(d0init)>zero) Call dv7scp(p,v(i),v(d0init))
      iv(nf0) = 0
      iv(nf1) = 0
      If (nd>=n) Go To 130
!
!     ***  SPECIAL CASE HANDLING OF FIRST FUNCTION AND GRADIENT EVALUATION
!     ***  -- ASK FOR BOTH RESIDUAL AND JACOBIAN AT ONCE
!
      g1 = iv(g)
      y1 = g1 + p
      Call dg7itb(b,d,v(g1),iv,liv,lv,p,p,v,x,v(y1))
      If (iv(1)/=1) Go To 310
      v(f) = zero
      Call dv7scp(p,v(g1),zero)
      iv(1) = -1
      qtr1 = iv(qtr)
      Call dv7scp(p,v(qtr1),zero)
      iv(regd) = 0
      rmat1 = iv(rmat)
      Go To 180
!
130   g1 = iv(g)
      y1 = g1 + p
      Call dg7itb(b,d,v(g1),iv,liv,lv,p,p,v,x,v(y1))
      If (iv(1)==2) Go To 140
      If (iv(1)>2) Go To 310
!
      v(f) = zero
      If (iv(nf1)==0) Go To 290
      If (iv(restor)/=2) Go To 290
      iv(nf0) = iv(nf1)
      Call dv7cpy(n,rd,r)
      iv(regd) = 0
      Go To 290
!
140   Call dv7scp(p,v(g1),zero)
      If (iv(mode)>0) Go To 280
      rmat1 = iv(rmat)
      qtr1 = iv(qtr)
      rd1 = qtr1 + p
      Call dv7scp(p,v(qtr1),zero)
      iv(regd) = 0
      If (nd<n) Go To 170
      If (n1/=1) Go To 170
      If (iv(mode)<0) Go To 180
      If (iv(nf1)==iv(nfgcal)) Go To 150
      If (iv(nf0)/=iv(nfgcal)) Go To 170
      Call dv7cpy(n,r,rd)
      Go To 160
150   Call dv7cpy(n,rd,r)
160   Call dq7apl(nd,n,p,dr,rd,0)
      Call dr7tvm(nd,min(n,p),v(y1),v(rd1),dr,rd)
      iv(regd) = 0
      Go To 190
!
170   iv(1) = -2
      If (iv(mode)<0) iv(1) = -3
180   Call dv7scp(p,v(y1),zero)
190   Call dv7scp(lh,v(rmat1),zero)
      Go To 290
!
!     ***  COMPUTE F(X)  ***
!
200   t = dv2nrm(nn,r)
      If (t>v(rlimit)) Go To 260
      v(f) = v(f) + half*t**2
      If (n2<n) Go To 300
      If (n1==1) iv(nf1) = iv(nfcall)
      Go To 130
!
!     ***  COMPUTE Y  ***
!
210   y1 = iv(g) + p
      yi = y1
      Do l = 1, p
        v(yi) = v(yi) + dd7tpr(nn,dr(1,l),r)
        yi = yi + 1
      End Do
      If (n2<n) Go To 300
      iv(1) = 2
      If (n1>1) iv(1) = -3
      Go To 290
!
!     ***  COMPUTE GRADIENT INFORMATION  ***
!
220   g1 = iv(g)
      ivmode = iv(mode)
      If (ivmode<0) Go To 230
      If (ivmode==0) Go To 240
      iv(1) = 2
!
!     ***  COMPUTE GRADIENT ONLY (FOR USE IN COVARIANCE COMPUTATION)  ***
!
      gi = g1
      Do l = 1, p
        v(gi) = v(gi) + dd7tpr(nn,r,dr(1,l))
        gi = gi + 1
      End Do
      Go To 250
!
!     *** COMPUTE INITIAL FUNCTION VALUE WHEN ND .LT. N ***
!
230   If (n<=nd) Go To 240
      t = dv2nrm(nn,r)
      If (t>v(rlimit)) Go To 260
      v(f) = v(f) + half*t**2
!
!     ***  UPDATE D IF DESIRED  ***
!
240   If (iv(dtype)>0) Call dd7upd(d,dr,iv,liv,lv,n,nd,nn,n2,p,v)
!
!     ***  COMPUTE RMAT AND QTR  ***
!
      qtr1 = iv(qtr)
      rmat1 = iv(rmat)
      Call dq7rad(nn,nd,p,v(qtr1),.True.,v(rmat1),dr,r)
      iv(nf1) = 0
      If (n1>1) Go To 250
      If (n2<n) Go To 300
!
!     ***  SAVE DIAGONAL OF R FOR COMPUTING Y LATER  ***
!
      rd1 = qtr1 + p
      l = rmat1 - 1
      Do i = 1, p
        l = l + i
        v(rd1) = v(l)
        rd1 = rd1 + 1
      End Do
!
250   If (n2<n) Go To 300
      If (ivmode>0) Go To 130
      iv(nf00) = iv(nfgcal)
!
!     ***  COMPUTE G FROM RMAT AND QTR  ***
!
      Call dl7vml(p,v(g1),v(rmat1),v(qtr1))
      iv(1) = 2
      If (ivmode==0) Go To 130
      If (n<=nd) Go To 130
!
!     ***  FINISH SPECIAL CASE HANDLING OF FIRST FUNCTION AND GRADIENT
!
      y1 = g1 + p
      iv(1) = 1
      Call dg7itb(b,d,v(g1),iv,liv,lv,p,p,v,x,v(y1))
      If (iv(1)/=2) Go To 310
      Go To 130
!
!     ***  MISC. DETAILS  ***
!
!     ***  X IS OUT OF RANGE (OVERSIZE STEP)  ***
!
260   iv(toobig) = 1
      Go To 130
!
!     ***  BAD N, ND, OR P  ***
!
270   iv(1) = 66
      Go To 320
!
!     ***  RECORD EXTRA EVALUATIONS FOR FINITE-DIFFERENCE HESSIAN  ***
!
280   iv(nfcov) = iv(nfcov) + 1
      iv(nfcall) = iv(nfcall) + 1
      iv(nfgcal) = iv(nfcall)
      iv(1) = -1
!
!     ***  RETURN FOR MORE FUNCTION OR GRADIENT INFORMATION  ***
!
290   n2 = 0
300   n1 = n2 + 1
      n2 = n2 + nd
      If (n2>n) n2 = n
      Go To 330
!
!     ***  PRINT SUMMARY OF FINAL ITERATION AND OTHER REQUESTED ITEMS  ***
!
310   g1 = iv(g)
320   Call ditsum(d,v(g1),iv,liv,lv,p,v,x)
!
330   Return
!     ***  LAST CARD OF DRN2GB FOLLOWS  ***
    End Subroutine drn2gb
    Subroutine dd7dgb(b,d,dig,dst,g,ipiv,ka,l,lv,p,pc,nwtst,step,td,tg,v,w,x0)
!
!     ***  COMPUTE DOUBLE-DOGLEG STEP, SUBJECT TO SIMPLE BOUNDS ON X  ***
!
!
!     DIMENSION L(P*(P+1)/2)
!
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  V SUBSCRIPTS  ***
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: two = 2.E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: dgnorm = 1, dst0 = 3, dstnrm = 2,    &
                                          grdfac = 45, gthg = 44, gtstep = 4,  &
                                          nreduc = 6, nwtfac = 46, preduc = 7, &
                                          radius = 8, stppar = 5
!     .. Scalar Arguments ..
      Integer                          :: ka, lv, p, pc
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(2,p), d(p), dig(p), dst(p), g(p),  &
                                          l(*), nwtst(p), step(p), td(p),      &
                                          tg(p), v(lv), w(p), x0(p)
      Integer                          :: ipiv(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: dnwtst, ghinvg, gnorm, gnorm0, nred, &
                                          pred, rad, t, t1, t2, ti, x0i, xi
      Real (Kind=wp), Save             :: meps2
      Integer                          :: i, j, k, p1, p1m1
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, dr7mdc, dv2nrm
      External                         :: dd7dog, dl7itv, dl7ivm, dl7tvm,      &
                                          dl7vml, dq7rsh, dv2axy, dv7cpy,      &
                                          dv7ipr, dv7scp, dv7shf, dv7vmp,      &
                                          i7shft
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max
!     .. Data Statements ..
      Data meps2/0.E+0_wp/
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      If (meps2<=zero) meps2 = two*dr7mdc(3)
      gnorm0 = v(dgnorm)
      v(dstnrm) = zero
      If (ka<0) Go To 100
      dnwtst = v(dst0)
      nred = v(nreduc)
100   pred = zero
      v(stppar) = zero
      rad = v(radius)
      If (pc>0) Go To 110
      dnwtst = zero
      Call dv7scp(p,step,zero)
      Go To 200
!
110   p1 = pc
      Call dv7cpy(p,td,d)
      Call dv7ipr(p,ipiv,td)
      Call dv7scp(pc,dst,zero)
      Call dv7cpy(p,tg,g)
      Call dv7ipr(p,ipiv,tg)
!
120   Call dl7ivm(p1,nwtst,l,tg)
      ghinvg = dd7tpr(p1,nwtst,nwtst)
      v(nreduc) = half*ghinvg
      Call dl7itv(p1,nwtst,l,nwtst)
      Call dv7vmp(p1,step,nwtst,td,1)
      v(dst0) = dv2nrm(pc,step)
      If (ka>=0) Go To 130
      ka = 0
      dnwtst = v(dst0)
      nred = v(nreduc)
130   v(radius) = rad - v(dstnrm)
      If (v(radius)<=zero) Go To 180
      Call dv7vmp(p1,dig,tg,td,-1)
      gnorm = dv2nrm(p1,dig)
      If (gnorm<=zero) Go To 180
      v(dgnorm) = gnorm
      Call dv7vmp(p1,dig,dig,td,-1)
      Call dl7tvm(p1,w,l,dig)
      v(gthg) = dv2nrm(p1,w)
      ka = ka + 1
      Call dd7dog(dig,lv,p1,nwtst,step,v)
!
!     ***  FIND T SUCH THAT X - T*STEP IS STILL FEASIBLE.
!
      t = one
      k = 0
      Do i = 1, p1
        j = ipiv(i)
        x0i = x0(j) + dst(i)/td(i)
        xi = x0i + step(i)
        If (xi<b(1,j)) Go To 140
        If (xi<=b(2,j)) Go To 160
        ti = (b(2,j)-x0i)/step(i)
        j = i
        Go To 150
140     ti = (b(1,j)-x0i)/step(i)
        j = -i
150     If (t<=ti) Go To 160
        k = j
        t = ti
160   End Do
!
!     ***  UPDATE DST, TG, AND PRED  ***
!
      Call dv7vmp(p1,step,step,td,1)
      Call dv2axy(p1,dst,t,step,dst)
      v(dstnrm) = dv2nrm(pc,dst)
      t1 = t*v(grdfac)
      t2 = t*v(nwtfac)
      pred = pred - t1*gnorm*((t2+one)*gnorm) - t2*(one+half*t2)*ghinvg -      &
        half*(v(gthg)*t1)**2
      If (k==0) Go To 180
      Call dl7vml(p1,w,l,w)
      t2 = one - t2
      Do i = 1, p1
        tg(i) = t2*tg(i) - t1*w(i)
      End Do
!
!     ***  PERMUTE L, ETC. IF NECESSARY  ***
!
      p1m1 = p1 - 1
      j = abs(k)
      If (j==p1) Go To 170
      Call dq7rsh(j,p1,.False.,tg,l,w)
      Call i7shft(p1,j,ipiv)
      Call dv7shf(p1,j,tg)
      Call dv7shf(p1,j,td)
      Call dv7shf(p1,j,dst)
170   If (k<0) ipiv(p1) = -ipiv(p1)
      p1 = p1m1
      If (p1>0) Go To 120
!
!     ***  UNSCALE STEP, UPDATE X AND DIHDI  ***
!
180   Call dv7scp(p,step,zero)
      Do i = 1, pc
        j = abs(ipiv(i))
        step(j) = dst(i)/td(i)
      End Do
!
!     ***  FUDGE STEP TO ENSURE THAT IT FORCES APPROPRIATE COMPONENTS
!     ***  TO THEIR BOUNDS  ***
!
      If (p1>=pc) Go To 200
      Call dv2axy(p,td,one,step,x0)
      k = p1 + 1
      Do i = k, pc
        j = ipiv(i)
        t = meps2
        If (j>0) Go To 190
        t = -t
        j = -j
        ipiv(i) = j
190     t = t*max(abs(td(j)),abs(x0(j)))
        step(j) = step(j) + t
      End Do
!
200   v(dgnorm) = gnorm0
      v(nreduc) = nred
      v(preduc) = pred
      v(radius) = rad
      v(dst0) = dnwtst
      v(gtstep) = dd7tpr(p,step,g)
!
      Return
!     ***  LAST LINE OF DD7DGB FOLLOWS  ***
    End Subroutine dd7dgb
    Subroutine dq7rfh(ierr,ipivot,n,nn,nopivk,p,q,r,rlen,w)
!
!     ***  COMPUTE QR FACTORIZATION VIA HOUSEHOLDER TRANSFORMATIONS
!     ***  WITH COLUMN PIVOTING  ***
!
!     ***  PARAMETER DECLARATIONS  ***
!
!     DIMENSION R(P*(P+1)/2)
!
!     ----------------------------  DESCRIPTION  ----------------------------
!
!     THIS ROUTINE COMPUTES A QR FACTORIZATION (VIA HOUSEHOLDER TRANS-
!     FORMATIONS) OF THE MATRIX  A  THAT ON INPUT IS STORED IN Q.
!     IF  NOPIVK  ALLOWS IT, THIS ROUTINE DOES COLUMN PIVOTING -- IF
!     K .GT. NOPIVK,  THEN ORIGINAL COLUMN  K  IS ELIGIBLE FOR PIVOTING.
!     THE  Q  AND  R  RETURNED ARE SUCH THAT COLUMN  I  OF  Q*R  EQUALS
!     COLUMN  IPIVOT(I)  OF THE ORIGINAL MATRIX  A.  THE UPPER TRIANGULAR
!     MATRIX  R  IS STORED COMPACTLY BY COLUMNS, I.E., THE OUTPUT VECTOR  R
!     CONTAINS  R(1,1), R(1,2), R(2,2), R(1,3), R(2,3), ..., R(P,P) (IN
!     THAT ORDER).  IF ALL GOES WELL, THEN THIS ROUTINE SETS  IERR = 0.
!     BUT IF (PERMUTED) COLUMN  K  OF  A  IS LINEARLY DEPENDENT ON
!     (PERMUTED) COLUMNS 1,2,...,K-1, THEN  IERR  IS SET TO  K AND THE R
!     MATRIX RETURNED HAS  R(I,J) = 0  FOR  I .GE. K  AND  J .GE. K.
!     THE ORIGINAL MATRIX  A  IS AN N BY P MATRIX.  NN  IS THE LEAD
!     DIMENSION OF THE ARRAY  Q  AND MUST SATISFY  NN .GE. N.  NO
!     PARAMETER CHECKING IS DONE.
!     PIVOTING IS DONE AS THOUGH ALL COLUMNS OF Q WERE FIRST
!     SCALED TO HAVE THE SAME NORM.  IF COLUMN K IS ELIGIBLE FOR
!     PIVOTING AND ITS (SCALED) NORM**2 LOSS IS MORE THAN THE
!     MINIMUM SUCH LOSS (OVER COLUMNS K THRU P), THEN COLUMN K IS
!     SWAPPED WITH THE COLUMN OF LEAST NORM**2 LOSS.
!
!        CODED BY DAVID M. GAY (FALL 1979, SPRING 1984).
!
!     --------------------------  LOCAL VARIABLES  --------------------------
!
!     /+
!/

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: one = 1.0E+0_wp
      Real (Kind=wp), Parameter        :: ten = 1.E+1_wp
      Real (Kind=wp), Parameter        :: wtol = 0.75E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.0E+0_wp
!     .. Scalar Arguments ..
      Integer                          :: ierr, n, nn, nopivk, p, rlen
!     .. Array Arguments ..
      Real (Kind=wp)                   :: q(nn,p), r(rlen), w(p)
      Integer                          :: ipivot(p)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: ak, big, qkk, s, singtl, t, t1, wk
      Real (Kind=wp), Save             :: bigrt, meps10, tiny, tinyrt
      Integer                          :: i, ii, j, k, kk, km1, kp1, nk1
!     .. External Procedures ..
      Real (Kind=wp), External         :: dd7tpr, dr7mdc, dv2nrm
      External                         :: dv2axy, dv7scl, dv7scp, dv7swp
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max, real, sqrt
!     .. Data Statements ..
      Data bigrt/0.0E+0_wp/, meps10/0.0E+0_wp/, tiny/0.E+0_wp/,                &
        tinyrt/0.E+0_wp/
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      ierr = 0
      If (meps10>zero) Go To 100
      bigrt = dr7mdc(5)
      meps10 = ten*dr7mdc(3)
      tinyrt = dr7mdc(2)
      tiny = dr7mdc(1)
      big = dr7mdc(6)
      If (tiny*big<one) tiny = one/big
100   singtl = real(max(n,p),kind=wp)*meps10
!
!     ***  INITIALIZE W, IPIVOT, AND DIAG(R)  ***
!
      j = 0
      Do i = 1, p
        ipivot(i) = i
        t = dv2nrm(n,q(1,i))
        If (t>zero) Go To 110
        w(i) = one
        Go To 120
110     w(i) = zero
120     j = j + i
        r(j) = t
      End Do
!
!     ***  MAIN LOOP  ***
!
      kk = 0
      nk1 = n + 1
      Do k = 1, p
        If (nk1<=1) Go To 210
        nk1 = nk1 - 1
        kk = kk + k
        kp1 = k + 1
        If (k<=nopivk) Go To 140
        If (k>=p) Go To 140
!
!        ***  FIND COLUMN WITH MINIMUM WEIGHT LOSS  ***
!
        t = w(k)
        If (t<=zero) Go To 140
        j = k
        Do i = kp1, p
          If (w(i)>=t) Go To 130
          t = w(i)
          j = i
130     End Do
        If (j==k) Go To 140
!
!             ***  INTERCHANGE COLUMNS K AND J  ***
!
        i = ipivot(k)
        ipivot(k) = ipivot(j)
        ipivot(j) = i
        w(j) = w(k)
        w(k) = t
        i = j*(j+1)/2
        t1 = r(i)
        r(i) = r(kk)
        r(kk) = t1
        Call dv7swp(n,q(1,k),q(1,j))
        If (k<=1) Go To 140
        i = i - j + 1
        j = kk - k + 1
        Call dv7swp(k-1,r(i),r(j))
!
!        ***  COLUMN K OF Q SHOULD BE NEARLY ORTHOGONAL TO THE PREVIOUS
!        ***  COLUMNS.  NORMALIZE IT, TEST FOR SINGULARITY, AND DECIDE
!        ***  WHETHER TO REORTHOGONALIZE IT.
!
140     ak = r(kk)
        If (ak<=zero) Go To 200
        wk = w(k)
!
!        *** SET T TO THE NORM OF (Q(K,K),...,Q(N,K))
!        *** AND CHECK FOR SINGULARITY.
!
        If (wk<wtol) Go To 150
        t = dv2nrm(nk1,q(k,k))
        If (t/ak<=singtl) Go To 200
        Go To 160
150     t = sqrt(one-wk)
        If (t<=singtl) Go To 200
        t = t*ak
!
!        *** DETERMINE HOUSEHOLDER TRANSFORMATION ***
!
160     qkk = q(k,k)
        If (t<=tinyrt) Go To 170
        If (t>=bigrt) Go To 170
        If (qkk<zero) t = -t
        qkk = qkk + t
        s = sqrt(t*qkk)
        Go To 190
170     s = sqrt(t)
        If (qkk<zero) Go To 180
        qkk = qkk + t
        s = s*sqrt(qkk)
        Go To 190
180     t = -t
        qkk = qkk + t
        s = s*sqrt(-qkk)
190     q(k,k) = qkk
!
!         ***  SCALE (Q(K,K),...,Q(N,K)) TO HAVE NORM SQRT(2)  ***
!
        If (s<=tiny) Go To 200
        Call dv7scl(nk1,q(k,k),one/s,q(k,k))
!
        r(kk) = -t
!
!        ***  COMPUTE R(K,I) FOR I = K+1,...,P AND UPDATE Q  ***
!
        If (k>=p) Go To 210
        j = kk + k
        ii = kk
        Do i = kp1, p
          ii = ii + i
          Call dv2axy(nk1,q(k,i),-dd7tpr(nk1,q(k,k),q(k,i)),q(k,k),q(k,i))
          t = q(k,i)
          r(j) = t
          j = j + i
          t1 = r(ii)
          If (t1>zero) w(i) = w(i) + (t/t1)**2
        End Do
      End Do
!
!     ***  SINGULAR Q  ***
!
200   ierr = k
      km1 = k - 1
      j = kk
      Do i = k, p
        Call dv7scp(i-km1,r(j),zero)
        j = j + i
      End Do
!
210   Return
!     ***  LAST CARD OF DQ7RFH FOLLOWS  ***
    End Subroutine dq7rfh
    Subroutine df7dhb(b,d,g,irt,iv,liv,lv,p,v,x)
!
!     ***  COMPUTE FINITE-DIFFERENCE HESSIAN, STORE IT IN V STARTING
!     ***  AT V(IV(FDH)) = V(-IV(H)).  HONOR SIMPLE BOUNDS IN B.
!
!     ***  IF IV(COVREQ) .GE. 0 THEN DF7DHB USES GRADIENT DIFFERENCES,
!     ***  OTHERWISE FUNCTION DIFFERENCES.  STORAGE IN V IS AS IN DG7LIT.
!
!     IRT VALUES...
!     1 = COMPUTE FUNCTION VALUE, I.E., V(F).
!     2 = COMPUTE G.
!     3 = DONE.
!
!
!     ***  PARAMETER DECLARATIONS  ***
!
!
!     ***  LOCAL VARIABLES  ***
!
!
!     ***  EXTERNAL SUBROUTINES  ***
!
!
!     DV7CPY.... COPY ONE VECTOR TO ANOTHER.
!     DV7SCP... COPY SCALAR TO ALL COMPONENTS OF A VECTOR.
!
!     ***  SUBSCRIPTS FOR IV AND V  ***
!
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Parameters ..
      Real (Kind=wp), Parameter        :: half = 0.5E+0_wp
      Real (Kind=wp), Parameter        :: hlim = 0.1E+0_wp
      Real (Kind=wp), Parameter        :: one = 1.E+0_wp
      Real (Kind=wp), Parameter        :: two = 2.E+0_wp
      Real (Kind=wp), Parameter        :: zero = 0.E+0_wp
      Integer, Parameter               :: covreq = 15, delta = 52,             &
                                          delta0 = 44, dltfdc = 42, f = 10,    &
                                          fdh = 74, fx = 53, h = 56,           &
                                          kagqt = 33, mode = 35, nfgcal = 7,   &
                                          savei = 63, switch = 12, toobig = 2, &
                                          w = 65, xmsave = 51
!     .. Scalar Arguments ..
      Integer                          :: irt, liv, lv, p
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(2,p), d(p), g(p), v(lv), x(p)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: del, del0, t, xm, xm1
      Integer                          :: gsave1, hes, hmi, hpi, hpm, i,       &
                                          ikind, k, l, m, mm1, mm1o2, newm1,   &
                                          pp1o2, stp0, stpi, stpm
      Logical                          :: offsid
!     .. External Procedures ..
      External                         :: dv7cpy, dv7scp
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max
!     .. Executable Statements ..
!
!     +++++++++++++++++++++++++++++++  BODY  ++++++++++++++++++++++++++++++++
!
      irt = 4
      ikind = iv(covreq)
      m = iv(mode)
      If (m>0) Go To 100
      hes = abs(iv(h))
      iv(h) = -hes
      iv(fdh) = 0
      iv(kagqt) = -1
      v(fx) = v(f)
!        *** SUPPLY ZEROS IN CASE B(1,I) = B(2,I) FOR SOME I ***
      Call dv7scp(p*(p+1)/2,v(hes),zero)
100   If (m>p) Go To 410
      If (ikind<0) Go To 180
!
!     ***  COMPUTE FINITE-DIFFERENCE HESSIAN USING BOTH FUNCTION AND
!     ***  GRADIENT VALUES.
!
      gsave1 = iv(w) + p
      If (m>0) Go To 110
!        ***  FIRST CALL ON DF7DHB.  SET GSAVE = G, TAKE FIRST STEP  ***
      Call dv7cpy(p,v(gsave1),g)
      iv(switch) = iv(nfgcal)
      Go To 140
!
110   del = v(delta)
      x(m) = v(xmsave)
      If (iv(toobig)==0) Go To 120
!
!     ***  HANDLE OVERSIZE V(DELTA)  ***
!
      del0 = v(delta0)*max(one/d(m),abs(x(m)))
      del = half*del
      If (abs(del/del0)<=hlim) Go To 200
!
120   hes = -iv(h)
!
!     ***  SET  G = (G - GSAVE)/DEL  ***
!
      del = one/del
      Do i = 1, p
        g(i) = del*(g(i)-v(gsave1))
        gsave1 = gsave1 + 1
      End Do
!
!     ***  ADD G AS NEW COL. TO FINITE-DIFF. HESSIAN MATRIX  ***
!
      k = hes + m*(m-1)/2
      l = k + m - 2
      If (m==1) Go To 130
!
!     ***  SET  H(I,M) = 0.5 * (H(I,M) + G(I))  FOR I = 1 TO M-1  ***
!
      mm1 = m - 1
      Do i = 1, mm1
        If (b(1,i)<b(2,i)) v(k) = half*(v(k)+g(i))
        k = k + 1
      End Do
!
!     ***  ADD  H(I,M) = G(I)  FOR I = M TO P  ***
!
130   l = l + 1
      Do i = m, p
        If (b(1,i)<b(2,i)) v(l) = g(i)
        l = l + i
      End Do
!
140   m = m + 1
      iv(mode) = m
      If (m>p) Go To 390
      If (b(1,m)>=b(2,m)) Go To 140
!
!     ***  CHOOSE NEXT FINITE-DIFFERENCE STEP, RETURN TO GET G THERE  ***
!
      del = v(delta0)*max(one/d(m),abs(x(m)))
      xm = x(m)
      If (xm<zero) Go To 150
      xm1 = xm + del
      If (xm1<=b(2,m)) Go To 170
      xm1 = xm - del
      If (xm1>=b(1,m)) Go To 160
      Go To 330
150   xm1 = xm - del
      If (xm1>=b(1,m)) Go To 160
      xm1 = xm + del
      If (xm1<=b(2,m)) Go To 170
      Go To 330
!
160   del = -del
170   v(xmsave) = xm
      x(m) = xm1
      v(delta) = del
      irt = 2
      Go To 410
!
!     ***  COMPUTE FINITE-DIFFERENCE HESSIAN USING FUNCTION VALUES ONLY.
!
180   stp0 = iv(w) + p - 1
      mm1 = m - 1
      mm1o2 = m*mm1/2
      hes = -iv(h)
      If (m>0) Go To 190
!        ***  FIRST CALL ON DF7DHB.  ***
      iv(savei) = 0
      Go To 290
!
190   If (iv(toobig)==0) Go To 210
!        ***  PUNT IN THE EVENT OF AN OVERSIZE STEP  ***
200   iv(fdh) = -2
      Go To 400
210   i = iv(savei)
      If (i>0) Go To 240
!
!     ***  SAVE F(X + STP(M)*E(M)) IN H(P,M)  ***
!
      pp1o2 = p*(p-1)/2
      hpm = hes + pp1o2 + mm1
      v(hpm) = v(f)
!
!     ***  START COMPUTING ROW M OF THE FINITE-DIFFERENCE HESSIAN H.  ***
!
      newm1 = 1
      Go To 310
220   hmi = hes + mm1o2
      If (mm1==0) Go To 230
      hpi = hes + pp1o2
      Do i = 1, mm1
        t = zero
        If (b(1,i)<b(2,i)) t = v(fx) - (v(f)+v(hpi))
        v(hmi) = t
        hmi = hmi + 1
        hpi = hpi + 1
      End Do
230   v(hmi) = v(f) - two*v(fx)
      If (offsid) v(hmi) = v(fx) - two*v(f)
!
!     ***  COMPUTE FUNCTION VALUES NEEDED TO COMPLETE ROW M OF H.  ***
!
      i = 0
      Go To 250
!
240   x(i) = v(delta)
!
!     ***  FINISH COMPUTING H(M,I)  ***
!
      stpi = stp0 + i
      hmi = hes + mm1o2 + i - 1
      stpm = stp0 + m
      v(hmi) = (v(hmi)+v(f))/(v(stpi)*v(stpm))
250   i = i + 1
      If (i>m) Go To 280
      If (b(1,i)<b(2,i)) Go To 260
      Go To 250
!
260   iv(savei) = i
      stpi = stp0 + i
      v(delta) = x(i)
      x(i) = x(i) + v(stpi)
      irt = 1
      If (i<m) Go To 410
      newm1 = 2
      Go To 310
270   x(m) = v(xmsave) - del
      If (offsid) x(m) = v(xmsave) + two*del
      Go To 410
!
280   iv(savei) = 0
      x(m) = v(xmsave)
!
290   m = m + 1
      iv(mode) = m
      If (m>p) Go To 380
      If (b(1,m)<b(2,m)) Go To 300
      Go To 290
!
!     ***  PREPARE TO COMPUTE ROW M OF THE FINITE-DIFFERENCE HESSIAN H.
!     ***  COMPUTE M-TH STEP SIZE STP(M), THEN RETURN TO OBTAIN
!     ***  F(X + STP(M)*E(M)), WHERE E(M) = M-TH STD. UNIT VECTOR.
!
300   v(xmsave) = x(m)
      newm1 = 3
310   xm = v(xmsave)
      del = v(dltfdc)*max(one/d(m),abs(xm))
      xm1 = xm + del
      offsid = .False.
      If (xm1<=b(2,m)) Go To 320
      offsid = .True.
      xm1 = xm - del
      If (xm-two*del>=b(1,m)) Go To 350
      Go To 330
320   If (xm-del>=b(1,m)) Go To 340
      offsid = .True.
      If (xm+two*del<=b(2,m)) Go To 360
!
330   iv(fdh) = -2
      Go To 400
!
340   If (xm>=zero) Go To 360
      xm1 = xm - del
350   del = -del
360   Go To (220,270,370) newm1
370   x(m) = xm1
      stpm = stp0 + m
      v(stpm) = del
      irt = 1
      Go To 410
!
!     ***  HANDLE SPECIAL CASE OF B(1,P) = B(2,P) -- CLEAR SCRATCH VALUES
!     ***  FROM LAST ROW OF FDH...
!
380   If (b(1,p)<b(2,p)) Go To 390
      i = hes + p*(p-1)/2
      Call dv7scp(p,v(i),zero)
!
!     ***  RESTORE V(F), ETC.  ***
!
390   iv(fdh) = hes
400   v(f) = v(fx)
      irt = 3
      If (ikind<0) Go To 410
      iv(nfgcal) = iv(switch)
      gsave1 = iv(w) + p
      Call dv7cpy(p,g,v(gsave1))
      Go To 410
!
410   Return
!     ***  LAST LINE OF DF7DHB FOLLOWS  ***
    End Subroutine df7dhb
