!-----------------------------------------------------------------------
!
!   R : A Computer Language for Statistical Data Analysis
!   Copyright (C) 1998-2016 The R Core Team
!
!   This program is free software; you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation; either version 2 of the License, or
!   (at your option) any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program; if not, a copy is available at
!   https://www.R-project.org/Licenses/
!
!-----------------------------------------------------------------------

!   Called from R's smooth.spline in ../R/smspline.R  as .Fortran(C, ..)
!    and from C's

!   An interface to sbart() --- fewer arguments BUT unspecified scrtch() dimension
!
!   NB: this routine alters ws [and isetup].
!   renamed for safety
!
    Subroutine rbart(penalt,dofoff,xs,ys,ws,ssw,n,knot,nk,coef,sz,lev,crit,    &
      iparms,spar,parms,scrtch,ld4,ldnk,ier)
!     Args:
!          ^^^^^^^^ dimension (9+2*ld4+ldnk)*nk = (17 + 1)*nk [last nk never accessed]
!     Vars:

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: crit, dofoff, penalt, spar, ssw
      Integer                          :: ier, ld4, ldnk, n, nk
!     .. Array Arguments ..
      Real (Kind=wp)                   :: coef(nk), knot(nk+4), lev(n),        &
                                          parms(5), scrtch(*), sz(n), ws(n),   &
                                          xs(n), ys(n)
      Integer                          :: iparms(4)
!     .. Local Scalars ..
      Integer                          :: isetup
!     .. External Procedures ..
      External                         :: sbart
!     .. Executable Statements ..

      If (iparms(4)==1) Then ! spar is lambda
        isetup = 2
      Else
        isetup = 0
      End If
!     = icrit   spar   ispar    iter
!     = lspar   uspar    tol      eps      ratio
!     = 0|2    xwy  == X'W y
!     =   hs0	      hs1	     hs2	    hs3		==> X'W X
!     =   sg0	      sg1	     sg2	    sg3		==> SIGMA
!     =   abd [ld4 x nk]						==> R
!     =   p1ip[ld4 x nk]          p2ip [ldnk x nk]
      Call sbart(penalt,dofoff,xs,ys,ws,ssw,n,knot,nk,coef,sz,lev,crit,        &
        iparms(1),spar,iparms(2),iparms(3),parms(1),parms(2),parms(3),         &
        parms(4),parms(5),isetup,scrtch(1),scrtch(nk+1),scrtch(2*nk+1),        &
        scrtch(3*nk+1),scrtch(4*nk+1),scrtch(5*nk+1),scrtch(6*nk+1),           &
        scrtch(7*nk+1),scrtch(8*nk+1),scrtch(9*nk+1),scrtch(9*nk+ld4*nk+1),    &
        scrtch(9*nk+2*ld4*nk+1),ld4,ldnk,ier)

      Return
    End Subroutine rbart
