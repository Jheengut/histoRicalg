!
!   The authors of this software are Cleveland, Grosse, and Shyu.
!   Copyright (c) 1989, 1992 by AT&T.
!   Permission to use, copy, modify, and distribute this software for any
!   purpose without fee is hereby granted, provided that this entire notice
!   is included in all copies of any software which is or includes a copy
!   or modification of this software and in all copies of the supporting
!   documentation for such software.
!   THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED
!   WARRANTY.  IN PARTICULAR, NEITHER THE AUTHORS NOR AT&T MAKE ANY
!   REPRESENTATION OR WARRANTY OF ANY KIND CONCERNING THE MERCHANTABILITY
!   OF THIS SOFTWARE OR ITS FITNESS FOR ANY PARTICULAR PURPOSE.

!       altered by B.D. Ripley to
!
!       remove unused variables
!       make phi in ehg139 double precision to match calling sequence
!
!       Note that  ehg182(errormsg_code)  is in ./loessc.c

    Subroutine ehg126(d,n,vc,x,v,nvmax)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: d, n, nvmax, vc
!     .. Array Arguments ..
      Real (Kind=wp)                   :: v(nvmax,d), x(n,d)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: alpha, beta, mu, t
      Real (Kind=wp), Save             :: machin
      Integer, Save                    :: execnt
      Integer                          :: i, j, k
!     .. External Procedures ..
      Real (Kind=wp), External         :: d1mach
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max, min, mod, real
!     .. Data Statements ..
      Data execnt/0/
!     .. Executable Statements ..
!     MachInf -> machin
      execnt = execnt + 1
      If (execnt==1) Then
!       initialize  d1mach(2) === DBL_MAX:
        machin = d1mach(2)
      End If
!     fill in vertices for bounding box of $x$
!     lower left, upper right
      Do k = 1, d
        alpha = machin
        beta = -machin
        Do i = 1, n
          t = x(i,k)
          alpha = min(alpha,t)
          beta = max(beta,t)
        End Do
!        expand the box a little
        mu = 0.005E0_wp*max(beta-alpha,1.E-10_wp*max(abs(alpha),abs(beta))+    &
          1.E-30_wp)
        alpha = alpha - mu
        beta = beta + mu
        v(1,k) = alpha
        v(vc,k) = beta
      End Do
!     remaining vertices
      Do i = 2, vc - 1
        j = i - 1
        Do k = 1, d
          v(i,k) = v(1+mod(j,2)*(vc-1),k)
          j = real(j,kind=wp)/2.E0_wp
        End Do
      End Do
      Return
    End Subroutine ehg126

    Subroutine ehg125(p,nv,v,vhit,nvmax,d,k,t,r,s,f,l,u)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: t
      Integer                          :: d, k, nv, nvmax, p, r, s
!     .. Array Arguments ..
      Real (Kind=wp)                   :: v(nvmax,d)
      Integer                          :: f(r,0:1,s), l(r,0:1,s), u(r,0:1,s),  &
                                          vhit(nvmax)
!     .. Local Scalars ..
      Integer                          :: h, i, i3, j, m, mm
      Logical                          :: i1, i2, match
!     .. External Procedures ..
      External                         :: ehg182
!     .. Executable Statements ..
      h = nv
      Do i = 1, r
        Do j = 1, s
          h = h + 1
          Do i3 = 1, d
            v(h,i3) = v(f(i,0,j),i3)
          End Do
          v(h,k) = t
!           check for redundant vertex
          match = .False.
          m = 1
!           top of while loop
100       If (.Not. match) Then
            i1 = (m<=nv)
          Else
            i1 = .False.
          End If
          If (.Not. (i1)) Go To 130
          match = (v(m,1)==v(h,1))
          mm = 2
!              top of while loop
110       If (match) Then
            i2 = (mm<=d)
          Else
            i2 = .False.
          End If
          If (.Not. (i2)) Go To 120
          match = (v(m,mm)==v(h,mm))
          mm = mm + 1
          Go To 110
!              bottom of while loop
120       m = m + 1
          Go To 100
!           bottom of while loop
130       m = m - 1
          If (match) Then
            h = h - 1
          Else
            m = h
            If (vhit(1)>=0) Then
              vhit(m) = p
            End If
          End If
          l(i,0,j) = f(i,0,j)
          l(i,1,j) = m
          u(i,0,j) = m
          u(i,1,j) = f(i,1,j)
        End Do
      End Do
      nv = h
      If (.Not. (nv<=nvmax)) Then
        Call ehg182(180)
      End If
      Return
    End Subroutine ehg125

    Function ehg138(i,z,a,xi,lo,hi,ncmax)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Function Return Value ..
      Integer                          :: ehg138
!     .. Scalar Arguments ..
      Integer                          :: i, ncmax
!     .. Array Arguments ..
      Real (Kind=wp)                   :: xi(ncmax), z(8)
      Integer                          :: a(ncmax), hi(ncmax), lo(ncmax)
!     .. Local Scalars ..
      Integer                          :: j
      Logical                          :: i1
!     .. Executable Statements ..
!     descend tree until leaf or ambiguous
      j = i
!     top of while loop
100   If (a(j)/=0) Then
        i1 = (z(a(j))/=xi(j))
      Else
        i1 = .False.
      End If
      If (.Not. (i1)) Go To 110
      If (z(a(j))<=xi(j)) Then
        j = lo(j)
      Else
        j = hi(j)
      End If
      Go To 100
!     bottom of while loop
110   ehg138 = j
      Return
    End Function ehg138

    Subroutine ehg106(il,ir,k,nk,p,pi,n)

!     Partial sorting of p(1, il:ir) returning the sort indices pi() only
!     such that p(1, pi(k)) is correct

!     implicit none
!     Arguments
!     Input:
!     using only       p(1, pi(*))
!     Output:

!     Variables

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: il, ir, k, n, nk
!     .. Array Arguments ..
      Real (Kind=wp)                   :: p(nk,n)
      Integer                          :: pi(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t
      Integer                          :: i, ii, j, l, r
!     .. Executable Statements ..

!     find the $k$-th smallest of $n$ elements
!     Floyd+Rivest, CACM Mar '75, Algorithm 489
      l = il
      r = ir
!     while (l < r )
100   If (.Not. (l<r)) Go To 170
!        to avoid recursion, sophisticated partition deleted
!        partition $x sub {l..r}$ about $t$
      t = p(1,pi(k))
      i = l
      j = r
      ii = pi(l)
      pi(l) = pi(k)
      pi(k) = ii
      If (t<p(1,pi(r))) Then
        ii = pi(l)
        pi(l) = pi(r)
        pi(r) = ii
      End If
!        top of while loop
110   If (.Not. (i<j)) Go To 160
      ii = pi(i)
      pi(i) = pi(j)
      pi(j) = ii
      i = i + 1
      j = j - 1
!           top of while loop
120   If (.Not. (p(1,pi(i))<t)) Go To 130
      i = i + 1
      Go To 120
!           bottom of while loop
130   Continue
!           top of while loop
140   If (.Not. (t<p(1,pi(j)))) Go To 150
      j = j - 1
      Go To 140
!           bottom of while loop
150   Go To 110
!        bottom of while loop
160   If (p(1,pi(l))==t) Then
        ii = pi(l)
        pi(l) = pi(j)
        pi(j) = ii
      Else
        j = j + 1
        ii = pi(r)
        pi(r) = pi(j)
        pi(j) = ii
      End If
      If (j<=k) Then
        l = j + 1
      End If
      If (k<=j) Then
        r = j - 1
      End If
      Go To 100
!     bottom of while loop
170   Return
    End Subroutine ehg106


    Subroutine ehg127(q,n,d,nf,f,x,psi,y,rw,kernel,k,dist,eta,b,od,w,rcond,    &
      sing,sigma,u,e,dgamma,qraux,work,tol,dd,tdeg,cdeg,s)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: f, rcond, tol
      Integer                          :: d, dd, k, kernel, n, nf, od, sing,   &
                                          tdeg
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(nf,k), dgamma(15), dist(n),        &
                                          e(15,15), eta(nf), q(d), qraux(15),  &
                                          rw(n), s(0:od), sigma(15), u(15,15), &
                                          w(nf), work(15), x(n,d), y(n)
      Integer                          :: cdeg(8), psi(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: i1, i10, i2, i4, i5, i6, i7, i8,     &
                                          rho, scal
      Real (Kind=wp), Save             :: machep
      Integer                          :: column, i, i3, i9, info, inorm2, j,  &
                                          jj, jpvt
      Integer, Save                    :: execnt
!     .. Local Arrays ..
      Real (Kind=wp)                   :: colnor(15), g(15)
!     .. External Procedures ..
      Real (Kind=wp), External         :: d1mach, ddot
      Integer, External                :: idamax
      External                         :: dqrdc, dqrsl, dsvdc, ehg106, ehg182, &
                                          ehg184
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max, min, sqrt
!     .. Data Statements ..
      Data execnt/0/
!     .. Executable Statements ..
!     colnorm -> colnor
!     E -> g
!     MachEps -> machep
!     V -> e
!     X -> b
      execnt = execnt + 1
      If (execnt==1) Then
!       initialize  d1mach(4) === 1 / DBL_EPSILON === 2^52  :
        machep = d1mach(4)
      End If
!     sort by distance
      Do i3 = 1, n
        dist(i3) = 0
      End Do
      Do j = 1, dd
        i4 = q(j)
        Do i3 = 1, n
          dist(i3) = dist(i3) + (x(i3,j)-i4)**2
        End Do
      End Do
      Call ehg106(1,n,nf,1,dist,psi,n)
      rho = dist(psi(nf))*max(1.E0_wp,f)
      If (rho<=0) Then
        Call ehg182(120)
      End If
!     compute neighborhood weights
      If (kernel==2) Then
        Do i = 1, nf
          If (dist(psi(i))<rho) Then
            i1 = sqrt(rw(psi(i)))
          Else
            i1 = 0
          End If
          w(i) = i1
        End Do
      Else
        Do i3 = 1, nf
          w(i3) = sqrt(dist(psi(i3))/rho)
        End Do
        Do i3 = 1, nf
          w(i3) = sqrt(rw(psi(i3))*(1-w(i3)**3)**3)
        End Do
      End If
      If (abs(w(idamax(nf,w,1)))==0) Then
        Call ehg184('at ',q(1),dd,1)
        Call ehg184('radius ',rho,1,1)
        If (.Not. .False.) Then
          Call ehg182(121)
        End If
      End If
!     fill design matrix
      column = 1
      Do i3 = 1, nf
        b(i3,column) = w(i3)
      End Do
      If (tdeg>=1) Then
        Do j = 1, d
          If (cdeg(j)>=1) Then
            column = column + 1
            i5 = q(j)
            Do i3 = 1, nf
              b(i3,column) = w(i3)*(x(psi(i3),j)-i5)
            End Do
          End If
        End Do
      End If
      If (tdeg>=2) Then
        Do j = 1, d
          If (cdeg(j)>=1) Then
            If (cdeg(j)>=2) Then
              column = column + 1
              i6 = q(j)
              Do i3 = 1, nf
                b(i3,column) = w(i3)*(x(psi(i3),j)-i6)**2
              End Do
            End If
            Do jj = j + 1, d
              If (cdeg(jj)>=1) Then
                column = column + 1
                i7 = q(j)
                i8 = q(jj)
                Do i3 = 1, nf
                  b(i3,column) = w(i3)*(x(psi(i3),j)-i7)*(x(psi(i3),jj)-i8)
                End Do
              End If
            End Do
          End If
        End Do
        k = column
      End If
      Do i3 = 1, nf
        eta(i3) = w(i3)*y(psi(i3))
      End Do
!     equilibrate columns
      Do j = 1, k
        scal = 0
        Do inorm2 = 1, nf
          scal = scal + b(inorm2,j)**2
        End Do
        scal = sqrt(scal)
        If (0<scal) Then
          Do i3 = 1, nf
            b(i3,j) = b(i3,j)/scal
          End Do
          colnor(j) = scal
        Else
          colnor(j) = 1
        End If
      End Do
!     singular value decomposition
      Call dqrdc(b,nf,nf,k,qraux,jpvt,work,0)
      Call dqrsl(b,nf,nf,k,qraux,eta,work,eta,eta,work,work,1000,info)
      Do i9 = 1, k
        Do i3 = 1, k
          u(i3,i9) = 0
        End Do
      End Do
      Do i = 1, k
        Do j = i, k
!         FIXME: this has i = 3 vs bound 2 in a ggplot2 test
          u(i,j) = b(i,j)
        End Do
      End Do
      Call dsvdc(u,15,k,k,sigma,g,u,15,e,15,work,21,info)
      If (.Not. (info==0)) Then
        Call ehg182(182)
      End If
      tol = sigma(1)*(100*machep)
      rcond = min(rcond,sigma(k)/sigma(1))
      If (sigma(k)<=tol) Then
        sing = sing + 1
        If (sing==1) Then
          Call ehg184('pseudoinverse used at',q(1),d,1)
          Call ehg184('neighborhood radius',sqrt(rho),1,1)
          Call ehg184('reciprocal condition number ',rcond,1,1)
        Else
          If (sing==2) Then
            Call ehg184('There are other near singularities as well.',rho,1,1)
          End If
        End If
      End If
!     compensate for equilibration
      Do j = 1, k
        i10 = colnor(j)
        Do i3 = 1, k
          e(j,i3) = e(j,i3)/i10
        End Do
      End Do
!     solve least squares problem
      Do j = 1, k
        If (tol<sigma(j)) Then
          i2 = ddot(k,u(1,j),1,eta,1)/sigma(j)
        Else
          i2 = 0.E0_wp
        End If
        dgamma(j) = i2
      End Do
      Do j = 0, od
!        bug fix 2006-07-04 for k=1, od>1.   (thanks btyner@gmail.com)
        If (j<k) Then
          s(j) = ddot(k,e(j+1,1),15,dgamma,1)
        Else
          s(j) = 0.E0_wp
        End If
      End Do
      Return
    End Subroutine ehg127

    Subroutine ehg131(x,y,rw,trl,diagl,kernel,k,n,d,nc,ncmax,vc,nv,nvmax,nf,f, &
      a,c,hi,lo,pi,psi,v,vhit,vval,xi,dist,eta,b,ntol,fd,w,vval2,rcond,sing,   &
      dd,tdeg,cdeg,lq,lf,setlf)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: f, fd, rcond, trl
      Integer                          :: d, dd, k, kernel, n, nc, ncmax, nf,  &
                                          ntol, nv, nvmax, sing, tdeg, vc
      Logical                          :: setlf
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(*), diagl(n), dist(n), eta(nf),    &
                                          lf(0:d,nvmax,nf), rw(n), v(nvmax,d), &
                                          vval(0:d,nvmax), vval2(0:d,nvmax),   &
                                          w(nf), x(n,d), xi(ncmax), y(n)
      Integer                          :: a(ncmax), c(vc,ncmax), cdeg(8),      &
                                          hi(ncmax), lo(ncmax), lq(nvmax,nf),  &
                                          pi(n), psi(n), vhit(nvmax)
!     .. Local Scalars ..
      Integer                          :: i1, i2, identi, j
!     .. Local Arrays ..
      Real (Kind=wp)                   :: delta(8)
!     .. External Procedures ..
      Real (Kind=wp), External         :: dnrm2
      External                         :: ehg124, ehg126, ehg139, ehg182
!     .. Executable Statements ..
!     Identity -> identi
!     X -> b
      If (.Not. (d<=8)) Then
        Call ehg182(101)
      End If
!     build $k$-d tree
      Call ehg126(d,n,vc,x,v,nvmax)
      nv = vc
      nc = 1
      Do j = 1, vc
        c(j,nc) = j
        vhit(j) = 0
      End Do
      Do i1 = 1, d
        delta(i1) = v(vc,i1) - v(1,i1)
      End Do
      fd = fd*dnrm2(d,delta,1)
      Do identi = 1, n
        pi(identi) = identi
      End Do
      Call ehg124(1,n,d,n,nv,nc,ncmax,vc,x,pi,a,xi,lo,hi,c,v,vhit,nvmax,ntol,  &
        fd,dd)
!     smooth
      If (trl/=0) Then
        Do i2 = 1, nv
          Do i1 = 0, d
            vval2(i1,i2) = 0
          End Do
        End Do
      End If
      Call ehg139(v,nvmax,nv,n,d,nf,f,x,pi,psi,y,rw,trl,kernel,k,dist,dist,    &
        eta,b,d,w,diagl,vval2,nc,vc,a,xi,lo,hi,c,vhit,rcond,sing,dd,tdeg,cdeg, &
        lq,lf,setlf,vval)
      Return
    End Subroutine ehg131

    Subroutine ehg133(n,d,vc,nvmax,nc,ncmax,a,c,hi,lo,v,vval,xi,m,z,s)
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: d, m, n, nc, ncmax, nvmax, vc
!     .. Array Arguments ..
      Real (Kind=wp)                   :: s(m), v(nvmax,d), vval(0:d,nvmax),   &
                                          xi(ncmax), z(m,d)
      Integer                          :: a(ncmax), c(vc,ncmax), hi(ncmax),    &
                                          lo(ncmax)
!     .. Local Scalars ..
      Integer                          :: i, i1
!     .. Local Arrays ..
      Real (Kind=wp)                   :: delta(8)
!     .. External Procedures ..
      Real (Kind=wp), External         :: ehg128
!     .. Executable Statements ..

      Do i = 1, m
        Do i1 = 1, d
          delta(i1) = z(i,i1)
        End Do
        s(i) = ehg128(delta,d,ncmax,vc,a,xi,lo,hi,c,v,nvmax,vval)
      End Do
      Return
    End Subroutine ehg133

    Subroutine ehg140(iw,i,j)

!     .. Implicit None Statement ..
      Implicit None
!     .. Scalar Arguments ..
      Integer                          :: i, j
!     .. Array Arguments ..
      Integer                          :: iw(i)
!     .. Executable Statements ..
      iw(i) = j
      Return
    End Subroutine ehg140

    Subroutine ehg141(trl,n,deg,k,d,nsing,dk,delta1,delta2)

!     coef, d, deg, del

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: delta1, delta2, trl
      Integer                          :: d, deg, dk, k, n, nsing
!     .. Local Scalars ..
      Real (Kind=wp)                   :: c1, c2, c3, c4, corx, z
      Integer                          :: i
!     .. Local Arrays ..
      Real (Kind=wp), Save             :: c(48)
      Real (Kind=wp)                   :: zz(1)
!     .. External Procedures ..
      Real (Kind=wp), External         :: ehg176
      External                         :: ehg184
!     .. Intrinsic Procedures ..
      Intrinsic                        :: exp, max, min, real, sqrt
!     .. Data Statements ..
      Data c/.2971620E0_wp, .3802660E0_wp, .5886043E0_wp, .4263766E0_wp,       &
        .3346498E0_wp, .6271053E0_wp, .5241198E0_wp, .3484836E0_wp,            &
        .6687687E0_wp, .6338795E0_wp, .4076457E0_wp, .7207693E0_wp,            &
        .1611761E0_wp, .3091323E0_wp, .4401023E0_wp, .2939609E0_wp,            &
        .3580278E0_wp, .5555741E0_wp, .3972390E0_wp, .4171278E0_wp,            &
        .6293196E0_wp, .4675173E0_wp, .4699070E0_wp, .6674802E0_wp,            &
        .2848308E0_wp, .2254512E0_wp, .2914126E0_wp, .5393624E0_wp,            &
        .2517230E0_wp, .3898970E0_wp, .7603231E0_wp, .2969113E0_wp,            &
        .4740130E0_wp, .9664956E0_wp, .3629838E0_wp, .5348889E0_wp,            &
        .2075670E0_wp, .2822574E0_wp, .2369957E0_wp, .3911566E0_wp,            &
        .2981154E0_wp, .3623232E0_wp, .5508869E0_wp, .3501989E0_wp,            &
        .4371032E0_wp, .7002667E0_wp, .4291632E0_wp, .4930370E0_wp/
!     .. Executable Statements ..

      If (deg==0) dk = 1
      If (deg==1) dk = d + 1
      If (deg==2) dk = real((d+2)*(d+1),kind=wp)/2.E0_wp
      corx = sqrt(k/real(n,kind=wp))
      z = (sqrt(k/trl)-corx)/(1-corx)
      If (nsing==0 .And. 1<z) Call ehg184('Chernobyl! trL<k',trl,1,1)
      If (z<0) Call ehg184('Chernobyl! trL>n',trl,1,1)
      z = min(1.0E0_wp,max(0.0E0_wp,z))
!     R fix
      zz(1) = z
      c4 = exp(ehg176(zz))
      i = 1 + 3*(min(d,4)-1+4*(deg-1))
      If (d<=4) Then
        c1 = c(i)
        c2 = c(i+1)
        c3 = c(i+2)
      Else
        c1 = c(i) + (d-4)*(c(i)-c(i-3))
        c2 = c(i+1) + (d-4)*(c(i+1)-c(i-2))
        c3 = c(i+2) + (d-4)*(c(i+2)-c(i-1))
      End If
      delta1 = n - trl*exp(c1*z**c2*(1-z)**c3*c4)
      i = i + 24
      If (d<=4) Then
        c1 = c(i)
        c2 = c(i+1)
        c3 = c(i+2)
      Else
        c1 = c(i) + (d-4)*(c(i)-c(i-3))
        c2 = c(i+1) + (d-4)*(c(i+1)-c(i-2))
        c3 = c(i+2) + (d-4)*(c(i+2)-c(i-1))
      End If
      delta2 = n - trl*exp(c1*z**c2*(1-z)**c3*c4)
      Return
    End Subroutine ehg141

    Subroutine lowesc(n,l,ll,trl,delta1,delta2)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: delta1, delta2, trl
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: l(n,n), ll(n,n)
!     .. Local Scalars ..
      Integer                          :: i, j
!     .. External Procedures ..
      Real (Kind=wp), External         :: ddot
!     .. Executable Statements ..
!     compute $LL~=~(I-L)(I-L)'$
      Do i = 1, n
        l(i,i) = l(i,i) - 1
      End Do
      Do i = 1, n
        Do j = 1, i
          ll(i,j) = ddot(n,l(i,1),n,l(j,1),n)
        End Do
      End Do
      Do i = 1, n
        Do j = i + 1, n
          ll(i,j) = ll(j,i)
        End Do
      End Do
      Do i = 1, n
        l(i,i) = l(i,i) + 1
      End Do
!     accumulate first two traces
      trl = 0
      delta1 = 0
      Do i = 1, n
        trl = trl + l(i,i)
        delta1 = delta1 + ll(i,i)
      End Do
!     $delta sub 2 = "tr" LL sup 2$
      delta2 = 0
      Do i = 1, n
        delta2 = delta2 + ddot(n,ll(i,1),n,ll(1,i),1)
      End Do
      Return
    End Subroutine lowesc

    Subroutine ehg169(d,vc,nc,ncmax,nv,nvmax,v,a,xi,c,hi,lo)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: d, nc, ncmax, nv, nvmax, vc
!     .. Array Arguments ..
      Real (Kind=wp)                   :: v(nvmax,d), xi(ncmax)
      Integer                          :: a(ncmax), c(vc,ncmax), hi(ncmax),    &
                                          lo(ncmax)
!     .. Local Scalars ..
      Integer                          :: i, j, k, mc, mv, p
!     .. Local Arrays ..
      Integer                          :: novhit(1)
!     .. External Procedures ..
      Integer, External                :: ifloor
      External                         :: ehg125, ehg182
!     .. Intrinsic Procedures ..
      Intrinsic                        :: mod, real
!     .. Executable Statements ..

!     as in bbox
!     remaining vertices
      Do i = 2, vc - 1
        j = i - 1
        Do k = 1, d
          v(i,k) = v(1+mod(j,2)*(vc-1),k)
          j = ifloor(real(j,kind=wp)/2.E0_wp)
        End Do
      End Do
!     as in ehg131
      mc = 1
      mv = vc
      novhit(1) = -1
      Do j = 1, vc
        c(j,mc) = j
      End Do
!     as in rbuild
      p = 1
!     top of while loop
100   If (.Not. (p<=nc)) Go To 110
      If (a(p)/=0) Then
        k = a(p)
!           left son
        mc = mc + 1
        lo(p) = mc
!           right son
        mc = mc + 1
        hi(p) = mc
        Call ehg125(p,mv,v,novhit,nvmax,d,k,xi(p),2**(k-1),2**(d-k),c(1,p),    &
          c(1,lo(p)),c(1,hi(p)))
      End If
      p = p + 1
      Go To 100
!     bottom of while loop
110   If (.Not. (mc==nc)) Then
        Call ehg182(193)
      End If
      If (.Not. (mv==nv)) Then
        Call ehg182(193)
      End If
      Return
    End Subroutine ehg169

    Function ehg176(z)
!
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Function Return Value ..
      Real (Kind=wp)                   :: ehg176
!     .. Array Arguments ..
      Real (Kind=wp)                   :: z(*)
!     .. Local Scalars ..
      Integer, Save                    :: d, nc, nv, vc
!     .. Local Arrays ..
      Real (Kind=wp), Save             :: v(10,1), vval(0:1,10), xi(17)
      Integer, Save                    :: a(17), c(2,17), hi(17), lo(17)
!     .. External Procedures ..
      Real (Kind=wp), External         :: ehg128
!     .. Data Statements ..
      Data d, vc, nv, nc/1, 2, 10, 17/
      Data a(1)/1/
      Data hi(1), lo(1), xi(1)/3, 2, 0.3705E0_wp/
      Data c(1,1)/1/
      Data c(2,1)/2/
      Data a(2)/1/
      Data hi(2), lo(2), xi(2)/5, 4, 0.2017E0_wp/
      Data c(1,2)/1/
      Data c(2,2)/3/
      Data a(3)/1/
      Data hi(3), lo(3), xi(3)/7, 6, 0.5591E0_wp/
      Data c(1,3)/3/
      Data c(2,3)/2/
      Data a(4)/1/
      Data hi(4), lo(4), xi(4)/9, 8, 0.1204E0_wp/
      Data c(1,4)/1/
      Data c(2,4)/4/
      Data a(5)/1/
      Data hi(5), lo(5), xi(5)/11, 10, 0.2815E0_wp/
      Data c(1,5)/4/
      Data c(2,5)/3/
      Data a(6)/1/
      Data hi(6), lo(6), xi(6)/13, 12, 0.4536E0_wp/
      Data c(1,6)/3/
      Data c(2,6)/5/
      Data a(7)/1/
      Data hi(7), lo(7), xi(7)/15, 14, 0.7132E0_wp/
      Data c(1,7)/5/
      Data c(2,7)/2/
      Data a(8)/0/
      Data c(1,8)/1/
      Data c(2,8)/6/
      Data a(9)/0/
      Data c(1,9)/6/
      Data c(2,9)/4/
      Data a(10)/0/
      Data c(1,10)/4/
      Data c(2,10)/7/
      Data a(11)/0/
      Data c(1,11)/7/
      Data c(2,11)/3/
      Data a(12)/0/
      Data c(1,12)/3/
      Data c(2,12)/8/
      Data a(13)/0/
      Data c(1,13)/8/
      Data c(2,13)/5/
      Data a(14)/0/
      Data c(1,14)/5/
      Data c(2,14)/9/
      Data a(15)/1/
      Data hi(15), lo(15), xi(15)/17, 16, 0.8751E0_wp/
      Data c(1,15)/9/
      Data c(2,15)/2/
      Data a(16)/0/
      Data c(1,16)/9/
      Data c(2,16)/10/
      Data a(17)/0/
      Data c(1,17)/10/
      Data c(2,17)/2/
      Data vval(0,1)/ -9.0572E-2_wp/
      Data v(1,1)/ -5.E-3_wp/
      Data vval(1,1)/4.4844E0_wp/
      Data vval(0,2)/ -1.0856E-2_wp/
      Data v(2,1)/1.005E0_wp/
      Data vval(1,2)/ -0.7736E0_wp/
      Data vval(0,3)/ -5.3718E-2_wp/
      Data v(3,1)/0.3705E0_wp/
      Data vval(1,3)/ -0.3495E0_wp/
      Data vval(0,4)/2.6152E-2_wp/
      Data v(4,1)/0.2017E0_wp/
      Data vval(1,4)/ -0.7286E0_wp/
      Data vval(0,5)/ -5.8387E-2_wp/
      Data v(5,1)/0.5591E0_wp/
      Data vval(1,5)/0.1611E0_wp/
      Data vval(0,6)/9.5807E-2_wp/
      Data v(6,1)/0.1204E0_wp/
      Data vval(1,6)/ -0.7978E0_wp/
      Data vval(0,7)/ -3.1926E-2_wp/
      Data v(7,1)/0.2815E0_wp/
      Data vval(1,7)/ -0.4457E0_wp/
      Data vval(0,8)/ -6.4170E-2_wp/
      Data v(8,1)/0.4536E0_wp/
      Data vval(1,8)/3.2813E-2_wp/
      Data vval(0,9)/ -2.0636E-2_wp/
      Data v(9,1)/0.7132E0_wp/
      Data vval(1,9)/0.3350E0_wp/
      Data vval(0,10)/4.0172E-2_wp/
      Data v(10,1)/0.8751E0_wp/
      Data vval(1,10)/ -4.1032E-2_wp/
!     .. Executable Statements ..
      ehg176 = ehg128(z,d,nc,vc,a,xi,lo,hi,c,v,nv,vval)
    End Function ehg176

    Subroutine lowesa(trl,n,d,tau,nsing,delta1,delta2)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: delta1, delta2, trl
      Integer                          :: d, n, nsing, tau
!     .. Local Scalars ..
      Real (Kind=wp)                   :: alpha, d1a, d1b, d2a, d2b
      Integer                          :: dka, dkb
!     .. External Procedures ..
      External                         :: ehg141
!     .. Intrinsic Procedures ..
      Intrinsic                        :: real
!     .. Executable Statements ..

      Call ehg141(trl,n,1,tau,d,nsing,dka,d1a,d2a)
      Call ehg141(trl,n,2,tau,d,nsing,dkb,d1b,d2b)
      alpha = real(tau-dka,kind=wp)/real(dkb-dka,kind=wp)
      delta1 = (1-alpha)*d1a + alpha*d1b
      delta2 = (1-alpha)*d2a + alpha*d2b
      Return
    End Subroutine lowesa

    Subroutine ehg191(m,z,l,d,n,nf,nv,ncmax,vc,a,xi,lo,hi,c,v,nvmax,vval2,lf,  &
      lq)
!     Args
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: d, m, n, ncmax, nf, nv, nvmax, vc
!     .. Array Arguments ..
      Real (Kind=wp)                   :: l(m,n), lf(0:d,nvmax,nf),            &
                                          v(nvmax,d), vval2(0:d,nvmax),        &
                                          xi(ncmax), z(m,d)
      Integer                          :: a(ncmax), c(vc,ncmax), hi(ncmax),    &
                                          lo(ncmax), lq(nvmax,nf)
!     .. Local Scalars ..
      Integer                          :: i, i1, i2, j, lq1, p
!     .. Local Arrays ..
      Real (Kind=wp)                   :: zi(8)
!     .. External Procedures ..
      Real (Kind=wp), External         :: ehg128
!     .. Executable Statements ..

      Do j = 1, n
        Do i2 = 1, nv
          Do i1 = 0, d
            vval2(i1,i2) = 0
          End Do
        End Do
        Do i = 1, nv
!           linear search for i in Lq
          lq1 = lq(i,1)
          lq(i,1) = j
          p = nf
!           top of while loop
100       If (.Not. (lq(i,p)/=j)) Go To 110
          p = p - 1
          Go To 100
!           bottom of while loop
110       lq(i,1) = lq1
          If (lq(i,p)==j) Then
            Do i1 = 0, d
              vval2(i1,i) = lf(i1,i,p)
            End Do
          End If
        End Do
        Do i = 1, m
          Do i1 = 1, d
            zi(i1) = z(i,i1)
          End Do
          l(i,j) = ehg128(zi,d,ncmax,vc,a,xi,lo,hi,c,v,nvmax,vval2)
        End Do
      End Do
      Return
    End Subroutine ehg191

    Subroutine ehg196(tau,d,f,trl)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: f, trl
      Integer                          :: d, tau
!     .. Local Scalars ..
      Real (Kind=wp)                   :: alpha, trla, trlb
      Integer                          :: dka, dkb
!     .. External Procedures ..
      External                         :: ehg197
!     .. Intrinsic Procedures ..
      Intrinsic                        :: real
!     .. Executable Statements ..
      Call ehg197(1,tau,d,f,dka,trla)
      Call ehg197(2,tau,d,f,dkb,trlb)
      alpha = real(tau-dka,kind=wp)/real(dkb-dka,kind=wp)
      trl = (1-alpha)*trla + alpha*trlb
      Return
    End Subroutine ehg196

    Subroutine ehg197(deg,tau,d,f,dk,trl)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: f, trl
      Integer                          :: d, deg, dk, tau
!     .. Local Scalars ..
      Real (Kind=wp)                   :: g1
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max, real
!     .. Executable Statements ..
      dk = 0
      If (deg==1) dk = d + 1
      If (deg==2) dk = real((d+2)*(d+1),kind=wp)/2.E0_wp
      g1 = (-0.08125E0_wp*d+0.13E0_wp)*d + 1.05E0_wp
      trl = dk*(1+max(0.E0_wp,(g1-f)/f))
      Return
    End Subroutine ehg197

    Subroutine ehg192(y,d,n,nf,nv,nvmax,vval,lf,lq)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: d, n, nf, nv, nvmax
!     .. Array Arguments ..
      Real (Kind=wp)                   :: lf(0:d,nvmax,nf), vval(0:d,nvmax),   &
                                          y(n)
      Integer                          :: lq(nvmax,nf)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: i3
      Integer                          :: i, i1, i2, j
!     .. Executable Statements ..

      Do i2 = 1, nv
        Do i1 = 0, d
          vval(i1,i2) = 0
        End Do
      End Do
      Do i = 1, nv
        Do j = 1, nf
          i3 = y(lq(i,j))
          Do i1 = 0, d
            vval(i1,i) = vval(i1,i) + i3*lf(i1,i,j)
          End Do
        End Do
      End Do
      Return
    End Subroutine ehg192

    Function ehg128(z,d,ncmax,vc,a,xi,lo,hi,c,v,nvmax,vval)

!     implicit none
!     Args
!     Vars

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Function Return Value ..
      Real (Kind=wp)                   :: ehg128
!     .. Scalar Arguments ..
      Integer                          :: d, ncmax, nvmax, vc
!     .. Array Arguments ..
      Real (Kind=wp)                   :: v(nvmax,d), vval(0:d,nvmax),         &
                                          xi(ncmax), z(d)
      Integer                          :: a(ncmax), c(vc,ncmax), hi(ncmax),    &
                                          lo(ncmax)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: ge, gn, gpe, gpn, gps, gpw, gs, gw,  &
                                          h, phi0, phi1, psi0, psi1, s, sew,   &
                                          sns, v0, v1, xibar
      Integer                          :: i, i1, i11, i12, ig, ii, j, lg, ll,  &
                                          m, nt, ur
      Logical                          :: i10, i2, i3, i4, i5, i6, i7, i8, i9
!     .. Local Arrays ..
      Real (Kind=wp)                   :: g(0:8,256), g0(0:8), g1(0:8)
      Integer                          :: t(20)
!     .. External Procedures ..
      External                         :: ehg182, ehg184
!     .. Intrinsic Procedures ..
      Intrinsic                        :: real
!     .. Executable Statements ..
!     locate enclosing cell
      nt = 1
      t(nt) = 1
      j = 1
!     top of while loop
100   If (.Not. (a(j)/=0)) Go To 110
      nt = nt + 1
      If (z(a(j))<=xi(j)) Then
        i1 = lo(j)
      Else
        i1 = hi(j)
      End If
      t(nt) = i1
      If (.Not. (nt<20)) Then
        Call ehg182(181)
      End If
      j = t(nt)
      Go To 100
!     bottom of while loop
110   Continue
!     tensor
      Do i12 = 1, vc
        Do i11 = 0, d
          g(i11,i12) = vval(i11,c(i12,j))
        End Do
      End Do
      lg = vc
      ll = c(1,j)
      ur = c(vc,j)
      Do i = d, 1, -1
        h = (z(i)-v(ll,i))/(v(ur,i)-v(ll,i))
        If (h<-.001E0_wp) Then
          Call ehg184('eval ',z(1),d,1)
          Call ehg184('lowerlimit ',v(ll,1),d,nvmax)
        Else
          If (1.001E0_wp<h) Then
            Call ehg184('eval ',z(1),d,1)
            Call ehg184('upperlimit ',v(ur,1),d,nvmax)
          End If
        End If
        If (-.001E0_wp<=h) Then
          i2 = (h<=1.001E0_wp)
        Else
          i2 = .False.
        End If
        If (.Not. i2) Then
          Call ehg182(122)
        End If
        lg = real(lg,kind=wp)/2.E0_wp
        Do ig = 1, lg
!           Hermite basis
          phi0 = (1-h)**2*(1+2*h)
          phi1 = h**2*(3-2*h)
          psi0 = h*(1-h)**2
          psi1 = h**2*(h-1)
          g(0,ig) = phi0*g(0,ig) + phi1*g(0,ig+lg) +                           &
            (psi0*g(i,ig)+psi1*g(i,ig+lg))*(v(ur,i)-v(ll,i))
          Do ii = 1, i - 1
            g(ii,ig) = phi0*g(ii,ig) + phi1*g(ii,ig+lg)
          End Do
        End Do
      End Do
      s = g(0,1)
!     blending
      If (d==2) Then
!        ----- North -----
        v0 = v(ll,1)
        v1 = v(ur,1)
        Do i11 = 0, d
          g0(i11) = vval(i11,c(3,j))
        End Do
        Do i11 = 0, d
          g1(i11) = vval(i11,c(4,j))
        End Do
        xibar = v(ur,2)
        m = nt - 1
!        top of while loop
120     If (m==0) Then
          i4 = .True.
        Else
          If (a(t(m))==2) Then
            i3 = (xi(t(m))==xibar)
          Else
            i3 = .False.
          End If
          i4 = i3
        End If
        If (.Not. (.Not. i4)) Go To 130
        m = m - 1
!           voidp junk
        Go To 120
!        bottom of while loop
130     If (m>=1) Then
          m = hi(t(m))
!           top of while loop
140       If (.Not. (a(m)/=0)) Go To 150
          If (z(a(m))<=xi(m)) Then
            m = lo(m)
          Else
            m = hi(m)
          End If
          Go To 140
!           bottom of while loop
150       If (v0<v(c(1,m),1)) Then
            v0 = v(c(1,m),1)
            Do i11 = 0, d
              g0(i11) = vval(i11,c(1,m))
            End Do
          End If
          If (v(c(2,m),1)<v1) Then
            v1 = v(c(2,m),1)
            Do i11 = 0, d
              g1(i11) = vval(i11,c(2,m))
            End Do
          End If
        End If
        h = (z(1)-v0)/(v1-v0)
!        Hermite basis
        phi0 = (1-h)**2*(1+2*h)
        phi1 = h**2*(3-2*h)
        psi0 = h*(1-h)**2
        psi1 = h**2*(h-1)
        gn = phi0*g0(0) + phi1*g1(0) + (psi0*g0(1)+psi1*g1(1))*(v1-v0)
        gpn = phi0*g0(2) + phi1*g1(2)
!        ----- South -----
        v0 = v(ll,1)
        v1 = v(ur,1)
        Do i11 = 0, d
          g0(i11) = vval(i11,c(1,j))
        End Do
        Do i11 = 0, d
          g1(i11) = vval(i11,c(2,j))
        End Do
        xibar = v(ll,2)
        m = nt - 1
!        top of while loop
160     If (m==0) Then
          i6 = .True.
        Else
          If (a(t(m))==2) Then
            i5 = (xi(t(m))==xibar)
          Else
            i5 = .False.
          End If
          i6 = i5
        End If
        If (.Not. (.Not. i6)) Go To 170
        m = m - 1
!           voidp junk
        Go To 160
!        bottom of while loop
170     If (m>=1) Then
          m = lo(t(m))
!           top of while loop
180       If (.Not. (a(m)/=0)) Go To 190
          If (z(a(m))<=xi(m)) Then
            m = lo(m)
          Else
            m = hi(m)
          End If
          Go To 180
!           bottom of while loop
190       If (v0<v(c(3,m),1)) Then
            v0 = v(c(3,m),1)
            Do i11 = 0, d
              g0(i11) = vval(i11,c(3,m))
            End Do
          End If
          If (v(c(4,m),1)<v1) Then
            v1 = v(c(4,m),1)
            Do i11 = 0, d
              g1(i11) = vval(i11,c(4,m))
            End Do
          End If
        End If
        h = (z(1)-v0)/(v1-v0)
!        Hermite basis
        phi0 = (1-h)**2*(1+2*h)
        phi1 = h**2*(3-2*h)
        psi0 = h*(1-h)**2
        psi1 = h**2*(h-1)
        gs = phi0*g0(0) + phi1*g1(0) + (psi0*g0(1)+psi1*g1(1))*(v1-v0)
        gps = phi0*g0(2) + phi1*g1(2)
!        ----- East -----
        v0 = v(ll,2)
        v1 = v(ur,2)
        Do i11 = 0, d
          g0(i11) = vval(i11,c(2,j))
        End Do
        Do i11 = 0, d
          g1(i11) = vval(i11,c(4,j))
        End Do
        xibar = v(ur,1)
        m = nt - 1
!        top of while loop
200     If (m==0) Then
          i8 = .True.
        Else
          If (a(t(m))==1) Then
            i7 = (xi(t(m))==xibar)
          Else
            i7 = .False.
          End If
          i8 = i7
        End If
        If (.Not. (.Not. i8)) Go To 210
        m = m - 1
!           voidp junk
        Go To 200
!        bottom of while loop
210     If (m>=1) Then
          m = hi(t(m))
!           top of while loop
220       If (.Not. (a(m)/=0)) Go To 230
          If (z(a(m))<=xi(m)) Then
            m = lo(m)
          Else
            m = hi(m)
          End If
          Go To 220
!           bottom of while loop
230       If (v0<v(c(1,m),2)) Then
            v0 = v(c(1,m),2)
            Do i11 = 0, d
              g0(i11) = vval(i11,c(1,m))
            End Do
          End If
          If (v(c(3,m),2)<v1) Then
            v1 = v(c(3,m),2)
            Do i11 = 0, d
              g1(i11) = vval(i11,c(3,m))
            End Do
          End If
        End If
        h = (z(2)-v0)/(v1-v0)
!        Hermite basis
        phi0 = (1-h)**2*(1+2*h)
        phi1 = h**2*(3-2*h)
        psi0 = h*(1-h)**2
        psi1 = h**2*(h-1)
        ge = phi0*g0(0) + phi1*g1(0) + (psi0*g0(2)+psi1*g1(2))*(v1-v0)
        gpe = phi0*g0(1) + phi1*g1(1)
!        ----- West -----
        v0 = v(ll,2)
        v1 = v(ur,2)
        Do i11 = 0, d
          g0(i11) = vval(i11,c(1,j))
        End Do
        Do i11 = 0, d
          g1(i11) = vval(i11,c(3,j))
        End Do
        xibar = v(ll,1)
        m = nt - 1
!        top of while loop
240     If (m==0) Then
          i10 = .True.
        Else
          If (a(t(m))==1) Then
            i9 = (xi(t(m))==xibar)
          Else
            i9 = .False.
          End If
          i10 = i9
        End If
        If (.Not. (.Not. i10)) Go To 250
        m = m - 1
!           voidp junk
        Go To 240
!        bottom of while loop
250     If (m>=1) Then
          m = lo(t(m))
!           top of while loop
260       If (.Not. (a(m)/=0)) Go To 270
          If (z(a(m))<=xi(m)) Then
            m = lo(m)
          Else
            m = hi(m)
          End If
          Go To 260
!           bottom of while loop
270       If (v0<v(c(2,m),2)) Then
            v0 = v(c(2,m),2)
            Do i11 = 0, d
              g0(i11) = vval(i11,c(2,m))
            End Do
          End If
          If (v(c(4,m),2)<v1) Then
            v1 = v(c(4,m),2)
            Do i11 = 0, d
              g1(i11) = vval(i11,c(4,m))
            End Do
          End If
        End If
        h = (z(2)-v0)/(v1-v0)
!        Hermite basis
        phi0 = (1-h)**2*(1+2*h)
        phi1 = h**2*(3-2*h)
        psi0 = h*(1-h)**2
        psi1 = h**2*(h-1)
        gw = phi0*g0(0) + phi1*g1(0) + (psi0*g0(2)+psi1*g1(2))*(v1-v0)
        gpw = phi0*g0(1) + phi1*g1(1)
!        NS
        h = (z(2)-v(ll,2))/(v(ur,2)-v(ll,2))
!        Hermite basis
        phi0 = (1-h)**2*(1+2*h)
        phi1 = h**2*(3-2*h)
        psi0 = h*(1-h)**2
        psi1 = h**2*(h-1)
        sns = phi0*gs + phi1*gn + (psi0*gps+psi1*gpn)*(v(ur,2)-v(ll,2))
!        EW
        h = (z(1)-v(ll,1))/(v(ur,1)-v(ll,1))
!        Hermite basis
        phi0 = (1-h)**2*(1+2*h)
        phi1 = h**2*(3-2*h)
        psi0 = h*(1-h)**2
        psi1 = h**2*(h-1)
        sew = phi0*gw + phi1*ge + (psi0*gpw+psi1*gpe)*(v(ur,1)-v(ll,1))
        s = (sns+sew) - s
      End If
      ehg128 = s
      Return
    End Function ehg128

    Function ifloor(x)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Function Return Value ..
      Integer                          :: ifloor
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: x
!     .. Executable Statements ..
      ifloor = x
      If (ifloor>x) ifloor = ifloor - 1
    End Function ifloor

!   DSIGN is unused, causes conflicts on some platforms
!       DOUBLE PRECISION function DSIGN(a1,a2)
!       DOUBLE PRECISION a1, a2
!       DSIGN=DABS(a1)
!       if(a2.ge.0)DSIGN=-DSIGN
!       end


!   ehg136()  is the workhorse of lowesf(.)
!     n = number of observations
!     m = number of x values at which to evaluate
!     f = span
!     nf = min(n, floor(f * n))
    Subroutine ehg136(u,lm,m,n,d,nf,f,x,psi,y,rw,kernel,k,dist,eta,b,od,o,     &
      ihat,w,rcond,sing,dd,tdeg,cdeg,s)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: f, rcond
      Integer                          :: d, dd, ihat, k, kernel, lm, m, n,    &
                                          nf, od, sing, tdeg
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(nf,k), dist(n), eta(nf), o(m,n),   &
                                          rw(n), s(0:od,m), u(lm,d), w(nf),    &
                                          x(n,d), y(n)
      Integer                          :: cdeg(8), psi(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: i2, scale, tol
      Integer                          :: i, i1, identi, info, j, l
!     .. Local Arrays ..
      Real (Kind=wp)                   :: dgamma(15), e(15,15), g(15,15),      &
                                          q(8), qraux(15), sigma(15), work(15)
!     .. External Procedures ..
      Real (Kind=wp), External         :: ddot
      External                         :: dqrsl, ehg127, ehg182
!     .. Executable Statements ..

!     V -> g
!     U -> e
!     Identity -> identi
!     L -> o
!     X -> b
      If (k>nf-1) Call ehg182(104)
      If (k>15) Call ehg182(105)
      Do identi = 1, n
        psi(identi) = identi
      End Do
      Do l = 1, m
        Do i1 = 1, d
          q(i1) = u(l,i1)
        End Do
        Call ehg127(q,n,d,nf,f,x,psi,y,rw,kernel,k,dist,eta,b,od,w,rcond,sing, &
          sigma,e,g,dgamma,qraux,work,tol,dd,tdeg,cdeg,s(0,l))
        If (ihat==1) Then
!           $L sub {l,l} =
!           V sub {1,:} SIGMA sup {+} U sup T
!           (Q sup T W e sub i )$
          If (.Not. (m==n)) Then
            Call ehg182(123)
          End If
!           find $i$ such that $l = psi sub i$
          i = 1
!           top of while loop
100       If (.Not. (l/=psi(i))) Go To 110
          i = i + 1
          If (.Not. (i<nf)) Then
            Call ehg182(123)
!           next line is not in current dloess
            Go To 110
          End If
          Go To 100
!           bottom of while loop
110       Do i1 = 1, nf
            eta(i1) = 0
          End Do
          eta(i) = w(i)
!           $eta = Q sup T W e sub i$
          Call dqrsl(b,nf,nf,k,qraux,eta,eta,eta,eta,eta,eta,1000,info)
!           $gamma = U sup T eta sub {1:k}$
          Do i1 = 1, k
            dgamma(i1) = 0
          End Do
          Do j = 1, k
            i2 = eta(j)
            Do i1 = 1, k
              dgamma(i1) = dgamma(i1) + i2*e(j,i1)
            End Do
          End Do
!           $gamma = SIGMA sup {+} gamma$
          Do j = 1, k
            If (tol<sigma(j)) Then
              dgamma(j) = dgamma(j)/sigma(j)
            Else
              dgamma(j) = 0.E0_wp
            End If
          End Do
!           voidp junk
!           voidp junk
          o(l,1) = ddot(k,g(1,1),15,dgamma,1)
        Else
          If (ihat==2) Then
!              $L sub {l,:} =
!              V sub {1,:} SIGMA sup {+}
!              ( U sup T Q sup T ) W $
            Do i1 = 1, n
              o(l,i1) = 0
            End Do
            Do j = 1, k
              Do i1 = 1, nf
                eta(i1) = 0
              End Do
              Do i1 = 1, k
                eta(i1) = e(i1,j)
              End Do
              Call dqrsl(b,nf,nf,k,qraux,eta,eta,work,work,work,work,10000,    &
                info)
              If (tol<sigma(j)) Then
                scale = 1.E0_wp/sigma(j)
              Else
                scale = 0.E0_wp
              End If
              Do i1 = 1, nf
                eta(i1) = eta(i1)*(scale*w(i1))
              End Do
              Do i = 1, nf
                o(l,psi(i)) = o(l,psi(i)) + g(1,j)*eta(i)
              End Do
            End Do
          End If
        End If
      End Do
      Return
    End Subroutine ehg136

!   called from lowesb() ... compute fit ..?..?...
!   somewhat similar to ehg136
    Subroutine ehg139(v,nvmax,nv,n,d,nf,f,x,pi,psi,y,rw,trl,kernel,k,dist,phi, &
      eta,b,od,w,diagl,vval2,ncmax,vc,a,xi,lo,hi,c,vhit,rcond,sing,dd,tdeg,    &
      cdeg,lq,lf,setlf,s)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: f, rcond, trl
      Integer                          :: d, dd, k, kernel, n, ncmax, nf, nv,  &
                                          nvmax, od, sing, tdeg, vc
      Logical                          :: setlf
!     .. Array Arguments ..
      Real (Kind=wp)                   :: b(nf,k), diagl(n), dist(n), eta(nf), &
                                          lf(0:d,nvmax,nf), phi(n), rw(n),     &
                                          s(0:od,nv), v(nvmax,d),              &
                                          vval2(0:d,nv), w(nf), x(n,d),        &
                                          xi(ncmax), y(n)
      Integer                          :: a(ncmax), c(vc,ncmax), cdeg(8),      &
                                          hi(ncmax), lo(ncmax), lq(nvmax,nf),  &
                                          pi(n), psi(n), vhit(nvmax)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: i1, i4, i7, scale, term, tol
      Integer                          :: i, i2, i3, i5, i6, identi, ii,       &
                                          ileaf, info, j, l, nleaf
!     .. Local Arrays ..
      Real (Kind=wp)                   :: dgamma(15), e(15,15), q(8),          &
                                          qraux(15), sigma(15), u(15,15),      &
                                          work(15), z(8)
      Integer                          :: leaf(256)
!     .. External Procedures ..
      Real (Kind=wp), External         :: ddot, ehg128
      External                         :: dqrsl, ehg127, ehg137, ehg182
!     .. Executable Statements ..

!     V -> e
!     Identity -> identi
!     X -> b
!     l2fit with trace(L)
      If (k>nf-1) Call ehg182(104)
      If (k>15) Call ehg182(105)
      If (trl/=0) Then
        Do i5 = 1, n
          diagl(i5) = 0
        End Do
        Do i6 = 1, nv
          Do i5 = 0, d
            vval2(i5,i6) = 0
          End Do
        End Do
      End If
      Do identi = 1, n
        psi(identi) = identi
      End Do
      Do l = 1, nv
        Do i5 = 1, d
          q(i5) = v(l,i5)
        End Do
        Call ehg127(q,n,d,nf,f,x,psi,y,rw,kernel,k,dist,eta,b,od,w,rcond,sing, &
          sigma,u,e,dgamma,qraux,work,tol,dd,tdeg,cdeg,s(0,l))
        If (trl/=0) Then
!           invert $psi$
          Do i5 = 1, n
            phi(i5) = 0
          End Do
          Do i = 1, nf
            phi(psi(i)) = i
          End Do
          Do i5 = 1, d
            z(i5) = v(l,i5)
          End Do
          Call ehg137(z,vhit(l),leaf,nleaf,d,nv,nvmax,ncmax,a,xi,lo,hi)
          Do ileaf = 1, nleaf
            Do ii = lo(leaf(ileaf)), hi(leaf(ileaf))
              i = phi(pi(ii))
              If (i/=0) Then
                If (.Not. (psi(i)==pi(ii))) Then
                  Call ehg182(194)
                End If
                Do i5 = 1, nf
                  eta(i5) = 0
                End Do
                eta(i) = w(i)
!                    $eta = Q sup T W e sub i$
                Call dqrsl(b,nf,nf,k,qraux,eta,work,eta,eta,work,work,1000,    &
                  info)
                Do j = 1, k
                  If (tol<sigma(j)) Then
                    i4 = ddot(k,u(1,j),1,eta,1)/sigma(j)
                  Else
                    i4 = 0.E0_wp
                  End If
                  dgamma(j) = i4
                End Do
                Do j = 1, d + 1
!                       bug fix 2006-07-15 for k=1, od>1.   (thanks btyner@gmail.com)
                  If (j<=k) Then
                    vval2(j-1,l) = ddot(k,e(j,1),15,dgamma,1)
                  Else
                    vval2(j-1,l) = 0.0E0_wp
                  End If
                End Do
                Do i5 = 1, d
                  z(i5) = x(pi(ii),i5)
                End Do
                term = ehg128(z,d,ncmax,vc,a,xi,lo,hi,c,v,nvmax,vval2)
                diagl(pi(ii)) = diagl(pi(ii)) + term
                Do i5 = 0, d
                  vval2(i5,l) = 0
                End Do
              End If
            End Do
          End Do
        End If
        If (setlf) Then
!           $Lf sub {:,l,:} = V SIGMA sup {+} U sup T Q sup T W$
          If (.Not. (k>=d+1)) Then
            Call ehg182(196)
          End If
          Do i5 = 1, nf
            lq(l,i5) = psi(i5)
          End Do
          Do i6 = 1, nf
            Do i5 = 0, d
              lf(i5,l,i6) = 0
            End Do
          End Do
          Do j = 1, k
            Do i5 = 1, nf
              eta(i5) = 0
            End Do
            Do i5 = 1, k
              eta(i5) = u(i5,j)
            End Do
            Call dqrsl(b,nf,nf,k,qraux,eta,eta,work,work,work,work,10000,info)
            If (tol<sigma(j)) Then
              scale = 1.E0_wp/sigma(j)
            Else
              scale = 0.E0_wp
            End If
            Do i5 = 1, nf
              eta(i5) = eta(i5)*(scale*w(i5))
            End Do
            Do i = 1, nf
              i7 = eta(i)
              Do i5 = 0, d
                If (i5<k) Then
                  lf(i5,l,i) = lf(i5,l,i) + e(1+i5,j)*i7
                Else
                  lf(i5,l,i) = 0
                End If
              End Do
            End Do
          End Do
        End If
      End Do
      If (trl/=0) Then
        If (n<=0) Then
          trl = 0.E0_wp
        Else
          i3 = n
          i1 = diagl(i3)
          Do i2 = i3 - 1, 1, -1
            i1 = diagl(i2) + i1
          End Do
          trl = i1
        End If
      End If
      Return
    End Subroutine ehg139

    Subroutine lowesb(xx,yy,ww,diagl,infl,iv,liv,lv,wv)
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: liv, lv
      Logical                          :: infl
!     .. Array Arguments ..
      Real (Kind=wp)                   :: diagl(*), wv(*), ww(*), xx(*), yy(*)
      Integer                          :: iv(*)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: trl
      Logical                          :: setlf
!     .. External Procedures ..
      Integer, External                :: ifloor
      External                         :: ehg131, ehg182, ehg183
!     .. Intrinsic Procedures ..
      Intrinsic                        :: real
!     .. Executable Statements ..

      If (.Not. (iv(28)/=173)) Then
        Call ehg182(174)
      End If
      If (iv(28)/=172) Then
        If (.Not. (iv(28)==171)) Then
          Call ehg182(171)
        End If
      End If
      iv(28) = 173
      If (infl) Then
        trl = 1.E0_wp
      Else
        trl = 0.E0_wp
      End If
      setlf = (iv(27)/=iv(25))
      Call ehg131(xx,yy,ww,trl,diagl,iv(20),iv(29),iv(3),iv(2),iv(5),iv(17),   &
        iv(4),iv(6),iv(14),iv(19),wv(1),iv(iv(7)),iv(iv(8)),iv(iv(9)),iv(iv(   &
        10)),iv(iv(22)),iv(iv(27)),wv(iv(11)),iv(iv(23)),wv(iv(13)),wv(iv(     &
        12)),wv(iv(15)),wv(iv(16)),wv(iv(18)),ifloor(iv(3)*wv(2)),wv(3),wv(iv( &
        26)),wv(iv(24)),wv(4),iv(30),iv(33),iv(32),iv(41),iv(iv(25)),wv(iv(    &
        34)),setlf)
      If (iv(14)<iv(6)+real(iv(4),kind=wp)/2.E0_wp) Then
        Call ehg183('k-d tree limited by memory; nvmax=',iv(14),1,1)
      Else
        If (iv(17)<iv(5)+2) Then
          Call ehg183('k-d tree limited by memory. ncmax=',iv(17),1,1)
        End If
      End If
      Return
    End Subroutine lowesb

!   lowesd() : Initialize iv(*) and v(1:4)
!   ------     called only by loess_workspace()  in ./loessc.c
    Subroutine lowesd(versio,iv,liv,lv,v,d,n,f,ideg,nvmax,setlf)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: f
      Integer                          :: d, ideg, liv, lv, n, nvmax, versio
      Logical                          :: setlf
!     .. Array Arguments ..
      Real (Kind=wp)                   :: v(lv)
      Integer                          :: iv(liv)
!     .. Local Scalars ..
      Integer                          :: bound, i, i1, i2, j, ncmax, nf, vc
!     .. External Procedures ..
      Integer, External                :: ifloor
      External                         :: ehg182
!     .. Intrinsic Procedures ..
      Intrinsic                        :: min, real
!     .. Executable Statements ..
!
!     unnecessary initialization of i1 to keep g77 -Wall happy
!
      i1 = 0
!     version -> versio
      If (.Not. (versio==106)) Then
        Call ehg182(100)
      End If
      iv(28) = 171
      iv(2) = d
      iv(3) = n
      vc = 2**d
      iv(4) = vc
      If (.Not. (0<f)) Then
        Call ehg182(120)
      End If
      nf = min(n,ifloor(n*f))
      iv(19) = nf
      iv(20) = 1
      If (ideg==0) Then
        i1 = 1
      Else
        If (ideg==1) Then
          i1 = d + 1
        Else
          If (ideg==2) Then
            i1 = real((d+2)*(d+1),kind=wp)/2.E0_wp
          End If
        End If
      End If
      iv(29) = i1
      iv(21) = 1
      iv(14) = nvmax
      ncmax = nvmax
      iv(17) = ncmax
      iv(30) = 0
      iv(32) = ideg
      If (.Not. (ideg>=0)) Then
        Call ehg182(195)
      End If
      If (.Not. (ideg<=2)) Then
        Call ehg182(195)
      End If
      iv(33) = d
      Do i2 = 41, 49
        iv(i2) = ideg
      End Do
      iv(7) = 50
      iv(8) = iv(7) + ncmax
      iv(9) = iv(8) + vc*ncmax
      iv(10) = iv(9) + ncmax
      iv(22) = iv(10) + ncmax
!     initialize permutation
      j = iv(22) - 1
      Do i = 1, n
        iv(j+i) = i
      End Do
      iv(23) = iv(22) + n
      iv(25) = iv(23) + nvmax
      If (setlf) Then
        iv(27) = iv(25) + nvmax*nf
      Else
        iv(27) = iv(25)
      End If
      bound = iv(27) + n
      If (.Not. (bound-1<=liv)) Then
        Call ehg182(102)
      End If
      iv(11) = 50
      iv(13) = iv(11) + nvmax*d
      iv(12) = iv(13) + (d+1)*nvmax
      iv(15) = iv(12) + ncmax
      iv(16) = iv(15) + n
      iv(18) = iv(16) + nf
      iv(24) = iv(18) + iv(29)*nf
      iv(34) = iv(24) + (d+1)*nvmax
      If (setlf) Then
        iv(26) = iv(34) + (d+1)*nvmax*nf
      Else
        iv(26) = iv(34)
      End If
      bound = iv(26) + nf
      If (.Not. (bound-1<=lv)) Then
        Call ehg182(103)
      End If
      v(1) = f
      v(2) = 0.05E0_wp
      v(3) = 0.E0_wp
      v(4) = 1.E0_wp
      Return
    End Subroutine lowesd

    Subroutine lowese(iv,liv,lv,wv,m,z,s)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: liv, lv, m
!     .. Array Arguments ..
      Real (Kind=wp)                   :: s(m), wv(*), z(m,1)
      Integer                          :: iv(*)
!     .. External Procedures ..
      External                         :: ehg133, ehg182
!     .. Executable Statements ..

      If (.Not. (iv(28)/=172)) Then
        Call ehg182(172)
      End If
      If (.Not. (iv(28)==173)) Then
        Call ehg182(173)
      End If
      Call ehg133(iv(3),iv(2),iv(4),iv(14),iv(5),iv(17),iv(iv(7)),iv(iv(8)),   &
        iv(iv(9)),iv(iv(10)),wv(iv(11)),wv(iv(13)),wv(iv(12)),m,z,s)
      Return
    End Subroutine lowese

!   "direct" (non-"interpolate") fit aka predict() :
    Subroutine lowesf(xx,yy,ww,iv,liv,lv,wv,m,z,l,ihat,s)
!     m = number of x values at which to evaluate

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: ihat, liv, lv, m
!     .. Array Arguments ..
      Real (Kind=wp)                   :: l(m,*), s(m), wv(*), ww(*), xx(*),   &
                                          yy(*), z(m,1)
      Integer                          :: iv(*)
!     .. Local Scalars ..
      Logical                          :: i1
!     .. External Procedures ..
      External                         :: ehg136, ehg182
!     .. Executable Statements ..
      If (171<=iv(28)) Then
        i1 = (iv(28)<=174)
      Else
        i1 = .False.
      End If
      If (.Not. i1) Then
        Call ehg182(171)
      End If
      iv(28) = 172
      If (.Not. (iv(14)>=iv(19))) Then
        Call ehg182(186)
      End If

!     do the work; in ehg136()  give the argument names as they are there:
!          ehg136(u,lm,m, n,    d,    nf,   f,   x,   psi,     y ,rw,
!     kernel,  k,     dist,       eta,       b,     od,o,ihat,
!     w,     rcond,sing,    dd,    tdeg,cdeg,  s)
      Call ehg136(z,m,m,iv(3),iv(2),iv(19),wv(1),xx,iv(iv(22)),yy,ww,iv(20),   &
        iv(29),wv(iv(15)),wv(iv(16)),wv(iv(18)),0,l,ihat,wv(iv(26)),wv(4),     &
        iv(30),iv(33),iv(32),iv(41),s)
      Return
    End Subroutine lowesf

    Subroutine lowesl(iv,liv,lv,wv,m,z,l)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: liv, lv, m
!     .. Array Arguments ..
      Real (Kind=wp)                   :: l(m,*), wv(*), z(m,1)
      Integer                          :: iv(*)
!     .. External Procedures ..
      External                         :: ehg182, ehg191
!     .. Executable Statements ..

      If (.Not. (iv(28)/=172)) Then
        Call ehg182(172)
      End If
      If (.Not. (iv(28)==173)) Then
        Call ehg182(173)
      End If
      If (.Not. (iv(26)/=iv(34))) Then
        Call ehg182(175)
      End If
      Call ehg191(m,z,l,iv(2),iv(3),iv(19),iv(6),iv(17),iv(4),iv(iv(7)),wv(iv( &
        12)),iv(iv(10)),iv(iv(9)),iv(iv(8)),wv(iv(11)),iv(14),wv(iv(24)),      &
        wv(iv(34)),iv(iv(25)))
      Return
    End Subroutine lowesl

    Subroutine lowesr(yy,iv,liv,lv,wv)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: liv, lv
!     .. Array Arguments ..
      Real (Kind=wp)                   :: wv(*), yy(*)
      Integer                          :: iv(*)
!     .. External Procedures ..
      External                         :: ehg182, ehg192
!     .. Executable Statements ..
      If (.Not. (iv(28)/=172)) Then
        Call ehg182(172)
      End If
      If (.Not. (iv(28)==173)) Then
        Call ehg182(173)
      End If
      Call ehg192(yy,iv(2),iv(3),iv(19),iv(6),iv(14),wv(iv(13)),wv(iv(34)),    &
        iv(iv(25)))
      Return
    End Subroutine lowesr

    Subroutine lowesw(res,n,rw,pi)
!     Tranliterated from Devlin's ratfor

!     implicit none
!     Args
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: res(n), rw(n)
      Integer                          :: pi(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: cmad, rsmall
      Integer                          :: i, i1, identi, nh
!     .. External Procedures ..
      Real (Kind=wp), External         :: d1mach
      Integer, External                :: ifloor
      External                         :: ehg106
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, real
!     .. Executable Statements ..

!     Identity -> identi

!     find median of absolute residuals
      Do i1 = 1, n
        rw(i1) = abs(res(i1))
      End Do
      Do identi = 1, n
        pi(identi) = identi
      End Do
      nh = ifloor(real(n,kind=wp)/2.E0_wp) + 1
!     partial sort to find 6*mad
      Call ehg106(1,n,nh,1,rw,pi,n)
      If ((n-nh)+1<nh) Then
        Call ehg106(1,nh-1,nh-1,1,rw,pi,n)
        cmad = 3*(rw(pi(nh))+rw(pi(nh-1)))
      Else
        cmad = 6*rw(pi(nh))
      End If
      rsmall = d1mach(1)
      If (cmad<rsmall) Then
        Do i1 = 1, n
          rw(i1) = 1
        End Do
      Else
        Do i = 1, n
          If (cmad*0.999E0_wp<rw(i)) Then
            rw(i) = 0
          Else
            If (cmad*0.001E0_wp<rw(i)) Then
              rw(i) = (1-(rw(i)/cmad)**2)**2
            Else
              rw(i) = 1
            End If
          End If
        End Do
      End If
      Return
    End Subroutine lowesw

    Subroutine lowesp(n,y,yhat,pwgts,rwgts,pi,ytilde)
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: pwgts(n), rwgts(n), y(n), yhat(n),   &
                                          ytilde(n)
      Integer                          :: pi(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: c, i1, i4, mad
      Integer                          :: i, i2, i3, m
!     .. External Procedures ..
      Integer, External                :: ifloor
      External                         :: ehg106
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, real, sqrt
!     .. Executable Statements ..
!     median absolute deviation (using partial sort):
      Do i = 1, n
        ytilde(i) = abs(y(i)-yhat(i))*sqrt(pwgts(i))
        pi(i) = i
      End Do
      m = ifloor(real(n,kind=wp)/2.E0_wp) + 1
      Call ehg106(1,n,m,1,ytilde,pi,n)
      If ((n-m)+1<m) Then
        Call ehg106(1,m-1,m-1,1,ytilde,pi,n)
        mad = (ytilde(pi(m-1))+ytilde(pi(m)))/2
      Else
        mad = ytilde(pi(m))
      End If
!     magic constant
      c = (6*mad)**2/5
      Do i = 1, n
        ytilde(i) = 1 - ((y(i)-yhat(i))**2*pwgts(i))/c
      End Do
      Do i = 1, n
        ytilde(i) = ytilde(i)*sqrt(rwgts(i))
      End Do
      If (n<=0) Then
        i4 = 0.E0_wp
      Else
        i3 = n
        i1 = ytilde(i3)
        Do i2 = i3 - 1, 1, -1
          i1 = ytilde(i2) + i1
        End Do
        i4 = i1
      End If
      c = n/i4
!     pseudovalues
      Do i = 1, n
        ytilde(i) = yhat(i) + (c*rwgts(i))*(y(i)-yhat(i))
      End Do
      Return
    End Subroutine lowesp

    Subroutine ehg124(ll,uu,d,n,nv,nc,ncmax,vc,x,pi,a,xi,lo,hi,c,v,vhit,nvmax, &
      fc,fd,dd)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: fd
      Integer                          :: d, dd, fc, ll, n, nc, ncmax, nv,     &
                                          nvmax, uu, vc
!     .. Array Arguments ..
      Real (Kind=wp)                   :: v(nvmax,d), x(n,d), xi(ncmax)
      Integer                          :: a(ncmax), c(vc,ncmax), hi(ncmax),    &
                                          lo(ncmax), pi(n), vhit(nvmax)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: diam
      Integer                          :: check, i4, inorm2, k, l, lower, m,   &
                                          offset, p, u, upper
      Logical                          :: i1, i2, leaf
!     .. Local Arrays ..
      Real (Kind=wp)                   :: diag(8), sigma(8)
!     .. External Procedures ..
      Integer, External                :: idamax
      External                         :: ehg106, ehg125, ehg129
!     .. Intrinsic Procedures ..
      Intrinsic                        :: real, sqrt
!     .. Executable Statements ..
      p = 1
      l = ll
      u = uu
      lo(p) = l
      hi(p) = u
!     top of while loop
100   If (.Not. (p<=nc)) Go To 130
      Do i4 = 1, dd
        diag(i4) = v(c(vc,p),i4) - v(c(1,p),i4)
      End Do
      diam = 0
      Do inorm2 = 1, dd
        diam = diam + diag(inorm2)**2
      End Do
      diam = sqrt(diam)
      If ((u-l)+1<=fc) Then
        i1 = .True.
      Else
        i1 = (diam<=fd)
      End If
      If (i1) Then
        leaf = .True.
      Else
        If (ncmax<nc+2) Then
          i2 = .True.
        Else
          i2 = (nvmax<nv+real(vc,kind=wp)/2.E0_wp)
        End If
        leaf = i2
      End If
      If (.Not. leaf) Then
        Call ehg129(l,u,dd,x,pi,n,sigma)
        k = idamax(dd,sigma,1)
        m = real(l+u,kind=wp)/2.E0_wp
        Call ehg106(l,u,m,1,x(1,k),pi,n)

!           all ties go with hi son
!           top of while loop
!       bug fix from btyner@gmail.com 2006-07-20
        offset = 0
110     If (((m+offset)>=u) .Or. ((m+offset)<l)) Go To 120
        If (offset<0) Then
          lower = l
          check = m + offset
          upper = check
        Else
          lower = m + offset + 1
          check = lower
          upper = u
        End If
        Call ehg106(lower,upper,check,1,x(1,k),pi,n)
        If (x(pi(m+offset),k)==x(pi(m+offset+1),k)) Then
          offset = -offset
          If (offset>=0) Then
            offset = offset + 1
          End If
          Go To 110
        Else
          m = m + offset
          Go To 120
        End If

!           bottom of while loop
120     If (v(c(1,p),k)==x(pi(m),k)) Then
          leaf = .True.
        Else
          leaf = (v(c(vc,p),k)==x(pi(m),k))
        End If
      End If
      If (leaf) Then
        a(p) = 0
      Else
        a(p) = k
        xi(p) = x(pi(m),k)
!           left son
        nc = nc + 1
        lo(p) = nc
        lo(nc) = l
        hi(nc) = m
!           right son
        nc = nc + 1
        hi(p) = nc
        lo(nc) = m + 1
        hi(nc) = u
        Call ehg125(p,nv,v,vhit,nvmax,d,k,xi(p),2**(k-1),2**(d-k),c(1,p),      &
          c(1,lo(p)),c(1,hi(p)))
      End If
      p = p + 1
      l = lo(p)
      u = hi(p)
      Go To 100
!     bottom of while loop
130   Return
    End Subroutine ehg124

    Subroutine ehg129(l,u,d,x,pi,n,sigma)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: d, l, n, u
!     .. Array Arguments ..
      Real (Kind=wp)                   :: sigma(d), x(n,d)
      Integer                          :: pi(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: alpha, beta, t
      Real (Kind=wp), Save             :: machin
      Integer, Save                    :: execnt
      Integer                          :: i, k
!     .. External Procedures ..
      Real (Kind=wp), External         :: d1mach
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max, min
!     .. Data Statements ..
      Data execnt/0/
!     .. Executable Statements ..
!     MachInf -> machin
      execnt = execnt + 1
      If (execnt==1) Then
!       initialize  d1mach(2) === DBL_MAX:
        machin = d1mach(2)
      End If
      Do k = 1, d
        alpha = machin
        beta = -machin
        Do i = l, u
          t = x(pi(i),k)
          alpha = min(alpha,x(pi(i),k))
          beta = max(beta,t)
        End Do
        sigma(k) = beta - alpha
      End Do
      Return
    End Subroutine ehg129

!   {called only from ehg127}  purpose...?...
    Subroutine ehg137(z,kappa,leaf,nleaf,d,nv,nvmax,ncmax,a,xi,lo,hi)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: d, kappa, ncmax, nleaf, nv, nvmax
!     .. Array Arguments ..
      Real (Kind=wp)                   :: xi(ncmax), z(d)
      Integer                          :: a(ncmax), hi(ncmax), leaf(256),      &
                                          lo(ncmax)
!     .. Local Scalars ..
      Integer                          :: p, stackt
!     .. Local Arrays ..
      Integer                          :: pstack(20)
!     .. External Procedures ..
      External                         :: ehg182
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max
!     .. Executable Statements ..
!     stacktop -> stackt
!     find leaf cells affected by $z$
      stackt = 0
      p = 1
      nleaf = 0
!     top of while loop
100   If (.Not. (0<p)) Go To 110
      If (a(p)==0) Then
!           leaf
        nleaf = nleaf + 1
        leaf(nleaf) = p
!           Pop
        If (stackt>=1) Then
          p = pstack(stackt)
        Else
          p = 0
        End If
        stackt = max(0,stackt-1)
      Else
        If (z(a(p))==xi(p)) Then
!              Push
          stackt = stackt + 1
          If (.Not. (stackt<=20)) Then
            Call ehg182(187)
          End If
          pstack(stackt) = hi(p)
          p = lo(p)
        Else
          If (z(a(p))<=xi(p)) Then
            p = lo(p)
          Else
            p = hi(p)
          End If
        End If
      End If
      Go To 100
!     bottom of while loop
110   If (.Not. (nleaf<=256)) Then
        Call ehg182(185)
      End If
      Return
    End Subroutine ehg137

!   -- For Error messaging, call the "a" routines at the bottom of  ./loessc.c  :
    Subroutine ehg183(s,i,n,inc)

!     .. Implicit None Statement ..
      Implicit None
!     .. Scalar Arguments ..
      Integer                          :: i, inc, n
      Character (*)                    :: s
!     .. External Procedures ..
      External                         :: ehg183a
!     .. Intrinsic Procedures ..
      Intrinsic                        :: len
!     .. Executable Statements ..
      Call ehg183a(s,len(s),i,n,inc)
    End Subroutine ehg183

    Subroutine ehg184(s,x,n,inc)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: x
      Integer                          :: inc, n
      Character (*)                    :: s
!     .. External Procedures ..
      External                         :: ehg184a
!     .. Intrinsic Procedures ..
      Intrinsic                        :: len
!     .. Executable Statements ..
      Call ehg184a(s,len(s),x,n,inc)
    End Subroutine ehg184
