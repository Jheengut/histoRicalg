!   ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++C
!                                                            C
!   HIERARCHICAL CLUSTERING using (user-specified) criterion. C
!                                                            C
!   Parameters:                                               C
!                                                            C
!   N                 the number of points being clustered    C
!   DISS(LEN)         dissimilarities in lower half diagonal  C
!                    storage; LEN = N.N-1/2,                 C
!   IOPT              clustering criterion to be used,        C
!   IA, IB, CRIT      history of agglomerations; dimensions   C
!                    N, first N-1 locations only used,       C
!   MEMBR, NN, DISNN  vectors of length N, used to store      C
!                    cluster cardinalities, current nearest  C
!                    neighbour, and the dissimilarity assoc. C
!                    with the latter.                        C
!                    MEMBR must be initialized by R to the   C
!                    default of  rep(1, N)                   C
!   FLAG              boolean indicator of agglomerable obj./ C
!                    clusters.                               C
!                                                            C
!   F. Murtagh, ESA/ESO/STECF, Garching, February 1986.       C
!   Modifications for R: Ross Ihaka, Dec 1996                 C
!                       Fritz Leisch, Jun 2000               C
!   all vars declared:   Martin Maechler, Apr 2001            C
!                                                            C
!   - R Bug PR#4195 fixed "along" qclust.c, given in the report C
!   - Testing: --> "hclust" in ../../../../tests/reg-tests-1b.R C
!   "ward.D2" (iOpt = 8): Martin Maechler, Mar 2014           C
!   ------------------------------------------------------------C
    Subroutine hclust(n,len,iopt,ia,ib,crit,membr,nn,disnn,flag,diss)
!     Args
!     Var
!     External function
!
!     was 1D+20

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: iopt, len, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: crit(n), disnn(n), diss(len),        &
                                          membr(n)
      Integer                          :: ia(n), ib(n), nn(n)
      Logical                          :: flag(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: d12, dmin
      Real (Kind=wp), Save             :: inf
      Integer                          :: i, i2, im, ind, ind1, ind2, j, j2,   &
                                          jj, jm, k, ncl
      Logical                          :: isward
!     .. External Procedures ..
      Integer, External                :: ioffst
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max, min, sqrt
!     .. Data Statements ..
      Data inf/1.E+300_wp/
!     .. Executable Statements ..
!
!     unnecessary initialization of im jj jm to keep g77 -Wall happy
!
      im = 0
      jj = 0
      jm = 0
!
!     Initializations
!
      Do i = 1, n
!        We do not initialize MEMBR in order to be able to restart the
!        algorithm from a cut.
!        MEMBR(I)=1.
        flag(i) = .True.
      End Do
      ncl = n

      If (iopt==8) Then ! Ward "D2" ---> using *squared* distances
        Do i = 1, len
          diss(i) = diss(i)*diss(i)
        End Do
      End If

!
!     Carry out an agglomeration - first create list of NNs
!     Note NN and DISNN are the nearest neighbour and its distance
!     TO THE RIGHT of I.
!
      Do i = 1, n - 1
        dmin = inf
        Do j = i + 1, n
          ind = ioffst(n,i,j)
          If (dmin>diss(ind)) Then
            dmin = diss(ind)
            jm = j
          End If
        End Do
        nn(i) = jm
        disnn(i) = dmin
      End Do

!     -- Repeat -------------------------------------------------------
100   Continue

!     Next, determine least diss. using list of NNs
      dmin = inf
      Do i = 1, n - 1
        If (flag(i) .And. disnn(i)<dmin) Then
          dmin = disnn(i)
          im = i
          jm = nn(i)
        End If
      End Do
      ncl = ncl - 1
!
!     This allows an agglomeration to be carried out.
!
      i2 = min(im,jm)
      j2 = max(im,jm)
      ia(n-ncl) = i2
      ib(n-ncl) = j2
!     WARD'S "D1", or "D2" MINIMUM VARIANCE METHOD -- iOpt = 1 or 8.
      isward = (iopt==1 .Or. iopt==8)
      If (iopt==8) dmin = sqrt(dmin)
      crit(n-ncl) = dmin
      flag(j2) = .False.
!
!     Update dissimilarities from new cluster.
!
      dmin = inf
      Do k = 1, n
        If (flag(k) .And. k/=i2) Then
          If (i2<k) Then
            ind1 = ioffst(n,i2,k)
          Else
            ind1 = ioffst(n,k,i2)
          End If
          If (j2<k) Then
            ind2 = ioffst(n,j2,k)
          Else
            ind2 = ioffst(n,k,j2)
          End If
          d12 = diss(ioffst(n,i2,j2))
!
!         WARD'S "D1", or "D2" MINIMUM VARIANCE METHOD - IOPT=8.
          If (isward) Then
            diss(ind1) = (membr(i2)+membr(k))*diss(ind1) +                     &
              (membr(j2)+membr(k))*diss(ind2) - membr(k)*d12
            diss(ind1) = diss(ind1)/(membr(i2)+membr(j2)+membr(k))
!
!           SINGLE LINK METHOD - IOPT=2.
          Else If (iopt==2) Then
            diss(ind1) = min(diss(ind1),diss(ind2))
!
!           COMPLETE LINK METHOD - IOPT=3.
          Else If (iopt==3) Then
            diss(ind1) = max(diss(ind1),diss(ind2))
!
!           AVERAGE LINK (OR GROUP AVERAGE) METHOD - IOPT=4.
          Else If (iopt==4) Then
            diss(ind1) = (membr(i2)*diss(ind1)+membr(j2)*diss(ind2))/          &
              (membr(i2)+membr(j2))
!
!           MCQUITTY'S METHOD - IOPT=5.
          Else If (iopt==5) Then
            diss(ind1) = (diss(ind1)+diss(ind2))/2
!
!           MEDIAN (GOWER'S) METHOD aka "Weighted Centroid" - IOPT=6.
          Else If (iopt==6) Then
            diss(ind1) = ((diss(ind1)+diss(ind2))-d12/2)/2
!
!           Unweighted CENTROID METHOD - IOPT=7.
          Else If (iopt==7) Then
            diss(ind1) = (membr(i2)*diss(ind1)+membr(j2)*diss(ind2)-membr(i2)* &
              membr(j2)*d12/(membr(i2)+membr(j2)))/(membr(i2)+membr(j2))
          End If

!
          If (i2<k) Then
            If (diss(ind1)<dmin) Then
              dmin = diss(ind1)
              jj = k
            End If
          Else ! i2 > k
!           FIX: the rest of the else clause is a fix by JB to ensure
!            correct nearest neighbours are found when a non-monotone
!            clustering method (e.g. the centroid methods) are used
            If (diss(ind1)<disnn(k)) Then ! find nearest neighbour of i2
              disnn(k) = diss(ind1)
              nn(k) = i2
            End If
          End If
        End If
      End Do
      membr(i2) = membr(i2) + membr(j2)
      disnn(i2) = dmin
      nn(i2) = jj
!
!     Update list of NNs insofar as this is required.
!
      Do i = 1, n - 1
        If (flag(i) .And. ((nn(i)==i2) .Or. (nn(i)==j2))) Then
!         (Redetermine NN of I:)
          dmin = inf
          Do j = i + 1, n
            If (flag(j)) Then
              ind = ioffst(n,i,j)
              If (diss(ind)<dmin) Then
                dmin = diss(ind)
                jj = j
              End If
            End If
          End Do
          nn(i) = jj
          disnn(i) = dmin
        End If
      End Do
!
!     Repeat previous steps until N-1 agglomerations carried out.
!
      If (ncl>1) Go To 100
!
!
      Return
    End Subroutine hclust
!     of HCLUST()
!
!
    Function ioffst(n,i,j)
!     Map row I and column J of upper half diagonal symmetric matrix
!     onto vector.

!     .. Implicit None Statement ..
      Implicit None
!     .. Function Return Value ..
      Integer                          :: ioffst
!     .. Scalar Arguments ..
      Integer                          :: i, j, n
!     .. Executable Statements ..
      ioffst = j + (i-1)*n - (i*(i+1))/2
      Return
    End Function ioffst

!   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++C
!                                                               C
!   Given a HIERARCHIC CLUSTERING, described as a sequence of    C
!   agglomerations, prepare the seq. of aggloms. and "horiz."    C
!   order of objects for plotting the dendrogram using S routine C
!   'plclust'.                                                   C
!                                                               C
!   Parameters:                                                  C
!                                                               C
!   IA, IB:       vectors of dimension N defining the agglomer-  C
!                 ations.                                       C
!   IIA, IIB:     used to store IA and IB values differently     C
!                (in form needed for S command 'plclust'        C
!   IORDER:       "horiz." order of objects for dendrogram       C
!                                                               C
!   F. Murtagh, ESA/ESO/STECF, Garching, June 1991               C
!                                                               C
!   HISTORY                                                      C
!                                                               C
!   Adapted from routine HCASS, which additionally determines    C
!   cluster assignments at all levels, at extra comput. expense C
!                                                               C
!   ---------------------------------------------------------------C
    Subroutine hcass2(n,ia,ib,iorder,iia,iib)
!     Args
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Scalar Arguments ..
      Integer                          :: n
!     .. Array Arguments ..
      Integer                          :: ia(n), ib(n), iia(n), iib(n),        &
                                          iorder(n)
!     .. Local Scalars ..
      Integer                          :: i, j, k, k1, k2, loc
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max, min
!     .. Executable Statements ..
!
!     Following bit is to get seq. of merges into format acceptable to plclust
!     I coded clusters as lowest seq. no. of constituents; S's 'hclust' codes
!     singletons as -ve numbers, and non-singletons with their seq. nos.
!
      Do i = 1, n
        iia(i) = ia(i)
        iib(i) = ib(i)
      End Do
      Do i = 1, n - 2
!        In the following, smallest (+ve or -ve) seq. no. wanted
        k = min(ia(i),ib(i))
        Do j = i + 1, n - 1
          If (ia(j)==k) iia(j) = -i
          If (ib(j)==k) iib(j) = -i
        End Do
      End Do
      Do i = 1, n - 1
        iia(i) = -iia(i)
        iib(i) = -iib(i)
      End Do
      Do i = 1, n - 1
        If (iia(i)>0 .And. iib(i)<0) Then
          k = iia(i)
          iia(i) = iib(i)
          iib(i) = k
        End If
        If (iia(i)>0 .And. iib(i)>0) Then
          k1 = min(iia(i),iib(i))
          k2 = max(iia(i),iib(i))
          iia(i) = k1
          iib(i) = k2
        End If
      End Do
!
!
!     NEW PART FOR 'ORDER'
!
      iorder(1) = iia(n-1)
      iorder(2) = iib(n-1)
      loc = 2
      Do i = n - 2, 1, -1
        Do j = 1, loc
          If (iorder(j)==i) Then
!           REPLACE IORDER(J) WITH IIA(I) AND IIB(I)
            iorder(j) = iia(i)
            If (j==loc) Then
              loc = loc + 1
              iorder(loc) = iib(i)
            Else
              loc = loc + 1
              Do k = loc, j + 2, -1
                iorder(k) = iorder(k-1)
              End Do
              iorder(j+1) = iib(i)
            End If
            Go To 100
          End If
        End Do
!       SHOULD NEVER REACH HERE
100     Continue
      End Do
!
!
      Do i = 1, n
        iorder(i) = -iorder(i)
      End Do
!
!
      Return
    End Subroutine hcass2
