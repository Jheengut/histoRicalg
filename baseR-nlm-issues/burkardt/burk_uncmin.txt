 
31 October   2018   3:22:18.517 PM      
 
UNCMIN_PRB
  FORTRAN77 version.
  Test the UNCMIN library.
  
 TEST01
   Test OPTIF0, the simple interface to UNCMIN.
  
 RESULT - Iterate            0
  
 Step
  
   0.00000       0.00000    
  
 X
  
   1.00000       1.00000    
 Function value =    783.98151627258801     
  
 Gradient vector
  
   759.398       2395.63    
  
 Hessian matrix
  
   1.00000       0.00000    
   0.00000       1.00000    
  
 RESULT - Iterate            2
  
 X
  
   20.0000      -20.1959    
 Function value =    90.999999390008171     
  
 Gradient vector
  
  0.629141E-06 -0.600787E-06
  
   Output from UNCMIN:
   Error code=           1
  
 EXPLAIN:
   UNCMIN returned a termination code of            1
  
 This code has the following meaning:
  
 The gradient is relatively close to zero.
 The current iterate is probably a good solution.
  
   F(X*) =   90.999999390008171     
   X* =   20.000000015249810       -20.195924304682116     
  
   (Partial reference results for comparison:)
       19.9145       -20.6011       -5.26250
       19.9900       -20.6230        19.9145
       20.0100       -20.6230        19.9145
       19.9900       -20.6023        19.9145
   Error code =           1
   F(X*) =    91.0001
   X* =    19.9900       -20.6230
  
  
  
 uncprb  function of 3 variables.
  
   Solve problem using line search.
  
 RESULT - Iterate            0
  
 Step
  
   0.00000       0.00000       0.00000    
  
 X
  
  -1.00000       0.00000       0.00000    
 Function value =    95579.451357303798     
  
 Gradient vector
  
  -0.00000      -61831.9      -6183.19    
  
 Hessian matrix
  
   200.000      -61831.9       0.00000    
  -61831.9       20000.0       2000.00    
   0.00000       2000.00       202.000    
  
 RESULT - Iterate           15
  
 X
  
   1.00000     -0.855374E-13 -0.830642E-12
 Function value =    8.4434898490436188E-025
  
 Gradient vector
  
 -0.610623E-11 -0.494644E-10  0.328516E-11
  
   termination code itrmcd=           1
  
 EXPLAIN:
   UNCMIN returned a termination code of            1
  
 This code has the following meaning:
  
 The gradient is relatively close to zero.
 The current iterate is probably a good solution.
  
   return code msg=           0
  
   x* =       0.99999999999996947       -8.5537399812474531E-014  -8.3064179595742699E-013
  
   f(x*) =     8.4434898490436188E-025
  
   gradient=  -6.1062266354425922E-012  -4.9464404334689922E-011   3.2851568415540391E-012
  
  
  
  
 uncprb  function of 3 variables.
  
   Solve problem using double dogleg method.
  
 RESULT - Iterate            0
  
 Step
  
   0.00000       0.00000       0.00000    
  
 X
  
  -1.00000       0.00000       0.00000    
 Function value =    95579.451357303798     
  
 Gradient vector
  
  -0.00000      -61831.9      -6183.19    
  
 Hessian matrix
  
   200.000      -61831.9       0.00000    
  -61831.9       20000.0       2000.00    
   0.00000       2000.00       202.000    
  
 RESULT - Iterate            9
  
 X
  
   1.00000      0.839933E-08  0.659617E-07
 Function value =    2.1160045229905760E-012
  
 Gradient vector
  
 -0.288384E-04  0.360632E-04 -0.347439E-05
  
   termination code itrmcd=           1
  
 EXPLAIN:
   UNCMIN returned a termination code of            1
  
 This code has the following meaning:
  
 The gradient is relatively close to zero.
 The current iterate is probably a good solution.
  
   return code msg=           0
  
   x* =       0.99999985580777384        8.3993303859146700E-009   6.5961742827351511E-008
  
   f(x*) =     2.1160045229905760E-012
  
   gradient=  -2.8838445534733249E-005   3.6063151243759206E-005  -3.4743911429409752E-006
  
  
  
  
 uncprb  function of 3 variables.
  
   Solve problem using more-hebdon method.
  
 RESULT - Iterate            0
  
 Step
  
   0.00000       0.00000       0.00000    
  
 X
  
  -1.00000       0.00000       0.00000    
 Function value =    95579.451357303798     
  
 Gradient vector
  
  -0.00000      -61831.9      -6183.19    
  
 Hessian matrix
  
   200.000      -61831.9       0.00000    
  -61831.9       20000.0       2000.00    
   0.00000       2000.00       202.000    
  
 RESULT - Iterate           10
  
 X
  
   1.00000     -0.527305E-12 -0.498973E-11
 Function value =    3.3516457552131588E-023
  
 Gradient vector
  
 -0.153877E-10 -0.566642E-09  0.466847E-10
  
   termination code itrmcd=           1
  
 EXPLAIN:
   UNCMIN returned a termination code of            1
  
 This code has the following meaning:
  
 The gradient is relatively close to zero.
 The current iterate is probably a good solution.
  
   return code msg=           0
  
   x* =       0.99999999999992306       -5.2730541179706801E-013  -4.9897333436479152E-012
  
   f(x*) =     3.3516457552131588E-023
  
   gradient=  -1.5387691121603463E-011  -5.6664154864637652E-010   4.6684688177338262E-011
  
 
UNCMIN_PRB
  Normal end of execution.
 
31 October   2018   3:22:18.518 PM      
