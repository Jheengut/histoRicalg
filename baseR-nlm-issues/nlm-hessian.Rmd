---
title: "Provenance of R's nlm() with an explicit Hessian matrix"
author: "John C Nash, Arpad Lucaks (??)
    Telfer School of Management,
    University of Ottawa,
    nashjc@uottawa.ca"
date: "October 31, 2018"
output: 
    pdf_document
bibliography: ../historicalg.bib
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Abstract

Applying the **R** function `nlm()` to the Wood 4 parameter
optimization test problem using the standard starting point 
`c(-3,-1,-3,-1)` gives a result that is far from optimal 
after 100 iterations when function, gradient and hessian are
supplied. 100 iterations is the default limit for this 
method. When just function and gradient are supplied, a 
solution is found much more quickly. Together with earlier 
concerns raised by Marie
Boehnstedt, which related to the Cholesky decomposition used
by `nlm()` that are now fixed by Martin Maechler in R 3.5,
there were concerns that there was a bug in `nlm()` and 
possibly in the original Fortran algorithm. Further checks
in this vignette suggest that the Wood case is simply
unfortunate, and with a higher iteration limit a satisfactory
solution is obtained. 

The actual origins and provenance of `nlm()` remain imprecise,
and documentation of the method and its Fortran antecedents
could be improved.

## Minimizing a nonlinear function when we have the Hessian

**R** has a number of tools for minimizing nonlinear functions, but many of them
only require a subprogram (in **R** a "function") for computing the nonlinear
function f(x, exdata) where x is a vector of parameters and exdata is some 
exogenous data. The exogenous data may not be needed. Some tools can make use
of, or even require, a subprogram to compute the gradient `g(x, exdata)` as a vector.
The nlm() function can, however, also use a Hessian subprogram hess(x, exdata) that
will return the matrix of second derivatives of f() at x. 

Unfortunately, as Marie Boehnstedt has shown, nlm() as implemented in R did not
do this correctly up to R version 3.4.x on the Rosenbrock test function, results
for which are presented below.
The issue, which appears to be a faulty implementation of the modified
Cholesky decomposition to ensure the Hessian approximation is positive definite,
was corrected by Martin Maechler in R-devel as at July 2017, and is part of R 3.5
now. See https://bugs.r-project.org/bugzilla3/show_bug.cgi?id=17249. (This did
NOT connect on July 19, 2018, however, and went to an error page.)
Note that R 3.4.4 and earlier will likely still fail, however.


## The Rosenbrock function

This was the original test problem that gave rise to Marie Boehnstedt's bug report.
The code below shows that `nlm()` finds a satisfactory minimum from the standard
starting point with either function and gradient or function, gradient and hessian.
Readers may skip the details and move to the Wood function section if desired.

```{r}
# sessionInfo() ## Include for extended documentation
#Example 1: Rosenbrock banana valley function
# Functions individually
f <- function(x){   # analytic gradient only
  x1 <- x[1]; x2 <- x[2]
  res<- 100*(x2 - x1*x1)^2 + (1-x1)^2 
} 
g <- function(x){
  x1 <- x[1]; x2 <- x[2]
  gg <- c(-400*x1*(x2 - x1*x1)-2*(1-x1), 200*(x2 - x1*x1)) 
}
h <- function(x){
    x1 <- x[1]; x2 <- x[2]
    a11 <- 2 - 400*x2 + 1200*x1*x1
    a21 <- -400*x1 
    matrix(c(a11, a21, a21, 200), 2, 2)
}

## The following function combines function and gradient so nlm() may be called
fg <- function(x){   # analytic gradient only
  gr <- function(x1, x2) c(-400*x1*(x2 - x1*x1)-2*(1-x1), 200*(x2 - x1*x1)) 
  x1 <- x[1]; x2 <- x[2]
  res<- 100*(x2 - x1*x1)^2 + (1-x1)^2 
  attr(res, "gradient") <- gr(x1, x2)
  return(res)
} 
sessionInfo()
x0 <- c(-1.2,1)
f0 <- f(x0)
f0 # Initial function value
g0 <- g(x0)
g0 # initial gradient value
h0 <- h(x0)
h0 # initial hessian value
fg0 <- fg(x0)
fg0 # to check the values of the combined function
nlm.fg <- nlm(fg, x0) # Result will be displayed below for comparison purposes
fgh <- function(x){  # analytic gradient and Hessian incorporated into single fn
  gr <- function(x1, x2) c(-400*x1*(x2 - x1*x1) - 2*(1-x1), 200*(x2 - x1*x1)) 
  h <- function(x1, x2){
    a11 <- 2 - 400*x2 + 1200*x1*x1
    a21 <- -400*x1 
    matrix(c(a11, a21, a21, 200), 2, 2)
  } 
  x1 <- x[1];  x2 <- x[2];  res<- 100*(x2 - x1*x1)^2 + (1-x1)^2 
  attr(res, "gradient") <- gr(x1, x2)
  attr(res, "hessian") <- h(x1, x2) 
  return(res)
}
fgh0<-fgh(x0)
fgh0 # check the results
nlm.fgh <- nlm(fgh, x0)
nlm.fg # f and g
nlm.fgh # f and g and h
#############
```

As at 2018-10-31, with R version 3.5.1 (2018-07-02), we see that `nlm()` 
gives very similar results in either fg or fgh modes. This was NOT the case earlier,
and in 2016 I tried the same code, which gave an unsatisfactory answer at that time
when the Hessian was supplied. Indeed, this led to the **histoRicalg** project.
Unfortunately, the outputs do not seem to have been kept. 

It is somewhat easier to run the same calculations through the optimx 
package @NashVaradhan11, @NashBest14. This unifies the syntax to call a number
of different optimizers, including `nlm`.

```{r, eval=FALSE}
# Put in the optimr example for completeness
library(optimx)
require(utils)
packageVersion("optimx") # sessionInfo()
onlm.fgh <- optimr(x0, fn=f, gr=g, hess=h, method="nlm")
onlm.fgh
```

JN also wrote two versions of a safeguarded Newton code (@snewton17) to allow for 
explicit Hessians  
to be used in a fairly straightforward way. The following example illustrates its use, 
but is not a comprehensive guide to this approach.

```{r}
# Using Safeguarded Newton method
require(optimx)
# Change trace to 1 to follow iterations
sn.fgh <- snewton(x0, f, g, h, control=list(maxit=200, trace=0))
sn.fgh
snm.fgh <- snewtonm(x0, f, g, h, control=list(maxit=200, trace=0))
snm.fgh
```

## The Wood function

Unfortunately, for the WOOD test function @more80, `nlm()` does not appear to return the right solution
with an explicit Hessian. Below we illustrate this, with the comparable results from `snewtonm()`.
The latter algorithm is a Newton-like method with a Marquardt stabilization. The issue turns
out to be that convergence is "slow" for nlm() on this function from this starting point
for this problem in the situation where a hessian is supplied, and the default of 100 iterations is too few. 

```{r}
# nlmwoodtry.R
#Example 2: Wood function
#
woodfunc.f <- function(x){
  val <- 100*(x[1]^2-x[2])^2+(1-x[1])^2+90*(x[3]^2-x[4])^2+(1-x[3])^2+
    10.1*((1-x[2])^2+(1-x[4])^2)+19.8*(1-x[2])*(1-x[4])
  return(val)
}
#gradient:
wfg <- function(x){
  g1 <- 400*x[1]^3-400*x[1]*x[2]+2*x[1]-2
  g2 <- -200*x[1]^2+220.2*x[2]+19.8*x[4]-40
  g3 <- 360*x[3]^3-360*x[3]*x[4]+2*x[3]-2
  g4 <- -180*x[3]^2+200.2*x[4]+19.8*x[2]-40
  return(c(g1,g2,g3,g4))
}
#hessian:
wfh <- function(x){
  h11 <- 1200*x[1]^2-400*x[2]+2;    h12 <- -400*x[1]; h13 <- h14 <- 0
  h22 <- 220.2; h23 <- 0;    h24 <- 19.8
  h33 <- 1080*x[3]^2-360*x[4]+2;    h34 <- -360*x[3]
  h44 <- 200.2
  H <- matrix(c(h11,h12,h13,h14,h12,h22,h23,h24,
                h13,h23,h33,h34,h14,h24,h34,h44),ncol=4)
  return(H)
}
#wood function with analytic gradient computed in attribute:
woodfunc.g <- function(x){
  res <- 100*(x[1]^2-x[2])^2+(1-x[1])^2+90*(x[3]^2-x[4])^2+(1-x[3])^2+
    10.1*((1-x[2])^2+(1-x[4])^2)+19.8*(1-x[2])*(1-x[4])
  attr(res,"gradient") <- wfg(x)
  return(res)
}
w0 <- c(-3,-1,-3,-1)

require(optimx)
woodsn2 <- snewtonm(w0, woodfunc.f, wfg, wfh)
woodsn2


nlm.wfg <- nlm(p=w0,woodfunc.g)
nlm.wfg
#
#wood function with analytic gradient and Hessian in attributes:
woodfunc.gh <- function(x){
  res <- 100*(x[1]^2-x[2])^2+(1-x[1])^2+90*(x[3]^2-x[4])^2+(1-x[3])^2+
    10.1*((1-x[2])^2+(1-x[4])^2)+19.8*(1-x[2])*(1-x[4])
  attr(res,"gradient") <- wfg(x)
  attr(res,"hessian") <- wfh(x)
  return(res)
}

nlm.wfgh <- nlm(p=w0,woodfunc.gh)
nlm.wfgh
# Note non-optimal function value
cat("But with the maxit default (iterlim) from optimx\n")
nlmo.wfgh <- nlm(p=w0,woodfunc.gh, iterlim=1000)
nlmo.wfgh


#################################################
cat("Now run the same cases using optimx\n")

## Put in optimx call 
require(optimx)
meths <- c("nlm", "snewton", "snewtonm")
npar <- 4
ctrl <- ctrldefault(npar)
cat("Default settings used in optimx\n")
dispdefault(ctrl)

cat("Use maxit=NULL to get the algorithm internal defaults\n")
woodfgh0 <- opm(w0, woodfunc.f, wfg, hess=wfh, method=meths, control=list(maxit=NULL, trace=0))
print(summary(woodfgh0))
cat("Using optimx defaults -- avoid maxit=NULL\n")
woodfgh1 <- opm(w0, woodfunc.f, wfg, hess=wfh, method=meths, control=list(trace=0))
print(summary(woodfgh1))
```

The above results suggest `nlm()` converges slowly with a Hessian supplied.

### Random starts

To investigate if particular starting parameters are the source of the observed slow
convergence with `nlm()`, the following code was run. To save space, the output is
suppressed, but we noted one start (the sixth) where `nlm()` took over 250 iterations,
whereas mostly it took 20-30 iterations.

```{r, eval=FALSE}
meths <- meths <- c("nlm", "snewton", "snewtonm", "Rvmmin")
set.seed(123456)
for (ii in 1:25){
    xstart <- runif(4, min=-8, max=8)
    cat("Start ",ii," is ")
    print(xstart)
    wfghx <- opm(xstart, woodfunc.f, wfg, hess=wfh, method=meths, control=list(trace=0))
    print(summary(wfghx))
}  
```


### Check against a Fortran version

This section can be omitted by the casual reader.

There are several Fortran codes available, but unfortunately it is not clear where and when
they originated. However, the Computational Chemistry Library has a version of UNCMIN at
http://www.ccl.net/cca/software/SOURCES/FORTRAN/uncmin/index.shtml. Moreover, the test
function `test.f` and the associated data file include the WOOD function with its
gradient and hessian. 

Getting this to compile and run some decades after it was written is non-trivial. I 
combined the codes into a file called `jnuncmintest.f` but attempts to compile gave
errors I was not able to decipher in a short time span, so I simplified the code
to only include the WOOD function, which I renamed. I also explicitly included
the starting data, getting file `jn180602.f` in the Gitlab repository directory
https://gitlab.com/nashjc/histoRicalg/tree/master/baseR-nlm-issues.

This compiles under fort77 (but fails under gfortran) and can be executed but the results 
in file `jn180602.out`
run many options and scalings, so are not immediately comparable with those above.
Further investigation is needed to determine which options **R** has incorporated
in the C translation of this code. 

?? Arpad: I think the work you've done on the fortran should be incorporated
here. It gives anyone contemplating digging into that code (quite apart from the 
R use of it) an important starting point.


## Examining the R source code

Some information on the provenance of the code for the nlm() function is buried in the **R** source code. 
I probably should have dug into this earlier, as several details of the code and its relatively 
recent repair were revealed.

`?nlm` gives a description of `nlm()` which includes the information

```
The current code is by Saikat DebRoy and the R Core team, 
using a C translation of Fortran code by Richard H. Jones.
```

and the references @DenSchnab83 and @Schnabel85.

Thus `nlm()` is founded on the UNCMIN optimizer. There are several variants of the source code for this optimizer.
Here we will note that we have acquired the following codes in the course of this effort:

 - The KMN code from @Kahaner89. The original book was accompanied by a disk.
   http://users.cla.umn.edu/~erm/data/ftools/kahaner/ appears to contain the code from the book above. In particular
   the files `uncmin.f`, `uncmnd.f`, `uncmnd.ex` and `rosenb.for` are relevant. It appears that the Kahaner, 
  Moler and Nash (KMN) code is the basis for several codes that appear on the Internet. In particular, the NIST 
  GAMS collection uses the double precision UNCMND.f. However, the KMN codes use **only** the user function and 
  not the user gradient or Hessian. That is optif9 driver that allows for a
   hessian function to be supplied is not present.

 - The GAMS code from the NIST Guide to Available Mathematical Software as module OPTIF9 in the CMLIB collection
   http://orion.math.iastate.edu/docs/cmlib/uncmin.html presents OPTIF0 and OPTIF9, but does **not** seem to offer
   the full code. 
   
 - Computational Chemistry List uncmin section http://www.ccl.net/cca/software/SOURCES/FORTRAN/uncmin/index.shtml
   seems to offer the full code with a re-worked main program `driver.f`, `test.f` and `test.dat`. 
   
 - the RS codes from Robert Schnabel sent to me at his instructions by Betty Eskow. I found there were too many 
   extra and/or missing pieces to this collection of code to be able to prepare a satisfactory program in a short
   time, especially after I discovered that `nlm()` referred to the CCL code.
   
 - John Burkardt has amassed a large collection of mathematical software in multiple programming languages. His 
   "official" site is http://people.sc.fsu.edu/~jburkardt. Unfortunately, while much other
   material is present, http://people.sc.fsu.edu/~jburkardt/f77_src/uncmin/uncmin.f is missing. Indeed, the f77_src 
   directory is missing uncmin completely. However, https://github.com/johannesgerer/jburkardt-f77, which claims 
   to be an official mirror of the Burkardt site is still functioning.

As another illustration of a growing concern about mathematical software, we note that almost none of the links in the page
http://perception.radiology.uiowa.edu/Research/ImprovedDBMROCMethodsforDiagnosticRadiology/NumericalOptimizers/UNCMIN/tabid/201/Default.aspx
are active. These claim to offer many of the codes just listed.
      
Let us examine parts of the R-3.5.1 source code more carefully.

###  R-3.5.1/src/appl/uncmin.c 

This code contains the comment
  
```
/* ../appl/uncmin.f
   -- translated by f2c (version of 1 June 1993 23:00:00).
   -- and hand edited by Saikat DebRoy
*/
```

The last routine in this file is optif9, a C code that matches the Fortran subroutine of the same
name in arguments. We now wish to discover how a call to `nlm()` in **R** connects to this
routine.

Furthermore, the file contains a link to the (apparent) original Fortran as
http://people.sc.fsu.edu/~jburkardt/f77_src/uncmin/uncmin.f, which we mentioned above. 
Since the link is dead, we retrieved the code from https://github.com/johannesgerer/jburkardt-f77.
This code, unlike the CCL one, uses lower case
for the code. Moreover, there are shell scripts to compile and run the programs, but
they are particularized to Burkardt's setup, so need some edit or else adjustment
of the user's environment. In particular, the variable \$ARCH needs to be set, and
the program f77split needs to be installed (https://people.sc.fsu.edu/~jburkardt/c_src/f77split/f77split.html).
On the other hand, simply concatenating the files `uncmin.f` and `uncmin_prb.f` and compiling
and executing the combined file yields a result, though there are some warnings. 
For now, we will not carry this line of investigation further.

```{r}
# should exist
Sys.which('bash')
Sys.which('sh')
```

```{r usebash, engine='bash'}
cd burkardt
cat uncmin_prb.f uncmin.f > try.f
echo "cat completed"
gfortran try.f
./a.out > burk_uncmin.txt
```

### src/library/stats/R/nlm.R

Contains the code defining the function. Note the call to 
  `.External2` with argument C_nlm, which is presumably in
`optimize.c` below.

```
nlm <- function(f, p, ..., hessian=FALSE, typsize=rep(1,length(p)),
		fscale=1, print.level=0, ndigit=12, gradtol=1e-6,
		stepmax=max(1000 * sqrt(sum((p/typsize)^2)), 1000),
		steptol=1e-6, iterlim=100, check.analyticals=TRUE)
{

    print.level <- as.integer(print.level)
    if(print.level < 0 || print.level > 2)
	stop("'print.level' must be in {0,1,2}")
    ## msg is collection of bits, i.e., sum of 2^k (k = 0,..,4):
    msg <- (1 + c(8,0,16))[1+print.level]
    if(!check.analyticals) msg <- msg + (2 + 4)
    .External2(C_nlm, function(x) f(x, ...), p, hessian, typsize, fscale,
               msg, ndigit, gradtol, stepmax, steptol, iterlim)
}
```

###  src/library/stats/src/optimize.c

The main call to nlm() appears to be the `SEXP nlm( ` line.  

/* NOTE: The actual Dennis-Schnabel algorithm `optif9' is in ../../../appl/uncmin.c */

SEXP nlm(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    SEXP value, names, v, R_gradientSymbol, R_hessianSymbol;

    double *x, *typsiz, fscale, gradtl, stepmx,
	steptol, *xpls, *gpls, fpls, *a, *wrk, dlt;

    int code, i, j, k, itnlim, method, iexp, omsg, msg,
	n, ndigit, iagflg, iahflg, want_hessian, itncnt;


/* .Internal(
 *	nlm(function(x) f(x, ...), p, hessian, typsize, fscale,
 *	    msg, ndigit, gradtol, stepmax, steptol, iterlim)
 */

...

    optif9(n, n, x, (fcn_p) fcn, (fcn_p) Cd1fcn, (d2fcn_p) Cd2fcn,
	   state, typsiz, fscale, method, iexp, &msg, ndigit, itnlim,
	   iagflg, iahflg, dlt, gradtl, stepmx, steptol, xpls, &fpls,
	   gpls, &code, a, wrk, &itncnt);

### src/include/R_ext/Applic.h

This header file contains information defining the call syntax to optif9.
   `void optif9(int nr, int n, double *x,` etc.

```
/* For use in package stats */

/* appl/uncmin.c : */

/* type of pointer to the target and gradient functions */
typedef void (*fcn_p)(int, double *, double *, void *);

/* type of pointer to the hessian functions */
typedef void (*d2fcn_p)(int, int, double *, double *, void *);

void fdhess(int n, double *x, double fval, fcn_p fun, void *state,
	    double *h, int nfd, double *step, double *f, int ndigit,
	    double *typx);

/* Also used in packages nlme, pcaPP */
void optif9(int nr, int n, double *x,
	    fcn_p fcn, fcn_p d1fcn, d2fcn_p d2fcn,
	    void *state, double *typsiz, double fscale, int method,
	    int iexp, int *msg, int ndigit, int itnlim, int iagflg,
	    int iahflg, double dlt, double gradtl, double stepmx,
	    double steptl, double *xpls, double *fpls, double *gpls,
	    int *itrmcd, double *a, double *wrk, int *itncnt);
```

### doc/NEWS

Under R-3.5.0 there is a note:

```
    * nlm(f, ..) for the case where f() has a "hessian" attribute now
      computes LL' = H + uI correctly.  (PR#17249).
```


This suggests that previous to R-3.5 releases, using a hessian was
NOT safe.

```
CHANGES IN R 3.1.3:

    * nlm() no longer modifies the callback argument in place (a new
      vector is allocated for each invocation, which mimics the
      implicit duplication that occurred in R < 3.1.0); note that this
      is a change from the previously documented behavior. (PR#15958)
```

```
CHANGES IN R 3.0.0:

    * optimize() and uniroot() no longer use a shared parameter object
      across calls.  (nlm(), nlminb() and optim() with numerical
      derivatives still do, as documented.)
```

###  src/gnuwin32/Maintainers.notes

This seems to be a file of "notes to self" for Windows builds by Brian Ripley.
The note about optif9 says only "for nlme".

###  src/library/tools/R/sotools.R

The only mention in this **R** routine of the `UNCMIN` or `nlm()` code is to `optif9` in a comment block. Possibly crud.

```
## non-API in Applic.h
## future <- c("dqrcf_", "dqrdc2_", "dqrls_", "dqrqty_", "dqrqy_", "optif9")
## d1mach_ and i1mach_ are mentioned (since R 2.15.3) in R-exts.
```

## Questions remaining about the `nlm()` code

An issue that is NOT yet addressed in this vignette is that of the options chosen by `nlm()` in 
calling the UNCMIN  code. There are several algorithms and a number of scaling, line search, 
termination and other strategies and tactics offered by this code. Each choice multiplies the 
possible specific method, which is NOT made clear to the user. Indeed, UNCMIN is treated as a
black box. It generally works very well, but when it does not, then specific reason and specific
choices leading to the poor outcome are not easily determined.

The code is not, of course, easy to follow, and a roadmap would be nice to have. 

## Other tests with and without hessian

These are included as a way to check the syntax and output for such cases.

### Weed infestation problem hobbs

```{r}
hobbs.f<- function(x){ # # Hobbs weeds problem -- function
    if (abs(12*x[3]) > 500) { # check computability
       fbad<-.Machine$double.xmax
       return(fbad)
    }
    res<-hobbs.res(x)
    f<-sum(res*res)
}


hobbs.res<-function(x){ # Hobbs weeds problem -- residual
# This variant uses looping
    if(length(x) != 3) stop("hobbs.res -- parameter vector n!=3")
    y<-c(5.308, 7.24, 9.638, 12.866, 17.069, 23.192, 31.443, 38.558, 50.156, 62.948,
         75.995, 91.972)
    t<-1:12
    if(abs(12*x[3])>50) {
       res<-rep(Inf,12)
    } else {
       res<-x[1]/(1+x[2]*exp(-x[3]*t)) - y
    }
}

hobbs.jac<-function(x){ # Jacobian of Hobbs weeds problem
   jj<-matrix(0.0, 12, 3)
   t<-1:12
    yy<-exp(-x[3]*t)
    zz<-1.0/(1+x[2]*yy)
     jj[t,1] <- zz
     jj[t,2] <- -x[1]*zz*zz*yy
     jj[t,3] <- x[1]*zz*zz*yy*x[2]*t
     jjret <- jj
     attr(jjret,"gradient") <- jj
   return(jjret)
}

hobbs.g<-function(x){ # gradient of Hobbs weeds problem
    # NOT EFFICIENT TO CALL AGAIN
    jj<-hobbs.jac(x)
    res<-hobbs.res(x)
    gg<-as.vector(2.*t(jj) %*% res)
    return(gg)
}

hobbs.rsd<-function(x) { # Jacobian second derivative
    rsd<-array(0.0, c(12,3,3))
    t<-1:12
    yy<-exp(-x[3]*t)
    zz<-1.0/(1+x[2]*yy)
    rsd[t,1,1]<- 0.0
    rsd[t,2,1]<- -yy*zz*zz
    rsd[t,1,2]<- -yy*zz*zz
    rsd[t,2,2]<- 2.0*x[1]*yy*yy*zz*zz*zz
    rsd[t,3,1]<- t*x[2]*yy*zz*zz
    rsd[t,1,3]<- t*x[2]*yy*zz*zz
    rsd[t,3,2]<- t*x[1]*yy*zz*zz*(1-2*x[2]*yy*zz)
    rsd[t,2,3]<- t*x[1]*yy*zz*zz*(1-2*x[2]*yy*zz)
##    rsd[t,3,3]<- 2*t*t*x[1]*x[2]*x[2]*yy*yy*zz*zz*zz
    rsd[t,3,3]<- -t*t*x[1]*x[2]*yy*zz*zz*(1-2*yy*zz*x[2])
    return(rsd)
}


hobbs.h <- function(x) { ## compute Hessian
#   cat("Hessian not yet available\n")
#   return(NULL)
    H<-matrix(0,3,3)
    res<-hobbs.res(x)
    jj<-hobbs.jac(x)
    rsd<-hobbs.rsd(x)
##    H<-2.0*(t(res) %*% rsd + t(jj) %*% jj)
    for (j in 1:3) {
       for (k in 1:3) {
          for (i in 1:12) {
             H[j,k]<-H[j,k]+res[i]*rsd[i,j,k]
          }
       }
    }
    H<-2*(H + t(jj) %*% jj)
    return(H)
}

x0good <- c(200, 50, 0.3)
x0bad <- c(1,1,1)

hobbs.fg <- function(x){
    val <- hobbs.f(x)
    grd <- hobbs.g(x)
    attr(val, "gradient") <- grd
    val
}    

hobbs.fgh <- function(x){
    val <- hobbs.f(x)
    grd <- hobbs.g(x)
    hes <- hobbs.h(x)
    attr(val, "gradient") <- grd
    attr(val, "hessian") <- hes
    val
}    

require(optimx)
meths <- c("nlm", "snewton", "snewtonm")

hnlmfggood <- nlm(hobbs.fg, x0good, print.level=1, hessian=TRUE)
hnlmfggood
hnlmfghgood <- nlm(hobbs.fgh, x0good, print.level=1, hessian=TRUE)
hnlmfghgood
hfghgood <- opm(x0good, hobbs.f, hobbs.g, hess=hobbs.h, meth=meths)
hfghgood

cat("Now try bad start\n")
hnlmfgbad <- nlm(hobbs.fg, x0bad, print.level=1, hessian=TRUE)
hnlmfgbad
hnlmfghbad <- nlm(hobbs.fgh, x0bad, print.level=1, hessian=TRUE)
hnlmfghbad
hfghbad <- opm(x0bad, hobbs.f, hobbs.g, hess=hobbs.h, method=meths, control=list(trace=0))
hfghbad

```

### Function trig

This function seems to give optimization methods some trouble. At least one
author suggests there may be multiple minima for this problem, which is 
of the nonlinear least squares type. All the methods tried below get a
reasonable answer, but some apparent failures of each method were noted
when a start using all parameters set to 4 was tried, especially of 
`snewtonm()`. It is possible the Marquardt stabilization (for function
minimization) is choosing a search direction that is not downhill. 
?? Do we need an Armijo criterion??


```{r}
## Trig function 
# ref  More' Garbow and Hillstrom, 1981, problem 26, from
#  Spedicato (their ref. 25)

trig.f <- function(x){
  res <- trig.res(x)
  f <- sum(res*res)
#  cat("FV=",f," at ")
#  print(x)
  f
}

trig.res <- function(x){
   n <- length(x)
   i <- 1:n
   res <- n - sum(cos(x)) + i*(1 - cos(x)) - sin(x) 
   return(res)
}

trig.jac <- function(x) { # not vectorized. Can it be?
## stop("Not defined")
   n <- length(x)
   J<-matrix(0,n,n)
   for (i in 1:n) {
      for (j in 1:n) {
         J[i,j]<-sin(x[j]) # we overwrite J[i,i]
      }
      J[i,i] <- (1+i) * sin(x[i])  - cos(x[i])
   }
   return(J)
}


trig.g <- function(x) { # unvectorized
  n<-length(x)
  res<-trig.res(x)
  J<-trig.jac(x)
  g<- as.vector(2.0 * ( t(J) %*% res ))
  return(g)
}

require(autodiffr)
ad_setup()
trig.h <- autodiffr::makeHessianFunc(trig.f)

trig.hj <- autodiffr::makeJacobianFunc(trig.g)
 


trig.fg <- function(x){
  val <- trig.f(x)
  grd <- trig.g(x)
  attr(val, "gradient") <- grd
  val
}
  
trig.fgh <- function(x){
  val <- trig.f(x)
  grd <- trig.g(x)
  hes <- trig.h(x)
  attr(val, "gradient") <- grd
  attr(val, "hessian") <- hes
  val
}

meths <- c("nlm", "snewton", "snewtonm", "ucminf")
require(optimx)
  
for (n in seq(2,12, by=2)) {
  cat("trig try for n=",n,"\n")
  x0 <- rep(1/n,n)
#  tnlmfg <- nlm(trig.fg, x0, print.level=0, hessian=TRUE)
#  cat("FG:")
#  print(tnlmfg)
#  tnlmfgh <- nlm(trig.fgh, x0, print.level=0, hessian=TRUE)
#  cat("FGH:")
#  print(tnlmfgh)
  trigfgh <- opm(x0, trig.f, trig.g, hess=trig.h, method=meths)
  ## Why not printing in loop?
  print(summary(trigfgh, par.select=1:2))
}

```

Note: case where snewtonm does NOT give as good a result.

Also note that no-hessian case seems to mess up snewton? methods
and stop program. Is this what we want?


## References

