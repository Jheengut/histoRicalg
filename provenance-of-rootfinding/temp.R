fsin <- function(x){
  sin(x) - 0.5
}

require(rootoned)

TraceSetup <- function(itn=0, trace=FALSE){
  # JN: Define globals here
  groot<-list(itn=itn, trace=trace)
  envroot<<-list2env(groot) # Note globals in FnTrace
  # end globals
}

FnTrace <- function(x, fn=NULL, label=NA, ...) { 
    # Substitute function to call when rootfinding
    # Evaluate fn(x, ...)
    val <- fn(x, ...)
    envroot$itn <- envroot$itn + 1 # probably more efficient ways
    if (trace) {
      cat("f(",x,")=",val," after ",itn," ",label,"\n")
    }
    val
  }
  
  
TraceSetup(trace=TRUE)
x<-pi/4
trace <- TRUE
tst <- root1d(FnTrace(x,fn=fsin), c(0,1.5))
