---
title: "Provenance of R's optim::BFGS, optim::CG and related minimizers"
author: "John C Nash,
    Telfer School of Management,
    University of Ottawa,
    nashjc@uottawa.ca"
date: "March 10, 2019"
output: 
    pdf_document
bibliography: ../../historicalg.bib
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r global_options, include=FALSE}
knitr::opts_chunk$set(fig.pos = 'h')
```

# Abstract

Gradient optimization methods have been a mainstay of tools for
numerical optimization of functions over the history of automatic
computing. In **R** they are well-represented in both the base and
in packages. However, some of the methods and the codes developed from
them arose before some aspects of hardware and software were standardized,
in particular the IEEE arithmetic @IEEE754-1985. As such there may be 
cases of unexpected behaviour or outright errors. A summary history of
some of the tools in **R** for such methods is presented to give
perspective on such methods and the occasions where they could be
used effectively.

## Overview: Minimizing a nonlinear function

**R** has a number of tools for minimizing nonlinear functions, some
requiring only a subprogram (in **R** a "function") for computing the nonlinear
function `f(x, exdata)` where `x` is a vector of parameters to be adjusted so that
the function takes on its minimal value, and `exdata` is some 
exogenous data. The exogenous data may not be needed. Some tools can make use
of, or even require, a subprogram to compute the gradient `g(x, exdata)` as a vector.
Of these latter -- **gradient** -- methods, the `optim::BFGS` method is one of the
most used. `optim::CG`, which is intended to minimize functions of many parameters,
was an early part of base-**R**, but I (the author of three codes that were adapted to **R** in
`optim()` by Brian Ripley) have never felt the original algorithm very successul, 
and I have provided what I consider improvements in more recent packages. These tools, 
and some related ones, are the subject of what follows.

## Background

In the mathematical description below, we will ignore exogenous data and assume
that it is supplied when needed. In mathematical terms, our problem is to find

$$ argmin{_x}{f(x)} $$

where $x$ is our set of parameters and $f()$ is a scalar real function of those
parameters.

The gradient of $f(x)$ is the vector valued function 

$$ g(x) = \partial{f(x)}/\partial{x} $$

The Hessian of $f(x)$ is the matrix of second partial derivatives

$$  H(x) = \partial^2{f(x)}/\partial{x^2}$$

With the gradient and Hessian, together with a starting set of parameters $x_0$ 
we can define a Newton iteration

$$  H(x_i) \delta = - g(x_i) $$

$$  x_{i+1} = x_i + s * \delta $$

where $s$ is some step size. 

Computing the gradient takes work, and the Hessian a lot more work. Both computations are error prone. 
The solution of the set of linear equations to get 
$\delta$ is also demanding of computational effort.

?? mention no newton methods directly, but nlm() can use info

### Quasi-Newton or Variable Metric methods

Some workers distinguish the terms **Quasi-Newton** and 
**Variable Metric**, but here we will treat them as essentially
interchangeable. 

To avoid the computation of the Hessian, some enterprising researchers 
developed ways to approximate it. Furthermore, they 
considered approximating an **inverse Hessian**, which avoids the solution of 
a set of linear equations at each iteration. However, there are a number of
methods that worked with an approximate Hessian and updated it at each
iteration. 

Suppose at iteration $i$ we have an approximation $B_i$ to the inverse Hessian $H^{-1}(x_i)$. Then
a step of the Newton iteration is approximated simply by

$$  \delta = - B_i * g(x_i) $$

From the 1960s through the 1980s there was a fairly lively academic industry 
in developing ways to 

  - choose a "good" step size $s$ (the **line search** sub-problem) to reduce or minimize
  $$f(x_i + s * \delta)$$
  - update the approximate inverse Hessian $B_i$ to $B_{i+1}$, or possibly an approximate
  Hessian if that approach was chosen.

One of the simpler sets of choices was published as @Fletcher70. To choose $s$ this used a 
combination cubic interpolation line search combined with simple backtracking (reducing
the current $s$ until a "sufficient decrease" condition is met). The update of $B$ is done
via what is now known as the Broyden-Fletcher-Goldfarb-Shanno or **BFGS** update. There are,
however, versions of this for both the Hessian and its inverse. Here we are working with
the inverse. It is, of course, necessary to have an initial Hessian approximation. Whether
or not it seems appropriate, the "approximation" used for the initial inverse Hessian is
simply the unit matrix. This has the advantage that the initial search direction is the
negative gradient, which means the first step is a **steepest-descent** step. 

The update formula used adjusts the current $B$ (we omit the subscripts where possible 
to keep things
a little tidier) by adding to it a matrix $C$. To define $C$ we need

$$ t = s * \delta = - s * B * g(x) $$

$$ y = g(x_{i+1}) - g(x_i) $$

$$  q = B * y $$

Then

$$ C = d_2 * t * t^T  - [ t * q^T + q * t^T]/d_1$$

where 

$$ d_1 = t^T * y $$

and

$$ d_2 = ( 1 + y^T*q/d_1)/d_1 $$

This update method was described by several authors in 1970: @Broyden70, @Fletcher70, @Shanno70A,
@Shanno70B, @Goldfarb70. Hence the BFGS appellation. An earlier update due to Davidon, Fletcher and
Powell gets the abbreviation **DFP**. See @Davidon59 (republished as @Davidon91), @FletPow63. A linear
combination of both updates can be created, giving a member of what is termed the Broyden family of
formulas.

Given the usage of these methods, it was unfortunate that the referees of Davidon's original paper
turned it down when he submitted it for publication. Some 32 years "late", the editors (I don't believe
they were the same people) corrected the oversight in @Davidon91. 


### nlm() 


### nlminb()


### Limited memory approaches and conjugate gradients

The need to store the matrix $B$ when the number of parameters is large motivated some workers to
look into methods that avoided the need to do so. Storing information for the BFGS updates of the
Hessian inverse as a set of vectors leads to the Limited Memory BFGS approach. See 
https://en.wikipedia.org/wiki/Limited-memory_BFGS. Most of the major work is linked in some
way to the article @Byrd1995. Note, however, a 2011 update as @Morales2011.

From a different starting point, various nonlinear conjugate gradient methods aim to provide good
search directions by modifying the gradient (which gives the steepest descent direction) to avoid
"hemstitching"" -- successive searches being almost parallel but back and forth across a "valley". 
There are a number of such approaches, with that of @FletReeves64 being among the first. Since 
the nonlinear conjugate gradients methods use but a few vectors to store information to update
the search direction, and these directions often have a close connection to those generated by
the Limited Memory BFGS approaches, we can consider the approaches related, though their 
algorithmic realizations are often quite dissimilar. Moreover, the CG methods generally only
save the "last" gradient and possibly search direction. While the initial linear CG code of 
@Hestenes1952 for solving linear equations was published nearly 7 decades ago, and the 
nonlinear adaptations were introduced in the 1960s, there has been a flurry of improvements 
since the late 1990s. Possibly overlooking some important work, I will single out efforts 
by the teams of Yuan/Dai and Hager/Zhang, namely papers linked to @Dai01 and @Hager2005, 
with a survey in @Hager2006b. My own work in the `Rcgmin` package is based on the first of these papers. 

Hager and Zhang's work makes several improvements to this, at the cost of more complicated code. Indeed,
the CG-DESCENT package published as @Hager2006a includes some ideas related to Limited Memory BFGS. 
Steven Scott helped me to wrap CG-DESCENT as the (experimental) **R** package `Rcgdescent` 
available at https://r-forge.r-project.org/projects/optimizer/, but this has a limited set of
the options of the original code.

A somewhat different application of conjugate gradients ideas is the Truncated
Newton algorithm. This uses a linear CG method to solve the Newton equations
approximately. There are, of course, many choices and details that give rise
to particular codes. 

## Provenance of the R optim::BFGS solver

If the source code for base **R** is in a directory that 
is named R-X.Y.Z (in my case 3.5.1 when this vignette was started) then the calling 
routine for optim() is `src/library/stats/R/optim.R`,
but this uses the `.External2` method to call `src/library/stats/src/optim.c`, where the function
`vmmin` is called. However, the source for `vmmin` is in `src/appl/optim.c`. I will venture
that having two files of the same name in different directories is tempting an error.

In `src/appl/optim.c`, above the vmmin routine is the comment

```{,comment}
/*  BFGS variable-metric method, based on Pascal code
in J.C. Nash, `Compact Numerical Methods for Computers', 2nd edition,
converted by p2c then re-crafted by B.D. Ripley */
```

As author of this work, I can say that the code used is Algorithm 21
of @Nash1979b, which for the Second Edition, 1990, was written in Turbo 
Pascal for computers of the IBM PC family. The First Edition presented 
step and description algorithms, which had been worked out using mainly
a Data General Nova which had partitions of between 3.5 K and 8 K bytes accessible
via a 10 character per second teletype. The floating point available had a 
24 bit mantissa, in a single level of precision with (as I recall) no guard digit.
Programming was in a fairly early and simple form of BASIC.

This machine was at Agriculture Canada. In the late 1970s, it was replaced
with a Data General Eclipse, but largely the facilities were the same, except
the floating point went to 6 hexadecimal digits with no guard digit. 
My algorithms in BASIC ran on these machines, as well as on HP 9830
and Tektronix 4051 "programmable calculators" (really personal 
computers). The algorithms were also translated
into Fortran, which ran mainly on IBM System 360 class computers, but some
codes (possibly modified) also were run 
on some other computers such as the Univac 1108, ICL 1906A and Xerox-Honeywell CP-6. 
The codes were distributed as NASHLIB 
(https://gams.nist.gov/cgi-bin/serve.cgi/Package/NASHLIB. I have been informed that 
the servers supporting this link will not be replaced when they fail.)

NASHLIB dates from the 1979-1981 period. However, the contents of the collection
(for linear algebra and function minimization) were developed in the mid-1970s
at Agriculture Canada to support economic modelling. Regular activities of the
ministry were run by remote job entry at service bureaux, and these had budgets.
The so-called "mini-computers" and "programmable desktop calculators" were more
available for speculative work without a formal budget because external agencies
did not require payment. (I was in the Research Division of the Economics
Branch at the time.) Thus the smaller machines allowed ideas outside the usual to be tried.
Sometimes this led to very useful tools and novel programs.

In 1975, I received an invitation (from Brian Ford) to collaborate with the Numerical Algorithms
Group in Oxford, and Agriculture Canada generously allowed me two 6-week periods
to do so. During the second of these, in the tail end of the hurricane of January
1976, I drove to Dundee to meet Roger Fletcher. He took an interest in the concept
of a program that used minimal memory (remember the 3.5 K for program and data, even
if the BASIC is tokenized). We found a printout of the Fortran for the method in
@Fletcher70, and with a ruler and pencil, Roger and I scratched out the lines of
code that were not strictly needed. As I recall, the main deletion was the cubic
interpolation line search. The resulting method simply used backtracking with an
acceptable point condition. 

On my return to Ottawa, I coded the method in BASIC and made one or two changes:

- I believe that I managed to re-use one vector, thereby reducing the storage
requirement to a square matrix and five vectors of length $n$, the number of 
parameters. 

- The very short (6 hexadecimal digit mantissa) floating point seemed to 
give early termination on some problems. Often this was due to failure of the update
of the approximate inverse Hessian when $d_1$ was not positive. This implies the
Hessian is not positive definite. As a quick and dirty fix, I simply checked if 
the current search was a steepest descent. If so, then the method is terminated.
If not, the inverse Hessian approximation is reset to the unit matrix and the 
cycle restarted. This "quick fix" has, of course, become permanent. Whether or
not it is still sensible is an open question.


## Evolution of the code

Considering the simplicity of the method, it is surprising to me
how robust and efficient it has shown itself to be over the last four decades.

The code does allow of some variations:

- the organization of some of the calculations may have an influence on outcomes. There
are opportunities for accumulation of inner products, and the update of the inverse 
Hessian approximation involves subtractions, so digit cancellation could be an issue.

- tolerances for termination, for the "acceptable point" (Armijo) condition, and other
settings could be changed. However, I have found the original settings seem to work
as well or better than other choices I have tried.

In the mid-1980s, the BASIC code was extended to allow for masks (fixed parameters)
and bounds (or box) constraints on the parameters @jnmws87. This adds possibly 50% more code
length, and vector storage for the constraints and indices to manage them (essentially $3*n$). 
but does permit a much wider variety of problems to be solved. Even for
unconstrained problems that may have difficult properties, imposing loose bounds
can prevent extreme steps in the parameters. To add this capability to **R**, I
put the package `Rvmmin` on CRAN in 2011 @p-Rvmmin. This code is, as of 2018, part
of a new `optimx` package on CRAN. It is entirely written in **R**.

## Extensions and related codes

There are other **R** packages with related capability. In particular, `ucminf` is based 
on the Fortran code of @Nielsen2000. This appears to use a very similar algorithm to
optim::BFGS, but employs a more sophisticated line search. (I
find the Fortran code shows this most clearly.) This does not, however, allow for
constraints in the form of bounds or masks. 

The approximate inverse Hessian could also be saved and used to provide some
estimates of parameter dispersion. Clearly, the use of a
steepest descents direction for the final line search before termination means
that the **penultimate** approximation must be saved. In practice, I have found
the approximate inverse Hessian bears little or no resemblance to the actual
Hessian. The may be because the construction of the approximation maintains
the positive definite condition. It is an open question whether the
approximation has any utility.

Package `mize` @Melville17 offers some options to build a gradient minimizer
and could possibly allow some algorithmic comparisons to be made, but I have
not had the time to delve into this.


## Provenance of the R optim::CG and related solvers

The funcion `optim()` in base-R has three of the optimizers from the 1990 edition
of Compact Numerical Methods (CMN). The direct-search Nelder-Mead method is in fact the 
default solver i.e., it uses `method="Nelder-Mead"` in the `optim()` call and does
not require a gradient routine to be supplied. In fact, Nelder-Mead will be used even if
a gradient routine is included unless some other method is suggested. 
The choice if `method="BFGS"` is the variable metric method described above. 
The third choice from CNM is `method="CG"` for conjugate gradients. 

Conjugate gradients methods aim to search "downhill" for better points on the
functional surface. The traditional first search direction is that of steepest
descents, namely, $- g$, the negative gradient. After a line search selects a
suitable step size, we have a new point, a new function value that is lower than
the initial point, and a new gradient. Using this information and possibly the
last search direction, we compute a new search direction that is somehow "conjugate"
to the previous one. For a problem with $n$ parameters, of course, there are only
$n$ independent search directions, so we need to restart the procedure on or before
that step. Indeed there are a number of strategies for deciding when to restart the
conjugacy cycle. And, of course, there are a number of choices for updating the 
search direction and for performing the line search. 

In this article on the provenance of the methods, the details will be discussed 
only in general terms. However, I will underline the importance of getting the
details right. 

The original Algorithm 22 of CNM which was converted to C and included in **R**
by Brian Ripley is a code that has never felt right to me. And I'm the author!
It offers three different search direction updates using the `type` element of
the `control` list. The default is `type=1` for the Fletcher-Reeves update (@FletReeves64), 
with 2 for Polak–Ribiere (Polak1969) and 3 for Beale–Sorenson (@Sorenson1969, @Beale1972). 

In the mid-1980's I updated the CG code (in BASIC) to handle bounds and masks. Then
in 2009 I incorporated the Yuan/Dai @Dai01 search direction update that melds the different
formulas used in `optim::CG`. This gives a remarkably effective method that retains a
surprisingly short code, and entirely in **R**.

## Provenance of the R optim::L-BFGS-B and related solvers

The base-R code lbfgsb.c (at writing in R-3.5.2/src/appl/) is commented:

```
/* l-bfgs-b.f -- translated by f2c (version 19991025).

  From ?optim:
  The code for method ‘"L-BFGS-B"’ is based on Fortran code by Zhu,
  Byrd, Lu-Chen and Nocedal obtained from Netlib (file 'opt/lbfgs_bcm.shar')

  The Fortran files contained no copyright information.

  Byrd, R. H., Lu, P., Nocedal, J. and Zhu, C.  (1995) A limited
  memory algorithm for bound constrained optimization.
  \emph{SIAM J. Scientific Computing}, \bold{16}, 1190--1208.
*/
```
The paper @Byrd95 builds on @Lu94limitedmemory. There have been a number
of other workers who have followed-up on this work, but **R** code and
packages seem to have largely stayed with codes derived from these original
papers. Though the date of the paper is 1995, the ideas it embodies were
around for a decade and a half at least, in particular in @Nocedal80 and
@LiuN89. The definitive Fortran code was published as @Zhu1997LBFGS. This
is available as `toms/778.zip` on www.netlib.org. A side-by-side comparison of the
main subroutines in the two downloads from Netlib unfortunately shows a lot of
differences. I have not tried to determine if these affect performance or are
simply cosmetic. 

More seriously perhaps, there were some deficiencies in the code(s), and in 2011 
Nocedal's team published a Fortran code with some corrections (@Morales2011).
Since the **R** code predates this, I prepared package `lbfgsb3` to wrap
the Fortran code. However, I did not discover any test cases where the
`optim::L-BFGS-B` and `lbfgsb3` were different, though I confess to only
running some limited tests. 

In 2016, I was at a Fields Institute optimization conference in Toronto
for the 70th birthday of Andy Conn. By sheer serendipity, Nocedal did not attend 
the conference, but
sat down next to me at the conference dinner. When I asked him about the key changes,
he said that the most important one was to fix the computation of the machine 
precision, which was not always correct in the 1995 code. Since **R** gets this 
number as `.Machine$double.eps`, the offending code is irrelevant. 

Within @Morales2011, there is also reported an improvement in the subspace
minimization that is applied in cases of bounds constraints. Since few of the
tests I have applied imporse such constraints, it is reasonable that I will 
not have observed performance differences between the base-R `optim` code
and my `lbfsgb3` package. More appropriate tests are welcome, and on my agenda.

Besides the ACM TOMS code, there are two related codes from the Northwestern team on NETLIB:
  - http://netlib.org/opt/lbfgs_um.shar
is for unconstrained minimization, while 

  - http://netlib.org/opt/lbfgs_bcm.shar handles bounds
constrained problems. 

To these are attached references @LiuN89 and @Byrd1995 respectively,
most likely reflecting the effort required to implement the constraints.

The unconstrained code has been converted to **C** under the leadership of 
Naoaki Okazaki (see http://www.chokkan.org/software/liblbfgs/, or the fork at  
https://github.com/MIRTK/LBFGS). This has been wrapped for **R** as @Coppola2014 as the
`lbfgs` package. This can be called from `optimx::optimr()`. 

Using Rcpp (see @DERFRcpp2011) and the Fortran code in package `lbfgs3`, Matthew Fidler 
developed package `lbfgsb3c`. As this provides a more standard call and return than
`lbfgsb3` Fidler and I are working to unify the two packages at the time of writing.

## Provenance of truncated Newton codes for R 

There are (at least) two implementations of truncated Newton methods available.

`nloptr::tnewton()` is a wrapper of the NLopt truncated Newton method translated
to C and somewhat modified by Steven G. Johnson in the nlopt project
(https://nlopt.readthedocs.io/en/latest/NLopt_Algorithms/) from Fortran 
code due to Ladislav Luksan (http://www.cs.cas.cz/luksan/subroutines.html). 
The many layers and translations make it difficult to unravel the particular
details in this code, and I do not feel confident to provide a competent
overview. 

`optimx::tn()` and `optimx::tnbc()` are translations from Matlab source of 
the Truncated Newton
codes of Stephen Nash (@SGN83) that I prepared, initially in package `Rtnmin`.
The code is entirely in **R**. In implementing the codes, the principal awkwardness
was in making available to different functions a quite extensive set of variables
relating to the function and gradient. My choice was to use the `list2env()` 
function to create an **environment** to hold this data. Note the 
survey of truncated newton methods in @NashTN2000.

## Discussion

This story of some of the codes available to **R** users is not finished. There are
certain to be related methods in packages or collections I have overlooked. Moreover,
I have not had the time or energy to fully explore some of the items I have mentioned.
Nevertheless, how codes come about is important to understanding their strengths and
weaknesses and in maintaining them to a level that users can reliably use them.


## References
