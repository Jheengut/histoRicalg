## comproottime.R -- tests from HWB
require(microbenchmark)
require(pracma)
require(rootoned)

f1  <- function(x){                          # [0, 1.2],     0.399 422 2917
  x^2 * (x^2/3 + sqrt(2)*sin(x)) - sqrt(3)/18
}
cat("Function f1 =  x^2 * (x^2/3 + sqrt(2)*sin(x)) - sqrt(3)/18 -- root at  0.399 422 2917\n")
t1u <- uniroot(f1, c(0, 1.2))
cat("uniroot:\n")
print(t1u)
tt1u <- microbenchmark(t1u <- uniroot(f1, c(0, 1.2)))
print(tt1u)
t1b <- brent(f1, 0,1.2)
cat("brent:\n")
print(t1b)
tt1b <- microbenchmark(t1b <- brent(f1, 0,1.2))
print(tt1b)
# source("zeroin.R")
t1z <- zeroin(f1, c(0,1.2))
cat("zeroin:\n")
print(t1z)
tt1z <- microbenchmark(t1z <- zeroin(f1, c(0,1.2)))
cat("zeroin:\n")
print(t1z)
print(tt1z)

require(Rmpfr)
t1m <- unirootR(f1, c(0,1.2))
cat("zeroin:\n")
print(t1m)
tt1m <- microbenchmark(t1m <- unirootR(f1, c(0,1.2)))
print(tt1m)

ll <- mpfr(0, 256)
uu <- mpfr(1.2, 256)

t1m256 <- unirootR(f1, c(ll, uu))
cat("zeroin:\n")
print(t1m256)
tt1m256 <- microbenchmark(t1m256 <- unirootR(f1, c(ll, uu)))
print(tt1m256)


stop()



t1 <- multrfind(f1, ri=c(0, 1.2), ftrace=FALSE, meths=mymeth)
print(t1)
cat("\n")

f2  <- function(x) 11*x^11 - 1              # [0.4, 1.6],   0.804 133 0975
cat("Function f2 = 11*x^11 - 1              # [0.4, 1.6],   0.804 133 0975\n")

t2 <- multrfind(f2, ri=c(0.4, 1.6), ftrace=FALSE, meths=mymeth)
print(t2)
cat("\n")

f3  <- function(x) 35*x^35 - 1              # [-0.5, 1.9],  0.903 407 6632
cat("Function f3 =  35*x^35 - 1              # [-0.5, 1.9],  0.903 407 6632\n")

t3 <- multrfind(f3, ri=c(-0.5, 1.9), ftrace=FALSE, meths=mymeth)
print(t3)
cat("\n")

f4  <- function(x)  2*(x*exp(-9) - exp(-9*x)) + 1  # [-0.5, 0.7],  0.077 014 24135
cat("Function f4 =    2*(x*exp(-9) - exp(-9*x)) + 1  # [-0.5, 0.7],  0.077 014 24135\n")

t4 <- multrfind(f4, ri=c(-0.5, 0.7), ftrace=FALSE, meths=mymeth)
print(t4)
cat("\n")

f5  <- function(x) x^2 - (1 - x)^9          # [-1.4, 1],    0.259 204 4937
cat("Function f5 =   x^2 - (1 - x)^9          # [-1.4, 1],    0.259 204 4937\n")

t5 <- multrfind(f5, ri=c(-1.4, 1), ftrace=FALSE, meths=mymeth)
print(t5)
cat("\n")

f6  <- function(x) (x-1)*exp(-9*x) + x^9    # [-0.8, 1.6],  0.536 741 6626
cat("Function f6 =   (x-1)*exp(-9*x) + x^9    # [-0.8, 1.6],  0.536 741 6626\n")

t6 <- multrfind(f6, ri=c(-0.8, 1.6), ftrace=FALSE, meths=mymeth)
print(t6)
cat("\n")

f7  <- function(x) x^2 + sin(x/9) - 1/4     # [-0.5, 1.9],  0.4475417621
cat("Function f7 =  x^2 + sin(x/9) - 1/4     # [-0.5, 1.9],  0.4475417621\n")

t7 <- multrfind(f7, ri=c(-0.5, 1.9), ftrace=FALSE, meths=mymeth)
print(t7)
cat("\n")

f8  <- function(x) 1/8 * (9 - 1/x)        # [0.001, 1.201], 0.111 111 1111
cat("Function f8 =  1/8 * (9 - 1/x)        # [0.001, 1.201], 0.111 111 1111\n")

t8 <- try(multrfind(f8, ri=c(0.001, 1.201), ftrace=FALSE, meths=mymeth))
print(t8)
cat("\n")

f9  <- function(x) tan(x) - x - 0.0463025   # [-0.9, 1.5],  0.500 000 0340
cat("Function f9 =  tan(x) - x - 0.0463025   # [-0.9, 1.5],  0.500 000 0340\n")

t9 <- multrfind(f1, ri=c(-0.9, 1.5), ftrace=FALSE, meths=mymeth)
print(t9)
cat("\n")

f10 <- function(x)  x^2 + x*sin(sqrt(75)*x) - 0.2  # [0.4, 1],     0.679 808 9215
cat("Function f1 =  x^2 + x*sin(sqrt(75)*x) - 0.2 -- root at  0.679 808 9215\n")
t10 <- multrfind(f1, ri=c(0.1, 1), ftrace=FALSE, meths=mymeth)
print(t10)
cat("\n")

f11 <- function(x) x^9 + 0.0001             # [-1.2, 0],   -0.359 381 3664
cat("Function f11 = x^9 + 0.0001     # [-1.2, 0],   -0.359 381 3664\n")

t11 <- multrfind(f11, ri=c(-1.2, 0), ftrace=FALSE, meths=mymeth)
print(t11)
cat("\n")

f12 <- function(x)  log(x) + x^2/(2*exp(1)) - 2 * x/sqrt(exp(1)) + 1  # [1, 3.4],     1.648 721 27070
cat("Function f12 =   log(x) + x^2/(2*exp(1)) - 2 * x/sqrt(exp(1)) + 1  # [1, 3.4],     1.648 721 27070\n")
t12 <- multrfind(f12, ri=c(1, 3.4), ftrace=FALSE, meths=mymeth)
print(t12)
cat("\n")

