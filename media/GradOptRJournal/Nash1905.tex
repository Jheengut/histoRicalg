% !TeX root = RJwrapper.tex
\title{Provenance of R's Gradient Optimizers}

\author{by John C Nash}

\maketitle

\abstract{
Gradient optimization methods (function minimizers) are well-represented 
in both the base and package universe of R. 
However, some of the methods and the codes developed from
them were published before hardware and software were standardized,
in particular IEEE arithmetic \citep{IEEE754A}. As such there have been
cases of unexpected behaviour or outright errors. These led to the
\textbf{histoRicalg} project to examine older codes in R. Here we present
a short provenance of some of the tools in R for gradient methods for function minimization.
}

\section{The task}

Assuming that any exogenous data needed to compute $f(x)$ is supplied when needed, 
our problem is to find

$$ argmin{_x}{f(x)} $$

\noindent
where $x$ is our set of $n$ parameters and $f()$ is a scalar real function of those
parameters.

The gradient of $f(x)$ is the vector valued function 

$$ g(x) = \partial{f(x)}/\partial{x} $$

The Hessian of $f(x)$ is the matrix of second partial derivatives

$$  H(x) = \partial^2{f(x)}/\partial{x^2}$$

We will be considering methods for this problem where a gradient can be supplied,
though many of the R tools will use a numerical approximation if needed. More generally,
\textit{optimization} includes constrained problems, so that we are really just looking
at \textit{function minimization} here, though some of the methods mentioned will allow for 
bounds to be imposed on the parameters, as well as fixing some parameters ("masks"). 

\section{Types of methods}

With the gradient and Hessian, together with a starting set of parameters $x_0$ 
we can define a Newton iteration to solve for the first-order conditions for a
minimum or maximum (an order $n$ root-finding problem)

$$  H(x_i) \delta = - g(x_i) $$

$$  x_{i+1} = x_i + s * \delta $$

\noindent
where $s$ is some step size. As far as I am aware, there is no explicit general-purpose
Newton method in base-R or CRAN apart from the demonstration programs \code{snewton()} 
and \code{snewtonm()} in 
\CRANpkg{optimx} and the $n=1$ rootfinder in \CRANpkg{pracma}, 
though it is used -- likely without sufficient checks -- inside
some special-purpose packages. While the ideas of Newton methods are valuable, their
implementation requires careful safeguards against exceptions. 

Moreover, computing the gradient takes work, and the Hessian a lot more work. Both computations are error prone. The solution of the set of linear equations to get 
$\delta$ is also demanding of computational effort.

\textbf{quasi-Newton} or \textbf{variable metric} methods (here we will treat 
these as essentially interchangeable names) attempt to avoid the computation 
of the Hessian by clever approximations. Furthermore, if we approximate an 
\textbf{inverse Hessian}, we avoid the solution of a set of linear equations at each iteration. 

Suppose at iteration $i$ we have an approximation $B_i$ to the inverse Hessian $H^{-1}(x_i)$.
Then a step of the Newton iteration is approximated simply by

$$  \delta = - B_i * g(x_i) $$

From the 1960s through the 1980s there was a fairly lively academic industry 
in developing ways to 

  - update the approximate inverse Hessian $B_i$ to $B_{i+1}$, or alternatively an approximate
  Hessian if using a direct variant;

  - choose a "good" step size $s$ (the \textbf{line search sub-problem} to reduce or minimize
  $$f(x_i + s * \delta)$$

One of the simpler sets of choices was published as \citep{Fletcher70}. To choose $s$ this used a 
combination cubic interpolation line search combined with simple backtracking (reducing
the current $s$ until a "sufficient decrease" condition is met). The update of $B$ is done
via what is now known as the Broyden-Fletcher-Goldfarb-Shanno or \textbf{BFGS} update of the 
inverse Hessian approximation. There are versions of this update for both 
the Hessian and its inverse.

It is, of course, necessary to have an initial Hessian approximation. Whether
or not it seems appropriate, the "approximation" used for the initial inverse Hessian is
simply the unit matrix. This has the advantage that the initial search direction is the
negative gradient, which means the first step is a \textbf{steepest-descent} step. Methods 
using an approximate Hessian rather than the inverse can use information about the 
Hessian supplied by user functions, and indeed re-evaluate such information during
the minimization process.

The BFGS appellation is due to a cluster of 1970 papers: \citep{Broyden70},
\citep{Fletcher70}, \citep{Shanno70A},
\citep{Shanno70B}, \citep{Goldfarb70}. An earlier update due to Davidon, Fletcher and
Powell gets the abbreviation \textbf{DFP} \citep{FletPow63}. A linear
combination of both updates can be created, giving a member of what is termed the Broyden 
family of formulas. Given the usage of these methods, it was unfortunate that the referees of
Davidon's original paper \citep{Davidon59} 
turned it down when he submitted it for publication.
Some 32 years "late", the editors (I don't believe they were the same people) corrected the
   oversight in \citep{Davidon91}. 

There are two other quasi-Newton threads particularly relevant to R. One is the UNCMIN
family of polyalgorithms based on the work of \citet{DenSchnab83} (see also \citet{DennisSchnabel96} and \citet{Schnabel1985}). The other is the \textbf{nlminb}
code of David Gay from the Bell Laboratories PORT collection of codes \citep{Fox1997}. This
is also a polyalgorithm, adjusting for the presence of gradient and possibly hessian code.

\section{Limited memory approaches and conjugate gradients}

The matrix $B$ when the number of parameters $n$ is large requires a lot of memory, motivating
method that conserve memory. The \textbf{Limited Memory BFGS} approach stores (some of) the 
updates as a set of vectors, generating the iteration implicitly. See 
\href{https://en.wikipedia.org/wiki/Limited-memory_BFGS}. 
Most of the major work is linked in some
way to the articles by \citep{Byrd1995} and \citep{LiuN89}. Note, however, a 2011 update as \citep{Morales2011}.

From a different starting point, various nonlinear conjugate gradient methods aim to provide good
search directions by modifying the gradient (which gives the steepest descent direction) to avoid
"hemstitching"" -- successive searches being almost parallel but back and forth across a 
"valley". There are a number of such approaches, with that of 
\citep{FletReeves64} being among the first. Since 
the nonlinear conjugate gradients methods use but a few vectors to store information to update
the search direction, and these directions often have a close connection to those generated by
the Limited Memory BFGS approaches, we can consider the approaches related, though their 
algorithmic realizations are often quite dissimilar. Moreover, the CG methods generally only
save the "last" gradient and possibly search direction. While the initial linear CG code of 
\citep{Hestenes1952} for solving linear equations was published nearly 7 decades ago, and the 
nonlinear adaptations were introduced in the 1960s, there has been a flurry of improvements 
since the late 1990s. Possibly overlooking some important work, I will single out efforts 
by the teams of Yuan/Dai and Hager/Zhang, namely papers linked to \citep{Dai01} and
\citep{Hager2005}, with a survey in \citep{Hager2006b}. 

A somewhat different application of conjugate gradients ideas is the Truncated
Newton algorithm. This uses a linear CG method to solve the Newton equations
approximately. There are, of course, many choices and details that give rise
to particular codes. 



\section{Gradient optimizers in R}

R has a number of tools for minimizing nonlinear functions, and the
\ctv{Optimization} task view is recommended reading for prospective users.
Some of the tools require only a subprogram (in R a \textit{function}) for computing the
nonlinear function `f(x, exdata)` where `x` is a vector of $n$ parameters to be adjusted so that
the function takes on its minimal value, and `exdata` is some 
exogenous data. The exogenous data may not be needed. The tools of interest in this
article make use of, or even require, a subprogram to compute the gradient 
`g(x, exdata)` as a vector.

In base-R, the \code{optim()} function includes three gradient methods as well as a derivative
free Nelder-Mead method \citep{NelderMead65} and a rather unfortunate simulated annealing
method. The Nelder-Mead and BFGS and CG codes came from the Second Edition \citep{Nash1990}
of my book, but the algorithms are essentially unchanged from \citet{jncnm79}. Brian Ripley
converted the Pascal code in the mid-1990s. 

The BFGS option is a low-memory, short-precision streamlining of \citep{Fletcher70}. 
In a variety of computing languages it has worked reliably
and quite efficiently for over four decades. I have more recently published the 
all-R version \CRANpkg{Rvmmin} ??ref, which
adds bounds and masks constraints. 
Anecdotal details of the history of the code can be found in ??ref to vignette??

\CRANpkg{ucminf} \citep{Nielsen2000} is based on similar ideas, but with more sophistication. It does not, 
however, allow for bounds and masks. There is a MATLAB version that I hope to translate
to R. 

Base-R has, in addition to \code{optim()} the functions \code{nlm()} and \code{nlminb()}. Understanding these programs, especially given that they are called from a C wrapper to the
original Fortran that itself is called from R, is very difficult, so I prefer to judge the
utility of the resulting R functions from their performance, which is generally good. 

\code{nlm()} is an implementation of UNCMIN. A large part of the book by 
\citet{DennisSchnabel96} is devoted to explaining it. I cannot recall ever trying to
adjust control parameters available in \code{nlm()}, which appear to be only a small
subset of the many options available. Nevertheless, \code{nlm()} mostly performs very well
on unconstrained problems, though in 2017 
Marie Boehnstedt pointed out a bug, now fixed by Martin Maechler since R 3.5. 
\code{nlm.Rd} notes 'The C code for the “perturbed” cholesky, choldc() has 
had a bug in all R versions before 3.4.1.' However, 
providing the Hessian in addition to the gradient
does not always provide better performance, as theorems concerning the convergence
properties of Newton-like methods have conditions that may not be satisfied unless
the current parameters are "close enough" to a solution.

\code{nlminb()} was, I believe, introduced into R to provide for bounds constraints
on parameters being adjusted to optimize a function. It also generally works well, 
but in 2010 (a long time after nlminb() appeared in R), users realized that nlminb() defaulted
to assuming a sum of squares was being minimized. 
See \url{http://r.789695.n4.nabble.com/Not-nice-behaviour-of-nlminb-windows-32-bit-version-2-11-1-td2283937.html}. The default termination test has now been altered. 

nlminb(), when not using an explicit Hessian, uses a factorized form of the BFGS update
of the approximate Hessian (not the inverse), so requires solution of a set of Newton-like
equations, to which a variant of Levenberg-Marquardt correction is applied. Overall, the
code is complicated and very difficult to follow, using constructs such as the
\textbf{computed GOTO} in Fortran. Similarly, UNMIN (at least as documented in \citet{DennisSchnabel96}) uses BFGS updates of an approximation of the Hessian rather
than its inverse. 

\CRANpkg{mize} \citep{Melville17} offers some options to build a gradient minimizer
and could possibly allow some algorithmic comparisons to be made, but I have
not had the time to delve into this.

The CG option of \code{optim()} is intended as a low-storage approach to minimize 
functions of many parameters, and 
was an early part of base-R, but I have never felt the original algorithm from the late 1970s 
to be very successful. My \CRANpkg{Rcgmin} \citep{p-Rcgmin} based on \citep{Dai01} performs better, and bounds
constraints are available. 

Hager and Zhang's work attempts several improvements to this approach, 
at the cost of more complicated 
code. Indeed, the CG-DESCENT package published as \citep{Hager2006a} includes some ideas related to Limited Memory BFGS. Steven Scott helped me to wrap CG-DESCENT as the (experimental) R 
package `Rcgdescent` available at \url{https://r-forge.r-project.org/projects/optimizer/}, 
but this has a limited set of the options of the original code. Collaboration is welcome to
test and possibly improve this code. 

\code{optim()}'s L-BFGS-B option is version of the code described in \citet{Byrd1995}
and published in \citet{Zhu1997LBFGS}, 
though the code was circulated some time before the formal publication. The appearance
of a "corrected" version of the Fortran code \citep{Morales2011} gave some of us who work on R codes cause for concern, and I 
prepared an R wrapper of the new code from 
\url{http://users.iems.northwestern.edu/~nocedal/lbfgsb.html} as \CRANpkg{lbfgsb3}. 
Matt Fiddler has prepared a cleaner packaging as \CRANpkg{lbfgsb3c}, and we intend
to merge ideas into a single package. However, a lack of discrepancies between results
from the new and existing codes was largely explained by Jorge Nocedal himself when
by pure chance we were seated next to each other at a birthday conference for Andy
Conn in 2016. The most important repair was for the computation of the machine 
precision. Since R gets this 
number as \code{.Machine\$double.eps}, the offending code is irrelevant. 
There is also a possible improvement in efficiency in handling bounds constraints,
but this remains to be tested thoroughly.

A similar unconstrained code has been converted to C under the leadership of 
Naoaki Okazaki  (see \url{http://www.chokkan.org/software/liblbfgs/}, or the fork at  
\url{https://github.com/MIRTK/LBFGS}). This has been wrapped for R as  \CRANpkg{lbfgs} 
\citep{Coppola2014}.

There are (at least) two implementations of truncated Newton methods available to R users.

\CRANpkg{nloptr} has \code{tnewton()}. This is a wrapper of the NLopt truncated Newton method translated to C and somewhat modified by Steven G. Johnson. See 
\url{https://nlopt.readthedocs.io/en/latest/NLopt_Algorithms/} or the Fortran 
code due to Ladislav Luksan \url{http://www.cs.cas.cz/luksan/subroutines.html}. 
The many layers and translations make it difficult to unravel the particular
details in this code, and I do not feel confident to provide a competent
overview. 

In my own \CRANpkg{optimx} I have translated my brother's MATLAB codes as \code{tn()} and \code{tnbc()} from \citep{SGN83}. 
The code is entirely in R. Note his
survey of truncated newton methods in \citep{NashTN2000}.

\section{Summary and perspective}

R has a number of tools for minimizing nonlinear functions. These are applied directly
by users or more frequently in R packages, particularly for estimation of models. Mostly
these work well, but they are not infallible, nor, as indicated, are even well-established
codes free from bugs. This led to the \textbf{histoRicalg} project 
(\url{https://gitlab.com/nashjc/histoRicalg/blob/master/README.md}) which has been
funded by the R Consortium, and which aims to consider as many long-standing methods
in R as we are able, not just in the optimization area. We welcome participation. 

Another goal of this article is to point out that optimization gives rise to problems that
challenge even very sophisticated codes. No single method will be a consistent winner, 
and results -- as always -- should be checked carefully.



\bibliography{RGradminRefs}


\address{John C. Nash\\
  Retired Professor\\
  University of Ottawa, Telfer School of Management\\
  Ottawa, Ontario\\
  Canada\\
  ??(ORCiD if desired)\\
  \email{nashjc@uottawa.ca}
}


